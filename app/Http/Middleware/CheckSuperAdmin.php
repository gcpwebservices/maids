<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class CheckSuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if (\Auth::user() && \Auth::user()->access != 99) {
            return redirect('/admin/forbidden');
            // return response(view('layouts.dashboard.forbidden'));
        }

        return $next($request);
    }
}
