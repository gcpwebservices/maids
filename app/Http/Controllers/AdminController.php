<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use DB;
use Auth;
use Hash;
use Validator; 
use Illuminate\Support\Facades\View;

//custom
use App\MaidsSchedule;
use App\DriversSchedule;
use App\SalesSummary;
use App\SalesDetails;
use App\Maids;
use App\Driver;
use App\Color;
use App\Customer;
use App\Schedule_address;
use App\Area;
use App\Subarea;
use DateTime;
use Carbon\Carbon;
// date_default_timezone_set('Asia/Dubai');
use Illuminate\Console\Scheduling\Schedule;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   

        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $this->procDoneTask();
        $now = Carbon::now();
        $now->subWeek();

        $income     = SalesDetails::where('created_at',  '>', $now->toDateString() )->get()->sum('payment_sum');
        $bmaids     = MaidsSchedule::where('created_at', '>', $now->toDateString() )->get()->count('maids_id');
        $bdrivers   = MaidsSchedule::where('created_at', '>', $now->toDateString() )->get()->count('driver_id');
        $bcustomers = MaidsSchedule::where('created_at', '>', $now->toDateString() )->get()->count('customer_id');

    
        $data['income']     =  $income;
        $data['bmaids']     =  $bmaids;
        $data['bdrivers']   =  $bdrivers;
        $data['bcustomers'] =  $bcustomers; 

        return view('dashboard',$data);

    }


    public function showChangePasswordForm(Request $request){
        $user = User::find(auth::user()->id);

        if(Hash::check($request->input('passwordold'), $user->password) && $request->input('password') == $request->input('password_confirmation')){
            $user->password = bcrypt($request->input('password'));
            $user->save();
            return back()->with('success','Password changed successfully.');
        }else{
            return back()->with('error','Error! Password is not changed.');
        }
    }

    public function dashBoardPaginate(){
        $timeline   = MaidsSchedule::where('completed','=','3')
                ->with(['getCustomer','getMaid','getSaleSummary'])
                ->paginate(6);
        return $timeline;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */



    public function maids_schedule()
    {       
        $this->procDoneTask();
        $mlist = Maids::get();
        $clist = Color::get();
        $dlist = Driver::get();
        $cuslist = Customer::get();

        $area  = Area::get();
        $sub   = Subarea::get();

        $data['mlist'] = $mlist;
        $data['dlist'] = $dlist;
        $data['customers'] = $cuslist;
        $data['area']     = $area;
        $data['sub']      = $sub;

        $data['currentD'] = date("Y-m-d");
        $data['currentT'] = date("H:i:s");
     
        return view('parts-dashboard.maids-schedule',$data);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function maids_itinerary(Request $request)
    {
        $now = new DateTime();
        $snum = $this->generateRandomString();
        $driverInputFields = [];

        if($request->method() == 'POST' ){

                $topay = $request->input('to_pay');
                $vat = ($topay * 0.05);
                $total   = ($topay + $vat);

                $dropstart = date('Y-m-d H:i:s',strtotime('-1 hour',strtotime($request->input('schedule_start'))));
                $dropend = $request->input('schedule_start');

                $pickupstart = $request->input('schedule_end');
                $pickupend = date('Y-m-d H:i:s',strtotime('+1 hour',strtotime($request->input('schedule_end'))));

            if($request->input('use')=='no'){

                $address = Schedule_address::create( $request->only(['customer_address','customer_id']));
                $maidsschedule = MaidsSchedule::create(array_merge( $request->except(['customer_address','use']), 
                ['customer_address_id'=> $address->id ], ['schedule_code'=> 'S#'. $snum ], [ 'completed'=> '3' ], 
                ['created_at'=> $now->format('Y-m-d H:i:s')],['area_id'=> $request->input('areavalue')],
                ['sub_area_id'=> $request->input('subareavalue')],['payment'=>'0.00'] ));

                $salesInput = [
                   
                    'schedule_id'    => $maidsschedule->id,
                    'service_number' => 'S#'. $snum,
                    'item'           => '1',
                    'price'          => $topay,
                    'payment_disc'   => '0',
                    'payment_vat'    => $vat,
                    'payment_refund' => '0',
                    'payment_sub'    => $total,
                    'payment_total'  => '0',
                    'created_date'   => $now->format('Y-m-d')

                ];

                $salesInsert = SalesSummary::create($salesInput);

                $driverInputFields = [

                    'maids_id' => $request->input('maids_id'),
                    'customer_id' => $request->input('customer_id'),
                    'customer_address_id' => $address->id,
                    'driver_id' => $request->input('driver_id'),
                    'drop_start' => $dropstart,
                    'drop_end' => $dropend,
                    'pickup_start' => $pickupstart,
                    'pickup_end' => $pickupend,
                    'schedule_color' => $request->input('schedule_color'),
                    'schedule_code' => $request->input('schedule_code'),
                    'created_at' => $now->format('Y-m-d H:i:s'),
                    'schedule_code'=> 'S#'. $snum,
                    'completed' => '0'

                ];

                $driversschedule = DriversSchedule::create($driverInputFields);
           
                $maidsschedule->save();
                $driversschedule->save();
                $salesInsert->save();

                return redirect()->back()->with('status',['success','Maid schedule is successfully added with service #:'.$snum]);

            }else{

                $maidsschedule = MaidsSchedule::create(array_merge( $request->except(['add','datetimes','customer_address','use']), 
                [ 'schedule_code'=> 'S#'. $snum ],['created_at'=> $now->format('Y-m-d H:i:s')],
                [ 'completed'=> '3' ] ,['area_id'=> $request->input('areavalue')],
                ['sub_area_id'=> $request->input('subareavalue')],['payment'=>'0.00']));

         
                $salesInput = [
                   
                    'schedule_id'    => $maidsschedule->id,
                    'service_number' => 'S#'. $snum,
                    'item'           => '1',
                    'price'          => $topay,
                    'payment_disc'   => '0',
                    'payment_vat'    => $vat,
                    'payment_refund' => '0',
                    'payment_sub'    => $total,
                    'payment_total'  => '0',
                    'created_date'   => $now->format('Y-m-d')
                ];

                $salesInsert = SalesSummary::create($salesInput);

                $driverInputFields = [

                    'maids_id' => $request->input('maids_id'),
                    'customer_id' => $request->input('customer_id'),
                    'driver_id' => $request->input('driver_id'),
                    'drop_start' => $dropstart,
                    'drop_end' => $dropend,
                    'pickup_start' => $pickupstart,
                    'pickup_end' => $pickupend,
                    'schedule_color' => $request->input('schedule_color'),
                    'schedule_code' => $request->input('schedule_code'),
                    'created_at' => $now->format('Y-m-d H:i:s'),
                    'schedule_code'=> 'S#'. $snum,
                    'completred' => '0'

                ];

                $driversschedule = DriversSchedule::create($driverInputFields);
             
                $maidsschedule->save();
                $driversschedule->save();
                $salesInsert->save();

                return redirect()->back()->with('status',['success','Maid schedule is successfully added with service #:'.$snum]);
            }

        }

        return view('parts-dashboard.maids-schedule');
    }   

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function maids_itinerary_update(Request $request)
    {   

        $arr[] ='';
        if($request->input('action') == 'view'){

            $maid_schedule = MaidsSchedule::where('id', '=', $request->input('id'))->with('getSaleSummary')->first();
            $area          = Area::where('area_id', '=', $maid_schedule->area_id)->first()->toArray();
            $subarea       = Subarea::where('id', '=', $maid_schedule->sub_area_id)->first()->toArray();
            $maids         = Maids::where('id', '=', $maid_schedule->maids_id)->first();
            // $sales         = SalesSummary::select('payment_sub')->where('schedule_id','=',$request->input('id'))->first()->toArray();

            if($maid_schedule->customer_address_id==null){

                $customer_address = Customer::where('id', '=', $maid_schedule->customer_id)->value('address');
                $customer_in_address = Schedule_address::where('id', '=', $maid_schedule->customer_address_id)->value('customer_address');
                $array = array_merge($maids->toArray(),$maid_schedule->toArray(), ['address'=>$customer_address],
                    ['customer_address'=>$customer_in_address]
                    ,$area,$subarea,['payment_sub'=>$maid_schedule->getSaleSummary['payment_sub']]);

                return json_encode($array);

            }else{
                $customer_address = Customer::where('id', '=', $maid_schedule->customer_id)->value('address');
                $customer_in_address = Schedule_address::where('id', '=', $maid_schedule->customer_address_id)->value('customer_address');
                $array = array_merge($maids->toArray(),$maid_schedule->toArray(), ['customer_address'=>$customer_in_address],
                    ['address'=>$customer_address]
                    ,$area,$subarea,['payment_sub'=>$maid_schedule->getSaleSummary['payment_sub']]);
                return json_encode($array);     
            }
    
        }else{
            
            $now = new DateTime();
            if($request->input('completed')=='1'){
                $arr = [

                    'area_id'        => $request->input('area_id'),
                    'sub_area_id'    => $request->input('sub_area_id'),
                    'schedule_start' => $request->input('schedule_start'),
                    'schedule_end'   => $request->input('schedule_end'),
                    'customer_id'    => $request->input('customer_id'), 
                    'maids_id'       => $request->input('maids_id'),
                    'driver_id'      => $request->input('driver_id'), 
                    'schedule_color' => $request->input('schedule_color'),
                    'completed'      => $request->input('completed'),
                    'payment'        => $request->input('payment'),
                    'updated_at'     => $now->format('Y-m-d H:i:s'),
                    'schedule_color' => '#228B22'
                        
                    ];
            }else{
                $arr = [
                    
                    'area_id'        => $request->input('area_id'),
                    'sub_area_id'    => $request->input('sub_area_id'),
                    'schedule_start' => $request->input('schedule_start'),
                    'schedule_end'   => $request->input('schedule_end'),
                    'customer_id'    => $request->input('customer_id'), 
                    'maids_id'       => $request->input('maids_id'),
                    'driver_id'      => $request->input('driver_id'), 
                    'schedule_color' => $request->input('schedule_color'),
                    'completed'      => $request->input('completed'),
                    'payment'        => $request->input('payment'),
                    'updated_at'     => $now->format('Y-m-d H:i:s')                        
                ];
            }
        


            if( $request->input('payment') != ''){
                $salesInput = [ 

                    'payment_disc'   => '0',
                    'payment_refund' => '0',
                    'payment_total'  => $request->input('payment'),
                    'payment_date'   => $now->format('y-m-d H:i:s'),
                    'updated_at'     => $now->format('y-m-d H:i:s')
                    
                ];
            }else{

                $salesInput = [

                    'payment_disc'   => '0',
                    'payment_refund' => '0',
                    'updated_at'     => $now->format('y-m-d H:i:s')

                ];

            }

            if($request->input('use')=='no'){

                $maid_schedule = MaidsSchedule::where('id', '=', $request->input('id'))->first();
                SalesSummary::where('schedule_id', $request->input('id'))->update($salesInput);

                if($maid_schedule->customer_address_id==null){

                    $schedule_address_id = Schedule_address::create(['customer_id'=>$request->input('customer_id'),
                        'customer_address'=>$request->input('customer_address'),'created_at'=> $now->format('Y-m-d H:i:s'),
                        'updated_at'=>$now->format('Y-m-d H:i:s')]);
                    $schedule_address_id->save();

                    MaidsSchedule::where('id', $request->input('id'))->update(array_merge($arr,['customer_address_id'=>$schedule_address_id->id]));
                    Schedule_address::where('id', $maid_schedule->customer_address_id)
                    ->update(['customer_address'=>$request->input('customer_address')]);

                }else{

                    MaidsSchedule::where('id', $request->input('id'))->update($arr);
                    Schedule_address::where('id', $maid_schedule->customer_address_id)
                    ->update(['customer_address'=>$request->input('customer_address')]);
                }

            }else{
                MaidsSchedule::where('id', $request->input('id'))->update($arr);
                SalesSummary::where('schedule_id', $request->input('id'))->update($salesInput);
            }
            
            // return redirect()->back()->with('status',['success','Maid schedule is successfully updated with service #:'.$snum]);

        }
  
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_available_maids(Request $request) {

        
        if(!empty($request->input('schedule_start')) && !empty($request->input('schedule_end'))){

            if($request->input('interval') > 0){
                $traveltime = ($request->input('interval') / 2); 
            }else{
                $traveltime = $request->input('interval'); 
            }

            $addminStart =  date("Y-m-d H:i:s", strtotime("+{$traveltime} minutes", strtotime($request->input('schedule_start'))));
            $addminEnd   =  date("Y-m-d H:i:s", strtotime("+{$traveltime} minutes", strtotime($request->input('schedule_end'))));

            $maid_schedule = MaidsSchedule::select('maids_id')
                        ->where('schedule_start', '<=',$addminEnd )
                        ->where('schedule_end', '>=',$addminStart )
                        ->where('completed', '=','3' )
                        ->get();

            $mlist = Maids::whereNotIn('id', $maid_schedule)->select('id','fullname')->get();

            return json_encode($mlist);

        }
    }


    public function getCountPerAreaMaids(Request $request) {
        $now = new DateTime();
        if($request->input('action')=='count'){
            $areacount = MaidsSchedule::Where('area_id','=',$request->input('area_id'))
                                        ->where('schedule_end', '>=',$now->format('Y-m-d H:i:s'))->get();
            return json_encode(count($areacount));
        }else{
    //else show the val from addbutton
            $area = Area::where('area_id','=',$request->input('area_id'))->get()->toArray();
            $subarea = Subarea::where('id','=',$request->input('subarea_id'))->get()->toArray();
            return json_encode(array_merge($area,$subarea));
        }

    }

    //dropdown subarea list and count
    public function getAreaSubArea(Request $request) {
        $arr=[];
        $now = new DateTime();

        if($request->input('area_id')==0){
            $subarea = Subarea::get();
        }else{
            $subarea = Subarea::where('area_id','=', $request->input('area_id'))->get();
        }

        foreach( $subarea as $key => $data){

           $subacount =  MaidsSchedule::where('sub_area_id', '=',$data->id)
                        ->where('completed', '=','3' )
                        ->where('schedule_end','>=',$now->format('Y-m-d H:i:s'))
                        ->get()->count();

            $arr[] =[

                'id' => $data->id,
                'area_id' => $data->area_id,
                'sub_area' => $data->sub_area,
                'count' => $subacount
            ];
 
        }  

        return json_encode(array_splice($arr,1)); 
    }

    //resource calendar
    public function getJsonRequestResource(Request $request) {

        $schedules = MaidsSchedule::get();
        $resource = [];

        foreach( $schedules as $schedule ){

            $resource[] = [

                'id'=> $schedule->id, 
                'maid'=> $schedule->getMaid['fullname'],
                'eventColor' => $schedule->schedule_color,
                'driver' => $schedule->getDriver['fullname']
       
            ];

        }

        return json_encode($resource);
    }


    //filter dropdown calendar
    public function getJsonRequestEvent(Request $request) {


        $now = new DateTime();
        if($request->input('area') == 0 && $request->input('subarea') ==  0){
            $schedules = MaidsSchedule::where('schedule_end', '>=',$now->format('Y-m-d H:i:s'))
                    // ->orWhere('completed','=','0')
                    ->where('completed','=','3')
                    // ->where('payment', '!=', '0.00')
                    ->get();       
        }elseif ($request->input('area') != 0 && $request->input('subarea') == 0) {
            $schedules = MaidsSchedule::Area($request->input('area'))
                    ->where('schedule_end', '>=',$now->format('Y-m-d H:i:s'))
                    // ->orWhere('completed','=','0')
                     ->where('completed','=','3')
                    //->where('payment', '=', '0.00')
                    ->get();
        }elseif ( $request->input('area') == 0 && $request->input('subarea') != 0)  {
            $schedules = MaidsSchedule::Subarea($request->input('subarea'))
                    ->where('schedule_end', '>=',$now->format('Y-m-d H:i:s'))
                    // ->orWhere('completed','=','0')
                     ->where('completed','=','3')
                    //->where('payment', '=', '0.00')
                    ->get();
        }else{
            $schedules = MaidsSchedule::Area($request->input('area'))->Subarea($request->input('subarea'))
                    ->where('schedule_end', '>=',$now->format('Y-m-d H:i:s'))
                    // ->orWhere('completed','=','0')
                    ->where('completed','=','3')
                    //->where('payment', '=', '0.00')
                    ->get();
        }   

    
        $events = [];

        foreach( $schedules as $schedule ){


            if($schedule->customer_address_id==null){
                $events[] = [

                    'id'=> $schedule->id, 
                    'resourceId'=> $schedule->id, 
                    'start'=> $schedule->schedule_start->format('Y-m-d H:i:s'), 
                    'end'=> $schedule->schedule_end->format('Y-m-d H:i:s'),
                    'title'=>  'Maid :'.$schedule->getMaid['fullname'],
                    'customer' => $schedule->getCustomer['fullname'],
                    'customer_address' => $schedule->getCustomer['address']
                ];

            }else{

                $events[] = [

                    'id'=> $schedule->id, 
                    'resourceId'=> $schedule->id, 
                    'start'=> $schedule->schedule_start->format('Y-m-d H:i:s'), 
                    'end'=> $schedule->schedule_end->format('Y-m-d H:i:s'),
                    'title'=>  'Maid :'.$schedule->getMaid['fullname'],
                    'customer' => $schedule->getCustomer['fullname'],
                    'customer_address' => $schedule->getCusAddress['customer_address']
                ];

            }
        }

        return json_encode($events);
    }

    public function getAreaOnly(Request $request){
        $area = Area::get();
        return json_encode($area);
    }

    //areacount and list
    public function getAreaAndCount(Request $request) {

        $arr=[];
        $now = new DateTime();

        $area = Area::get();
  
        foreach( $area as $key => $data){

            $areacount =  MaidsSchedule::where('area_id', '=',$data->id)
                        ->where('completed', '=','3' )
                        ->where('schedule_end','>=',$now->format('Y-m-d H:i:s'))
                        ->get()->count();

            $arr[] =[

                'id' => $data->id,
                'area_id' => $data->area_id,
                'area' => $data->area,
                'count' => $areacount
            ];
 
        }

        return json_encode($arr);
       
    }

    //dropdown span label area and subarea
    public function getlabel(Request $request){
        
        if($request->input('action')=='forarea'){
            $area = Area::where('id','=',$request->input('id'))->first();
            return json_encode($area);
        }else{
            $subarea = Subarea::where('id','=',$request->input('id'))->first();
            return json_encode($subarea);
        }
    }

    public function procDoneTask() {
        $now = new DateTime();
        MaidsSchedule::where('schedule_end', '<',$now->format('Y-m-d H:i:s'))
                    ->update(['completed'=>'1']);
        DriversSchedule::where('pickup_end', '<',$now->format('Y-m-d H:i:s'))
                ->update(['completed'=>'1']);
    }

    public function generateRandomString($length = 4) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
        return $randomString;
    }

    public function getAmount(Request $request){

        $employee_number = $request->input('employee_number');
        $startTime = Carbon::parse($request->input('timefrom'));
        $finishTime = Carbon::parse($request->input('timeto'));

        $totalDuration = $finishTime->diffInRealMinutes($startTime);



        $topay = ($totalDuration / 60 * 35);

        if($employee_number == 0 ){
            $new_topay = $topay;
        }else{
            $new_topay = $topay * $employee_number;
        }

        $vat = ($new_topay * 0.05);
   
        $forview   = ($new_topay + $vat);


        $array=[

            'view' => number_format($forview, 2, '.', ','),
            'amount' => number_format($topay, 2, '.', ',')

        ];

        return json_encode($array);
    }


    #driver schedule

        /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function drivers_schedule() {

        $schedules = DriversSchedule::where('completed', '=','0')->get();
        $resource = [];
        $events = [];
        $children = [];

        // dd($schedules);
        foreach( $schedules as $schedule ){

            $resource[] = [

                'id'=> $schedule->id, 
                'maid'=> $schedule->getMaid['fullname'],
                'eventColor' => $schedule->schedule_color,
                'driver' => $schedule->getDriver['fullname'],
          
       
            ];


            if($schedule->customer_address_id==null){
                $events[] = [

                    'id'=> $schedule->id, 
                    'resourceId'=> $schedule->id, 
                    'start'=> $schedule->drop_start->format('Y-m-d H:i:s'), 
                    'end'=> $schedule->drop_end->format('Y-m-d H:i:s'),
                    'title'=>  $schedule->schedule_code,
                    'driver' => $schedule->getDriver['fullname'],
                    'customer' => $schedule->getCustomer['fullname'],
                    'customer_address' => $schedule->getCustomer['address'],
                    'toolbar' => '1'
                ];

            }
        }

        foreach( $schedules as $schedule ){

            $resource[] = [

                'id'=> $schedule->id, 
                'maid'=> $schedule->getMaid['fullname'],
                'eventColor' => $schedule->schedule_color,
                'driver' => $schedule->getDriver['fullname'],

       
            ];


            if($schedule->customer_address_id==null){
                $events[] = [

                    'id'=> $schedule->id, 
                    'resourceId'=> $schedule->id, 
                    'start'=> $schedule->pickup_start->format('Y-m-d H:i:s'), 
                    'end'=> $schedule->pickup_end->format('Y-m-d H:i:s'),
                    'title'=>  $schedule->schedule_code,
                    'driver' => $schedule->getDriver['fullname'],
                    'customer' => $schedule->getCustomer['fullname'],
                    'customer_address' => $schedule->getCustomer['address'],
                    'toolbar' => '0'
                ];

            }
        }

            $mlist = Maids::get();
            $clist = Color::get();
            $dlist = Driver::get();
            $cuslist = Customer::get();

            $data['maids'] = $mlist;
            $data['colors'] = $clist;
            $data['drivers'] = $dlist;
            $data['customers'] = $cuslist;

            $data['currentD'] = date("Y-m-d");
            $data['resource'] = json_encode($resource);
            $data['events'] = json_encode($events);

            $data['getedit'] = $resource;
     
        return view('parts-dashboard.drivers-schedule',$data);
    }
    


}
