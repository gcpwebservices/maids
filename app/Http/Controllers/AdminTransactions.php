<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use DB;

use Illuminate\Support\Facades\View;



//custom

use App\MaidsSchedule;

use App\SalesSummary;

use App\DriversSchedule;

use App\Maids;

use App\Driver;

use App\Color;

use App\Customer;

use App\Schedule_address;

use App\Area;

use App\Subarea;

use App\User;

use DateTime;

use Carbon\Carbon; //fordate

date_default_timezone_set('Asia/Dubai');

use Illuminate\Support\Facades\Route;

use Faker\Generator;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Storage;

use App\Repositories\UserRepository;

Carbon::setWeekStartsAt(Carbon::SUNDAY);

Carbon::setWeekEndsAt(Carbon::SATURDAY);



use Yajra\DataTables\DataTables;





class AdminTransactions extends Controller

{



    /**

     * Display a listing of the resource.

     *

     * @return Response

     */

    public function CustomersView()

    {   

        return view('parts-dashboard.admin-transactions-customers');

    }



    public function MaidsView()

    {   

        return view('parts-dashboard.admin-transactions-maids');

    }



    // public function TranSummaryFilter(Request $request){



    //     $from = $request->input('datefrom');

    //     $to   = $request->input('dateto');



    //     $arr = [];



    //     if($from == '' && $to == ''){



    //         $bookedqty  = SalesSummary::count();

    //         $refundqty  = SalesSummary::where('payment_refund', '!=', '0.00')->count();

    //         $paymentqty = SalesSummary::where('payment_total', '!=', '0.00')->count();





    //         $disc       = SalesSummary::sum('payment_disc');

    //         $vat        = SalesSummary::sum('payment_vat');

    //         $payment    = SalesSummary::sum('payment_total');





    //     }else{



    //         $bookedqty  = SalesSummary::whereDate('created_date', '>=',  $from)

    //                                     ->whereDate('created_date', '<=',  $to)

    //                                     ->count();



    //         $refundqty  = SalesSummary::where('payment_refund', '!=', '0.00')

    //                                     ->whereDate('created_date', '>=',  $from)

    //                                     ->whereDate('created_date', '<=',  $to)

    //                                     ->count();



    //         $paymentqty = SalesSummary::where('payment_total', '!=', '0.00')

    //                                     ->whereDate('created_date', '>=',  $from)

    //                                     ->whereDate('created_date', '<=',  $to)

    //                                     ->count();



    //         $disc       = SalesSummary::whereDate('created_date', '>=',  $from)

    //                                     ->whereDate('created_date', '<=',  $to)

    //                                     ->sum('payment_disc');



    //         $vat        = SalesSummary::whereDate('created_date', '>=',  $from)

    //                                     ->whereDate('created_date', '<=',  $to)

    //                                     ->sum('payment_vat');



    //         $payment    = SalesSummary::whereDate('created_date', '>=',  $from)

    //                                     ->whereDate('created_date', '<=',  $to)

    //                                     ->sum('payment_total');



    //     }



    //     $total      = $disc+$vat+$payment;

    //     $arr = [



    //         'bookqty'   => $bookedqty,

    //         'refundqty' => $refundqty,

    //         'paidqty'   => $paymentqty,



    //         'total'     => number_format((float)$payment, 2, '.', ''),



    //         'disc'      => $disc,

    //         'vat'       => $vat,

    //         'grand'     => number_format((float)$total, 2, '.', '')



    //     ];



    //    return response()->json($arr);

    // }





    public function TransactionTableCustomers(Request $request)

    {   



        $opevents = MaidsSchedule::select('maids_schedule.*')->with(['getCustomer','getSaleSummary']);

        if( $request->input('datefrom') != '' && $request->input('dateto') != ''){

            $opevents->whereDate('created_at', '>=',  $request->input('datefrom'))

                        ->whereDate('created_at', '<=',  $request->input('dateto'));

        }



            return Datatables::of( $opevents )->editColumn('payment', function($event){



                return $event->payment != 0.00 ? '<span class="label label-success">Paid</span>' : '<span class="label label-danger">Unpaid</span>';



            })->editColumn('get_sale_summary.created_date', function($event){



                return $event->getSaleSummary['created_date']->format('l j F Y');



            })->editColumn('completed', function($event){



                if($event->completed == 1 ){

                    return '<span class="label label-success">Done</span>';

                }elseif($event->completed == 3){

                    return '<span class="label label-warning">Ongoing</span>';

                }else{

                    return '<span class="label label-danger">Pending</span>';

                }

              

            })->editColumn('created_at', function($event){



                return $event->created_at->format('l j F Y');



            })->rawColumns(['created_at','completed','get_customer.fullname','payment','get_sale_summary.payment_total','get_sale_summary.payment_vat',

            'get_sale_summary.created_date','get_sale_summary.payment_sub'])->make( true );



    }



    public function TransactionCustomerView($id){



            $maids = MaidsSchedule::where('id',$id)->with(['getCustomer','getSaleSummary','getMaid'])->first();



            return view('parts-dashboard.admin-transactions-view-customer', ['data'=>$maids]);



    }





    public function TransactionTableMaids(Request $request){



        $maids = MaidsSchedule::select('maids_schedule.*')->with(['getMaid','getSaleSummary']);

        if( $request->input('datefrom') != '' && $request->input('dateto') != ''){

            $maids->whereDate('created_at', '>=',  $request->input('datefrom'))

                ->whereDate('created_at', '<=',  $request->input('dateto'));

        }



            return Datatables::of( $maids )->editColumn('payment', function($event){



                return $event->payment != 0.00 ? '<span class="label label-success">Paid</span>' : '<span class="label label-danger">Unpaid</span>';



            })->editColumn('get_sale_summary.created_date', function($event){



                return $event->getSaleSummary['created_date']->format('l j F Y');



            })->editColumn('completed', function($event){



                if($event->completed == 1 ){

                    return '<span class="label label-success">Done</span>';

                }elseif($event->completed == 3){

                    return '<span class="label label-warning">Ongoing</span>';

                }else{

                    return '<span class="label label-danger">Pending</span>';

                }

              

            })->editColumn('schedule_start', function($event){

   

                return $event->schedule_start->format('h:i A');

           

            })->editColumn('schedule_end', function($event){

   

                return $event->schedule_end->format('h:i A');

           

            })->addColumn('schedule_date', function($event){

   

                return $event->schedule_start->format('Y-m-d');

           

            })->rawColumns(['created_at','completed','get_maid.fullname','payment','get_sale_summary.payment_total','get_sale_summary.payment_vat',

            'get_sale_summary.created_date','get_sale_summary.payment_sub','schedule_start','schedule_end'])->make( true );

    }



    public function TransactionMaidView($id){

        $records = MaidsSchedule::where('id','=',$id)->with(['getMaid','getCustomer','getSaleSummary'])->first();

        return view('parts-dashboard.admin-transactions-view-maid', ['data'=>$records]);

    }





}	