<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use DB;

use Illuminate\Support\Facades\View;



//custom

use App\MaidsSchedule;

use App\SalesSummary;

use App\SalesDetails;

use App\DriversSchedule;

use App\Maids;

use App\Driver;

use App\Color;

use App\Customer;

use App\Schedule_address;

use App\Area;

use App\Subarea;

use App\User;

use DateTime;

use Carbon\Carbon; //fordate

date_default_timezone_set('Asia/Dubai');

use Illuminate\Support\Facades\Route;

use Faker\Generator;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Storage;

use App\Repositories\UserRepository;





class AdminCustomers extends Controller

{



    /**

     * Display a listing of the resource.

     *

     * @return Response

     */

    public function index()

    {
        $clist = Customer::where('account','0')->get();
        $data['clist'] = $clist;

		return view('parts-dashboard.customers',$data);
    }



    /**

     * Show the form for creating a new resource.

     *

     * @return Response

     */

    public function create()

    {

        if (\Auth::user() && \Auth::user()->access != 99) {
            return redirect('/admin/forbidden');
        }else{
            $data['area']  = Area::get();
            return view('parts-dashboard.customers-add' ,$data);
        }
    }



    /**

     * Store a newly created resource in storage.

     *

     * @return Response

     */

    public function store(Request $request)

    {



        if ($request->hasFile('photo')) {

            $path    = $request->file('photo')->store('public/customers');
            // $path = Storage::disk('uploads')->put('uploads', $request->file('photo'));
            $newpath = basename($path);

        }else{

            $newpath = '';

        }



		$now         = new DateTime();

		$fullname    = $request->input('fullname');

        $email       = $request->input('email');

        $mobile      = $request->input('mobile');

		$gender      = $request->input('gender') !='' ? $request->input('gender') : '';

		$birthdate   = $request->input('birthdate') !='' ? $request->input('birthdate') : $now; 



		$address     = $request->input('address');

        $area        = $request->input('area');

        $subarea     = $request->input('subarea_val');



        $property    = $request->input('property');

        $room        = $request->input('room');

        $toilet      = $request->input('toilet');

        $garden      = $request->input('garden');

        $parking     = $request->input('parking');



		   	$arr = [

                

	    		'fullname'   => $fullname,

                'mobile'     => $mobile,

                'email'      => $email,

	    		'gender'     => $gender,

	    		'birthdate'  => $birthdate,

	 

                'address'    => $address,

                'area'       => $area,

                'subarea'    => $subarea,



                'property'   => $property,

                'room'       => $room,

                'toilet'     => $toilet,

                'garden'     => $garden,

                'parking'    => $parking,



                'photo'      => $newpath,
                'account'    => '0'

	    	];


    	$newcus = Customer::create($arr);

    	$newcus->save();



        return redirect()->route('customers.index')->with('status',['success','New Customer is created successfully']);

    

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return Response

     */

    public function show(Request $request, $id)

    {	

	  	$arr=[];



	  	$status='';

  		$msched   = MaidsSchedule::where('customer_id', '=', $id)->get();

  		$cdata    = Customer::where('id', $id)->first();



		$totalhours = MaidsSchedule::where('maids_id', '=', $id)->get();

		$totalbooked = MaidsSchedule::where('customer_id', '=', $id)->count();



		$totalDuration = 0;

		foreach($totalhours as $list){

			$startTime = Carbon::parse($list->schedule_start);

			$finishTime = Carbon::parse($list->schedule_end);

			$totalDuration += intval($finishTime->diffInHours($startTime));

		}



		$hours = [

			'totalbooked' => $totalbooked,

			'totalhours' => $totalDuration

		];



        $appointment = MaidsSchedule::where('customer_id',$id)->with(['getSaleSummary', 'getSalesDetails', 'getCustomer','getMaid'])->get();

        foreach($msched as $list){

              $profit[] = SalesDetails::PaymentSum($list->schedule_code);

        }

      

	  	$data['hours'] = $hours;

        $data['msched'] = $appointment;

        $data['mdata'] = $cdata;

        $data['tot_rev'] = empty($profit) ? '0' : $profit;


		return view('parts-dashboard.customers-details',$data);

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return Response

     */

    public function edit($id)

    {   
        if (\Auth::user() && \Auth::user()->access != 99) {
            return redirect('/admin/forbidden');
        }else{

     		$cdata = Customer::where('id', $id)->first();

            $data['area']  = Area::get();

            $data['subarea']  = Subarea::get();

     		$data['cedit'] = $cdata;

    		return view('parts-dashboard.customers-edit',$data);
        }
    }



    /**

     * Update the specified resource in storage.    

     *

     * @param  int  $id

     * @return Response

     */

    public function update(Request $request, $id)

    {						



		$customer = Customer::find($id);

		$now = new DateTime();



		if ($request->hasFile('photo')) {

		  $path = $request->file('photo')->store('public/customers');

		  $newpath = basename($path);

		}else{

		  $newpath = $customer->photo;

		}



		$customer->fullname   = $request->input('fullname');

		$customer->gender     = $request->input('gender');

        $customer->birthdate  = $request->input('birthdate');

		$customer->email      = $request->input('email');

		$customer->mobile     = $request->input('mobile');

        $customer->address    = $request->input('address');

        $customer->area       = $request->input('area');

        $customer->Subarea    = $request->input('subarea_val');

        $customer->property   = $request->input('property');

        $customer->room       = $request->input('room');

        $customer->toilet     = $request->input('toilet');

        $customer->garden     = $request->input('garden');

        $customer->parking    = $request->input('parking');

		$customer->photo      = $newpath;



		$customer->save();



		return redirect()->route('customers.edit', $customer->id)->with('status',['success','Customer is edited successfully']);

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return Response

     */

    public function delcustomer(Request $request)

    {   

        $id = $request->input('id');

       	$cdata =  Customer::where('id',$id)->update(['account'=>1]);

    }



    public function getareas(Request $request){



        $area_id = $request->input('area');

        $action  = $request->input('actionfor');



            if($action == 'ini'){

                $data = Subarea::where('area_id','=',$area_id)->first();

            }else{

                $data = Subarea::where('area_id','=',$area_id)->get();

            }



        return json_encode($data);

        

    }



}	