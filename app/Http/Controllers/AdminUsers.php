<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\View;

//custom
use App\MaidsSchedule;
use App\DriversSchedule;
use App\Maids;
use App\Driver;
use App\Color;
use App\Customer;
use App\Schedule_address;
use App\Area;
use App\Subarea;
use App\User;
use DateTime;
use Carbon\Carbon; //fordate
date_default_timezone_set('Asia/Dubai');
use Illuminate\Support\Facades\Route;
use Faker\Generator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Repositories\UserRepository;


class AdminUsers extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
      	$ulist = User::get();
        $data['ulist'] = $ulist;
		return view('parts-dashboard.users',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('parts-dashboard.users-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
		$now         = new DateTime();
		$fullname    = $request->input('fullname');
        $username    = $request->input('username');
		$access      = $request->input('access');
		$email       = $request->input('email');
		$created_at  = $now->format('Y-m-d H:i:s');

		   	$arr = [
	    		'name' => $fullname,
	    		'username' => $username,
                'email' => $email,
	    		'access' => $access,
	    		'created_at' => $created_at,
                'password' => Hash::make('1234')
	    	];

    	$newuser = User::create($arr);
    	$newuser->save();
	 	return redirect()->route('users.index')->with('status',['success','New User is created successfully']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(Request $request, $id)
    {	

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
 		$mdata = User::where('id', $id)->first();

 		$data['uedit'] = $mdata;
		return view('parts-dashboard.users-edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {						

		$user = User::find($id);
		$now = new DateTime();

		$user->name       = $request->input('fullname');
		$user->email      = $request->input('email');
        $user->access     = $request->input('access');
		$user->username   = $request->input('username');

		$user->save();

		return redirect()->route('users.edit', $user->id)->with('status',['success','User is edited successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delusers(Request $request)
    {   
        $id = $request->input('id');
        $cdata =  User::find($id)->delete();
    }

}	