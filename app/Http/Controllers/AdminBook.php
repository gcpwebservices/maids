<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\View;

//custom
use App\MaidsSchedule;
use App\SalesSummary;
use App\SalesDetails;
use App\DriversSchedule;
use App\Maids;
use App\Driver;
use App\Color;
use App\Customer;
use App\Schedule_address;
use App\Area;
use App\Subarea;
use App\User;
use DateTime;
use Carbon\Carbon; //fordate
// date_default_timezone_set('Asia/Dubai');
use Illuminate\Support\Facades\Route;
use Faker\Generator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Repositories\UserRepository;
Carbon::setWeekStartsAt(Carbon::SUNDAY);
Carbon::setWeekEndsAt(Carbon::SATURDAY);
use Yajra\DataTables\DataTables;

class AdminBook extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $clist = Customer::get();
        $area  = Area::get();
        $sub   = Subarea::get();
        $mlist = Maids::get();
        $dlist = Driver::get();

        $data['currentD'] = date("Y-m-d");
        $data['currentT'] = date("H:i:s");
        $data['clist']    = $clist;
        $data['area']     = $area;
        $data['sub']      = $sub;
        $data['mlist']    = $mlist;
        $data['dlist']    = $dlist;

		return view('parts-dashboard.admin-appointments', $data);
    }

    public function getCUstomerDetails(Request $request){

        $clist = Customer::where('id',$request->input('customer'))->with(['getCusArea','getCusSubArea'])->first();
        return json_encode($clist);
    }

    public function MaidsScheduleTableView(){

        $clist = Customer::get();
        $area  = Area::get();
        $sub   = Subarea::get();
        $mlist = Maids::get();
        $dlist = Driver::get();

        $data['currentD'] = date("Y-m-d");
        $data['currentT'] = date("H:i:s");
        $data['clist']    = $clist;
        $data['area']     = $area;
        $data['sub']      = $sub;
        $data['mlist']    = $mlist;
        $data['dlist']    = $dlist;

        return view('parts-dashboard.maids-schedule-table',$data);
    }

    public function newAppointment(Request $request){

        $now   = new DateTime();
        $snum  = $this->generateRandomString();
        $color = $this->rand_color();
        $topay = $request->input('to_pay');
        $vat   = ($topay * 0.05);
        $total = ($topay + $vat);

        $sum_vat = 0;
        $sum_topay = 0;
        $sum_total_pay = 0;


        if($request->input('interval') > 0){
            $traveltime = ($request->input('interval') / 2); 
        }else{
            $traveltime = $request->input('interval'); 
        }

        //arival time
        $destinationGo =  date("Y-m-d H:i:s", strtotime("+{$traveltime} minutes", strtotime($request->input('schedule_start'))));
        $desdtinationBack   =  date("Y-m-d H:i:s", strtotime("+{$traveltime} minutes", strtotime($request->input('schedule_end'))));

        $sched_date = date("Y-m-d", strtotime($request->input('schedule_start')));

        if($request->input('num_employee') > 1){
            $arr = explode(",",$request->input('maids_id')[0]);
            for ($i = 0; $i < count($arr); $i++) {
          
                $maidsschedule = MaidsSchedule::create([
                    'maids_id'  => $arr[$i],
                    'schedule_code' => 'S#'. $snum,
                    'area_id'   => $request->input('area_id'),
                    'sub_area_id' => $request->input('subarea_id'),
                    'driver_id' => $request->input('driver_id'),
                    'customer_id' => $request->input('customer_id'),
                    'schedule_start' => $request->input('schedule_start'),
                    'schedule_end' => $request->input('schedule_end'),
                    'notes' => $request->input('notes'),
                    'schedule_start'=>$destinationGo,
                    'schedule_end'=>$desdtinationBack,
                    'schedule_date'=>$sched_date,
                    'completed'=> '3',
                    'payment'=>'0.00',
                    'schedule_color'=>$color,
                    'created_at' => $now->format('Y-m-d H:i:s'),
                ]);

                $salesInput = [
                   
                    'schedule_id'    => $maidsschedule->id,
                    'service_number' => 'S#'. $snum,
                    'item'           => '1',
                    'price'          => $topay,
                    'payment_disc'   => '0',
                    'payment_vat'    => $vat,
                    'payment_refund' => '0',
                    'payment_sub'    => $total,
                    'payment_total'  => '0',
                    'created_date'   => $now->format('Y-m-d'),
                    'is_paid'        => '0',
                    'cancel'         => '0'
                ];

                $salesInsert = SalesSummary::create($salesInput);

                $sum_vat+= $vat;
                $sum_total_pay+= $total;

            }

            $salesDetail = [

                'summary_id'        => $salesInsert->id,
                'service_number'    => $salesInsert->service_number,
                'payment_disc_sum'  => '0',
                'payment_ref_sum'   => '0',
                'payment_vat_sum'   => $sum_vat,
                'payment_sub_sum'   => $sum_total_pay,
                'payment_total_sum' => $sum_total_pay,
                'payment_sum'       => '0',
                'payment_status'    => '0',
                'transaction_type'  => '0'
            ];

            $salesDInsert = SalesDetails::create($salesDetail);

        }else{
            $maidsschedule = MaidsSchedule::create([
                'maids_id'  => $request->input('maids_id'),
                'schedule_code' => 'S#'. $snum,
                'area_id'   => $request->input('area_id'),
                'sub_area_id' => $request->input('subarea_id'),
                'driver_id' => $request->input('driver_id'),
                'customer_id' => $request->input('customer_id'),
                'schedule_start' => $request->input('schedule_start'),
                'schedule_end' => $request->input('schedule_end'),
                'notes' => $request->input('notes'),
                'schedule_start'=>$destinationGo,
                'schedule_end'=>$desdtinationBack,
                'schedule_date'=>$sched_date,
                'completed'=> '3',
                'payment'=>'0.00',
                'schedule_color'=>$color,
                'created_at' => $now->format('Y-m-d H:i:s'),
            ]);

            $salesInput = [
               
                'schedule_id'    => $maidsschedule->id,
                'service_number' => 'S#'. $snum,
                'item'           => '1',
                'price'          => $topay,
                'payment_disc'   => '0',
                'payment_vat'    => $vat,
                'payment_refund' => '0',
                'payment_sub'    => $total,
                'payment_total'  => '0',
                'created_date'   => $now->format('Y-m-d'),
                'is_paid'        => '0',
                'cancel'         => '0'
            ];

            $salesInsert = SalesSummary::create($salesInput);

            $salesDetail = [

                'summary_id'        => $salesInsert->id,
                'service_number'    => $salesInsert->service_number,
                'payment_disc_sum'  => '0',
                'payment_ref_sum'   => '0',
                'payment_vat_sum'   => $vat,
                'payment_sub_sum'   => $total,
                'payment_total_sum' => $total,
                'payment_sum'       => '0',
                'payment_status'    => '0',
                'transaction_type'  => '0'
            ];

            $salesDInsert = SalesDetails::create($salesDetail);

            $salesInsert->save();
            $salesDInsert->save();

        }

        
        return $snum;

    }

    public function MaidsScheduleRecur(Request $request){

        $startTime  = $request->input('schedule_timeF');
        $finishTime = $request->input('schedule_timeT');

        $startDate  = $request->input('schedule_dateF');
        $finishDate = $request->input('schedule_dateT');

        $now   = new DateTime();
        $snum  = $this->generateRandomString();
        $color = $this->rand_color();
        $topay = $request->input('to_pay');
        $vat   = ($topay * 0.05);
        $total_topay = ($topay+$vat);

        $sum_vat = 0;
        $sum_topay = 0;
        $sum_total_pay = 0;


        $arrDates = array();
        $arrDate = array();

        if($request->input('action') == 'save')
        {

            foreach($request->input('days') as $numDay) 
            {
                $return = $this->getDays($numDay,$startDate,$finishDate);

                foreach($return as $list)
                {

                    $returnGo = date('Y-m-d H:i:s', strtotime("$list $startTime"));
                    $returnBack = date('Y-m-d H:i:s', strtotime("$list $finishTime"));

                    if($request->input('interval') > 0){
                        $traveltime = ($request->input('interval') / 2); 
                    }else{
                        $traveltime = $request->input('interval'); 
                    }

                    //arival time
                    $destinationGo =  date("Y-m-d H:i:s", strtotime("+{$traveltime} minutes", strtotime($returnGo)));
                    $desdtinationBack   =  date("Y-m-d H:i:s", strtotime("+{$traveltime} minutes", strtotime($returnBack)));
                    $sched_date = date("Y-m-d", strtotime($request->input('schedule_dateF')));

                    if($request->input('num_employee_2') > 1){
                        $arr = explode(",",$request->input('maids_id_2')[0]);
          
                        for ($i = 0; $i < count($arr); $i++) {
                            $maidsschedule_input = [
                                'maids_id'  => $arr[$i],
                                'schedule_code' => 'S#'. $snum,
                                'area_id'   => $request->input('area_id'),
                                'sub_area_id' => $request->input('subarea_id'),
                                'driver_id' => $request->input('driver_id'),
                                'customer_id' => $request->input('customer_id'),
                                'schedule_start' => $request->input('schedule_start'),
                                'schedule_end' => $request->input('schedule_end'),
                                'notes' => $request->input('notes'),
                                'schedule_start'=>$destinationGo,
                                'schedule_end'=>$desdtinationBack,
                                'schedule_date'=>$sched_date,
                                'completed'=> '3',
                                'payment'=>'0.00',
                                'schedule_color'=>$color,
                                'created_at' => $now->format('Y-m-d H:i:s'),
                            ];

                            $maidsschedule = MaidsSchedule::create($maidsschedule_input);

                            $salesInput = [
                               
                                'schedule_id'    => $maidsschedule->id,
                                'service_number' => 'S#'. $snum,
                                'item'           => '1',
                                'price'          => $topay,
                                'payment_disc'   => '0',
                                'payment_vat'    => $vat,
                                'payment_refund' => '0',
                                'payment_sub'    => $total_topay,
                                'payment_total'  => '0',
                                'created_date'   => $now->format('Y-m-d'),
                                'is_paid'        => '0',
                                'cancel'         => '0'
                            ];

                            $salesInsert = SalesSummary::create($salesInput);

                            $sum_vat+= $vat;
                            $sum_total_pay+= $total_topay;
                        }

                    }else{

           
                        $maidsschedule_input = [
                            'maids_id'  => $request->input('maids_id_2'),
                            'schedule_code' => 'S#'. $snum,
                            'area_id'   => $request->input('area_id'),
                            'sub_area_id' => $request->input('subarea_id'),
                            'driver_id' => $request->input('driver_id'),
                            'customer_id' => $request->input('customer_id'),
                            'schedule_start' => $request->input('schedule_start'),
                            'schedule_end' => $request->input('schedule_end'),
                            'notes' => $request->input('notes'),
                            'schedule_start'=>$destinationGo,
                            'schedule_end'=>$desdtinationBack,
                            'schedule_date'=>$sched_date,
                            'completed'=> '3',
                            'payment'=>'0.00',
                            'schedule_color'=>$color,
                            'created_at' => $now->format('Y-m-d H:i:s'),
                        ];



                        $maidsschedule = MaidsSchedule::create($maidsschedule_input);

                        $salesInput = [
                           
                            'schedule_id'    => $maidsschedule->id,
                            'service_number' => 'S#'. $snum,
                            'item'           => '1',
                            'price'          => $topay,
                            'payment_disc'   => '0',
                            'payment_vat'    => $vat,
                            'payment_refund' => '0',
                            'payment_sub'    => $total_topay,
                            'payment_total'  => '0',
                            'created_date'   => $now->format('Y-m-d'),
                            'is_paid'        => '0',
                            'cancel'         => '0'
                        ];

                        $salesInsert = SalesSummary::create($salesInput);

                        $sum_vat+= $vat;
                        $sum_total_pay+= $total_topay;

                    }
                }
            }

            $salesDetail = [

                    'summary_id'        => $salesInsert->id,
                    'service_number'    => $salesInsert->service_number,
                    'payment_disc_sum'  => '0',
                    'payment_ref_sum'   => '0',
                    'payment_vat_sum'   => $sum_vat,
                    'payment_sub_sum'   => $sum_total_pay,
                    'payment_total_sum' => $sum_total_pay,
                    'payment_sum'       => '0',
                    'payment_status'    => '0',
                    'transaction_type'  => '1'
            ];

            $salesDInsert = SalesDetails::create($salesDetail);


        }else{

            $num_employee_2 = $request->input('num_employee_2');
            $startTime = Carbon::parse($request->input('timefrom'));
            $finishTime = Carbon::parse($request->input('timeto'));

            $totalDuration = $finishTime->diffInRealMinutes($startTime);
            $ctr=0;
            foreach($request->input('days') as $numDay) 
            {
                $return = $this->getDays($numDay,$startDate,$finishDate);

                foreach($return as $list)
                {
                    $ctr++;
                }

            }
            $topay = ($totalDuration / 60 * 35);
            if($num_employee_2 == 0 ){
                $newtopay = $topay;
            }else{
                $newtopay = $topay * $num_employee_2;
            }
          
            $vat = ($newtopay * 0.05);
            $total   = ($newtopay + $vat);
            $forview = $total * $ctr;

            $array=[

                'view' => number_format($forview, 2, '.', ','),
                'amount' => number_format($newtopay, 2, '.', ',')

            ];


            return json_encode($array);


        }
   
        
    }

    public function MaidsScheduleTable(Request $request){

        $maids = MaidsSchedule::select('maids_schedule.*')->with(['getMaid','getSaleSummary','getCustomer','getDriver']);

        if( $request->input('datefrom') != '' && $request->input('dateto') != ''){
            $maids->whereDate('created_at', '>=',  $request->input('datefrom'))
                        ->whereDate('created_at', '<=',  $request->input('dateto'));
        }

            return Datatables::of( $maids )->editColumn('payment', function($event){
                $details = SalesDetails::where('service_number', $event->schedule_code)->first();

                if( $details->payment_sum == '0.00' && $details->payment_status == '0' ){
                    return '<span class="label label-danger">Unpaid</span>';
                }elseif( $details->payment_sum != '0.00' && $details->payment_status == '3' ){
                    return '<span class="label label-warning">Partially Paid</span>';
                }elseif( $details->payment_status == '1'){
                    return '<span class="label label-success">Paid</span>';
                }
    

            })->addColumn('transaction', function($event){
                $details = SalesDetails::where('service_number', $event->schedule_code)->first();
                if( $details->transaction_type == '1'){
                    return '<span class="label label-default">Reccuring</span>';
                }else{
                    return '<span class="label label-default">One time</span>';
                }

            })->editColumn('get_sale_summary.created_date', function($event){

                return $event->getSaleSummary['created_date']->format('l j F Y');

            })->editColumn('completed', function($event){

                if($event->completed == 1 ){
                    return '<span class="label label-success">Done</span>';
                }elseif($event->completed == 3){
                    return '<span class="label label-warning">On&nbspqueue</span>';
                }else{
                    return '<span class="label label-danger">Cancelled</span>';
                }

            })->editColumn('schedule_start', function($event){
   
                return $event->schedule_start->format('h:i A').' To '.$event->schedule_end->format('h:i A');
           
            })->editColumn('schedule_end', function($event){
   
                return $event->schedule_end->format('h:i A');
           
            })->addColumn('schedule_date', function($event){
   
                return $event->schedule_start->format('l j F Y');
           
            })->rawColumns(['created_at','completed','get_maid.fullname','payment','transaction','get_sale_summary.payment_total',
                'get_sale_summary.payment_vat','get_sale_summary.created_date','get_sale_summary.payment_sub',
                'schedule_start','schedule_end','get_driver.fullname'])->make( true );


    }

    public function MaidsScheduleTableCancel(Request $request){

        $maids = MaidsSchedule::select('maids_schedule.*')->with(['getMaid','getSaleSummary','getCustomer','getDriver'])
                            ->where('completed','=','3')
                            ->wherehas('getSalesDetails', function($q) {
                    $q->where('payment_status', '0');
                });

        if( $request->input('datefrom') != '' && $request->input('dateto') != ''){
            $maids->whereDate('created_at', '>=',  $request->input('datefrom'))
                        ->whereDate('created_at', '<=',  $request->input('dateto'));
        }

            return Datatables::of( $maids )->editColumn('payment', function($event){
                $details = SalesDetails::where('service_number', $event->schedule_code)->first();

                if( $details->payment_sum == '0.00' && $details->payment_status == '0' ){
                    return '<span class="label label-danger">Unpaid</span>';
                }elseif( $details->payment_sum != '0.00' && $details->payment_status == '3' ){
                    return '<span class="label label-warning">Partially Paid</span>';
                }elseif( $details->payment_status == '1'){
                    return '<span class="label label-success">Paid</span>';
                }
    

            })->addColumn('transaction', function($event){
                $details = SalesDetails::where('service_number', $event->schedule_code)->first();
                if( $details->transaction_type == '1'){
                    return '<span class="label label-default">Reccuring</span>';
                }else{
                    return '<span class="label label-default">One time</span>';
                }

            })->editColumn('get_sale_summary.created_date', function($event){

                return $event->getSaleSummary['created_date']->format('l j F Y');

            })->editColumn('completed', function($event){

                if($event->completed == 1 ){
                    return '<span class="label label-success">Done</span>';
                }elseif($event->completed == 3){
                    return '<span class="label label-warning">On&nbspqueue</span>';
                }else{
                    return '<span class="label label-danger">Cancelled</span>';
                }

            })->editColumn('schedule_start', function($event){
   
                return $event->schedule_start->format('h:i A').' To '.$event->schedule_end->format('h:i A');
           
            })->editColumn('schedule_end', function($event){
   
                return $event->schedule_end->format('h:i A');
           
            })->addColumn('schedule_date', function($event){
   
                return $event->schedule_start->format('l j F Y');
           
            })->rawColumns(['created_at','completed','get_maid.fullname','payment','transaction','get_sale_summary.payment_total',
                'get_sale_summary.payment_vat','get_sale_summary.created_date','get_sale_summary.payment_sub',
                'schedule_start','schedule_end','get_driver.fullname'])->make( true );


    }

    public function MaidsScheduleTableDetail(Request $request){

        $merge=[];
        $maids_schedule = MaidsSchedule::where('id',$request->input('maids_schedule_id'))
            ->with(['getMaid','getSaleSummary','getCustomer','getDriver','getArea','getSubarea'])->first();

        $payment_details = SalesDetails::where('service_number', $maids_schedule->schedule_code)->first();
        $merge= ['maids_relation'=>$maids_schedule,'payment_details'=>$payment_details];
        return json_encode($merge);

    }


    public function editAppointment(Request $request){

        if($request->input('interval') > 0){
            $traveltime = ($request->input('interval') / 2); 
        }else{
            $traveltime = 0; 
        }

        //arival time
        $destinationGo =  date("Y-m-d H:i:s", strtotime("+{$traveltime} minutes", 
            strtotime($request->input('schedule_start'))));
        $desdtinationBack   =  date("Y-m-d H:i:s", strtotime("+{$traveltime} minutes", 
            strtotime($request->input('schedule_end'))));
        $sched_date = date("Y-m-d", strtotime($request->input('schedule_start')));
        $arr_customer = [

            'area'    => $request->input('area'),
            'subarea' => $request->input('subarea'),
            'address' => $request->input('address')

        ];

        $arr_appointment = [

            'customer_id'    => $request->input('customer_id'),
            'area_id'        => $request->input('area'),
            'sub_area_id'    => $request->input('subarea'),
            'notes'          => $request->input('service'),
            'schedule_date'  => $sched_date,
            'schedule_start' => $destinationGo,
            'schedule_end'   => $desdtinationBack,
            'maids_id'       => $request->input('maids_id'),
            'driver_id'      => $request->input('driver_id'),
            'completed'      => $request->input('area')
        ];

        $customer = Customer::where('id',$request->input('customer_id'))
                                ->update($arr_customer);

        $appointment = MaidsSchedule::where('id',$request->input('appointment_id'))
                                ->update($arr_appointment);

        $vat = ($request->input('amount') * 0.05);
        $salesummary = SalesSummary::where('schedule_id', $request->input('appointment_id'))
            ->update(['payment_sub'=>$request->input('topay'), 'payment_vat'=>$vat, 'price'=>$request->input('amount')]);

        $payment_vat = SalesSummary::where('service_number',$request->input('schedule_code'))->sum('payment_vat');
        $payment_sub = SalesSummary::where('service_number',$request->input('schedule_code'))->sum('payment_sub');

        $paymentdetails  = SalesDetails::where('service_number',$request->input('schedule_code'))
            ->update(['payment_sub_sum' => $payment_sub, 'payment_total_sum' => $payment_sub
                ,'payment_vat_sum'=>$payment_vat ]);
    }   


    public function cancelAppointment(Request $request){
        $now = new DateTime();
        if($request->input('action') == 'check'){

            $sched = MaidsSchedule::where('id', $request->input('id'))->first();
            $details = SalesDetails::where('service_number', $sched->schedule_code)->first();

            if($sched->completed == '3' && $details->payment_status == '0'){
                return 'true';
            }elseif($sched->completed == '2'){
                return 'cancelled';
            }else{
                return 'false';
            }

        }else{
        
            $sched = MaidsSchedule::where('id', $request->input('id'))->first();
            $sched->update(['completed' => 2, 'cancel_note' => $request->input('comment')]);

            $summary = SalesSummary::where('schedule_id', $sched->id)->first();
            $summary->update(['cancel'=> 1, 'cancel_dt' => $now, 'cancel_note'=> $request->input('comment') ]);

            $details = SalesDetails::where('service_number',$summary->service_number)->first();

            $vat = ($details->payment_vat_sum - $summary->payment_vat);
            $sub = ($details->payment_sub_sum - $summary->payment_sub);

            $total_sum = ($details->payment_total_sum - $summary->payment_sub);

            $detailsUpdate = SalesDetails::where('service_number',$summary->service_number)
                        ->update(['payment_vat_sum' => $vat, 'payment_sub_sum' => $sub, 'payment_total_sum' => $total_sum ]);

        }


    }



    public function getDays($day, $dateFromString, $dateToString)
    {   

        $dateFrom = new \DateTime($dateFromString);
        $dateTo = new \DateTime($dateToString);
        $dates = [];

        if($day==1){
            $textDay = 'next monday';
        }elseif($day==2){
            $textDay = 'next tuesday';
        }elseif($day==3){
            $textDay = 'next wednesday';
        }elseif($day==4){
            $textDay = 'next thursday';
        }elseif($day==5){
            $textDay = 'next friday';
        }elseif($day==6){
            $textDay = 'next saturday';
        }else{
            $textDay = 'next sunday';
        }

        if ($dateFrom > $dateTo) {
            return $dates;
        }

        if ($day != $dateFrom->format('N')) {
            $dateFrom->modify($textDay);
        }

        while ($dateFrom <= $dateTo) {
            $dates[] = $dateFrom->format('Y-m-d');
            $dateFrom->modify('+1 week');
        }

        return $dates;
    }


    public function generateRandomString($length = 4) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
        return $randomString;
    }

    public function rand_color() {
        $rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
        $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
        return $color;
    }

    public function quickAdd(Request $request){

        if ($request->hasFile('photo')) {
            $path    = $request->file('photo')->store('public/customers');
            $newpath = basename($path);
        }else{
            $newpath = '';
        }

        $now         = new DateTime();
        $fullname    = $request->input('fullname');
        $email       = $request->input('email');
        $mobile      = $request->input('mobile');
        $gender      = $request->input('gender') !='' ? $request->input('gender') : '';
        $birthdate   = $request->input('birthdate') !='' ? $request->input('birthdate') : $now; 

        $address     = $request->input('address');
        $area        = $request->input('addArea');
        $subarea     = $request->input('add_subarea_val');

            $arr = [
                'fullname'   => $fullname,
                'mobile'     => $mobile,
                'email'      => $email,
                'gender'     => $gender,
                'birthdate'  => $birthdate,
     
                'address'    => $address,
                'area'       => $area,
                'subarea'    => $subarea,
                'photo'      => $newpath
            ];

        $newcus = Customer::create($arr);
        $newcus->save();

       return redirect()->route('appointments')->with('status',['success','New Customer is created successfully']);
    }

    public function quickEdit(Request $request){
        $customer = Customer::where('id',$request->input('customer'))
            ->update(['area'=>$request->input('area'),'subarea'=>$request->input('subarea'),'address'=>$request->input('address')]);
    }

}	