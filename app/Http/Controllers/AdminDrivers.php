<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use DB;

use Illuminate\Support\Facades\View;



//custom

use App\MaidsSchedule;

use App\DriversSchedule;

use App\Maids;

use App\Driver;

use App\Color;

use App\Customer;

use App\Schedule_address;

use App\Area;

use App\Subarea;

use App\User;

use DateTime;

use Carbon\Carbon; //fordate

date_default_timezone_set('Asia/Dubai');

use Illuminate\Support\Facades\Route;

use Faker\Generator;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Storage;

use App\Repositories\UserRepository;





class AdminDrivers extends Controller

{



    /**

     * Display a listing of the resource.

     *

     * @return Response

     */

    public function index()

    {

      	$dlist = Driver::where('account','0')->get();

        $data['dlist'] = $dlist;

		return view('parts-dashboard.drivers',$data);

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return Response

     */

    public function create()

    {
        if (\Auth::user() && \Auth::user()->access != 99) {
            return redirect('/admin/forbidden');
        }else{
            return view('parts-dashboard.drivers-add');
        }
    }



    /**

     * Store a newly created resource in storage.

     *

     * @return Response

     */

    public function store(Request $request)

    {



    	if ($request->hasFile('photo')) {

            $path = $request->file('photo')->store('public/drivers');

            $newpath = basename($path);

        }else{

        	$newpath = '';

        }



		$now = new DateTime();

		$fullname    = $request->input('fullname');

		$gender      = $request->input('gender');

		$birthdate   = $request->input('birthdate');

		$created_at  = $now->format('Y-m-d H:i:s');

		$address     = $request->input('address');

		$mobile      = $request->input('mobile');

		$shift_start = $request->input('shift_start');

		$shift_end   = $request->input('shift_end');


		   	$arr = [

	    		'fullname' => $fullname,

	    		'gender' => $gender,

	    		'birthdate' => $birthdate,

	    		'created_at' => $created_at,

	    		'mobile' => $mobile,

	    		'shift_start' => $shift_start,

	    		'shift_end' => $shift_end,

	    		'photo' => $newpath,

                'account' => 0



	    	];


    	$newdriver = Driver::create($arr);

    	$newdriver->save();

	 	return redirect()->route('drivers.index')->with('status',['success','New Driver is created successfully']);



    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return Response

     */

    public function show(Request $request, $id)

    {	

	  	$arr=[];

  		$dsched = MaidsSchedule::where('driver_id', '=', $id)->get();

  		$ddata = Driver::where('id', $id)->first();



		$totalhours = MaidsSchedule::where('maids_id', '=', $id)->get();

		$totalbooked = MaidsSchedule::where('maids_id', '=', $id)->count();



		$totalDuration = 0;

		foreach($totalhours as $list){

			$startTime = Carbon::parse($list->schedule_start);

			$finishTime = Carbon::parse($list->schedule_end);

			$totalDuration += intval($finishTime->diffInHours($startTime));

		}



		$hours = [

			'totalbooked' => $totalbooked,

			'totalhours' => $totalDuration

		];



	  	foreach($dsched as $list){

			$customer = Customer::where('id', '=', $list->customer_id)->value('fullname');

	  		$arr[] = [

	  			'schedule_code' => $list->schedule_code,

	  			'schedule_start' => $list->schedule_start->format('Y-m-d'),

	  			'time' => $list->schedule_start->format('h:i A'). ' - ' .$list->schedule_end->format('h:i A'),

	  			'status' => $list->completed,

	  			'client' => $customer,

                'customer_id' =>$list->customer_id

	  		];

	  	}

        

	  	$data['hours'] = $hours;

        $data['dsched'] = $arr;

        $data['ddata'] = $ddata;



		return view('parts-dashboard.drivers-details',$data);

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return Response

     */

    public function edit($id)

    {
        if (\Auth::user() && \Auth::user()->access != 99) {
            return redirect('/admin/forbidden');
        }else{
            $ddata = Driver::where('id', $id)->first();
            $data['dedit'] = $ddata;
            return view('parts-dashboard.drivers-edit',$data);
        }
    }



    /**

     * Update the specified resource in storage.

     *

     * @param  int  $id

     * @return Response

     */

    public function update(Request $request, $id)

    {						

		$driver = Driver::find($id);

		$now = new DateTime();

		if ($request->hasFile('photo')) {

		$path = $request->file('photo')->store('public/drivers');

        $newpath = basename($path);

		}else{

            $newpath = $driver->photo;

		}

		$driver->fullname    = $request->input('fullname');
		$driver->gender      = $request->input('gender');
		$driver->birthdate   = $request->input('birthdate');
		$driver->mobile      = $request->input('mobile');
		$driver->shift_start = $request->input('shift_start');
		$driver->shift_end   = $request->input('shift_end');
		$driver->photo       = $newpath;

		$driver->save();

		return redirect()->route('drivers.edit', $driver->id)->with('status',['success','Driver is edited successfully']);

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return Response

     */

    public function delmaids(Request $request)

    {   

        $id = $request->input('id');

        $ddata =  Driver::where('id',$id)->update(['account'=>1]);

    }



}	