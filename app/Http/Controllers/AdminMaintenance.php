<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use DB;

use Illuminate\Support\Facades\View;



//custom

use App\MaidsSchedule;
use App\DriversSchedule;
use App\Maids;
use App\Driver;
use App\Color;
use App\Customer;
use App\Schedule_address;
use App\Area;
use App\Subarea;
use App\User;
use DateTime;
use Carbon\Carbon; //fordate
date_default_timezone_set('Asia/Dubai');
use Illuminate\Support\Facades\Route;
use Faker\Generator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;


class AdminMaintenance extends Controller
{	



    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('auth');



    }



   	public function listmaidcustomerdriver(){



		$route = Route::currentRouteName();



		if( $route == 'add-maids') {

	      	$mlist = Maids::get();

	        $data['mlist'] = $mlist;

			return view('parts-dashboard.add-maids',$data);

		}



		if( $route == 'add-drivers') {

	      	$dlist = Driver::get();

	        $data['dlist'] = $dlist;

			return view('parts-dashboard.add-drivers',$data);

		}



		if( $route == 'add-customers') {

	      	$clist = Customer::get();

	        $data['clist'] = $clist;

			return view('parts-dashboard.add-customers',$data);

		}



		if( $route == 'add-users') {

	      	$users = User::get();

	        $data['ulist'] = $users;

			return view('parts-dashboard.add-users',$data);

		}

  

    }





    public function users(Request $request){



		$now = new DateTime();

		$fullname   = $request->input('name');

		$email      = $request->input('email');

		$username   = $request->input('username');

		$action     = $request->input('action');



		if($action == 'users'){

	   		$arr = [

	    		'name' => $fullname,

	    		'username' => $username,

	    		'email' => $email,

	    		'password' => Hash::make('1234')

	    	];



	    	$newuser = User::create($arr);

	    	$newuser->save();

     		return response()->json($newuser);



		}



		if($action == 'getid'){

			$id = $request->input('id');

	    	$returnid = User::where('id', $id)->first();

     		return response()->json($returnid);

		}



		if($action == 'edituser'){

			$id = $request->input('id');

	   		$arr = [

	    		'name' => $fullname,

	    		'username' => $username,

	    		'email' => $email

	    	];



			$edituser =	User::where('id', $id)->first()->update($arr);



     		return response()->json($edituser);

		}



		if($action == 'deluser'){

			$id = $request->input('id');

			$deluser =	User::where('id', $id)->delete();

     		return response()->json($deluser);

		}



	

    }



    public function maids(Request $request){



		$now = new DateTime();

		$fullname    = $request->input('fullname');

		$gender      = $request->input('gender');

		$birthdate   = $request->input('birthdate');

		$created_at  = $now->format('Y-m-d H:i:s');

		$address     = $request->input('address');

		$mobile      = $request->input('mobile');

		$shift_start = $request->input('shift_start');

		$shift_end   = $request->input('shift_end');

		$action      = $request->input('action');



		if($action == 'addmaids'){

			   	$arr = [

	    		'fullname' => $fullname,

	    		'gender' => $gender,

	    		'birthdate' => $birthdate,

	    		'created_at' => $created_at,

	    		'mobile' => $mobile,

	    		'shift_start' => $shift_start,

	    		'shift_end' => $shift_end



	    	];



	    	$newmaid = Maids::create($arr);

	    	$newmaid->save();

     		return response()->json($newmaid);



		}



		if($action == 'getid'){

			$id = $request->input('id');

	    	$returnid = Maids::where('id', $id)->first();

     		return response()->json($returnid);

		}



		if($action == 'editmaids'){

			$id = $request->input('id');

   		   	$arr = [

	    		'fullname' => $fullname,

	    		'gender' => $gender,

	    		'birthdate' => $birthdate,

	    		'updated_at' => $created_at

	    	];

			$editmaids =	Maids::where('id', $id)->first()->update($arr);

     		return response()->json($editmaids);

		}



		if($action == 'delmaids'){

			$id = $request->input('id');

			$delmaids =	Maids::where('id', $id)->delete();

     		return response()->json($delmaids);

		}



	

    }



    public function drivers(Request $request){

    	

		$now = new DateTime();

		$fullname   = $request->input('fullname');

		$gender     = $request->input('gender');

		$birthdate  = $request->input('birthdate');

		$created_at = $now->format('Y-m-d H:i:s');

		$address    = $request->input('address');

		$action     = $request->input('action');





		if($action == 'adddrivers'){

		   	$arr = [

	    		'fullname' => $fullname,

	    		'gender' => $gender,

	    		'birthdate' => $birthdate,

	    		'created_at' => $created_at

	    	];



	    	$newdriver = Driver::create($arr);

	    	$newdriver->save();

     		return response()->json($newdriver);



		}



		if($action == 'getid'){

			$id = $request->input('id');

	    	$returnid = Driver::where('id', $id)->first();

     		return response()->json($returnid);

		}



		if($action == 'editdrivers'){

			$id = $request->input('id');

   		   	$arr = [

	    		'fullname' => $fullname,

	    		'gender' => $gender,

	    		'birthdate' => $birthdate,

	    		'updated_at' => $created_at

	    	];

			$editdrivers = Driver::where('id', $id)->first()->update($arr);

     		return response()->json($editdrivers);

		}



		if($action == 'deldriver'){

			$id = $request->input('id');

			$deldriver =	Driver::where('id', $id)->delete();

     		return response()->json($deldriver);

		}



	

    }



    public function customers(Request $request){

    	

		$now = new DateTime();

		$fullname   = $request->input('fullname');

		$gender     = $request->input('gender');

		$birthdate  = $request->input('birthdate');

		$created_at = $now->format('Y-m-d H:i:s');

		$address    = $request->input('address');

		$action     = $request->input('action');





		if($action == 'addcustomer'){

		   	$arr = [

	    		'fullname' => $fullname,

	    		'gender' => $gender,

		  		'address' => $address,

	    		'birthdate' => $birthdate,

	    		'created_at' => $created_at

	    	];



	    	$newcustomer = Customer::create($arr);

	    	$newcustomer->save();

     		return response()->json($newcustomer);



		}



		if($action == 'getid'){

			$id = $request->input('id');

	    	$returnid = Customer::where('id', $id)->first();

     		return response()->json($returnid);

		}



		if($action == 'editcustomer'){

			$id = $request->input('id');

   		   	$arr = [

	    		'fullname' => $fullname,

	    		'gender' => $gender,

		  		'address' => $address,

	    		'birthdate' => $birthdate,

	    		'created_at' => $created_at

	    	];

			$editcustomer = Customer::where('id', $id)->first()->update($arr);

     		return response()->json($editcustomer);

		}



		if($action == 'delcustomer'){

			$id = $request->input('id');

			$delcustomer =	Customer::where('id', $id)->delete();

     		return response()->json($delcustomer);

		}



	

    }



    public function paginate(Request $request){

  //     	$mlist = Maids::get();

  //       $data['mlist'] = $mlist;



    }





    //dropdown



    public function getMaidDetails(Request $request){

	   	$id = $request->input('id');

   	 

    	if($request->input('action') == 'top'){

		   	$mlist = Maids::where('id', '=', $id)->first();

		   	$merge = array_merge(['returnfor' => 'top'],$mlist->toArray());

			return response()->json($merge);



    	}



    	if($request->input('action') == 'bottom'){



			$totalhours = MaidsSchedule::where('maids_id', '=', $id)->get();

			$totalbooked = MaidsSchedule::where('maids_id', '=', $id)->count();



			$totalDuration = 0;

			foreach($totalhours as $list){

				$startTime = Carbon::parse($list->schedule_start);

				$finishTime = Carbon::parse($list->schedule_end);

				$totalDuration += intval($finishTime->diffInHours($startTime));

			}



			$arr = [

				'returnfor' => 'bottom',

				'totalbooked' => $totalbooked,

				'totalhours' => $totalDuration

			];



	 		return response()->json($arr);

		

    	}



    	if($request->input('action') == 'table'){



    		$limit = $request->input('limit');



    		if(!empty($limit)){

				$mlist = MaidsSchedule::where('maids_id', '=', $id)->take($limit)->get();

    		}else{

				$mlist = MaidsSchedule::where('maids_id', '=', $id)->get();

    		}



		

		  	

		

		  	$arr=[];



		  	$status='';

		  	foreach($mlist as $list){

  				$customer = Customer::where('id', '=', $list->customer_id)->value('fullname');

		  		if($list->completed == '1'){

		  			$status = 'Completed';

		  		}else{

		  			$status = 'in Progress';

		  		}



		  		$arr[] = [

		  			'schedule_code' => $list->schedule_code,

		  			'schedule_start' => $list->schedule_start->format('Y-m-d'),

		  			'time' => $list->schedule_start->format('h:i A'). ' - ' .$list->schedule_end->format('h:i A'),

		  			'status' => $status,

		  			'client' => $customer

		  		];



			

		  	}

		  

			return response()->json($arr);

    	}

 

    }



  



}