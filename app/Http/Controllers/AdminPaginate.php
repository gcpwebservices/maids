<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\View;

//custom
use App\MaidsSchedule;
use App\SalesSummary;
use App\DriversSchedule;
use App\Maids;
use App\Driver;
use App\Color;
use App\Customer;
use App\Schedule_address;
use App\Area;
use App\Subarea;
use App\User;
use DateTime;
use Carbon\Carbon; //fordate
date_default_timezone_set('Asia/Dubai');
use Illuminate\Support\Facades\Route;
use Faker\Generator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Repositories\UserRepository;
Carbon::setWeekStartsAt(Carbon::SUNDAY);
Carbon::setWeekEndsAt(Carbon::SATURDAY);
use Illuminate\Support\Collection;

class AdminPaginate extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {   
        return view('parts-dashboard.paginate');

    }

    public function getDropdown(){
        $customers = Customer::get();
        return $customers;
    }

    public function returnApiMaids(){
        $maids = Maids::get();
        return $maids;
    }


    public function returnApi($id,$name){

    
        if($id=='0' && $name == '0'){
            $customers = Customer::paginate(3);
        }else{
            $customers = Customer::where('id','=',$id)
                    ->orWhere('fullname', 'like',"%$name%")
                    ->paginate(3);
        }
        return $customers;
    }

    public function filterpagiante(Request $request){
        $get = $request->input('filter');
        $maids = Maids::where('fullname','LIKE', '%'. $get .'%')->paginate(5)->setPath('');

        $pagination = $maids->appends(array(
            'filter' =>$request->input('filter'),
        ));

        if (count ( $maids ) > 0){
            return view('parts-dashboard. paginate',['maids'=>$maids])->withQuery($get);
        }else{
            return view ( 'parts-dashboard.paginate',['maids'=>$maids])->withMessage ( 'No Details found. Try to search again !' );
        }
       
    }

    public function apiTest(Request $request){
        $customers = Customer::paginate(3);
        return $customers;
    }




}	