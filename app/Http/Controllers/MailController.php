<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Mail;

use App\Http\Controllers\Controller;
use App\Mail\AdminMail;

class MailController extends Controller
{
  	public function sendEmail(Request $request){

	// $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
 //    $beautymail->send('mail.welcome', [], function($message)
 //    {
 //      $message
 //  			->from('gcarlopadua@yahoo.com')
 //  			->to('gian@nrsinfoways.com', 'NRS Admin')
 //  			->subject('Welcome!');
 //    });
  		
  		$service_code = $request->input('service_code');
  		$type = $request->input('email_type');

      	$email = $request->input('customer_email');
      	$fullname = $request->input('customer_name');

	  	$mail = Mail::to($email)->send(new AdminMail($fullname,$type));


	  	return response()->json(['success']);

   }

}
