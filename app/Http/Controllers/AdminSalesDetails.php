<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\View;

//custom
use App\MaidsSchedule;
use App\SalesSummary;
use App\DriversSchedule;
use App\Maids;
use App\Driver;
use App\Color;
use App\Customer;
use App\Schedule_address;
use App\Area;
use App\Subarea;
use App\User;
use DateTime;
use Carbon\Carbon; //fordate
date_default_timezone_set('Asia/Dubai');
use Illuminate\Support\Facades\Route;
use Faker\Generator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Repositories\UserRepository;
Carbon::setWeekStartsAt(Carbon::SUNDAY);
Carbon::setWeekEndsAt(Carbon::SATURDAY);


class AdminSalesDetails extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {                 
		return view('parts-dashboard.admin-sales-details');
    }


    public function TranSummaryFilter(Request $request){

        $from = $request->input('datefrom');
        $to   = $request->input('dateto');

        $arr = [];

        if($from == '' && $to == ''){

            $bookedqty  = SalesSummary::count();
            $refundqty  = SalesSummary::where('payment_refund', '!=', '0.00')->count();
            $paymentqty = SalesSummary::where('payment_total', '!=', '0.00')->count();


            $disc       = SalesSummary::sum('payment_disc');
            $vat        = SalesSummary::sum('payment_vat');
            $payment    = SalesSummary::sum('payment_total');


        }else{

            $bookedqty  = SalesSummary::whereDate('created_date', '>=',  $from)
                                        ->whereDate('created_date', '<=',  $to)
                                        ->count();

            $refundqty  = SalesSummary::where('payment_refund', '!=', '0.00')
                                        ->whereDate('created_date', '>=',  $from)
                                        ->whereDate('created_date', '<=',  $to)
                                        ->count();

            $paymentqty = SalesSummary::where('payment_total', '!=', '0.00')
                                        ->whereDate('created_date', '>=',  $from)
                                        ->whereDate('created_date', '<=',  $to)
                                        ->count();

            $disc       = SalesSummary::whereDate('created_date', '>=',  $from)
                                        ->whereDate('created_date', '<=',  $to)
                                        ->sum('payment_disc');

            $vat        = SalesSummary::whereDate('created_date', '>=',  $from)
                                        ->whereDate('created_date', '<=',  $to)
                                        ->sum('payment_vat');

            $payment    = SalesSummary::whereDate('created_date', '>=',  $from)
                                        ->whereDate('created_date', '<=',  $to)
                                        ->sum('payment_total');

        }



        $total      = $disc+$vat+$payment;

        $arr = [

            'bookqty'   => $bookedqty,
            'refundqty' => $refundqty,
            'paidqty'   => $paymentqty,

            'total'     => number_format((float)$payment, 2, '.', ''),

            'disc'      => $disc,
            'vat'       => $vat,
            'grand'     => number_format((float)$total, 2, '.', '')

        ];

       return response()->json($arr);
    }


}	