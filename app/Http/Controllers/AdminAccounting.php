<?php



namespace App\Http\Controllers;
use App\Mail\AdminMail;

use App\Exports\ExportData;

use Illuminate\Http\Request;

use DB;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Mail;

//custom

use App\MaidsSchedule;

use App\SalesSummary;

use App\SalesDetails;

use App\DriversSchedule;

use App\Maids;

use App\Driver;

use App\Color;

use App\Customer;

use App\Schedule_address;

use App\Area;

use App\Subarea;

use App\User;

use DateTime;

use Carbon\Carbon; //fordate

date_default_timezone_set('Asia/Dubai');

use Illuminate\Support\Facades\Route;

use Faker\Generator;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Storage;

use App\Repositories\UserRepository;

Carbon::setWeekStartsAt(Carbon::SUNDAY);

Carbon::setWeekEndsAt(Carbon::SATURDAY);

use Yajra\DataTables\DataTables;

use \PDF;

use PhpOffice\PhpSpreadsheet\Spreadsheet;

use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use Maatwebsite\Excel\Facades\Excel;





class AdminAccounting extends Controller

{   





    /**

     * Display a listing of the resource.

     *

     * @return Response

     */

    public function index()

    {

		return view('parts-dashboard.admin-accounting');

    }



    public function paidView()

    {

        return view('parts-dashboard.admin-accounting-paid');

    }



    public function unpaidView()

    {

        return view('parts-dashboard.admin-accounting-unpaid');

    }



    public function AccountingTable(Request $request){



        $maids = MaidsSchedule::select('maids_schedule.*')->with(['getMaid','getSaleSummary','getCustomer'

                ,'getDriver','getSalesDetails']);

        if( $request->input('datefrom') != '' && $request->input('dateto') != ''){

            $maids->whereDate('created_at', '>=',  $request->input('datefrom'))
                        ->whereDate('created_at', '<=',  $request->input('dateto'));
        }

            return Datatables::of( $maids )->editColumn('get_sales_details.payment_status', function($event){

                $details = SalesDetails::where('service_number', $event->schedule_code)->first();

                if( $details->payment_sum == '0.00' 

                    && $details->payment_status == '0' ){

                    return '<span class="label label-danger">Unpaid</span>';

                }elseif( $details->payment_sum  != '0.00' 

                    && $details->payment_status == '3' ){

                    return '<span class="label label-warning">Partially Paid</span>';

                }elseif( $details->payment_status == '1'){

                    return '<span class="label label-success">Paid</span>';

                }

    

            })->editColumn('get_sales_details.transaction_type', function($event){

                $details = SalesDetails::where('service_number', $event->schedule_code)->first();

                if( $details->transaction_type == '1'){

                    return '<span class="label label-default">Reccuring</span>';

                }else{

                    return '<span class="label label-default">One time</span>';

                }



            })->editColumn('get_sale_summary.created_date', function($event){



                return $event->getSaleSummary['created_date']->format('l j F Y');



            })->editColumn('completed', function($event){



                if($event->completed == 1 ){

                    return '<span class="label label-success">Done</span>';

                }elseif($event->completed == 3){

                    return '<span class="label label-warning">On queue</span>';

                }else{

                    return '<span class="label label-danger">Cancelled</span>';

                }



            })->editColumn('schedule_start', function($event){

   

                return $event->schedule_start->format('h:i A').' To '.$event->schedule_end->format('h:i A');

           

            })->editColumn('schedule_end', function($event){

   

                return $event->schedule_end->format('h:i A');

           

            })->editColumn('schedule_date', function($event){

   

                return $event->schedule_start->format('l j F Y');

           

            })->rawColumns(['created_at','completed','schedule_code','get_maid.fullname','get_customer.fullname'

            ,'get_sales_details.payment_status','get_sale_summary.payment_total'

            ,'get_sale_summary.payment_vat','get_sale_summary.created_date'

            ,'get_sale_summary.payment_sub','schedule_start','schedule_end','schedule_date'

            ,'get_driver.fullname','get_sales_details.transaction_type','get_sale_summary.price'])->make( true );

    }



    public function AccountingType(Request $request){



        if($request->input('ttype') == 'paid'){

            $maids = MaidsSchedule::select('maids_schedule.*')->whereHas('getSalesDetails', function ($query) {

                        $query->where('payment_status','=','1');

                        })->with(['getMaid','getCustomer','getDriver','getSaleSummary']);

        }else{

            $maids = MaidsSchedule::select('maids_schedule.*')->whereHas('getSalesDetails', function ($query) {

                        $query->where('payment_status','=','0');

                    })->with(['getMaid','getCustomer','getDriver','getSaleSummary']);

        }



        if( $request->input('datefrom') != '' && $request->input('dateto') != ''){

            $maids->whereDate('created_at', '>=',  $request->input('datefrom'))

                        ->whereDate('created_at', '<=',  $request->input('dateto'));

        }

            return Datatables::of( $maids )->addColumn('transaction', function($event){



                $details = SalesDetails::where('service_number', $event->schedule_code)->first();

                if( $details->transaction_type == '1'){

                    return '<span class="label label-default">Reccuring</span>';

                }else{

                    return '<span class="label label-default">One time</span>';

                }



            })->editColumn('get_sale_summary.created_date', function($event){



                return $event->getSaleSummary['created_date']->format('l j F Y');



            })->editColumn('completed', function($event){



                if($event->completed == 1 ){

                    return '<span class="label label-success">Done</span>';

                }elseif($event->completed == 3){

                    return '<span class="label label-warning">On queue</span>';

                }else{

                    return '<span class="label label-danger">Cancelled</span>';

                }



            })->editColumn('schedule_start', function($event){

                return $event->schedule_start->format('h:i A').' To '.$event->schedule_end->format('h:i A');

            })->editColumn('schedule_end', function($event){

                return $event->schedule_end->format('h:i A');

            })->editColumn('schedule_date', function($event){

                return $event->schedule_start->format('l j F Y');

            })->rawColumns(['schedule_code','get_customer.fullname','get_maid.fullname','get_driver.fullname'

            ,'transaction','completed','schedule_date','schedule_start','schedule_end'

            ,'get_sale_summary.price'])->make( true );

    }



 	public function TransactionDetails($id)

    {	

        $trans = MaidsSchedule::where('id',$id)->with(['getCustomer'])->first();
        $transactions = MaidsSchedule::where('schedule_code', $trans->schedule_code)
        ->with(['getCustomer','getSaleSummary','getMaid'])->get();
        $details = SalesDetails::where('service_number', $trans->schedule_code)->first();

        $data['maids_schedule'] = $trans;
        $data['maids_relation'] = $transactions;
        $data['sales_details'] = $details;
		return view('parts-dashboard.admin-accounting-details', $data);

    }

    public function checkBalance(Request $request){

        $return=[];
        $details = SalesDetails::where('id', $request->input('sum_details_id'))->first();
        $maids_schedule = MaidsSchedule::where('schedule_code',$details->service_number)->count();

        if( $maids_schedule > 1){
            $return = [    
                'partial'       =>'yes', 
                'total_payment' => $details->payment_sum,
                'topay'         => $details->payment_total_sum 
            ];
        }else{
            $return = [    
                'partial'       =>'no', 
                'total_payment' => $details->payment_sum,
                'topay'         => $details->payment_total_sum 
            ];
        }
        return json_encode($return);
    }

    public function payAllSumId(Request $request){
        $summary = SalesSummary::where('service_number', $request->input('service_number'))
                    ->where('is_paid','=','0')
                    ->where('cancel', '=','0')
                    ->get();
        return json_encode($summary);
    }

    public function payPerSumId(Request $request){
        $summary_id = $request->input('sum_id');
        $summary = SalesSummary::where('id', $summary_id)
                    ->where('is_paid','=','0')
                    ->where('cancel', '=','0')
                    ->first();
        return json_encode($summary);
    }


    public function savepayment(Request $request){

        $trans = MaidsSchedule::where('id',$request->input('transaction_id'))->with(['getCustomer'])->first();

        $transactions = MaidsSchedule::where('schedule_code', $trans->schedule_code)
            ->with(['getCustomer','getSaleSummary','getMaid','getArea','getSubArea'])->get();
        $details = SalesDetails::where('service_number', $trans->schedule_code)->first();

        $data['maids_schedule'] = $trans;
        $data['maids_relation'] = $transactions;
        $data['sales_details'] = $details;
        $paper = array(0,0,595.28,841.89);
        $pdf = PDF::loadView('transaction.invoicemail', $data);
        $pdf->setPaper($paper);
        // $pdf->save(storage_path().$trans->getCustomer['fullname'].'.pdf');
        $file_path = storage_path('app/public/invoices');

        $pdf->save($file_path.'/'.$trans->getCustomer['fullname'].'.pdf');

        $now = new DateTime();

        foreach($request->input('to_pay_summary') as $list){
            $payment_sub = SalesSummary::where('id', $list)->first();
            $salesInput_Sum =[
                'payment_date'  => $now->format('y-m-d H:i:s'),
                'is_paid'       => '1',
                'payment_total' => $payment_sub->payment_sub,
                'payment_note'  => $request->input('to_pay_notes')
            ];
            $paymentSummary = SalesSummary::where('id', $list)->update($salesInput_Sum);
        }

        $salesInput = [ 
            'payment_disc'   => '0',
            'payment_refund' => '0',
            'payment_total'  => $request->input('payment'),
            'payment_date'   => $now->format('y-m-d H:i:s'),
            'updated_at'     => $now->format('y-m-d H:i:s') 
        ];

        $sale_details = [
            'payment_status' => '1',
            'payment_date'   => $now->format('y-m-d'),
        ];

        $addition = SalesDetails::where('id',$request->input('sum_details_id'))->increment('payment_sum',$request->input('payment'));

        if($addition){
            $details = SalesDetails::where('id', $request->input('sum_details_id'))->first();
            if($details->payment_total_sum == $details->payment_sum){
                $paymentDetails = SalesDetails::where('id', $request->input('sum_details_id'))->update($sale_details);
            }else{
                $paymentDetails = SalesDetails::where('id', $request->input('sum_details_id'))->update(['payment_status'=>'3']);
            }
        }


    }

    public function exporting(Request $request, $id){

        $trans = MaidsSchedule::where('id',$id)->with(['getCustomer'])->first();
        $transactions = MaidsSchedule::where('schedule_code', $trans->schedule_code)
            ->with(['getCustomer','getSaleSummary','getMaid'])->get();
        $details = SalesDetails::where('service_number', $trans->schedule_code)->first();

        $data['maids_schedule'] = $trans;
        $data['maids_relation'] = $transactions;
        $data['sales_details'] = $details;
        $type = $request->input('export');

        if( $type == '1'){
            $pdf = PDF::loadView('parts-dashboard.admin-accounting-pdf', $data);
            // $pdf->save(storage_path().'_filename.pdf');
            return $pdf->stream($trans->getCustomer['fullname'].'.pdf');  
        }

        if( $type == '2')
        {
            return Excel::download( new ExportData($id), $trans->getCustomer['fullname'].'.xlsx');
        }
    }

    public function invoice(Request $request){

        $trans = MaidsSchedule::where('id','66')->with(['getCustomer'])->first();

        $transactions = MaidsSchedule::where('schedule_code', $trans->schedule_code)
            ->with(['getCustomer','getSaleSummary','getMaid','getArea','getSubArea'])->get();
        $details = SalesDetails::where('service_number', $trans->schedule_code)->first();

        $data['maids_schedule'] = $trans;
        $data['maids_relation'] = $transactions;
        $data['sales_details'] = $details;
    
        return view('transaction.invoice', $data);

    }

    public function sendEmailReceipt(Request $request){

        $trans = MaidsSchedule::where('id',$request->input('transaction_id'))->with(['getCustomer'])->first();

        $transactions = MaidsSchedule::where('schedule_code', $trans->schedule_code)
            ->with(['getCustomer','getSaleSummary','getMaid','getArea','getSubArea'])->get();
        $details = SalesDetails::where('service_number', $trans->schedule_code)->first();

        $data['maids_schedule'] = $trans;
        $data['maids_relation'] = $transactions;
        $data['sales_details'] = $details;

        $subject = 'Receipt for '.$trans->schedule_code;
        $type = 'receipt';
        $mail = Mail::to('gian@nrsinfoways.com')->send(new AdminMail($subject,$data, $type));

    }

    public function sendEmailAccount(Request $request){
        
        $service_code = $request->input('service_code');
        $type = $request->input('email_type');
        $email = $request->input('customer_email');
        $fullname = $request->input('customer_name');

        $mail = Mail::to($email)->send(new AdminMail($fullname,$type));
        return response()->json(['success']);
        
    }

  

}	





