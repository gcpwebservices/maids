<?php



namespace App\Http\Controllers;
use App\Mail\AdminMail;


use Illuminate\Http\Request;

use DB;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Mail;


//custom

use App\MaidsSchedule;

use App\SalesSummary;

use App\SalesDetails;

use App\DriversSchedule;

use App\Maids;

use App\Driver;

use App\Color;

use App\Customer;

use App\Schedule_address;

use App\Area;

use App\Subarea;

use App\User;

use DateTime;

use Carbon\Carbon; //fordate

date_default_timezone_set('Asia/Dubai');

use Illuminate\Support\Facades\Route;

use Faker\Generator;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Storage;

use App\Repositories\UserRepository;

Carbon::setWeekStartsAt(Carbon::SUNDAY);

Carbon::setWeekEndsAt(Carbon::SATURDAY);

use Yajra\DataTables\DataTables;



class AdminSales extends Controller

{



    /**

     * Display a listing of the resource.

     *

     * @return Response

     */

    public function index()

    {
		return view('parts-dashboard.admin-sales');

    }


    public function year(Request $request){
        $arr=[];
        for($i=1; $i <= 12; $i++){
            $sum = SalesDetails::select('payment_sum')->whereMonth('payment_date', '=', $i)->get()->sum('payment_sum');
            $arr[] = $sum;
        }
        return response()->json($arr);
    }

    public function week(Request $request){

        $sun = Carbon::now()->startOfWeek();
        $mon = $sun->copy()->addDay();
        $tue = $mon->copy()->addDay();
        $wed = $tue->copy()->addDay();
        $thu = $wed->copy()->addDay();
        $fri = $thu->copy()->addDay();
        $sat = $fri->copy()->addDay();

        $sunday     = SalesDetails::whereDay('payment_date', $sun->format('d'))->get()->sum('payment_sum');
        $monday     = SalesDetails::whereDay('payment_date', $mon->format('d'))->get()->sum('payment_sum');
        $tuesday    = SalesDetails::whereDay('payment_date', $tue->format('d'))->get()->sum('payment_sum');
        $wednesday  = SalesDetails::whereDay('payment_date', $wed->format('d'))->get()->sum('payment_sum');
        $thursday   = SalesDetails::whereDay('payment_date', $thu->format('d'))->get()->sum('payment_sum');
        $friday     = SalesDetails::whereDay('payment_date', $fri->format('d'))->get()->sum('payment_sum');
        $saturday   = SalesDetails::whereDay('payment_date', $sat->format('d'))->get()->sum('payment_sum');

        $arr['values']=[$sunday,$monday,$tuesday,$wednesday,$thursday,$friday,$saturday];
        $arrdates['dates']=[$sun,$sat];
        $new = array_merge($arr,$arrdates);
        return response()->json($new);  
    }



    public function month(Request $request){

        $week1 = Carbon::now()->startOfMonth();
        $week2 = $week1->copy()->addDay(7);
        $week3 = $week2->copy()->addDay(7);
        $week4 = $week3->copy()->addDay(7);
        $week5 = $week4->copy()->addDay(7);
        $week6 = Carbon::now()->endOfMonth();

        $first  = SalesDetails::whereBetween('payment_date', [$week1,  $week2])->get()->sum('payment_sum');
        $second = SalesDetails::whereBetween('payment_date', [$week2,  $week3])->get()->sum('payment_sum');
        $third  = SalesDetails::whereBetween('payment_date', [$week3,  $week4])->get()->sum('payment_sum');
        $fourt  = SalesDetails::whereBetween('payment_date', [$week4,  $week5])->get()->sum('payment_sum');
        $fifth  = SalesDetails::whereBetween('payment_date', [$week5,  $week6])->get()->sum('payment_sum');

        $arr=[$first,$second,$third,$fourt,$fifth];
        $new = array_merge($arr);
        return response()->json($new);
    }



    public function dateRange(Request $request){

        $from = $request->input('datefrom');
        $to = $request->input('dateto');
        $orders = DB::select("SELECT SUM(payment_sum) as payment, payment_date
                                FROM sale_details
                                WHERE payment_date >= '".$from."' 
                                AND payment_date <= '".$to."' 
                                GROUP BY payment_date
                            ");
        return $orders;
    }





    public function TranSummaryFilter(Request $request){

        $from = $request->input('datefrom');
        $to   = $request->input('dateto');

        $arr = [];

        if($from == '' && $to == ''){

            $bookedqty  = SalesSummary::count();
            $refundqty  = SalesSummary::where('cancel', '=', '1')->count();
            $paymentqty = SalesDetails::where('payment_status', '=', '1')->count();

            $disc       = SalesDetails::sum('payment_disc_sum');
            $vat        = SalesDetails::sum('payment_vat_sum');
            $payment    = SalesDetails::sum('payment_total_sum');


        }else{


            $bookedqty  = SalesSummary::whereDate('created_date', '>=',  $from)
                                        ->whereDate('created_date', '<=',  $to)
                                        ->count();

            $refundqty  = SalesSummary::where('cancel', '=', '1')
                                        ->whereDate('created_date', '>=',  $from)
                                        ->whereDate('created_date', '<=',  $to)
                                        ->count();

            $paymentqty = SalesDetails::where('payment_status', '=', '1')
                                        ->whereDate('payment_date', '>=',  $from)
                                        ->whereDate('payment_date', '<=',  $to)
                                        ->count();

            $disc       = SalesDetails::whereDate('payment_date', '>=',  $from)
                                        ->whereDate('payment_date', '<=',  $to)
                                        ->sum('payment_disc_sum');

            $vat        = SalesDetails::whereDate('payment_date', '>=',  $from)
                                        ->whereDate('payment_date', '<=',  $to)
                                        ->sum('payment_vat_sum');

            $payment    = SalesDetails::whereDate('payment_date', '>=',  $from)
                                        ->whereDate('payment_date', '<=',  $to)
                                        ->sum('payment_total_sum');

        }

        $total  = $disc+$payment;

        $arr = [

            'bookqty'   => $bookedqty,
            'refundqty' => $refundqty,
            'paidqty'   => $paymentqty,
            'total'     => number_format((float)$payment, 2, '.', ''),
            'disc'      => $disc,
            'vat'       => $vat,
            'grand'     => number_format((float)$total, 2, '.', '')

        ];

       return $arr;

    }



    public function TopCustomers(Request $request){

        $LastMonth = Carbon::now()->startOfMonth()->subMonth(3);
        $CurrentMonth = Carbon::now()->endOfMonth(); 

        $appoint = MaidsSchedule::select('maids_schedule.*')
                ->whereDate ('created_at','>',$LastMonth)
                ->whereDate ('created_at','<',$CurrentMonth)
                ->with(['getCustomer'])->groupBy('customer_id')->get();


            foreach($appoint as  $list){

                $payment = SalesDetails::PaymentSum($list->schedule_code);
                $count = MaidsSchedule::CountCus($list->customer_id);

                if($payment != '0'){

                    $arr[] = [

                        'customer_id' => $list->getCustomer->id,
                        'fullname' => $list->getCustomer->fullname,
                        'photo' => $list->getCustomer->photo,
                        'address' => $list->getCustomer->address,
                        'booking' => $count,
                        'profit' => $payment,
                        'registration' => $list->getCustomer->created_at

                    ]; 

                }


            }

            if(!empty($arr)){
                usort($arr, function ($a, $b) {
                    return $a['profit'] < $b['profit'];
                });
                $data['data'] = $arr;
            }else{
                $data['data'] = '';
            }


        return view('parts-dashboard.admin-top-customers', $data);

    }



    public function TopMaids(Request $request){

        $LastMonth = Carbon::now()->startOfMonth()->subMonth(3);
        $CurrentMonth = Carbon::now()->endOfMonth(); 

        $appoint = MaidsSchedule::select('maids_schedule.*')
                ->whereDate ('created_at','>',$LastMonth)
                ->whereDate ('created_at','<',$CurrentMonth)
                ->with(['getMaid'])->groupBy('maids_id')->get();
       
        $data['data'] = '';
    
            foreach($appoint as  $list){

                $count = MaidsSchedule::CountMaids($list->maids_id);

                $arr[] = [

                    'id' => $list->getMaid->id,
                    'fullname' => $list->getMaid->fullname,
                    'photo' => $list->getMaid->photo,
                    'dob' => $list->getMaid->birthdate->format('j F Y'),
                    'gender' => $list->getMaid->gender,
                    'booking' => $count

                ];

            }


            if(!empty($arr)){
                usort($arr, function ($a, $b) {
                    return $a['booking'] < $b['booking'];
                });
                $data['data'] = $arr;
            }

        return view('parts-dashboard.admin-top-maids' ,$data);

    }



    public function ActiveCustomers(Request $request){

        $month = date("m", strtotime("-3 months"));
        $appoint = MaidsSchedule::select('maids_schedule.*')->whereMonth('created_at', '>', $month)
                    ->with(['getCustomer'])->groupBy('customer_id')->get();

            foreach($appoint as  $list){

                $payment = SalesDetails::PaymentSum($list->schedule_code);
                $count = MaidsSchedule::CountCus($list->customer_id);
                $affected = MaidsSchedule::where('customer_id', '=', $list->customer_id)->pluck('notes')->toArray();
                $newarray[] = implode(",",array_unique($affected));

                $arr[] = [
                    'id'  => $list->getCustomer->id,
                    'fullname' => $list->getCustomer->fullname,
                    'photo' => $list->getCustomer->photo,
                    'booking' => $count,
                    'profit' => $payment,
                    'services'  => implode(",",array_unique($affected)),
                    'registration' => $list->getCustomer->created_at,
                    'last_email' => $list->getCustomer->last_email,
                    'last_email_type' =>$list->getCustomer->last_email_type

                ];

            }

        $data['data'] = $arr;
        return view('parts-dashboard.admin-active-customers', $data);

    }


  
    public function InactiveCustomers(Request $request){

        $prev = date("m", strtotime("-3 months"));
        $current = date("m");

        $LastMonth = Carbon::now()->startOfMonth()->subMonth(3);
        $CurrentMonth = Carbon::now()->endOfMonth(); 

        $active = MaidsSchedule::groupBy('customer_id')
                ->whereDate ('created_at','>',$LastMonth)
                ->whereDate ('created_at','<',$CurrentMonth)
                ->pluck('customer_id');
        
        $customers = Customer::whereNotIn('id', $active)->get();

        $newarray=[];
        foreach($customers as $key => $item)
        {
            $affected = MaidsSchedule::where('customer_id', '=', $item)->pluck('notes')->toArray();
            $newarray[] = implode(",",array_unique($affected));
        }

        $data['data'] = $customers;
        $data['notes'] = $newarray;

        return view('parts-dashboard.admin-inactive-customers', $data);

    }



    public function CancelledAppoint(Request $request){

        $maids = MaidsSchedule::select('maids_schedule.*')
                ->whereHas('getSaleSummary', function ($query) {
                            $query->where('cancel','=','1');
                })->with(['getMaid','getCustomer','getDriver','getSaleSummary','getSalesDetails']);

            if( $request->input('datefrom') != '' && $request->input('dateto') != ''){
                $maids->whereDate('created_at', '>=',  $request->input('datefrom'))
                    ->whereDate('created_at', '<=',  $request->input('dateto'));
            }

            return Datatables::of( $maids )->addColumn('transaction', function($event){

                $details = SalesDetails::where('service_number', $event->schedule_code)->first();
                if( $details->transaction_type == '1'){
                    return '<span class="label label-default">Reccuring</span>';
                }else{
                    return '<span class="label label-default">One time</span>';
                }

            })->editColumn('get_sale_summary.created_date', function($event){
                return $event->getSaleSummary['created_date']->format('l j F Y');
            })->editColumn('completed', function($event){

                if($event->completed == 1 ){
                    return '<span class="label label-success">Done</span>';
                }elseif($event->completed == 3){
                    return '<span class="label label-warning">On queue</span>';
                }else{
                    return '<span class="label label-danger">Cancelled</span>';
                }

            })->editColumn('schedule_start', function($event){
                return $event->schedule_start->format('h:i A').' To '.$event->schedule_end->format('h:i A');
            })->editColumn('schedule_end', function($event){
                return $event->schedule_end->format('h:i A');
            })->editColumn('schedule_date', function($event){
                return $event->schedule_start->format('l j F Y');
            })->rawColumns(['schedule_code','get_customer.fullname','get_maid.fullname','get_driver.fullname'
            ,'transaction','completed','schedule_date','schedule_start','schedule_end'
            ,'get_sale_summary.price'])->make( true );

    }

    public function emailCustomers(Request $request){
        // missyou = 1
        // account = 2
        // receipt = 3
        $email_type = $request->input('checkbox_val');

        if( $email_type == '1') {
            $customer = Customer::where('id',$request->input('customer_id'))->first();
            $last_email = Customer::where('id', $customer->id)->update(['last_email' => Carbon::now(),'last_email_type' => 1 ]);
            $data['customer'] = $customer;
            $subject = 'We Miss You! ';
            $type = 'missyou';
            $mail = Mail::to('gian@nrsinfoways.com')->send(new AdminMail($subject,$data, $type));

        }elseif( $email_type == '2'){
            $customer = Customer::where('id',$request->input('customer_id'))->first();
            $last_email = Customer::where('id', $customer->id)->update(['last_email' => Carbon::now(),'last_email_type' => 2 ]);
            $data['customer'] = $customer;
            $subject = 'Your account summary';
            $type = 'receipt';
            $mail = Mail::to($customer->email)->send(new AdminMail($subject,$data, $type));
        }else{
            $customer = Customer::where('id',$request->input('customer_id'))->first();
            $last_email = Customer::where('id', $customer->id)->update(['last_email' => Carbon::now(),'last_email_type' => 3 ]);
            $data['customer'] = $customer;
            $subject = 'Your account summary';
            $type = 'account';
            $mail = Mail::to($customer->email)->send(new AdminMail($subject,$data, $type));
        }
        
        

    }


}	