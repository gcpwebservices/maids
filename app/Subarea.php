<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subarea extends Model
{
    protected $fillable = [
        'area_id','sub_area'
    ];
    protected $table = 'sub_area';
    public $timestamps = false;

    public function getMaidsSchedule(){
        return $this->hasMany('App\MaidsSchedule','sub_area_id','id');
    }
}
