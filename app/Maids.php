<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maids extends Model
{
    protected $fillable = [
        'fullname','gender','birthdate','created_at','updated_at','mobile','shift_start','shift_end','photo','account'
    ];
    protected $table = 'maids_representative';
    public $timestamps = false;
  	protected $dates = ['birthdate','created_at','updated_at'];

    public function getSchedules(){
    	return $this->hasMany('App\MaidsSchedule','id','maids_id');
    }
}
