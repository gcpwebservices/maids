<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriversSchedule extends Model
{
    protected $fillable = [
        'maids_id', 
        'customer_id', 
        'driver_id',
        'schedule_start',
        'schedule_end',
        'schedule_color',
        'completed',
        'customer_address_id',
        'schedule_code',
    ];

    protected $table = 'drivers_schedule';
    public $timestamps = false;
    protected $dates = ['schedule_start','schedule_end'];

    public function getDriver(){
    	return $this->belongsTo('App\Driver','maids_id','id');
    }

    public function getCustomer(){
    	return $this->belongsTo('App\Customer','customer_id','id');
    }

    public function getMaid(){
    	return $this->belongsTo('App\Maids','driver_id','id');
    }

    public function getColor(){
        return $this->belongsTo('App\Color','schedule_color','colorvalue');
    }

    public function getCusAddress(){
        return $this->belongsTo('App\Schedule_address','customer_address_id','id');
    }

}
