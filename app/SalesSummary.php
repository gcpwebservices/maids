<?php



namespace App;



use Illuminate\Database\Eloquent\Model;



class SalesSummary extends Model

{

    protected $fillable = [

        'schedule_id','service_number','payment_disc','payment_refund','payment_vat','payment_total','created_date','created_dt','payment_date','payment_sub','price','item','cancel','cancel_dt','cancel_note','is_paid','payment_note'

    ];

    protected $table = 'sale_summary';

    public $timestamps = true;

  	protected $dates = ['created_date','created_dt','payment_date','cancel_dt'];



    public function getServiceCode(){

        return $this->hasMany('App\MaidsSchedule','service_number','schedule_code');

    }

}

