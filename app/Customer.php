<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'fullname','address','gender','birthdate','created_at','updated_at','photo','mobile','email','area','id','subarea',
        'property','room','toilet','garden','parking','account','last_email','last_email_type'
    ];
  	protected $dates = ['birthdate','created_at','updated_at'];
    protected $table = 'customer';
    public $timestamps = true;


    public function getCusArea(){
       	return $this->belongsTo('App\Area','area','id');
    }

 	  public function getCusSubArea(){
       	return $this->belongsTo('App\Subarea','subarea','id');
    }

    public function scopeNotInCus($query , $id){
        return $query->whereNotIn('id', $id)->value('id');
    }
}
