<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminMail extends Mailable
{
    use Queueable, SerializesModels;
    public $arrEmail = [];
    // public $message;
    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct($subject,$arrEmail, $type)
    {
        $this->arrEmail = $arrEmail;
        $this->subject = $subject;
        $this->type = $type;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   

        if($this->type == 'receipt'){
            return $this->subject($this->subject)->view('mail.receipt', $this->arrEmail); 
        }elseif($this->type == 'missyou'){
            return $this->subject($this->subject)->view('mail.missyou', $this->arrEmail); 
        }else{
            return $this->subject($this->subject)->view('mail.account', $this->arrEmail); 
        }

        // $data['name'] = $this->fullname;
        
        // if($this->type == 'missyou'){
        //     $data['subject'] = 'We missed you!';
        //     return $this->view('mail.missyou', $data);
        // }elseif($this->type == 'account'){
        //     $data['subject'] = 'Account Summary';
        //     return $this->view('mail.account', $data);
        // }else{
        //     $data['subject'] = 'Check out our new offer!';
        //     return $this->view('mail.promotion', $data);
        // }

    }
}
