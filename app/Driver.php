<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $fillable = [
        'fullname','gender','birthdate','photo','created_at','updated_at','account','mobile','shit_start','shift_end'
    ];
    protected $table = 'driver_representative';
    public $timestamps = false;
  	protected $dates = ['birthdate','created_at','updated_at'];
}
