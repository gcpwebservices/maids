<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $fillable = [
        'colorname','colorvalue'
    ];
    protected $table = 'colors';
    public $timestamps = false;

    public function getColorList(){
        return $this->hasMany('App\MaidsSchedule','colorvalue','schedule_color');
    }
}
