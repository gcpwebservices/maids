<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule_address extends Model
{
    protected $fillable = [
        'customer_id','customer_address'
    ];

}
