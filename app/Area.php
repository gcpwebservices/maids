<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $fillable = [
        'area_id','area'
    ];
    protected $table = 'area';
    public $timestamps = false;


    public function getAreaSubarea(){
    	return $this->belongsTo('App\Subarea','area_id','area_id');
    }

    public function getMaidsArea(){
        return $this->hasMany('App\MaidsSchedule','id','area_id');
    }

}
