<?php



namespace App;



use Illuminate\Database\Eloquent\Model;



class SalesDetails extends Model

{

    protected $fillable = [



        'summary_id',

        'service_number',

        'payment_disc_sum',

        'payment_ref_sum',

        'payment_vat_sum',

        'payment_sub_sum',

        'payment_total_sum',

        'payment_sum',

        'payment_status',

        'transaction_type',

        'payment_date',

    ];

    

    protected $table = 'sale_details';

    public $timestamps = true;

    protected $dates = ['payment_date','cancel_dt'];


    public function getSummary(){

       	return $this->belongsTo('App\SalesSummary','service_number','service_number');

    }

    public function scopegetUnpaid($query){
         return $query->where('payment_status','=','0')->get();
    }

     public function scopePaymentSum( $query, $service_number ){

        return $query->where('service_number', $service_number)->sum('payment_sum');

    }

    public function scopeGroupServiceCode($query, $service_number){

        return $query->where('service_number');

    }



}

