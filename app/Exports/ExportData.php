<?php

namespace App\Exports;

use App\MaidsSchedule;
use App\SalesSummary;
use App\SalesDetails;
use App\DriversSchedule;
use App\Maids;
use App\Driver;
use App\Color;
use App\Customer;
use App\Schedule_address;
use App\Area;
use App\Subarea;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

// use Maatwebsite\Excel\Concerns\FromCollection;

class ExportData implements FromView
{
	public $transactionID;
	public function __construct($id = ''){
		$this->transactionID = $id;
	}
    /**
    * @return \Illuminate\Support\Collection
    */

   	public function view(): View
    {	
        $trans = MaidsSchedule::where('id',$this->transactionID)->with(['getCustomer'])->first();
        $transactions = MaidsSchedule::where('schedule_code', $trans->schedule_code)
            ->with(['getCustomer','getSaleSummary','getMaid'])->get();

        $details = SalesDetails::where('service_number', $trans->schedule_code)->first();

        $data['maids_schedule'] = $trans;
        $data['maids_relation'] = $transactions;
        $data['sales_details'] = $details;

        return view('parts-dashboard.admin-accounting-pdf',$data);
    }
}
