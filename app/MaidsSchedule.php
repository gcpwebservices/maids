<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaidsSchedule extends Model
{
    protected $fillable = [
        'maids_id', 
        'customer_id', 
        'driver_id',
        'schedule_start',
        'schedule_end',
        'schedule_color',
        'completed',
        'customer_address_id',
        'schedule_code',
        'area_id',
        'sub_area_id',
        'created_at',
        'updated_at',
        'payment',
        'created_date',
        'notes',
        'schedule_date',
        'cancel_note'
    ];

    protected $table = 'maids_schedule';
    public $timestamps = false;
    protected $dates = ['schedule_start','schedule_end','created_at','updated_at','created_date','schedule_date'];

    public function getMaid(){
    	return $this->belongsTo('App\Maids','maids_id','id');
    }

    public function getSaleSummary(){
        return $this->belongsTo('App\SalesSummary','id','schedule_id');
    }

    public function getUnpaid(){
        return $this->belongsTo('App\SalesDetails','schedule_code','service_number')
            ->where('payment_status','=','0');
    }

    public function getSalesDetails(){
        return $this->belongsTo('App\SalesDetails','schedule_code','service_number');
    }

    public function getServiceCode(){
        return $this->belongsTo('App\SalesSummary','schedule_code','service_number');
    }

    public function getCustomer(){
    	return $this->belongsTo('App\Customer','customer_id','id');
    }

    public function getDriver(){
    	return $this->belongsTo('App\Driver','driver_id','id');
    }

    public function getColor(){
        return $this->belongsTo('App\Color','schedule_color','colorvalue');
    }

    public function getCusAddress(){
        return $this->belongsTo('App\Schedule_address','customer_address_id','id');
    }

    public function getArea(){
        return $this->belongsTo('App\Area','area_id','id');
    }

    public function getSubarea(){
        return $this->belongsTo('App\Subarea','sub_area_id','id');
    }

    public function scopeArea( $query, $areaid ){
        return $query->where('area_id', $areaid);
    }

    public function scopeSubarea( $query, $subareaid ){
        return $query->where('sub_area_id', $subareaid);
    }

    public function scopeCompleted( $query, $x ){
        return $query->where('completed', $x);
    }

    public function scopeCompletedDatetime( $query, $x ){
        return $query->where('schedule_end', '>=',$x);
    }

    public function scopeGroupCus($query){
        return $query->groupBy('customer_id');
    }

    public function scopeCountCus($query, $id){
        return $query->where('customer_id',$id)->count('customer_id');
    }

    public function scopeCountMaids($query, $id){
        return $query->where('maids_id',$id)->count('maids_id');
    }



  
}
