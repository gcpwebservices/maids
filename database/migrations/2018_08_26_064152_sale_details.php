<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SaleDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('summary_id')->unsigned();
            $table->string('service_number', 255);
            $table->decimal('payment_disc_sum', 15, 2);
            $table->decimal('payment_ref_sum', 15, 2);
            $table->decimal('payment_vat_sum', 15, 2);
            $table->decimal('payment_sub_sum', 15, 2);
            $table->decimal('payment_total_sum', 15, 2);
            $table->integer('payment_status')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
