<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DriversSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('drivers_schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('maids_id')->unsigned();
            $table->integer('customer_id')->unsigned();
            $table->integer('customer_address_id')->unsigned();
            $table->integer('driver_id')->unsigned();
            $table->dateTime('schedule_start');
            $table->dateTime('schedule_end');
            $table->string('schedule_color', 255);
            $table->string('schedule_code', 11);
            $table->integer('completed')->unsigned();
            $table->decimal('payment', 15, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('drivers_schedule');
    }
}
