require('./bootstrap');
var $ = require('jquery');
window.Vue = require('vue');
Vue.use(require('vue-resource'));
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.use(require('vue-moment'));

let paginate = Vue.component('data-component', require('./components/DataComponent.vue'));
let dashpaginate = Vue.component('dashpaginate', require('./components/DashPaginate.vue'));


var appOne = new Vue({
    el: '#appOne',
    component:{
    	paginate,
    	dashpaginate
    } 
});




