<div class="top-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="social">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 hidden-sm hidden-xs">
                <div class="call-info">
                    <p class="call-text"><i class="fa fa-envelope-open-o"></i><strong>info@nrsinfoways.com</strong></p>
                </div>
            </div>
            <div class="col-md-3 col-sm-12">
                <div class="call-info">
                    <p class="call-text"><i class="fa fa-phone"></i><strong>Call Now: +971 4 370 91 81</strong></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="header-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-2 col-sm-12 col-xs-12">
                <div class="logo"><a href="{{ url('/')}}" style="font-size: 25px;">Maids<!-- <img src="{{ asset('landingtemplate/assets/images/logo.png') }}" alt=""> --></a></div>
            </div>
            <div class="col-lg-9 col-md-10 col-sm-12 col-xs-12">
                <div class="navigation">
                    <div id="navigation">
                        <ul>
                            <li class="active"><a href="{{ url('/') }}" title="Home">Home</a></li>
                            <li><a href="{{ url('about') }}" title="About us">About us</a> </li>
                            <li class="has-sub"><a href="#" title="Service">Service</a>
                                <ul>
                                    <li><a href="{{ url('services') }}" title="Service-list">Service list</a></li>
                                    <li><a href="{{ url('service-single') }}" title="service-single">Service Single</a></li>
                                </ul>
                            </li>
                            <li><a href="pricing.html" title="Pricing">Pricing</a></li>
                            <li class="has-sub"><a href="blog-default.html" title="Blog ">Blog</a>
                                <ul>
                                    <li><a href="blog-default.html" title="Blog">Blog Default</a></li>
                                    <li><a href="blog-single.html" title="Blog Single ">Blog Single</a></li>
                                </ul>
                            </li>
                            <li class="has-sub"><a href="#" title="Features ">Features</a>
                                <ul>
                                    <li><a href="testimonials.html" title="Testimonials">Testimonials</a>
                                        <li><a href="faq.html" title="FAQ">FAQ</a></li>
                                        <li><a href="404-error.html" title="404-error">404-error</a> </li>
                                        <li><a href="styleguide.html" title="Styleguide">styleguide</a> </li>
                                </ul>
                                </li>
                                <li><a href="contact-us.html" title="Contact Us">Contact Us</a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- header-section close -->