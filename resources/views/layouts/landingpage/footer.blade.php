 <!-- cta-section start -->
<div class="cta-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h1>Have Any Questions Call us Now: +971 4 370 91 81</h1>
            </div>
        </div>
    </div>
</div>
<!-- cta-section close -->
 <!-- footer start -->
<div class="footer">
    <div class="container">
        <div class="row">
            <!-- footer-contactinfo-start -->
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="footer-widget">
                    <h3 class="footer-title">About Maids</h3>
                    <p>Maids is committed to providing high quality cleaning services</p>
                    <div class="">
                        <ul>
                            <li>
                                <div class="">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                                <div class="footer-address">
                                    <ul>
                                        <li> 103-12 Mohammad Al Makhawi Building</li>
                                        <li> Oud Metha – 319, Umm Hureir Road,</li>
                                        <li> Dubai, United Arab Emirates</li>
                                    </ul>
                                </div>
                            </li>
                            <li><div class=""><i class="fa fa-envelope-open"></i></div><div class="footer-address">info@nrsinfoways.com</div></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- footer-contactinfo-close -->
            <!-- footer-useful links-start -->
            <div class=" col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="footer-widget">
                    <h3 class="footer-title">Our Services</h3>
                    <ul>
                        <li><a href="#">Home Cleaning </a></li>
                        <li><a href="#">Office Cleaning </a></li>
                        <li><a href="#">Commercial Cleaning</a></li>
                    </ul>
                </div>
            </div>
            <!-- footer-useful links-close -->
            <!-- footer-useful links-start -->
            <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <div class="footer-widget">
                    <h3 class="footer-title">Quick Links</h3>
                    <ul>
                        <li><a href="#">About us </a></li>
                        <li><a href="#">Services </a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Testimonials </a></li>
                        <li> <a href="#">Pricing</a></li>
                    </ul>
                </div>
            </div>

<!-- tiny-footer-start -->
<div class="tiny-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">copyright@NRS Infoways. All right reserved</div>
        </div>
<!-- tiny-footer-start -->
    </div>
</div>
<!-- footer close -->