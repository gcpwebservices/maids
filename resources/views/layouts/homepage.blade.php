<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
	<link rel="icon" type="image/png" href="assets/img/favicon.png" />
	<title>Paper Bootstrap Wizard by Creative Tim</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <link href="{{asset('css/styles.css')}}" rel="stylesheet" />
	<!-- CSS Files -->
    <link href="{{asset('booktemplate/css/bootstrap.min.css')}}" rel="stylesheet" />
	<link href="{{asset('booktemplate/css/paper-bootstrap-wizard.css')}}" rel="stylesheet" />

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link href="{{asset('booktemplate/css/demo.css')}}" rel="stylesheet" />

	<!-- Fonts and Icons -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>

	<link href="{{asset('booktemplate/css/themify-icons.css')}}" rel="stylesheet">

	<link href="{{asset('calendar/fullcalendar.css')}}" rel="stylesheet">
	<link href="{{asset('calendar/fullcalendar.print.css')}}" rel="stylesheet">


	<link href="{{asset('datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

</head>

<body>
<div class="image-container ">
    <!--   Big container   -->

<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="navbar-container">
		<div class="navbar-header">
			<button class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button">
				<span class="sr-only">Toggle navigation</span> 
				<span class="icon-bar"></span> 
				<span class="icon-bar"></span> 
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="{{ url('') }}" title="Cleaning Services | Domestic Cleaner">
				Maids
				<!-- <img class="navbar-logo" src="/static/img/logo_small.png?type=new"> -->
			</a>
		</div>
		<div class="collapse navbar-collapse navbar-ex1-collapse" id="nav-collapse">

		</div>
	</div>

</nav>
<!-- echo a section -->
@yield('booking')


	    <div class="footer">
	        <div class="container text-center">
	             Made with <i class="fa fa-heart heart"></i> by <a href="https://www.nrsinfoways.com/">NRS Infoways</a>.  
	        </div>
	    </div>
	
</div> <!--  big container -->

</body>


	<!--   Core JS Files   -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js" type="text/javascript"></script>
	<script src="{{asset('booktemplate/js/jquery-2.2.4.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('booktemplate/js/bootstrap.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('booktemplate/js/jquery.bootstrap.wizard.js')}}" type="text/javascript"></script>

	<!--  Plugin for the Wizard -->
	<script src="{{asset('booktemplate/js/paper-bootstrap-wizard.js')}}" type="text/javascript"></script>

	<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
	<script src="{{asset('booktemplate/js/jquery.validate.min.js')}}" type="text/javascript"></script>

	<!--   calendar   -->
	<script src="{{asset('calendar/fullcalendar.js')}}"></script>

	<!--   datetimepicker   -->
	<script src="{{asset('datetimepicker/js/bootstrap-material-datetimepicker.js')}}" type="text/javascript"></script>
	<!--   timepicker   -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

	<!--   creditcard   -->
	<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/3.0.0/jquery.payment.js" type="text/javascript"></script>
	<script src="{{asset('js/creditcard.js')}}" type="text/javascript"></script>

<script type="text/javascript">

$(document).ready(function(){


	var storedFunc = (function() {

	  	var task = 0;
      	var bnt = 0;
      	var add = 3;
      	var newtotal = 0;

    	var time = '';
      	var date = '';

      	var bed = 0;
  		var bat = 0;
		var address = '';
  		var city = '';
  		var freq = '';

	  	return {

		    callFunc: function() {

		    	var wototal = bnt+task;
		    	var total = bnt+task+add;
		    	newtotal = parseFloat(total, 10);
				$('#value').html(newtotal);
			 	$('#value').data('id', newtotal);
				$('#hours').append($("<option selected='selected' ></option>").attr("value",newtotal).text(newtotal));
				$('#pnewhours').html(newtotal);
				$('#time').data('id',time);
				$('#date').data('id',date);

				$('#showtime').html(time);
				$('#showdate').html(date);

				$('#showbed').html(bed);
				$('#showbat').html(bat);
				$('#showcity').html(time);
		    },

		    valueTask: function(newVar) {
		    	task = newVar;
		    },

		    valueBnt: function(newVar) {
				bnt = newVar;
		    },

		     valueTime: function(newVar) {
				time = newVar;
		    },

	       	valueDate: function(newVar) {
				date = newVar;
		    },

     		valueBed: function(newVar) {
				bed = newVar;
		    },

     		valueBat: function(newVar) {
				bat = newVar;
		    },

		};
	})();



	//for stepper
	// $('#locations, #date, #review, #confirm').click(function(){return false;});
	// $( "#nextdate, #nextlocation" ).prop( "disabled", true );

	// $('input[name=emailaddress]').change(function() {
	// 	var email = $('input[name=emailaddress]').val();
	// 	if(email !=''){
	// 		$( "#nextlocation" ).prop( "disabled", false );
	// 		$('#locations').click(function(){return true;});
	// 	}
	// });

	$('input[name=timepicker]').change(function() {
		var time = $('input[name=timepicker]').val();
			if(time !=''){
			$( "#nextdate" ).prop( "disabled", false );
			$('#date').click(function(){return true;});
		}
	});

	//for working hours 
	$('#hours-warning-message').hide();
		$("#hours").change(function(){
	     	var hours = this.value;
	    if(hours=="3.0" || hours=='3.5'){
	     	$("#hours-warning-message").show();
	    }else{
	   	 	$("#hours-warning-message").hide()
	    }
  	}); 	

	//for bedrooms and bathrooms
 	$("#bedrooms,#bathrooms ").on('change',function() {
		var taskandmanual  = $('#value').data("id");
	 	var totalbed = 0;
 	 	var totaltoilet = 0;

	  $("#bedrooms").each(function() {
	      totalbed += parseFloat((this.value), 10)||0;
	      storedFunc.valueBed(totalbed);
	  });
	    $("#bathrooms").each(function() {
	      totaltoilet += parseFloat((this.value), 10)||0;
	      storedFunc.valueBat(totaltoilet);
	  });

  		var totalbnt = totalbed+totaltoilet;
  		storedFunc.valueBnt(totalbnt);
  		storedFunc.callFunc();

	});

	//for task
	$('.choice').on('click',function(){
		var sum = 0;
		  	$('input[name=task]:checked').each(function(e){
		  		// alert($(this).data('id'));
	  		    sum += parseFloat($(this).data('id'), 10);
			  	$('#tasktoclean').append("<p>"+ $(this).val() +"</p>");
		 	});

  		//for task show
		var seen = {};
		$('#tasktoclean > p').each(function() {
		    var txt = $(this).text();
		    if (seen[txt])
		        $(this).remove();
		    else
		        seen[txt] = true;
		});


		var forfreq = 0;
	  	$('input[name=frequency]:checked').each(function(){
  		    forfreq = $(this).val();
		  	$('#showfreq').html(forfreq);
	 	});

		storedFunc.valueTask(sum);
  		storedFunc.callFunc();

	});


	// for manual hours
 	$("#hours ").change(function() {
		var totalmanual = 0;
  			totalmanual += parseFloat($(this).val(), 10)||0;
  			$("#value").html(totalmanual);
  			$('#pnewhours').html(totalmanual);
  	});

	//for calendar
	$('#calendardate').click(function(){
		$('#myModal').modal('show'); 
	});

	$('#calendar').fullCalendar({
	   dayClick: function (date, allDay, jsEvent, view) {
        $(".fc-state-highlight").removeClass("fc-state-highlight");
        $(jsEvent.target).addClass("fc-state-highlight");
	        var dateObject = (date.getDate()+"-"+date.getMonth()+"-"+date.getFullYear());
	         // $(this).css('background-color', 'red');
	  		storedFunc.valueDate(dateObject);
	  		storedFunc.callFunc();

	   }
	});

	//fortimepicker
    $('#timepicker').timepicker({
        change: function(time) {
			var getValue = $(this).val();
			storedFunc.valueTime(getValue);
     		storedFunc.callFunc();
        },

	    timeFormat: 'h:mm p',
	    interval: 60,
	    minTime: '24',
	    maxTime: '7:00pm',
	    defaultTime: '07',
	    startTime: new Date(),
	    dynamic: true,
	    dropdown: true,
    	scrollbar: false
    });

    //for mobile number input
 	$('input[name="mobilenumber"]').on("keypress keyup blur",function (event) {    
       	$(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
	});


    //for showdata
    $('#nextlocation, #previousdate').click(function(){
   		var showaddress = $('#street').val();
    		$('#showaddress').html(showaddress);
		var email = $('#emailaddress').val();
			$('#showemail').html(email);
    });
    $('#modalclose').click(function(){
    	var showdate = $('#date').data('id');
			document.getElementById('calendardate').value = showdate;
    });

    //for weeks
    // $('#nextlocation').click(function(){
	   //  if($('#type').hasClass('active')){

		  //   var count = 0;
		  //   $('.choice').click(function(){
				// $(this).each(function() {
				// 	if($(this).hasClass('active')){
				// 		if(count > 0){
				// 			$('.choice').removeClass('active');
				// 			count = 0;
				// 		}else{
				// 			count++;
				// 		}
				// 	}
				// });
		  //   });
	   //  }

    // });

 //    $('#previousdate').click(function(){
	// 	$('#type').removeClass('active');
	// });
	
	// $('.choice').click(function(){
	// 	alert($('#task1').val());
	// });


});





</script>

</html>
