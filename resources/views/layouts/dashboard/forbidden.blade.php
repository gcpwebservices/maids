
@extends('layouts.app')

@section('content-styles')
    <!-- Custom CSS -->

    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">

@endsection

@section('content')
<body class="fix-header fix-sidebar">


    <!-- Main wrapper  -->
    <div class="error-page" id="wrapper">
        <div class="error-box">
            <div class="error-body text-center">
          
                <h3 class="text-uppercase">Access Denied! </h3>
                <p class="text-muted m-t-30 m-b-30">Sorry, you don't have access to this page.</p>
                <a class="btn btn-info btn-rounded waves-effect waves-light m-b-40" href="{{ url('/') }}">Back to home</a> </div>
        
        </div>
    </div>
            



                                                                                                                                                                                                                                                                                                                                                                                                                
</body>

@endsection

@section('content-scripts')

    <script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>
    <!--stickey kit -->


<script type="text/javascript">



</script>
@endsection