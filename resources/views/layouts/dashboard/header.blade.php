<div class="preloader">

    <svg class="circular" viewBox="25 25 50 50">

		<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>

</div>

<!-- Main wrapper  -->

<div id="main-wrapper">

    <!-- header header  -->

    <div class="header">

        <nav class="navbar top-navbar navbar-expand-md navbar-light">

            <!-- Logo -->

                <div class="navbar-header">

                    <a class="navbar-brand" href="{{ url('') }}">

                        <!-- Logo icon -->

                        <b><img src="{{ asset('img/logoblue.png')}}" alt="homepage" class="dark-logo" width="220"></b>

                        <b><img src="{{ asset('img/logosmall.png')}}" alt="homepage" class="dark-logo2"  width="40"/></span></b>

                        <!--End Logo icon -->

                        <!-- Logo text -->

                   <!--      <span><img src="{{ asset('img/logosmall.png')}}" alt="homepage" class="dark-logo2" /></span> -->

                    </a>

                </div>

            <!-- End Logo -->

            <div class="navbar-collapse">

                <!-- toggle and nav items -->

                <ul class="navbar-nav mr-auto mt-md-0">

                    <!-- This is  -->

                    <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>

                    <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>

            

                </ul>



                <ul class="navbar-nav my-lg-0" style="font-size: 13px !important;" id="menus">



                    <?php $segment = request()->segment(2); ?>

                    <?php $third = request()->segment(3); ?>

                    <?php $lastsegment = request()->segment(4); ?>

                    <?php $routes = Route::current()->getName(); ?>



                    @if ($segment== '')

                        <li class="breadcrumb-item"><a href="{{ url('/') }}" class="text-primary">Dashboard</a></li>

                    @else

                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>

                    @endif

                    

                    @if ($segment== 'users')

                        <li class="breadcrumb-item">Maintenance</li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/users') }}" class="text-primary">Users</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/maids') }}">Maids</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/drivers') }}">Drivers</a></li>

        

                    @elseif ($segment== 'maids')

                        <li class="breadcrumb-item">Maintenance</li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/users') }}">Users</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/maids') }}" class="text-primary">Maids</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/drivers') }}">Drivers</a></li>

                     @elseif ($segment== 'drivers')

                        <li class="breadcrumb-item">Maintenance</li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/users') }}">Users</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/maids') }}">Maids</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/drivers') }}" class="text-primary">Drivers</a></li>

    

                    @endif





                    @if ($segment== 'customers')

                        <li class="breadcrumb-item"><a href="{{ url('/admin/customers') }}" class="text-primary">Customers</a></li>

                    @endif



                    @if ($third == 'sales')

                        <li class="breadcrumb-item">Reports</li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/sales') }}" class="text-primary">Sales</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/top-customers') }}">Top Customers</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/top-maids') }}" >Top Maids</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/active-customers') }}" >Active Customers</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/inactive-customers') }}" >Inactive Customers</a></li>

                        <li class="breadcrumb-item">

                            <a href="{{ url('/admin/reports/cancelled-appointments') }}">Cancelled Appointments</a>

                        </li>



                    @elseif ($third== 'top-customers')

                        <li class="breadcrumb-item">Reports</li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/sales') }}" >Sales</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/top-customers') }}" class="text-primary">Top Customers</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/top-maids') }}" >Top Maids</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/active-customers') }}" >Active Customers</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/inactive-customers') }}" >Inactive Customers</a></li>

                        <li class="breadcrumb-item">

                            <a href="{{ url('/admin/reports/cancelled-appointments') }}">Cancelled Appointments</a>

                        </li>



                    @elseif ($third== 'top-maids')

                        <li class="breadcrumb-item">Reports</li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/sales') }}">Sales</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/top-customers') }}">Top Customers</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/top-maids') }}" class="text-primary">Top Maids</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/active-customers') }}" >Active Customers</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/inactive-customers') }}" >Inactive Customers</a></li>

                        <li class="breadcrumb-item">

                            <a href="{{ url('/admin/reports/cancelled-appointments') }}">Cancelled Appointments</a>

                        </li>

                    @elseif ($third== 'active-customers')

                        <li class="breadcrumb-item">Reports</li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/sales') }}">Sales</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/top-customers') }}">Top Customers</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/top-maids') }}">Top Maids</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/active-customers') }}" class="text-primary">Active Customers</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/inactive-customers') }}" >Inactive Customers</a></li>

                        <li class="breadcrumb-item">

                            <a href="{{ url('/admin/reports/cancelled-appointments') }}">Cancelled Appointments</a>

                        </li>

                    @elseif ($third== 'inactive-customers')

                        <li class="breadcrumb-item">Reports</li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/sales') }}">Sales</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/top-customers') }}">Top Customers</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/top-maids') }}">Top Maids</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/active-customers') }}" >Active Customers</a></li>

                        <li class="breadcrumb-item">

                            <a href="{{ url('/admin/reports/inactive-customers') }}" class="text-primary">Inactive Customers</a>

                        </li>

                        <li class="breadcrumb-item">

                            <a href="{{ url('/admin/reports/cancelled-appointments') }}">Cancelled Appointments</a>

                        </li>

                   @elseif ($third== 'cancelled-appointments')

                        <li class="breadcrumb-item">Reports</li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/sales') }}">Sales</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/top-customers') }}">Top Customers</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/top-maids') }}">Top Maids</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/active-customers') }}" >Active Customers</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/reports/inactive-customers') }}">Inactive Customers</a></li>

                        <li class="breadcrumb-item">

                            <a href="{{ url('/admin/reports/cancelled-appointments') }}" class="text-primary">Cancelled Appointments</a>

                        </li>

                    @endif





                    @if ( $segment== 'appointments'  )

                        <li class="breadcrumb-item">Scheduler</li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/maids-schedule') }}">Calendar</a></li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/appointments') }}" class="text-primary">New Appointment</a></li>

                        <li class="breadcrumb-item active">

                            <a href="{{ url('admin/cancel-appointment') }}">Cancel Appointment</a>

                        </li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/maids-schedule-table') }}">Schedule Table</a></li>            

                    @elseif ( $segment== 'maids-schedule' )

                        <li class="breadcrumb-item">Scheduler</li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/maids-schedule') }}" class="text-primary">Calendar</a></li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/appointments') }}" >New Appointment</a></li>

                        <li class="breadcrumb-item active">

                            <a href="{{ url('admin/cancel-appointment') }}">Cancel Appointment</a>

                        </li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/maids-schedule-table') }}">Schedule Table</a></li>

                    @elseif ( $segment== 'maids-schedule-table' )

                        <li class="breadcrumb-item">Scheduler</li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/maids-schedule') }}">Calendar</a></li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/appointments') }}">New Appointment</a></li>

                        <li class="breadcrumb-item active">

                            <a href="{{ url('admin/cancel-appointment') }}">Cancel Appointment</a>

                        </li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/maids-schedule-table') }}" class="text-primary">Schedule Table</a></li>

                    @elseif ( $segment== 'cancel-appointment' )

                        <li class="breadcrumb-item">Scheduler</li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/maids-schedule') }}">Calendar</a></li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/appointments') }}">New Appointment</a></li>

                        <li class="breadcrumb-item active">

                            <a href="{{ url('admin/cancel-appointment') }}" class="text-primary">Cancel Appointment</a>

                        </li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/maids-schedule-table') }}" >Schedule Table</a></li>

                    @endif





                    @if ( $segment == 'accounting' )

                        <li class="breadcrumb-item">Accounting</li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/accounting') }}" class="text-primary">All Transactions </a></li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/accounting-paid') }}">Paid Transactions </a></li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/accounting-unpaid') }}">Unpaid Transactions </a></li>

                    @elseif ( $segment== 'accounting-paid' )

                        <li class="breadcrumb-item">Accounting</li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/accounting') }}">All Transactions </a></li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/accounting-paid') }}"class="text-primary">Paid Transactions </a></li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/accounting-unpaid') }}">Unpaid Transactions </a></li>

                    @elseif ( $segment== 'accounting-unpaid' )

                        <li class="breadcrumb-item">Accounting</li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/accounting') }}">All Transactions </a></li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/accounting-paid') }}">Paid Transactions </a></li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/accounting-unpaid') }}"class="text-primary">Unpaid Transactions </a></li>

                    @endif



                </ul>

                <!-- User profile and search -->

                <ul class="navbar-nav my-lg-0">



                    <li class="nav-item dropdown">

                        <a class="nav-link dropdown-toggle text-success  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                            {{ Auth::user()->name }}

                        </a>

                        <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                            <ul class="dropdown-user">
                                @if( Auth::user()->access == 99 )
                                <li>
                                    <a href="{{  url('admin/changePassword') }}"><i class="fa fa-key"></i> Change Password</a>
                                </li>
                                @endif

                                <li>
                                    <a href="{{  url('admin/logout') }}"><i class="fa fa-power-off"></i> Logout</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

    <!-- End header header -->

    <!-- Left Sidebar  -->

    <div class="left-sidebar">

        <!-- Sidebar scroll-->

        <div class="scroll-sidebar">

            <!-- Sidebar navigation-->

            <nav class="sidebar-nav">

                <ul id="sidebarnav">

                    <li class="nav-devider"></li>

                    @if($segment == '')


                    <li class="active"> 

                        <a class="" href="{{url('/')}}" aria-expanded="true">

                            <i class="fa fa-tachometer"></i>

                            <span class="hide-menu">Dashboard </span>

                        </a>

                    </li>

                    @else
                    <li class=""> 

                        <a class="" href="{{url('/')}}" aria-expanded="true">

                            <i class="fa fa-tachometer"></i>

                            <span class="hide-menu">Dashboard </span>

                        </a>

                    </li>

                    @endif

                    @if( $segment== 'maids-schedule' || $segment== 'appointments' || $segment== 'maids-schedule-table' 

                    || $segment =='cancel-appointment')

                    <li class="active"> 



                        <a class="has-arrow" href="#"  aria-expanded="true"><i class="fa fa-calendar"></i>

                            <span class="hide-menu">Scheduler </span>

                        </a>



                        <ul aria-expanded="true" class="collapse">



                            @if( $segment== 'maids-schedule' )

                                <li><a href="{{url('admin/maids-schedule')}}" class="text-primary"> Calendar</a></li>

                                <li><a href="{{url('admin/appointments')}}"> New Appointment </a></li>

                                <li><a href="{{url('admin/cancel-appointment')}}"> Cancel Appointment </a></li>

                                <li><a href="{{url('admin/maids-schedule-table')}}"> Schedule Table </a></li>

                            @elseif($segment== 'appointments')

                                <li><a href="{{url('admin/maids-schedule')}}"> Calendar </a></li>

                                <li><a href="{{url('admin/appointments')}}" class="text-primary"> New Appointment </a></li>

                                <li><a href="{{url('admin/cancel-appointment')}}"> Cancel Appointment </a></li>

                                <li><a href="{{url('admin/maids-schedule-table')}}"> Schedule Table </a></li>

                            @elseif($segment== 'maids-schedule-table')

                                <li><a href="{{url('admin/maids-schedule')}}"> Calendar </a></li>

                                <li><a href="{{url('admin/appointments')}}"> New Appointment </a></li>

                                <li><a href="{{url('admin/cancel-appointment')}}"> Cancel Appointment </a></li>

                                <li><a href="{{url('admin/maids-schedule-table')}}" class="text-primary"> Schedule Table </a></li>

                            @elseif($segment== 'cancel-appointment')

                                <li><a href="{{url('admin/maids-schedule')}}"> Calendar </a></li>

                                <li><a href="{{url('admin/appointments')}}"> New Appointment </a></li>

                                <li><a href="{{url('admin/cancel-appointment')}}" class="text-primary"> Cancel Appointment </a></li>

                                <li><a href="{{url('admin/maids-schedule-table')}}"> Schedule Table </a></li>

                            @endif

                          

                        </ul>

                   

                    </li>

                    @else

                    <li class=""> 


                        <a class="has-arrow" href="#"  aria-expanded="false"><i class="fa fa-calendar"></i>

                            <span class="hide-menu">Scheduler </span>

                        </a>

                        <ul aria-expanded="false" class="collapse">

                            <li><a href="{{url('admin/maids-schedule')}}"> Calendar </a></li>

                            <li><a href="{{url('admin/appointments')}}"> New Appointment </a></li>

                            <li><a href="{{url('admin/cancel-appointment')}}"> Cancel Appointment </a></li>

                            <li><a href="{{url('admin/maids-schedule-table')}}"> Schedule Table </a></li>

                        </ul>

                   

                    </li>

                    @endif







                        @if ($segment== 'users' || $segment== 'maids' || $segment=='drivers' )

                        <li class="active"> 

                            <a class="has-arrow" href="#" aria-expanded="true"><i class="fa fa-wrench"></i>

                                <span class="hide-menu">Maintenance </span>

                            </a>

                            <ul aria-expanded="true" class="collapse">

                                @if($segment== 'users')

                                    <li><a href="{{url('admin/users')}}"  class="text-primary"> Users </a></li>

                                    <li><a href="{{url('admin/maids')}}"> Maids </a></li>

                                    <li><a href="{{url('admin/drivers')}}"> Drivers </a></li>

                                @elseif($segment== 'maids')

                                    <li><a href="{{url('admin/users')}}"> Users </a></li>

                                    <li><a href="{{url('admin/maids')}}"  class="text-primary"> Maids </a></li>

                                    <li><a href="{{url('admin/drivers')}}"> Drivers </a></li>

                                @elseif($segment== 'drivers')

                                    <li><a href="{{url('admin/users')}}"> Users </a></li>

                                    <li><a href="{{url('admin/maids')}}"> Maids </a></li>

                                    <li><a href="{{url('admin/drivers')}}"  class="text-primary"> Drivers </a></li>

                                @endif

                            </ul>

                        </li>

                        @else

                        <li class=""> 

                            <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-wrench"></i>

                                <span class="hide-menu">Maintenance </span>

                            </a>

                            <ul aria-expanded="false" class="collapse">

                                <li><a href="{{url('admin/users')}}"> Users </a></li>

                                <li><a href="{{url('admin/maids')}}"> Maids </a></li>

                                <li><a href="{{url('admin/drivers')}}"> Drivers </a></li>

                            </ul>

                        </li>

                        @endif

        



                    @if($segment == 'customers')

                    <li class="active"> 

                        <a class="" href="{{url('admin/customers')}}" aria-expanded="true">

                            <i class="fa fa-user"></i>

                            <span class="hide-menu">Customers </span>

                        </a>

                           

                    </li>

                    @else

                    <li class=""> 

                        <a class="" href="{{url('admin/customers')}}" aria-expanded="true">

                            <i class="fa fa-user"></i>

                            <span class="hide-menu">Customers </span>

                        </a>

                           

                    </li>

                    @endif



                    @if($third == 'sales' || $third == 'top-customers' || $third == 'top-maids' 

                        || $third == 'active-customers' || $third == 'inactive-customers' || $third == 'cancelled-appointments' )

                    <li class="active"> 

                        <a class="has-arrow" href="#" aria-expanded="true">

                            <i class="fa fa-line-chart"></i>

                            <span class="hide-menu">Reports </span>

                        </a>

                        <ul aria-expanded="true" class="collapse">

                            @if($third == 'sales')

                                <li><a href="{{url('admin/reports/sales')}}" class="text-primary"> Sales </a></li>

                                <li><a href="{{url('admin/reports/top-customers')}}"> Top Customers </a></li>

                                <li><a href="{{url('admin/reports/top-maids')}}"> Top Maids </a></li>

                                <li><a href="{{url('admin/reports/active-customers')}}"> Active Customers </a></li>

                                <li><a href="{{url('admin/reports/inactive-customers')}}"> Inactive Customers </a></li>

                                <li>

                                    <a href="{{ url('/admin/reports/cancelled-appointments') }}">Cancelled Appointments</a>

                                </li>

                            @elseif($third == 'top-customers')

                                <li><a href="{{url('admin/reports/sales')}}"> Sales </a></li>

                                <li><a href="{{url('admin/reports/top-customers')}}" class="text-primary"> Top Customers </a></li>

                                <li><a href="{{url('admin/reports/top-maids')}}"> Top Maids </a></li>

                                <li><a href="{{url('admin/reports/active-customers')}}"> Active Customers </a></li>

                                <li><a href="{{url('admin/reports/inactive-customers')}}"> Inactive Customers </a></li>

                                <li>

                                    <a href="{{ url('/admin/reports/cancelled-appointments') }}">Cancelled Appointments</a>

                                </li>

                            @elseif($third == 'top-maids')

                                <li><a href="{{url('admin/reports/sales')}}"> Sales </a></li>

                                <li><a href="{{url('admin/reports/top-customers')}}"> Top Customers </a></li>

                                <li><a href="{{url('admin/reports/top-maids')}}" class="text-primary"> Top Maids </a></li>

                                <li><a href="{{url('admin/reports/active-customers')}}"> Active Customers </a></li>

                                <li><a href="{{url('admin/reports/inactive-customers')}}"> Inactive Customers </a></li>

                                <li>

                                    <a href="{{ url('/admin/reports/cancelled-appointments') }}">Cancelled Appointments</a>

                                </li>

                            @elseif($third == 'active-customers')

                                <li><a href="{{url('admin/reports/sales')}}"> Sales </a></li>

                                <li><a href="{{url('admin/reports/top-customers')}}"> Top Customers </a></li>

                                <li><a href="{{url('admin/reports/top-maids')}}"> Top Maids </a></li>

                                <li><a href="{{url('admin/reports/active-customers')}}" class="text-primary"> Active Customers </a></li>

                                <li><a href="{{url('admin/reports/inactive-customers')}}"> Inactive Customers </a></li>

                                <li>

                                    <a href="{{ url('/admin/reports/cancelled-appointments') }}">Cancelled Appointments</a>

                                </li>

                            @elseif($third == 'inactive-customers')

                                <li><a href="{{url('admin/reports/sales')}}"> Sales </a></li>

                                <li><a href="{{url('admin/reports/top-customers')}}"> Top Customers </a></li>

                                <li><a href="{{url('admin/reports/top-maids')}}"> Top Maids </a></li>

                                <li><a href="{{url('admin/reports/active-customers')}}"> Active Customers </a></li>

                                <li><a href="{{url('admin/reports/inactive-customers')}}" class="text-primary"> Inactive Customers </a></li>

                                <li>

                                    <a href="{{ url('/admin/reports/cancelled-appointments') }}">Cancelled Appointments</a>

                                </li>

                            @elseif($third == 'cancelled-appointments')

                                <li><a href="{{url('admin/reports/sales')}}"> Sales </a></li>

                                <li><a href="{{url('admin/reports/top-customers')}}"> Top Customers </a></li>

                                <li><a href="{{url('admin/reports/top-maids')}}"> Top Maids </a></li>

                                <li><a href="{{url('admin/reports/active-customers')}}"> Active Customers </a></li>

                                <li><a href="{{url('admin/reports/inactive-customers')}}"> Inactive Customers </a></li>

                                <li>

                                    <a href="{{ url('/admin/reports/cancelled-appointments') }}" class="text-primary">Cancelled Appointments</a>

                                </li>

                            @endif

                        </ul>

                    </li>

                    @else

                    <li class=""> 

                        <a class="has-arrow" href="#" aria-expanded="false">

                            <i class="fa fa-line-chart"></i>

                            <span class="hide-menu">Reports </span>

                        </a>

                        <ul aria-expanded="false" class="collapse">

                            <li><a href="{{url('admin/reports/sales')}}"> Sales </a></li>

                            <li><a href="{{url('admin/reports/top-customers')}}"> Top Customers </a></li>

                            <li><a href="{{url('admin/reports/top-maids')}}"> Top Maids </a></li>

                            <li><a href="{{url('admin/reports/active-customers')}}"> Active Customers </a></li>

                            <li><a href="{{url('admin/reports/inactive-customers')}}"> Inactive Customers </a></li>

                            <li>

                                <a href="{{ url('/admin/reports/cancelled-appointments') }}">Cancelled Appointments</a>

                            </li>

           

                        </ul>

                    </li>

                    @endif





                    @if($segment == 'accounting' || $segment == 'accounting-paid' ||  $segment == 'accounting-unpaid' )

                    <li class="active"> 

                        <a class="has-arrow" href="{{ url('admin/accounting') }}" aria-expanded="true">

                            <i class="fa fa-calculator"></i>

                            <span class="hide-menu">Accounting </span>

                        </a>

                        <ul aria-expanded="true" class="collapse">

                            @if($segment == 'accounting' )

                                <li><a href="{{url('admin/accounting')}}" class="text-primary"> All Transactions </a></li>

                                <li><a href="{{url('admin/accounting-paid')}}"> Paid Transactions </a></li>

                                <li><a href="{{url('admin/accounting-unpaid')}}"> Unpaid Transactions </a></li>

                            @elseif($segment == 'accounting-paid' )

                                <li><a href="{{url('admin/accounting')}}"> All Transactions </a></li>

                                <li><a href="{{url('admin/accounting-paid')}}" class="text-primary"> Paid Transactions </a></li>

                                <li><a href="{{url('admin/accounting-unpaid')}}"> Unpaid Transactions </a></li>

                           @elseif($segment == 'accounting-unpaid' )

                                <li><a href="{{url('admin/accounting')}}"> All Transactions </a></li>

                                <li><a href="{{url('admin/accounting-paid')}}"> Paid Transactions </a></li>

                                <li><a href="{{url('admin/accounting-unpaid')}}" class="text-primary"> Unpaid Transactions </a></li> 

                            @endif

                        </ul>

                    </li>

                    @else

                    <li class=""> 

                        <a class="has-arrow" href="{{ url('admin/accounting') }}" aria-expanded="false">

                            <i class="fa fa-calculator"></i>

                            <span class="hide-menu">Accounting </span>

                        </a>

                        <ul aria-expanded="false" class="collapse">

                            <li><a href="{{url('admin/accounting')}}"> All Transactions </a></li>

                            <li><a href="{{url('admin/accounting-paid')}}"> Paid Transactions </a></li>

                            <li><a href="{{url('admin/accounting-unpaid')}}"> Unpaid Transactions </a></li>

                        </ul>

                    </li>

        

                    @endif


                    @if( $segment == 'products')

                    <li class="active">
                        <a class="" href="{{ url('admin/products') }}" aria-expanded="false">

                            <i class="fa fa-shopping-cart"></i>

                            <span class="hide-menu">Products </span>

                        </a>
                    </li>
            
                    @else

                    <li class="">
                        <a class="" href="{{ url('admin/products') }}" aria-expanded="false">

                            <i class="fa fa-shopping-cart"></i>

                            <span class="hide-menu">Products </span>

                        </a>
                    </li>

                    @endif

                </ul>

            </nav>

            <!-- End Sidebar navigation -->

        </div>

        <!-- End Sidebar scroll-->

    </div>

    <!-- End Left Sidebar  -->

    <!-- Page wrapper  -->

    <div class="page-wrapper">

        <!-- Bread crumb -->

        <div class="row page-titles">

            <div class="col-md-5 align-self-center">

                <h3 class="text-primary">

                    

             <!--        <?php $lastsegment = request()->segment(4); ?>

                    <?php $routes = Route::current()->getName(); ?>

                    

                    @if ( $routes== 'dashboard'  )

                            {{'Dashboard'}}



                    @elseif ( $routes== 'maids-schedule' )

                            {{'Schedule Calendar'}}

                    @elseif ( $routes== 'drivers-schedule' )

                            {{'Drivers Schedule'}}

                    @elseif ( $routes== 'maids.index' )

                            {{'Maids'}}

                    @elseif ( $routes== 'maids.show' )

                            {{'Maid Details'}}

                    @elseif ( $routes== 'maids.create' )

                            {{'Add Maids'}}

                    @elseif ( $routes== 'maids.edit' )

                            {{'Edit Maids'}}

                    @elseif ( $routes== 'users.index' )

                            {{'Users'}}

                    @elseif ( $routes== 'users.create' )

                            {{'Add Users'}}

                    @elseif ( $routes== 'users.edit' )

                            {{'Edit Users'}}

                    @elseif ( $routes== 'customers.index' )

                            {{'Customers'}}

                    @elseif ( $routes== 'customers.create' )

                            {{'Add Customers'}}

                    @elseif ( $routes== 'customers.edit' )

                            {{'Edit Customers'}}

                    @elseif ( $routes== 'customers.show' )

                            {{'Customer Details'}}

                    @endif



                    <?php $segment = request()->segment(2); ?>

     

                    @if ( $segment== 'sales'  )

                            {{'Sales'}}

                    @elseif ( $segment== 'transactions-customers' )

                            {{'Customers Transactions'}}

                    @elseif ( $segment== 'transactions-maids' )

                            {{'Maids Transactions'}}

                    @elseif ( $segment== 'appointments' )

                            {{'New Appointment'}}

                    @elseif ( $segment== 'maids-schedule-table' )

                            {{'Schedule Table'}}

                    @endif



                    @if ( $lastsegment == 'customer-transaction' || $lastsegment == 'maid-transaction' )

                        {{ 'Transaction Details' }}

                    @endif 

 -->

                </h3> 

            </div>

            <div class="col-md-7 align-self-center">

                <ol class="breadcrumb">

               <!--      @if ($segment== '')

                        <li class="breadcrumb-item"><a href="{{ url('/') }}" class="text-primary">Home</a></li>

                    @else

                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>

                    @endif

                    

                    @if ($segment== 'users')

                        <li class="breadcrumb-item">Maintenance</li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/users') }}" class="text-primary">Users</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/maids') }}">Maids</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/customers') }}">Customers</a></li>

                    @elseif ($segment== 'maids')

                        <li class="breadcrumb-item">Maintenance</li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/users') }}">Users</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/maids') }}" class="text-primary">Maids</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/customers') }}">Customers</a></li>

                    @elseif ($segment== 'customers')

                        <li class="breadcrumb-item">Maintenance</li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/users') }}">Users</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/maids') }}">Maids</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/customers') }}" class="text-primary">Customers</a></li>

                    @endif



                    @if ($segment== 'sales')

                        <li class="breadcrumb-item">Reports</li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/sales') }}" class="text-primary">Sales</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/transactions-customers') }}">Customers Transaction</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/transactions-maids') }}">Maids Transaction</a></li>

                    @elseif ($segment== 'transactions-customers')

                        <li class="breadcrumb-item">Reports</li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/sales') }}" >Sales</a></li>

                        <li class="breadcrumb-item"><a href="{{url('/admin/transactions-customers')}}" class="text-primary">Customers Transaction</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/transactions-maids') }}">Maids Transaction</a></li>

                    @elseif ($segment== 'transactions-maids')

                        <li class="breadcrumb-item">Reports</li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/sales') }}">Sales</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/transactions-customers') }}">Customers Transaction</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/transactions-maids') }}" class="text-primary">Maids Transaction</a></li>

                    @endif





                    @if ( $segment== 'appointments'  )

                        <li class="breadcrumb-item">Scheduler</li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/appointments') }}" class="text-primary">New Appointment</a></li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/maids-schedule') }}">Calendar</a></li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/maids-schedule-table') }}">Schedule Table</a></li>

                    @elseif ( $segment== 'maids-schedule' )

                        <li class="breadcrumb-item">Scheduler</li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/appointments') }}">New Appointment</a></li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/maids-schedule') }}" class="text-primary">Calendar</a></li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/maids-schedule-table') }}">Schedule Table</a></li>

                    @elseif ( $segment== 'maids-schedule-table' )

                        <li class="breadcrumb-item">Scheduler</li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/appointments') }}">New Appointment</a></li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/maids-schedule') }}">Calendar</a></li>

                        <li class="breadcrumb-item active"><a href="{{ url('admin/maids-schedule-table') }}" class="text-primary">Schedule Table</a></li>

                    @endif





                    @if ( $lastsegment == 'customer-transaction'  )

                        <li class="breadcrumb-item">Reports</li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/sales') }}" >Sales</a></li>

                        <li class="breadcrumb-item"><a href="{{url('/admin/transactions-customers')}}" class="text-primary">Customers Transaction</a></li>



                    @elseif ( $lastsegment== 'maid-transaction' )

                        <li class="breadcrumb-item">Reports</li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/sales') }}">Sales</a></li>

                        <li class="breadcrumb-item"><a href="{{ url('/admin/transactions-maids') }}" class="text-primary">Maids Transaction</a></li>

                    @endif

           -->

                </ol>   

            </div>

        </div>

        <!-- End Bread crumb -->