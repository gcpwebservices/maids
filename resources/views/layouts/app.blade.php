<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('dashtemplate/images/maids.png')}}">

    <title>{{ config('app.name', 'Maids') }}</title>

    @yield('content-styles')


</head>

@yield('content')


@yield('content-scripts')

</html>
