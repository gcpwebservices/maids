@extends('layouts.app')

@section('content-styles')
    <link href="{{ asset('dashtemplate/css/invoice.css') }}" rel="stylesheet" />
@endsection

@section('content')

<body>
	<header class="clearfix">
		<div class="container">
				<figure>
				<img class="logo" src="{{ asset('img/logoblue.png') }}" width="300" alt="">
			</figure>

			<div class="company-contact">
				<div class="phone left">
				
					<a href="tel:602-519-0450">(602) 519-0450</a>
					<span class="helper"></span>
				</div>
				<div class="email right">
		
					<a href="mailto:maidssoftware@info.com">maidssoftware@info.com</a>
					<span class="helper"></span>
				</div>
			</div>
		</div>
	</header>

	<section>
		<div class="container">
			<div class="details clearfix">
				<div class="client left">
					<p>INVOICE TO:</p>
					<p class="name">{{  $maids_schedule->getCustomer['fullname'] }}</p>
					<p> {{  $maids_schedule->getCustomer['address'] }}, {{  $maids_schedule->getArea['area'] }}, 
					{{  $maids_schedule->getSubArea['sub_area'] }}</p>
					<p>{{  $maids_schedule->getCustomer['email'] }}</p>
				</div>
				<div class="data right">
					<div class="title">Invoice: {{$maids_schedule->schedule_code}}</div>
					<div class="date">
						Date of Invoice: {{ $sales_details['updated_at']->format('l j F Y') }}<br>
	                 	@if( $sales_details['payment_status'] == '1' )
	                  		Full payment received on: 
	                        {{ $sales_details['updated_at']->format('l j F Y') }}<br>
	                    @endif
					</div>
				</div>
			</div>

			<table border="0" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<th class="desc">Description</th>
						<th class="qty">Quantity</th>
						<th class="unit">Unit price</th>
						<th class="total">Total + VAT</th>
					</tr>
				</thead>
				<tbody>
		            @foreach($maids_relation as $data_list)
	             		@if( $data_list->getSaleSummary['payment_total'] != '0.00' )
						<tr>
							<td class="desc"><h3>Cleaning</h3>{{$data_list->notes}}</td>
							<td class="qty">1</td>
							<td class="unit"> {{ $data_list->getSaleSummary['price'] }}</td>
							<td class="total"> {{ $data_list->getSaleSummary['payment_sub'] }}</td>
						</tr>
						@endif
					@endforeach
				</tbody>
			</table>
			<div class="no-break">
				<table class="grand-total">
					<tbody>
						<tr>
							<td class="desc"></td>
							<td class="qty"></td>
							<td class="unit">SUBTOTAL:</td>
							
							<?php $sum_sub = 0;?>
							@foreach($maids_relation as $data_list)
			             		@if( $data_list->getSaleSummary['payment_total'] != '0.00' )
									<?php $sum_sub = $sum_sub + $data_list->getSaleSummary['price']; ?>
								@endif
							@endforeach
							<td class="total">AED {{ $sum_sub }}</td>
						</tr>
						<tr>
							<td class="desc"></td>
							<td class="qty"></td>
							<td class="unit">VAT 5%:</td>
							<td class="total">AED {{ $sales_details['payment_vat_sum'] }}</td>
						</tr>
						<tr>
							<td class="desc"></td>
							<td class="unit" colspan="2">GRAND TOTAL:</td>
							<td class="total">AED  {{ ($sales_details['payment_sum'])  }}</td>
						</tr>

						<tr>
							<td class="desc"></td>
							<td class="unit" colspan="2">BALANCE :</td>
							<td class="total">AED     {{ number_format($sales_details['payment_total_sum'] - $sales_details['payment_sum'], 2, '.', ',')  }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</section>

	<footer>
		<div class="container">
			<div class="thanks">Thank you!</div>
			<div class="notice">
				<div>NOTICE:</div>
				<div>A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
			</div>
			<div class="end">Invoice was created on a computer and is valid without the signature and seal.</div>
		</div>
	</footer>
</body>
@endsection