@extends('layouts.app')

@section('content-styles')

    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />

    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">

<style type="text/css">
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .title {
        float: left !important;
        padding-top: 20px;
    }
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
</style>
@endsection

@section('content')
<body>
    <div class="invoice-box">
       
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title text-center">
                                <img src="{{ asset('img/logoblue.png') }}" style="width:100%; max-width:300px;">
                            </td>
                            
                            <td>    
                                    <?php  $now = new DateTime();?>
                                    {{$now->format('l j F Y')}}<br>
                                    Invoice #: {{$maids_schedule->schedule_code}}<br>

                                    @if( $sales_details['payment_status'] == '1' )
                                        <small>Full payment received on: 
                                        {{ $sales_details['updated_at']->format('l j F Y') }}</small><br>
                                    @endif

                                    @if($sales_details['payment_status'] == '0')
                                        <span class="label label-danger">Unpaid</span>
                                    @elseif( $sales_details['payment_status'] == '3' )
                                        <span class="label label-warning">Partialy Paid</span>
                                    @elseif( $sales_details['payment_status'] == '1' )
                                        <span class="label label-success">Paid</span>
                                    @endif
                           
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                {{  $maids_schedule->getCustomer['address'] }}<br>
                                {{  $maids_schedule->getArea['area'] }}<br>
                                {{  $maids_schedule->getSubArea['sub_area'] }}<br>
                            </td>
                            
                            <td>
                               
                                {{  $maids_schedule->getCustomer['fullname'] }}<br>
                                {{  $maids_schedule->getCustomer['email'] }}<br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Item
                </td>
                

                <td>
                    Price
                </td>
            </tr>

            @foreach($maids_relation as $data_list)
                @if( $data_list->getSaleSummary['payment_total'] != '0.00' )
                <tr class="item">
                    <td>
                        {{$data_list->notes}}
                    </td>
                    <td>
                        {{ $data_list->getSaleSummary['payment_total'] }}
                    </td>
                </tr>
                @endif
            @endforeach

            <tr class="total">
                <td></td>
                <td>
                    {{$sales_details->payment_sum}}
                </td>
            </tr>

            </table>
            <br>
            <table class="table" style="width:300px !important;">
                <tbody>
                    <tr>
                        <td class="no-border">Subtotal</td>
                        <td class="text-right no-border">{{ $sales_details['payment_sub_sum'] }}</td>
                    </tr>
                    <tr>
                        <td>Discount</td>
                        <td class="text-right">{{ $sales_details['payment_disc_sum'] }}</td>
                    </tr>
                    <tr>
                        <td>VAT(5%)</td>
                        <td class="text-right">{{ $sales_details['payment_vat_sum'] }}</td>
                    </tr>
                    <tr>
                        <th class="font-bold text-15">Total</th>
                        <th class="font-bold text-right">
                            {{ ($sales_details['payment_total_sum'])  }}
                        </th>
                    </tr>
                     <tr>
                        <th class="font-bold text-15">Paid</th>
                        <th class="font-bold text-right">
                            -{{ ($sales_details['payment_sum'])  }}
                        </th>
                    </tr>
                    <tr>
                        <th class="font-bold text-15">Balance</th>
                        <th class="font-bold text-right text-danger">
                            {{ number_format($sales_details['payment_total_sum'] - $sales_details['payment_sum'], 2, '.', ',')  }}
                        </th>
                    </tr>
                </tbody>
            </table>
    </div>
</body>

@endsection