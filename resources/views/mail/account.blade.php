@extends('layouts.app')

@section('content-styles')
<link href="{{ asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('dashtemplate/css/email.css') }}" rel="stylesheet" />
@endsection

@section('content')
<body bgcolor="#4680ff">

<!-- HEADER -->
<table class="head-wrap" bgcolor="#4680ff">
    <tr>
        <td></td>
        <td class="header container">
            
            <div class="content">
                <table bgcolor="#4680ff">
                    <tr>
                        <td><img src="{{ asset('img/logowhite.png') }}" /></td>
                    </tr>
                </table>
            </div>
                
        </td>
        <td></td>
    </tr>
</table><!-- /HEADER -->


<!-- BODY -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <div class="content">
            <table>
                <tr>
                    <td>
                        
                        <h3>Hello, {{ $customer['fullname'] }}</h3>
                        <p class="lead">I just wanted to drop you a quick note to remind you that [amount owed on invoice] in respect of our invoice [invoice reference number] is due for payment on [date due].

                        I would be really grateful if you could confirm that everything is on track for payment. </p>
                        
                        <!-- A Real Hero (and a real human being) -->
                     
                        
       
                                                
                        <br/>

                        <p class="callout">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt. <a href="#">Do it Now! &raquo;</a>
                        </p><!-- /Callout Panel -->                        
                                                
                        <!-- social & contact -->
                        <table class="social" width="100%">
                            <tr>
                                <td>
                                    
                                    <!--- column 1 -->
                                    <table align="left" class="column">
                                        <tr>
                                            <td>                
                                                
                                                <h5 class="">Connect with Us:</h5>
                                                <p class=""><a href="#" class="soc-btn fb">Facebook</a> <a href="#" class="soc-btn tw">Twitter</a> <a href="#" class="soc-btn gp">Google+</a></p>
                        
                                                
                                            </td>
                                        </tr>
                                    </table><!-- /column 1 -->  
                                    
                                    <!--- column 2 -->
                                    <table align="left" class="column">
                                        <tr>
                                            <td>                
                                                                            
                                                <h5 class="">Contact Info:</h5>                                             
                                                <p>Phone: <strong>408.341.0600</strong><br/>
                                                Email: <strong><a href="emailto:hseldon@trantor.com">info@maidssoftware.com</a></strong></p>
                
                                            </td>
                                        </tr>
                                    </table><!-- /column 2 -->
                                    
                                    <span class="clear"></span> 
                                    
                                </td>
                            </tr>
                        </table><!-- /social & contact -->
                    
                    
                    </td>
                </tr>
            </table>
            </div>
                                    
        </td>
        <td></td>
    </tr>
</table><!-- /BODY -->

<!-- FOOTER -->
<table class="footer-wrap">
    <tr>
        <td></td>
        <td class="container">
            
                <!-- content -->
                <div class="content">
                <table>
                <tr>
                    <td align="center">
                        <p>
                            <a href="#">Terms</a> |
                            <a href="#">Privacy</a> |
                            <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>
                        </p>
                    </td>
                </tr>
            </table>
                </div><!-- /content -->
                
        </td>
        <td></td>
    </tr>
</table><!-- /FOOTER -->

</body>
</html>
@endsection