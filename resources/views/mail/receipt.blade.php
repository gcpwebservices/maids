@extends('layouts.app')

@section('content-styles')
<!-- <link href="{{ asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css') }}" rel="stylesheet"> -->


<link rel="stylesheet" type="text/css" href="{{ asset('invoice/bootstrap/css/bootstrap.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('invoice/font-awesome/css/font-awesome.min.css') }}" />
<link href="{{ asset('dashtemplate/css/email.css') }}" rel="stylesheet" />


    <style>
        .head-wrap {
            background-color: rgb(70, 128, 255);
        }
        .height {
            min-height: 200px;
        }

        .icon {
            font-size: 47px;
            color: #5CB85C;
        }

        .iconbig {
            font-size: 77px;
            color: #5CB85C;
        }

        .table > tbody > tr > .emptyrow {
            border-top: none;
        }

        .table > thead > tr > .emptyrow {
            border-bottom: none;
        }

        .table > tbody > tr > .highrow {
            border-top: 3px solid;
        }

        .IssueLabel {
            height: 20px;
            padding: 0.15em 4px;
            font-size: 12px;
            font-weight: 600;
            line-height: 15px;
            border-radius: 2px;
            box-shadow: inset 0 -1px 0 rgba(27,31,35,0.12);
        }

        .success {
            background-color: #3c763d;
            color: #fff;
        }

        .warning {
            background-color: #8a6d3b;
            color: #fff;
        }

        .danger {
            background-color: #a94442;
            color: #fff;
        }
    </style>

    <script type="text/javascript" src="{{ asset('invoice/js/jquery-1.10.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('invoice/bootstrap/js/bootstrap.min.js') }}"></script>

@endsection

@section('content')
<body bgcolor="#4680ff">

<!-- HEADER -->
<table class="head-wrap" bgcolor="#4680ff">
    <tr>
        <td></td>
        <td class="header container">
            
                <div class="content">
                    <table bgcolor="#4680ff">
                    <tr>
                        <td><img src="{{ asset('img/logowhite.png') }}" /></td>
                    </tr>
                </table>
                </div>
                
        </td>
        <td></td>
    </tr>
</table><!-- /HEADER -->


<!-- BODY -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <div class="content">
            <table>
                <tr>
                    <td>
                        
                        <h3>Dear, {{$maids_schedule->getCustomer['fullname']}}</h3>
                        <p class="lead">We've just received your Payment and would like to thank you.
                        It has been a pleasure doing business with you. 
                        </p><br>
                        <p class="lead">We wish you the best of luck.</p>
                        <br>
                        <p class="lead">Sincerely,</p>
                        <!-- A Real Hero (and a real human being) -->
                     
                    
                                            
                        <br/>

                        <div class="container">

                            <div class="page-header text-center">
                                 
                            </div>


                            <div class=" callout">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="row">
                                            <div class="text-primary col-md-6">
                                                <h3>INVOICE #: {{$maids_schedule->schedule_code}}</h3>
                                            </div>
                                            <div class="text-primary col-md-6 text-right">
                                                <?php  $now = new DateTime();?>
                                                @if( $sales_details['payment_status'] != '1')
                                                    <small>Invoice Date: {{$now->format('l j F Y')}}</small><br>
                                                @endif
                                                
                                                    @if( $sales_details['payment_status'] == '1' )
                                                        <small>Full payment received on: 
                                                        {{ $sales_details['updated_at']->format('l j F Y') }}</small><br>
                                                    @endif

                                                    @if($sales_details['payment_status'] == '0')
                                                        <span class="IssueLabel danger">UNPAID</span>
                                                    @elseif( $sales_details['payment_status'] == '3' )
                                                        <span class="IssueLabel warning">PARTIALY PAID</span>
                                                    @elseif( $sales_details['payment_status'] == '1' )
                                                        <span class="IssueLabel success">PAID</span>
                                                    @endif
                                            </div>
                                        </div>
                                  
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-6 col-lg-6 pull-left">
                                                <div class="panel panel-default height">
                                                    <div class="panel-heading">Customer</div>
                                                    <div class="panel-body">
                                                        <strong>{{  $maids_schedule->getCustomer['fullname'] }}</strong><br>
                                                        {{  $maids_schedule->getCustomer['email'] }}<br>
                                              
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-xs-12 col-md-6 col-lg-6 pull-right">
                                                <div class="panel panel-default height">
                                                    <div class="panel-heading">Address</div>
                                                    <div class="panel-body">
                                   
                                                        {{  $maids_schedule->getCustomer['address'] }}<br>
                                                        {{  $maids_schedule->getArea['area'] }}<br>
                                                        {{  $maids_schedule->getSubArea['sub_area'] }}<br>
                                    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3 class="text-center"><strong>INVOICE SUMMARY</strong></h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <td><strong>Description</strong></td>
                                                                <td class="emptyrow"></td>
                                                                <td class="text-center"><strong>Price</strong></td>
                                                                <td class="text-center"><strong>Quantity</strong></td>
                                                                <td class="text-right"><strong>VAT(5%)</strong></td>
                                                                <td class="text-right"><strong>Total</strong></td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($maids_relation as $data_list)
                                                                @if( $data_list->getSaleSummary['payment_total'] != '0.00' )
                                                                <tr>
                                                                    <td>Cleaning<br>
                                                                        <span class="text-primary">{{$data_list->notes}}</span>
                                                                    </td>
                                                                    <td class="emptyrow" style="border-top: 1px solid #ddd;"></td>
                                                                    <td class="text-center">{{$data_list->getSaleSummary['price']}}</td>
                                                                    <td class="text-center">1</td>
                                                                    <td class="text-right">{{$data_list->getSaleSummary['payment_vat']}}</td>
                                                                    <td class="text-right">{{$data_list->getSaleSummary['payment_total']}}</td>
                                                                </tr>  
                                                                @endif
                                                            @endforeach  
                                                            <tr>
                                                                <td class="highrow"></td>
                                                                <td class="highrow"></td>
                                                                <td class="highrow"></td>
                                                                <td class="highrow"></td>
                                                                <td class="highrow text-right"><strong>Subtotal</strong></td>
                                                                <td class="highrow text-right">{{ $sales_details['payment_sub_sum'] - $sales_details['payment_vat_sum'] }} AED</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="emptyrow"></td>
                                                                <td class="emptyrow"></td>
                                                                <td class="emptyrow"></td>
                                                                <td class="emptyrow"></td>
                                                                <td class="emptyrow text-right"><strong>VAT</strong></td>
                                                                <td class="emptyrow text-right">{{$sales_details['payment_vat_sum']}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="emptyrow"></td>
                                                                <td class="emptyrow"></td>
                                                                <td class="emptyrow"></td>
                                                                <td class="emptyrow"></td>
                                                                <td class="emptyrow text-right"><strong>Total Paid</strong></td>
                                                                <td class="emptyrow text-right">{{$sales_details['payment_sum']}}</td>
                                                            <tr>
                                                                <td class="emptyrow"></td>
                                                                <td class="emptyrow"></td>
                                                                <td class="emptyrow"></td>
                                                                <td class="emptyrow"></td>
                                                                <td class="emptyrow text-right"><strong>Balance</strong></td>
                                                                <td class="emptyrow text-right">{{  $sales_details['payment_sub_sum'] - $sales_details['payment_sum'] }}</td>
                                                            </tr> 

                                                            </tr>                     
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                       
                                                
                        <!-- social & contact -->
                        <table class="social" width="100%">
                            <tr>
                                <td>
                                    
                                    <!--- column 1 -->
                                    <table align="left" class="column">
                                        <tr>
                                            <td>                
                                                
                                                <h5 class="">Connect with Us:</h5>
                                                <p class=""><a href="#" class="soc-btn fb">Facebook</a> <a href="#" class="soc-btn tw">Twitter</a> <a href="#" class="soc-btn gp">Google+</a></p>
                        
                                                
                                            </td>
                                        </tr>
                                    </table><!-- /column 1 -->  
                                    
                                    <!--- column 2 -->
                                    <table align="left" class="column">
                                        <tr>
                                            <td>                
                                                                            
                                                <h5 class="">Contact Info:</h5>                                             
                                                <p>Phone: <strong>408.341.0600</strong><br/>
                Email: <strong><a href="emailto:hseldon@trantor.com">info@maidssoftware.com</a></strong></p>
                
                                            </td>
                                        </tr>
                                    </table><!-- /column 2 -->
                                    
                                    <span class="clear"></span> 
                                    
                                </td>
                            </tr>
                        </table><!-- /social & contact -->
                    
                    
                    </td>
                </tr>
            </table>
            </div>
                                    
        </td>
        <td></td>
    </tr>
</table><!-- /BODY -->

<!-- FOOTER -->
<table class="footer-wrap">
    <tr>
        <td></td>
        <td class="container">
            
                <!-- content -->
                <div class="content">
                <table>
                <tr>
                    <td align="center">
                        <p>
                            <a href="#">Terms</a> |
                            <a href="#">Privacy</a> |
                            <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>
                        </p>
                    </td>
                </tr>
            </table>
                </div><!-- /content -->
                
        </td>
        <td></td>
    </tr>
</table><!-- /FOOTER -->

</body>
</html>
@endsection