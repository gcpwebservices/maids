<!DOCTYPE html>
<html>
<head>
	<title>Kaya Skin Clinic</title>
	<link rel="icon" type="image/png" sizes="56x56" href="http://switchonomics.com/kaya/2018/11/3/favicon.png">
</head>
<style type="text/css">



	body{ 
	 	height: 200px;
		width: 650px;
		position: absolute;
		left: 40%;
		margin-left: -200px;
		height: 100%;
		font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", 
  		Helvetica, Arial, "Lucida Grande", sans-serif;
  		font-size: 15px;
	}

	.logo img {
		width: 110px;
   		padding-bottom: 5px;
   		margin-top: -8px;
	}

	.header img {
		width: 650px;	
		
	}

	.content {

	}

	.footer img {
		width: 650px;
	 	position: relative;
 	  	left: 0;
    	margin-bottom: -5px;
	}

</style>

<body>
	<div class="logo"><img src="http://switchonomics.com/kaya/2018/11/3/images/newlogo.png"></div>
	<div class="header"><img src="http://switchonomics.com/kaya/2018/11/3/images/newheader.jpg"></div>
	<div class="main-wrapper">
		<div class="content">
	        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
	            <tr>
	                <td align="center" valign="top">
	                    <table border="0" cellpadding="20" cellspacing="0" width="600" id="emailContainer">
	                        <tr>
	                            <td valign="top">
	                                Dear FN,<br>
	                                <p align="center">We look forward to seeing you at our clinic at 12:00 on 11/19/2018 at Kaya Branch 1 <br>
	                                Do you give us a call on 888-88-88 if you wish to reschedule or need any <br>
	                                other assistance.<br>
                                 	</p>
	                            </td>
	                        </tr>
	                    </table>
	                </td>
	            </tr>
	        </table>	
    		<div class="button" align="center"><a href="#"><img src="http://switchonomics.com/kaya/2018/11/3/images/button1.jpg" width="230"><a></div>

	        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
	            <tr>
	                <td align="center" valign="top">
	                    <table border="0" cellpadding="20" cellspacing="0" width="600" id="emailContainer">
	                        <tr>
	                            <td align="center" valign="top">
                            		<a href="#"><img src="http://switchonomics.com/kaya/2018/11/3/images/img1.jpg" width="180"></a><br>
                 					Kaya's range of services<br> 
                 					that help you be at your<br> 
                 					beautiful best
	                            </td>
                                    <td align="center" valign="top">
            		         		<a href="#"><img src="http://switchonomics.com/kaya/2018/11/3/images/img2.jpg" width="180"></a><br>
                 					Everything you need to<br> 
                 					know on skin, hair and<br> 
                 					body, presented as <br>
                 					quick and easy reads
	                            </td>
                                    <td align="center" valign="top">
            		         		<a href="#"><img src="http://switchonomics.com/kaya/2018/11/3/images/img3.jpg" width="180"></a><br>
                 					Know more about the<br> 
                 					region's biggest team<br> 
                 					of dermatologists
	                            </td>
	                        </tr>
	                    </table>
	                </td>
	            </tr>
	        </table>	

		</div>
	
	</div>
	<div class="footer"><img src="http://switchonomics.com/kaya/2018/11/3/images/footer.jpg" border="0" usemap="#Map">
		<map name="Map">
			<area shape="rect" coords="101,105,207,118" href="https://www.kayaskinclinic.com/uae/en/treatments/ultima-laser-hair-reduction/">
			<area shape="rect" coords="218,104,326,118" href="https://www.kayaskinclinic.com/uae/en/treatments/age-control/">
			<area shape="rect" coords="335,106,479,118" href="https://www.kayaskinclinic.com/uae/en/treatments/acne-acne-scar/">
			<area shape="rect" coords="486,104,560,119" href="https://www.kayaskinclinic.com/uae/en/treatments/hair-strength-therapies/">
			<area shape="rect" coords="115,123,233,139" href="https://www.kayaskinclinic.com/uae/en/treatments/pigmentation/">
			<area shape="rect" coords="243,122,344,138" href="https://www.kayaskinclinic.com/uae/en/treatments/skin-glow/">
			<area shape="rect" coords="354,123,452,135" href="https://kayaskinproducts.com/products/">
			<area shape="rect" coords="462,123,538,135" href="https://www.kayaskinclinic.com/uae/en/treatments/body-solutions/">
			<area shape="circle" coords="174,214,9" href="https://www.facebook.com/kayaskinclinic/">
			<area shape="circle" coords="199,213,8" href="https://www.kayaskinclinic.com/uae/en/">
			<area shape="circle" coords="223,214,8" href="mailto:kayacare@kayame.net">
			<area shape="circle" coords="248,213,8" href="https://www.youtube.com/user/KayaSkinClinicArabia">
			<area shape="circle" coords="271,213,7" href="https://www.instagram.com/kayaclinicarabia/">
		</map>
	</div>
</body>
</html>