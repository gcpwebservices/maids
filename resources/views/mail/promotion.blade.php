@extends('layouts.app')

@section('content-styles')
<link href="{{ asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('dashtemplate/css/email.css') }}" rel="stylesheet" />
@endsection

@section('content')
<body bgcolor="#4680ff">

<!-- HEADER -->
<table class="head-wrap" bgcolor="#4680ff">
    <tr>
        <td></td>
        <td class="header container">
            
                <div class="content">
                    <table bgcolor="#4680ff">
                    <tr>
                        <td><img src="{{ asset('img/logowhite.png') }}" /></td>
                    </tr>
                </table>
                </div>
                
        </td>
        <td></td>
    </tr>
</table><!-- /HEADER -->


<!-- BODY -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <div class="content">
            <table>
                <tr>
                    <td>
                        
                        <h3>Hello, {{$name}}</h3>
                        <p class="lead">We hope you've been doing well since we saw you last. We'd love to keep working with you if there's an opportunity. </p>
                        
                        <!-- A Real Hero (and a real human being) -->
                        <p><img src="{{ asset('img/wemiss.jpg') }}" /></p><!-- /hero -->
                        
       
                                                
                        <br/>
                        <br/>                           
                                                
                        <!-- social & contact -->
                        <table class="social" width="100%">
                            <tr>
                                <td>
                                    
                                    <!--- column 1 -->
                                    <table align="left" class="column">
                                        <tr>
                                            <td>                
                                                
                                                <h5 class="">Connect with Us:</h5>
                                                <p class=""><a href="#" class="soc-btn fb">Facebook</a> <a href="#" class="soc-btn tw">Twitter</a> <a href="#" class="soc-btn gp">Google+</a></p>
                        
                                                
                                            </td>
                                        </tr>
                                    </table><!-- /column 1 -->  
                                    
                                    <!--- column 2 -->
                                    <table align="left" class="column">
                                        <tr>
                                            <td>                
                                                                            

                
                                            </td>
                                        </tr>
                                    </table><!-- /column 2 -->
                                    
                                    <span class="clear"></span> 
                                    
                                </td>
                            </tr>
                        </table><!-- /social & contact -->
                    
                    
                    </td>
                </tr>
            </table>
            </div>
                                    
        </td>
        <td></td>
    </tr>
</table><!-- /BODY -->

<!-- FOOTER -->
<table class="footer-wrap">
    <tr>
        <td></td>
        <td class="container">
            
                <!-- content -->
                <div class="content">
                <table>
                <tr>
                    <td align="center">
                        <p>
                            <a href="#">Terms</a> |
                            <a href="#">Privacy</a> |
                            <a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>
                        </p>
                    </td>
                </tr>
            </table>
                </div><!-- /content -->
                
        </td>
        <td></td>
    </tr>
</table><!-- /FOOTER -->

</body>
</html>
@endsection