@extends('layouts.app')

@section('content-styles')

    <link href="{{ asset('landingtemplate/assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Style CSS -->
    <link href="{{ asset('landingtemplate/assets/css/style.css') }}" rel="stylesheet">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="{{ asset('landingtemplate/assets/css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('landingtemplate/assets/css/owl.theme.default.css') }}" rel="stylesheet">
    <!-- FontAwesome CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('landingtemplate/assets/css/fontello.css') }}">
    <link href="{{ asset('landingtemplate/assets/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


@endsection

@section('content')
<body>	

	<!-- header section -->
    <?php $routes = Route::current()->getName() ?>
    @if ( $routes== 'landing' || $routes == 'about' || $routes == 'service' || $routes == 'service-single')
        @include('layouts.landingpage.header')
    @endif
	<!-- header-section close -->

    <!-- slider-start -->
    <div class="slider">
        <div class="owl-carousel slider">
            <div class="item">
                <div class="slider-img"> <img src="{{ asset('landingtemplate/assets/images/slider-1.jpg') }}" alt=""></div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                            <div class="slider-captions">
                                <h1 class="slider-title">Domestic &amp; 
								Commercial Cleaning</h1>
                                <p class="slider-text hidden-xs">Cleaning Services we are passionate about providing
                            	a flexible service.</p>
                                <a href="about.html" class="btn btn-default btn-lg hidden-sm hidden-xs">Click Here for a Free Estimate</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="slider-img"><img src="{{ asset('landingtemplate/assets/images/slider-2.jpg') }}" alt=""></div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-12  col-xs-12">
                            <div class="slider-captions">
                                <h1 class="slider-title">We help you to keep your place clean</h1>
                                <p class="slider-text hidden-xs">We use specialize and quality equipment tools for cleaning ! </p>
                                <a href="{{ url('about') }}" class="btn btn-default btn-lg hidden-sm hidden-xs">meet team</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="slider-img"><img src="{{ asset('landingtemplate/assets/images/slider-3.jpg') }}" alt=""></div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-12  col-xs-12">
                            <div class="slider-captions">
                                <h1 class="slider-title">Quality work with Affordable price</h1>
                                <p class="slider-text hidden-xs">We brings professional cleaning services right to your home. </p>
                                <a href="{{ url('services')}}" class="btn btn-default btn-lg hidden-sm hidden-xs">view services</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider-close -->
    <!-- service-start -->
    <div class="space-medium">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="section-title">
                        <!-- section title start-->
                        <h1>Professional Cleaning Services</h1>
                        <p>We Provided Quality Cleaning Services.</p>
                    </div>
                    <!-- /.section title start-->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="service-block">
                        <div class="service-img"><a href="#"><img src="{{ asset('landingtemplate/assets/images/service-1.jpg') }}" alt="" class="img-responsive"></a> </div>
                        <div class="service-content">
                            <h3><a href="#">Office Cleaning</a></h3>
                            <p>Professional cleaning services, office cleaning and cleaning contractors. </p>
                            <a href="#" class="btn-link"> read more</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="service-block">
                        <div class="service-img"><a href="#"><img src="{{ asset('landingtemplate/assets/images/service-2.jpg') }}" alt="" class="img-responsive"> </a></div>
                        <div class="service-content">
                            <h3><a href="#">House Cleaning</a></h3>
                            <p>House Cleaning onsectetur terdum vulputate mauris vestibulum ullamcorper eget. </p>
                            <a href="#" class="btn-link"> read more</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="service-block">
                        <div class="service-img"><a href="#"><img src="{{ asset('landingtemplate/assets/images/service-3.jpg') }}" alt="" class="img-responsive"></a> </div>
                        <div class="service-content">
                            <h3><a href="#">Commercial Cleaning</a></h3>
                            <p>Eleifend etiam scelerisque tortor sed porta ultrices risus nunc eleifend. </p>
                            <a href="#" class="btn-link"> read more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- service-close -->
    <!-- features-start -->
    <div class="space-medium bg-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="section-title">
                        <!-- section title start-->
                        <h1>Top Few Reasons to Give us a Try</h1>
                        <p>Feugiat etiam ut justo ut sem molestie viverra id act massa pellentesque non
                            <br>tellus urna donc orci nulla erat orci consequat.</p>
                    </div>
                    <!-- /.section title start-->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="feature-block mb30">
                        <div class="feature-icon"><i class="icon-round68"></i> </div>
                        <div class="feature-content">
                            <h4>Well - Trained Cleaners</h4>
                            <p>Molestie viverra id act massa pellentesque non tellus urna donc orci consequat. </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="feature-block mb30">
                        <div class="feature-icon"><i class="icon-round68"></i> </div>
                        <div class="feature-content">
                            <h4>Fast &amp; Effective Service</h4>
                            <p>Feugiat etiam ut justo ut sem molestie vellus urna donc orci nulla erat orci consequat. </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="feature-block mb30">
                        <div class="feature-icon"><i class="icon-round68"></i> </div>
                        <div class="feature-content">
                            <h4>Quality Control</h4>
                            <p>Feugiat etiam ut justo ut sem molestie vellus urna donc orci nulla erat orci consequat. </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="feature-block">
                        <div class="feature-icon"><i class="icon-round68"></i> </div>
                        <div class="feature-content">
                            <h4>100% Quality Guarentee</h4>
                            <p>Molestie viverra id act massa pellentesque non tellus urna donc orci consequat. </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="feature-block">
                        <div class="feature-icon"><i class="icon-round68"></i> </div>
                        <div class="feature-content">
                            <h4>Standard Cleaning Tools</h4>
                            <p>Feugiat etiam ut justo ut sem molestie vellus urna donc orci nulla erat orci consequat. </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="feature-block">
                        <div class="feature-icon"><i class="icon-round68"></i> </div>
                        <div class="feature-content">
                            <h4>Daily Task List</h4>
                            <p>Cras commodo ligula a urna egestas porta dui porta bibendum urna at semper. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- features-close -->
    <!-- pricing-start -->
    <div class="space-medium bg-default">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="section-title">
                        <!-- section title start-->
                        <h1 class="text-white">Pricing Plan </h1>
                    </div>
                    <!-- /.section title start-->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="price-head">
                        <h3> House Cleaning</h3>
                    </div>
                    <div class="price-block">
                        <div class="price-content">
                            <h4>Kitchen &amp; Bedrooms</h4>
                            <div class="price-text">
                                <p>$28/<small style="font-size: 10px;"> hour</small></p>
                            </div>
                            <ul>
                                <li>Wipe Cabinets</li>
                                <li>Wipes Tops</li>
                                <li>Dust Tops of Cabinets</li>
                            </ul>
                        </div>
                        <div class="price-content">
                            <h4>Bathroom</h4>
                            <div class="price-text">
                                <p>$29/ <small style="font-size: 10px;">hour</small></p>
                            </div>
                            <ul>
                                <li>Dust Counters</li>
                                <li>Dust Light</li>
                                <li>Wipe Countertops</li>
                            </ul>
                        </div>
                        <div class="price-content">
                            <h4>Living Room</h4>
                            <div class="price-text">
                                <p>$29/ <small style="font-size: 10px;">hour</small></p>
                            </div>
                            <ul>
                                <li>Make Beds</li>
                                <li>Dust Furniture</li>
                                <li>Dust Mini-blinds</li>
                            </ul>
                        </div>
                        <div class="text-center mt30"><a href="{{ url('/booking')}}" class="btn btn-primary btn-sm">book your order now</a></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="price-head">
                        <h3>Office Cleaning</h3>
                    </div>
                    <div class="price-block">
                        <div class="price-content">
                            <h4>Once a Week</h4>
                            <div class="price-text">
                                <p>$270/<small style="font-size: 10px;"> hour</small></p>
                            </div>
                            <ul>
                                <li>( 4 Sessions - 3 hrs per Session )</li>
                                
                            </ul>
                        </div>
                        <div class="price-content">
                            <h4>Twice a Week</h4>
                            <div class="price-text">
                                <p>$574/ <small style="font-size: 10px;">hour</small></p>
                            </div>
                            <ul>
                                <li>( 8 Sessions - 3 hrs per Session )</li>
                                </ul>
                        </div>
                        <div class="price-content">
                            <h4>Thrice a Week</h4>
                            <div class="price-text">
                                <p>$670/ <small style="font-size: 10px;">hour</small></p>
                            </div>
                            <ul>
                                <li>( 12 Sessions - 3 hrs per Session )</li>
                                
                            </ul>
                        </div>
                        <div class="price-content">
                            <h4>Mon - Fri Daily</h4>
                            <div class="price-text">
                                <p>$990/ <small style="font-size: 10px;">hour</small></p>
                            </div>
                            <ul>
                                <li>( 20 Sessions - 2 hrs per Session )</li>
                                
                            </ul>
                        </div>
                        <div class="text-center mt30"><a href="{{ url('/booking')}}" class="btn btn-primary btn-sm">book your order now</a></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="price-head">
                        <h3>Commercial Cleaning</h3>
                    </div>
                    <div class="price-block">
                        <div class="price-content">
                            <h4>Kitchen &amp; Bedrooms</h4>
                            <div class="price-text">
                                <p>$28/<small style="font-size: 10px;"> hour</small></p>
                            </div>
                            <ul>
                                <li>Wipe Cabinets</li>
                                <li>Wipes Tops</li>
                                <li>Dust Tops of Cabinets</li>
                            </ul>
                        </div>
                        <div class="price-content">
                            <h4>Bathroom</h4>
                            <div class="price-text">
                                <p>$29/ <small style="font-size: 10px;">hour</small></p>
                            </div>
                            <ul>
                                <li>Dust Counters</li>
                                <li>Dust Light</li>
                                <li>Wipe Countertops</li>
                            </ul>
                        </div>
                        <div class="price-content">
                            <h4>Living Room</h4>
                            <div class="price-text">
                                <p>$29/ <small style="font-size: 10px;">hour</small></p>
                            </div>
                            <ul><li>Make Beds</li>
                                <li>Dust Furniture</li>
                                <li>Dust Mini-blinds</li>
                            </ul>
                        </div>
                        <div class="text-center mt30"><a href="{{ url('/booking')}}" class="btn btn-primary btn-sm">book your order now</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- pricing-close -->
    <!-- testimonial-start -->
    <div class="space-medium bg-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="section-title">
                        <!-- section title start-->
                        <h1>Listen to what our clients say about us. </h1>
                    </div>
                    <!-- /.section title start-->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="testimonial-block">
                        <div class="testimonial-content">
                            <p>“We appreciate your dedication to excellent service provided to our facility Monday-Friday.”</p>
                        </div>
                        <div class="testimonial-meta">
                            <h5>James Fedralle</h5>
                            <span>( Manager)</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="testimonial-block">
                        <div class="testimonial-content">
                            <p>“You are awesome! All of the cleaners have done an amazing job! It makes such a difference to walk into a clean home each week!” </p>
                        </div>
                        <div class="testimonial-meta">
                            <h5>Keli J. Farrand</h5>
                            <span>(House Wife)</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt30">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                    <a href="" class="btn btn-default btn-lg">view our testimonials</a>
                </div>
            </div>
        </div>
    </div>
    <!-- testimonial-close -->
    <!-- news-start -->
    <div class="space-medium">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="section-title">
                        <!-- section title start-->
                        <h1>Latest News</h1>
                        <p>Check Our New Latest Blogs</p>
                    </div>
                    <!-- /.section title start-->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="post-block">
                        <div class="post-img">
                            <a href="#" class="imghover">
                    <img src="{{ asset('landingtemplate/assets/images/post-img-small-1.jpg') }}" alt="" class="img-responsive"></a>
                        </div>
                        <!-- post block -->
                        <div class="meta-date">
                            <span class="meta-date-number">14</span>
                            <span class="meta-date-text"> AUG </span>
                        </div>
                        <div class="post-content">
                            <h3><a href="#" class="title">Cleaning Kit is Very Essential</a></h3>
                            <div class="meta">
                                <span class="meta-categories"><a href="#">Home Cleaning</a></span>
                                <span>/</span>
                                <span class="meta-comments">comments 22</span>
                            </div>
                            <p>Eleifend etiam scelerisque tortor sed porta ultrices risus nunc eleifend.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="post-block">
                        <div class="post-img">
                            <a href="#" class="imghover">
                              <img src="{{ asset('landingtemplate/assets/images/post-img-small-2.jpg') }}" alt="" class="img-responsive">
                               </a>
                        </div>
                        <!-- post block -->
                        <div class="meta-date">
                            <span class="meta-date-number">15</span>
                            <span class="meta-date-text"> AUG </span>
                        </div>
                        <div class="post-content">
                            <h3><a href="#" class="title">7 Tips to Clean House</a></h3>
                            <div class="meta">
                                <span class="meta-categories"><a href="#">Office Cleaning</a></span>
                                <span>/</span>
                                <span class="meta-comments">comments 14</span>
                            </div>
                            <p>Adipiscing elit Integer lacinia malesuada donec amet vestibulum orci.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="post-block">
                        <div class="post-img">
                            <a href="#" class="imghover">
                                <img src="{{ asset('landingtemplate/assets/images/post-img-small-3.jpg') }}" alt="" class="img-responsive"></a>
                        </div>
                        <!-- post block -->
                        <div class="meta-date">
                            <span class="meta-date-number">15</span>
                            <span class="meta-date-text"> AUG </span>
                        </div>
                        <div class="post-content">
                            <h3><a href="#" class="title">How to Keep Your Home Clean</a></h3>
                            <div class="meta">
                                <span class="meta-categories"><a href="#">Cleaning</a></span>
                                <span>/</span>
                                <span class="meta-comments">comments 10</span>
                            </div>
                            <p>Consectetur et interdum vulputate mauris vestibulum ullamcorper eget.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- news-close -->

    <?php $routes = Route::current()->getName() ?>
    @if ( $routes== 'landing' || $routes == 'about' || $routes == 'service' || $routes == 'service-single')
        @include('layouts.landingpage.footer')
    @endif

</body>
@endsection

@section('content-scripts')


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset('landingtemplate/assets/js/jquery.min.js') }}" type="text/javascript"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('landingtemplate/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('landingtemplate/assets/js/navigation.js') }}" type="text/javascript"></script>
    <script src="{{ asset('landingtemplate/assets/js/menumaker.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('landingtemplate/assets/js/jquery.sticky.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landingtemplate/assets/js/sticky-header.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landingtemplate/assets/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landingtemplate/assets/js/slider.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landingtemplate/assets/js/testimonial-slider.js') }}"></script>

@endsection