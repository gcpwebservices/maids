@extends('layouts.app')

@section('content-styles')

    <link href="{{ asset('landingtemplate/assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Style CSS -->
    <link href="{{ asset('landingtemplate/assets/css/style.css') }}" rel="stylesheet">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="{{ asset('landingtemplate/assets/css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('landingtemplate/assets/css/owl.theme.default.css') }}" rel="stylesheet">
    <!-- FontAwesome CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('landingtemplate/assets/css/fontello.css') }}">
    <link href="{{ asset('landingtemplate/assets/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


@endsection

@section('content')

<body>
    
<!-- header section -->
<?php $routes = Route::current()->getName() ?>
@if ( $routes== 'landing' || $routes == 'about' || $routes == 'service' || $routes == 'service-single')
    @include('layouts.landingpage.header')
@endif
<!-- header-section close -->

    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="page-section">
                        <h1 class="page-title">Office Cleaning</h1>
                        <p class="page-text">We offer a comprehensive range of Home &amp; Commercial cleaning services. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page-header-close -->
    <!-- service-start -->
    <div class="space-medium">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="sidenav">
                        <ul class="arrow arrow-right">
                            <li class=""><a href="#">Office Cleaning</a></li>
                            <li><a href="#">House Cleaning</a></li>
                            <li><a href="#">Commercial Cleaning</a></li>
                      </ul>
                    </div>
                    
                    <div class="widget-cta">
                    <div class="widget-cta-content">
                        
                        <p>Exculsive Online Offer 30% off First Month's Cleaning</p>
                        <a href="{{url('/booking')}}" class="btn btn-default btn-sm">book now </a>
                        </div>
                    </div>
                    <div class="widget">
                    <div class="widget-testimonial">
                        <p>“You are awesome! All of the cleaners have done an amazing job! It makes such a difference to walk into a clean home each week!”</p>
                        <div class="testimonial-meta">
                            <h5>Keli J. Farrand</h5>
                            <span>(House Wife)</span>
                        </div>
                       
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="mb30">
                        <div class="service-img mb30"> <img src="./images/service-img.jpg" alt="" class="img-responsive"></div>
                        <p class="lead">An Office Cleaning Crew takes care of all the Important Cleaning Tasks 
    That Create a Tidy Office Environment.</p>
                        <p>The employees in a clean, tidy office work more efficiently than they would in a messy way of environment. Also, potential clients who visit a clean office leave with a favorable impression of the place & allows the employees of an office to concentrate on their work.  </p>
                        <p>Some offices assign cleaning tasks to employees and that takes away from productive time spent doing work and with the clients.</p>
                    </div>
                    <hr>
                    <div class="row">
                        
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <h3>Office Cleaning Package</h3>
                            <p>Provide the clean office you &amp; your employees 
    deserve at a low cost.</p>
                            <div class="service-content">
                                <ul class="angle angle-right">
                                    <li>Daily Routine Cleaning</li>
                                    <li>Reception, Offices &amp; Stairwells</li>
                                    <li>Kitchens, Canteens &amp; Washrooms</li>
                                    <li>Periodical Deep Cleans</li>
                                    <li>Hard Floor Cleaning &amp; Maintenance</li>
                                     <li>Internal Glazing</li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                         <div class="service-img"><img src="./images/service-img-2.jpg" alt="" class="img-responsive"></div>   
                        </div >
                        <hr>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Office Cleaning Services Benefits</h3>
                            <p>Duis malesuadavelitvel dapibus tinciduntst magna lobortis annec pulvinar lorem massa et nisl. Morbi viverraquam accumsan eleifend pulvinar libero mauris ultricies leeu auctor liberonunc ut massaoin quis dignissim seusce aliqueteratet ultricies volutpaunc tortor iaculis risus.</p>
                            <p>Placerat lorem dignissim lorem acmaximus leousce vulputate eros risuseget dignissim leo scelerisque iaecenas dignissim elementumnisl ut sagittiullam varius enimate. Morbi viverraquam accumsan eleifend pultsnnec pulvinar lorem massa et nisl.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- service-close -->

    <?php $routes = Route::current()->getName() ?>
    @if ( $routes== 'landing' || $routes == 'about' || $routes == 'service' || $routes == 'service-single')
        @include('layouts.landingpage.footer')
    @endif

</body>
@endsection


@section('content-scripts')


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset('landingtemplate/assets/js/jquery.min.js') }}" type="text/javascript"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('landingtemplate/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('landingtemplate/assets/js/navigation.js') }}" type="text/javascript"></script>
    <script src="{{ asset('landingtemplate/assets/js/menumaker.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('landingtemplate/assets/js/jquery.sticky.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landingtemplate/assets/js/sticky-header.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landingtemplate/assets/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landingtemplate/assets/js/slider.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landingtemplate/assets/js/testimonial-slider.js') }}"></script>

@endsection