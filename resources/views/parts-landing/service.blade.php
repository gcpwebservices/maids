@extends('layouts.app')

@section('content-styles')

    <link href="{{ asset('landingtemplate/assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Style CSS -->
    <link href="{{ asset('landingtemplate/assets/css/style.css') }}" rel="stylesheet">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="{{ asset('landingtemplate/assets/css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('landingtemplate/assets/css/owl.theme.default.css') }}" rel="stylesheet">
    <!-- FontAwesome CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('landingtemplate/assets/css/fontello.css') }}">
    <link href="{{ asset('landingtemplate/assets/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


@endsection

@section('content')

<body>

<!-- header section -->
<?php $routes = Route::current()->getName() ?>
@if ( $routes== 'landing' || $routes == 'about' || $routes == 'service')
    @include('layouts.landingpage.header')
@endif

    <!-- page-header-start -->
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="page-section">
                        <h1 class="page-title">Services</h1>
                        <p class="page-text">We offer a comprehensive range of Home &amp; Commercial cleaning services.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page-header-close -->
   <!-- service-start -->
    <div class="space-medium">
        <div class="container">
           
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="service-block">
                        <div class="service-img"><a href="#"><img src="./images/service-1.jpg" alt="" class="img-responsive"></a> </div>
                        <div class="service-content">
                            <h3><a href="#">Office Cleaning</a></h3>
                            <p>Professional cleaning services, office cleaning and cleaning contractors. </p>
                            <ul class="angle angle-right">
                                <li>Receptions/Common Area</li>
                                <li>Conference Room</li>
                                <li>Restroom</li>
                                <li>Break Room</li>
                                <li>Inner Office/Cubicles</li>
                            </ul>
                            <a href="#" class="btn-link"> read more</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="service-block">
                        <div class="service-img"><a href="#"><img src="./images/service-2.jpg" alt="" class="img-responsive"></a> </div>
                        <div class="service-content">
                            <h3><a href="#">House Cleaning</a></h3>
                            <p>House Cleaning onsectetur terdum vulputate mauris vestibulum ullamcorper eget. </p>
                            <ul class="angle angle-right">
                                <li>Kitchen</li>
                                <li>Living Room</li>
                                <li>Dining Room</li>
                                <li>Bathrooms</li>
                                <li>Throught the house</li>
                                <li>Dining Room</li>
                                <li>Other</li>
                                
                            </ul>
                            <a href="#" class="btn-link"> read more</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="service-block">
                        <div class="service-img"><a href="#"><img src="./images/service-3.jpg" alt="" class="img-responsive"></a> </div>
                        <div class="service-content">
                            <h3><a href="#">Commercial Cleaning</a></h3>
                            <p>Eleifend etiam scelerisque tortor sed porta ultrices risus nunc eleifend. </p>
                             <ul class="angle angle-right">
                                <li>Office Cleaning Service</li>
                                <li>Office Deep Cleaning</li>
                                <li>Carpet Cleaning</li>
                                <li>Retail Shop Cleaning</li>
                                <li>Hotels &amp; Guesthouses</li>
                                
                                
                            </ul>
                            <a href="#" class="btn-link"> read more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- cta-section close -->
    <?php $routes = Route::current()->getName() ?>
    @if ( $routes== 'landing' || $routes == 'about' || $routes == 'service')
        @include('layouts.landingpage.footer')
    @endif

</body>
@endsection


@section('content-scripts')


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset('landingtemplate/assets/js/jquery.min.js') }}" type="text/javascript"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('landingtemplate/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('landingtemplate/assets/js/navigation.js') }}" type="text/javascript"></script>
    <script src="{{ asset('landingtemplate/assets/js/menumaker.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('landingtemplate/assets/js/jquery.sticky.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landingtemplate/assets/js/sticky-header.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landingtemplate/assets/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landingtemplate/assets/js/slider.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landingtemplate/assets/js/testimonial-slider.js') }}"></script>

@endsection