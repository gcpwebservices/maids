@extends('layouts.app')

@section('content-styles')

    <link href="{{ asset('landingtemplate/assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Style CSS -->
    <link href="{{ asset('landingtemplate/assets/css/style.css') }}" rel="stylesheet">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="{{ asset('landingtemplate/assets/css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('landingtemplate/assets/css/owl.theme.default.css') }}" rel="stylesheet">
    <!-- FontAwesome CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('landingtemplate/assets/css/fontello.css') }}">
    <link href="{{ asset('landingtemplate/assets/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


@endsection

@section('content')

<body>

    <!-- header section -->
    <?php $routes = Route::current()->getName() ?>
    @if ( $routes== 'landing' || $routes == 'about' || $routes == 'service')
        @include('layouts.header')
    @endif
    <!-- header-section close -->

    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="page-section">
                        <h1 class="page-title">About Us</h1>
                        <p class="page-text">Shine Cleaning Website Template is committed to providing high quality cleaning services.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page-header-close -->
    <div class="space-medium">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                    <img src="{{ asset('landingtemplate/assets/images/about-pic.jpg') }}" alt="" class="img-responsive">
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                    <div class="">
                        <h1>We're ready to clean up your <br>home at anytime!</h1>
                        <p class="lead">We're basically the best! We're prepared to tidy up your home, office, house with a shocking and intense accuracy.</p>
                        <p>Duis sollicitudin hendrerit bibendum. Phasellus interdum, nisi vel elementum convallis, urna turpis commodo velitac pulvinar quam magna sitae hen Maecenas dignissim tortor id varius sodales. </p>
                        <a href="#" class="btn btn-default btn-lg">contact now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- counter-start-->
    <div class="space-small bg-default">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                    <div class="counter-block">
                        <div class="counter-content">
                            <h1 class="counter-title">3000</h1>
                            <span class="counter-text">Project Completed</span> </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                    <div class="counter-block">
                        <div class="counter-content">
                            <h1 class="counter-title">1840</h1>
                            <span class="counter-text">Satisfied Customers</span> </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                    <div class="counter-block">
                        <div class="counter-content">
                            <h1 class="counter-title">2474</h1>
                            <span class="counter-text">Cleaning Advices</span> </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                    <div class="counter-block">
                        <div class="counter-content">
                            <h1 class="counter-title">423</h1>
                            <span class="counter-text">Staff Across The World</span> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- counter-close-->
    <!-- features-start -->
    <div class="space-medium bg-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="section-title">
                        <!-- section title start-->
                        <h1>Top Few Reasons to Give us a Try</h1>
                        <p>Feugiat etiam ut justo ut sem molestie viverra id act massa pellentesque non
                            <br>tellus urna donc orci nulla erat orci consequat.</p>
                    </div>
                    <!-- /.section title start-->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="feature-block mb30">
                        <div class="feature-icon"><i class="icon-round68"></i> </div>
                        <div class="feature-content">
                            <h4>Well - Trained Cleaners</h4>
                            <p>Molestie viverra id act massa pellentesque non tellus urna donc orci consequat. </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="feature-block  mb30">
                        <div class="feature-icon"><i class="icon-round68"></i> </div>
                        <div class="feature-content">
                            <h4>Fast &amp; Effective Service</h4>
                            <p>Feugiat etiam ut justo ut sem molestie vellus urna donc orci nulla erat orci consequat. </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="feature-block mb30">
                        <div class="feature-icon"><i class="icon-round68"></i> </div>
                        <div class="feature-content">
                            <h4>Quality Control</h4>
                            <p>Feugiat etiam ut justo ut sem molestie vellus urna donc orci nulla erat orci consequat. </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="feature-block">
                        <div class="feature-icon"><i class="icon-round68"></i> </div>
                        <div class="feature-content">
                            <h4>100% Quality Guarentee</h4>
                            <p>Molestie viverra id act massa pellentesque non tellus urna donc orci consequat. </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="feature-block">
                        <div class="feature-icon"><i class="icon-round68"></i> </div>
                        <div class="feature-content">
                            <h4>Standard Cleaning Tools</h4>
                            <p>Feugiat etiam ut justo ut sem molestie vellus urna donc orci nulla erat orci consequat. </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="feature-block">
                        <div class="feature-icon"><i class="icon-round68"></i> </div>
                        <div class="feature-content">
                            <h4>Daily Task List</h4>
                            <p>Cras commodo ligula a urna egestas porta dui porta bibendum urna at semper. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- features-close -->
    <div class="space-medium">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="section-title">
                        <!-- section title start-->
                        <h1>Meet our Cleaning Staff</h1>
                        <p>Duis sollicitudin hendrerit bibendum. Phasellus interdum, nisi vel elementum convallis
                            <br> urna turpis commodo velit, ac pulvinar quam magna sed orci.</p>
                    </div>
                    <!-- /.section title start-->
                </div>
            </div>
            <div class="row">
                <!-- member-1-start -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="team-block">
                        <div class="team-img imghover"> <img src="{{ asset('landingtemplate/assets/images/team-member-1.jpg') }}" alt="" class="img-responsive">
                        </div>
                        <div class="team-content mt20">
                            <h3 class="team-title">Merry Tanscom</h3>
                        </div>
                    </div>
                </div>
                <!-- member-1-close -->
                <!-- member-2-start -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="team-block">
                        <div class="team-img imghover"> <img src="{{ asset('landingtemplate/assets/images/team-member-2.jpg') }}" alt="" class="img-responsive">
                        </div>
                        <div class="team-content mt20">
                            <h3 class="team-title">Lesley Dingle</h3>
                        </div>
                    </div>
                </div>
                <!-- member-2-close -->
                <!-- member-3-start -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="team-block">
                        <div class=" team-img imghover"> <img src="{{ asset('landingtemplate/assets/images/team-member-3.jpg') }}" alt="" class="img-responsive">
                        </div>
                        <div class="team-content mt20">
                            <h3 class="team-title">James Fipher</h3>
                        </div>
                    </div>
                </div>
                <!-- member-3-close -->
            </div>
        </div>
    </div>

    <?php $routes = Route::current()->getName() ?>
    @if ( $routes== 'landing' || $routes == 'about' || $routes == 'service')
        @include('layouts.footer')
    @endif

</body>

@endsection


@section('content-scripts')


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset('landingtemplate/assets/js/jquery.min.js') }}" type="text/javascript"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('landingtemplate/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('landingtemplate/assets/js/navigation.js') }}" type="text/javascript"></script>
    <script src="{{ asset('landingtemplate/assets/js/menumaker.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('landingtemplate/assets/js/jquery.sticky.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landingtemplate/assets/js/sticky-header.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landingtemplate/assets/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landingtemplate/assets/js/slider.js') }}"></script>
    <script type="text/javascript" src="{{ asset('landingtemplate/assets/js/testimonial-slider.js') }}"></script>

@endsection