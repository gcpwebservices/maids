
@extends('layouts.app')

@section('content-styles')
    <!-- Custom CSS -->
    <link href="{{ asset('dashtemplate/css/lib/chartist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/lib/calendar2/semantic.ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/lib/calendar2/pignose.calendar.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/lib/owl.carousel.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />

    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/lib/sweetalert/sweetalert.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/lib/toastr/toastr.min.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/colorpicker/css/bootstrap-colorpicker.css') }}" rel="stylesheet">

    <link href="{{asset('dashtemplate/fullcalendar/lib/fullcalendar.min.css') }}" rel='stylesheet'/>
    <link href="{{asset('dashtemplate/fullcalendar/lib/fullcalendar.print.min.css') }}" rel='stylesheet' media='print' />
    <link href="{{asset('dashtemplate/fullcalendar/scheduler.min.css') }}" rel='stylesheet' />

<!--     <link href="{{asset('dashtemplate/timepicker/css/bootstrap-timepicker.css') }}" rel='stylesheet' />
    <link href="{{asset('dashtemplate/timepicker/css/bootstrap-timepicker.min.css') }}" rel='stylesheet' />
    <link href="{{asset('dashtemplate/timepicker/css/timepicker.less') }}" rel='stylesheet' /> -->

    <link rel="stylesheet" href="{{asset('dashtemplate/timepicker/jquery.ui.timepicker.css') }}">
    <link rel="stylesheet" href="{{asset('dashtemplate/timepicker/jquery-ui-1.10.0.custom.min.css') }}">

    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

@endsection

@section('content')
<body class="fix-header fix-sidebar mini-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <?php $routes = Route::current()->getName() ?>
    @if ( $routes== 'dashboard' || $routes== 'maids-schedule' )
        @include('layouts.dashboard.header')
    @endif

    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->

        @if(session('status'))
        <div id="successMessage" class="alert alert-{{session('status')[0]}}" style="background-color: #dff0d8 !important;color: #3c763d!important; border-color: #d6e9c6 !important;">
        {{session('status')[1]}}
        </div>
        @endif


            <div class="row p-t-10">
                <div class="col-md-12">
                    <div class="pull-right">     
                        <button type="button" class="btn btn-success btn-sm m-b-10 m-l-5">
                            <a href="{{ url('admin/appointments') }}" style="color:white;"><i class="fa fa-calendar"></i> 
                            New Appointment</a></li></button>
                    </div>
                
                </div>
               
       
            </div>

 
<!--         <div class="form-body card">
            <div class="row p-t-20">
            <input type="hidden" class="form-control" name="dropdownarea" id="dropdownarea"/>
            <input type="hidden" class="form-control" name="dropdownsub" id="dropdownsub"/>
                
                <div class="col-md-3">
        
                        <div class="form-group has-danger dropdown">
                                <label class="control-label">Area: <span id="maidsinarea" style="color:red;"></span></label>
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle col-md-12" type="button" data-toggle="dropdown">
                                    <span id="arealabel">Select</span>
                                    <span class="caret"></span></button>
                                    <ul  id="area" class="dropdown-menu">
                        
                                        <li value="0">
                                            <a href="#">All
                                                <span id="allareas" class="label label-rouded label-danger pull-right"></span>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                        </div>
            
                </div>
       

                <div class="col-md-3">

                        <div class="form-group has-danger dropdown">
                                <label class="control-label">Subarea: <span id="maidsinarea" style="color:red;"></span></label>
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle col-md-12" type="button" data-toggle="dropdown"  
                                    id="subdisable" >
                                    <span id="subarealabel">Select</span>

                                    <span class="caret"></span></button>
                                    <ul  id="subarea" class="dropdown-menu">
                                        <li value="0">
                                        </li>
                                    </ul>
                                </div>

                        </div>
               
                </div>
     

       
                <div class="col-md-3">
                    <div class="form-group has-danger">
                        <label class="control-label">New Appointment: <span id="" style="color:red;"></span></label>
                        <div class="">
                            <button type="button" id="addmaidsched" class="btn btn-success col-md-8" > <i class="fa fa-user"></i> </button>
                        </div>
                    </div>
                </div>
           
       
            </div>

        </div> -->



        <div class="row card" style="margin:0px!important;">    

            <div id='calendar'></div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->

    <!-- modal -->

    <div class="modal" id="myModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
              
                <!-- Modal Header -->
                <div class="modal-header ">
                  <h4 class="modal-title col-md-10" >Add Itinerary : <span id="servicenumber"></span></h4>
                </div>
          
                <!-- Modal body -->
                <div class="modal-body">
                   <form method="POST" action="{{route('maids-itinerary')}}">
                    @csrf

                    <div class="form-body">
                        <div class="row">

                           <div class="col-md-6">                     
                                <div class="form-group has-danger">
                                    <label class="control-label">Area:</label>
                                    <span id="areaval" style="color:red;"></span>
                                    <input type="hidden" class="form-control" name="areavalue" id="areavalue"/>
                                </div>                     
                            </div>

                            <div class="col-md-6">
                                <div class="form-group has-danger">
                                    <label class="control-label">Sub Area:</label>
                                    <span id="subareaval" style="color:red;"></span>
                                    <input type="hidden" class="form-control" name="subareavalue" id="subareavalue"/>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Select Date:</label>
                                    <input type="hidden" class="form-control" name="id" id="id" value=""/>
                                    <input type="text" class="form-control" name="datetimes" id="datetimes" autocomplete="off" style="height:34px;"/>
                                            <input type="hidden" class="form-control" name="schedule_start" id="schedule_start"/>
                                            <input type="hidden" class="form-control" name="schedule_end" id="schedule_end"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Time from:</label>
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input id="timefrom" type="text" name="timefrom" class="form-control input-small" autocomplete="off" 
                                        style="height: 34px;">
                         
                                    </div>
                                </div>
                            </div>

                             <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Time To:</label>
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input id="timeto" type="text" name="timeto" class="form-control input-small" autocomplete="off" 
                                        style="height: 34px;">
           
                                    </div>
                                </div>
                            </div>

                        

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Select Travel Interval</label>
                                    <select class="form-control custom-select" name="interval" id="interval">
                                        <option value="0">--Select--</option>
                                        <option value="10">10 mins</option>
                                        <option value="20">20 mins</option>
                                        <option value="30">30 mins</option>
                                        <option value="40">40 mins</option>
                                        <option value="50">50 mins</option>
                                        <option value="60">60 mins</option>
                                        <option value="70">1 hour and 10 mins</option>
                                        <option value="80">1 hour and 20 mins</option>
                                        <option value="90">1 hour and 30 mins</option>
                                        <option value="100">1 hour and 40 mins</option>
                                        <option value="110">1 hour and 50 mins</option>
                                        <option value="120">2 hours</option>
                                    </select>
                                </div>
                            </div>
            


                            <div class="col-md-5">
                                <div class="form-group has-danger">
                                <label class="control-label">Availability: </label>
                                    <div class="col-md-12">
                                        <span id="availmaids" style="color:red;"></span>
                                       <!--  <button type="button" id="checkavail" class="btn btn-success"> <i class="fa fa-check"></i> </button> -->
                                    </div>
                                </div>
                            </div>


                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Select Maid</label>
                                    <select class="form-control custom-select" name="maids_id" id="maids_id" required="required">
                                        <option value="">--Select--</option>
        
                                    </select>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-3">
                                <div class="form-group has-danger">
                                    <label class="control-label">Schedule Color</label>
                                    <input id="schedule_color" name="schedule_color" type="color" class="form-control" style="height:34px;" autocomplete="off"/>
           
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Select Driver</label>
                                    <select class="form-control custom-select" name="driver_id" id="driver_id">
                                        <option value="">--Select--</option>
                                        @foreach ($drivers as $list)
                                          <option value="{{ $list->id }}">{{ $list->fullname }}</option>
                                        @endforeach 
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Select Customer</label>
                                    <select class="form-control custom-select" name="customer_id" id="customer_id" required="required">
                                        <option value="">--Select--</option>
                                        @foreach ($customers as $list)
                                        <option value="{{ $list->id }}">{{ $list->fullname }}</option>
                                        @endforeach 
                                    </select>

                                </div>
                            </div>
                        </div>

       
                        <!--/row-->
                        <!--row-->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group checkbox">
                                    <div class="checkbox">
                                        <label style="font-size: 1em">
                                            <input type="checkbox" name="useaddress" value="" id="useaddress">
                                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                            Use customer's current address ? 
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
            
           
                        <div class="row">

                            <div class="col-md-12 " id="customer_address_hide">
                                <div class="form-group">
                                    <label>Enter new customer address</label>
                                    <input type="text" class="form-control" name="customer_address" id="customer_address" autocomplete="off">
                                </div>
                            </div>
     
                        </div>

                        <div class="row">

                            <div class="col-md-4">
                                <label>Amount To Pay:</label>
                                <input id="to_pay_view" readonly="" style="font-size:36px;height:60px" value="0.00 AED" type="text" class="form-control input-lg text-center autonumeric font-bold">
                                <input type="hidden" name="to_pay" value="" id="to_pay">
                            </div>
     
                        </div>


                    </div>
                    <!-- </form> -->
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" id="save" class="btn btn-success" ><i class="fa fa-check"></i> Save</button>
                    <input type="hidden" class="form-control" name="use" id="use">
                    <button type="button" class="btn btn-inverse" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                </div>
                </form>
              </div>
            </div>
    </div>

    <!-- modal end -->


      <!-- modal edit--> 
    <div class="modal" id="myModal2">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
              
                <!-- Modal Header -->
                <div class="modal-header ">
                  <h4 class="modal-title col-md-10" >Edit Itinerary : <span id="servicenumber_edit" style="color:red;"></span></h4>
                </div>
          
                <!-- Modal body -->
                <div class="modal-body">

                    <div class="form-body">
                        <div class="row">
                         
                            <div class="col-md-6">                     
                                <div class="form-group has-danger dropdown">
                                    <label class="control-label">Area: <span id="maidsinarea" style="color:red;"></span></label>
                                    <div class="dropdown">
                                        <button class="btn btn-primary dropdown-toggle col-md-8" type="button" data-toggle="dropdown">
                                        <span id="arealabel_edit">Select</span>
                                        <span class="caret"></span></button>
                                        <ul  id="area_edit" class="dropdown-menu">
                                            <li value="0">
                                                <a href="#">All
                                                    <span id="allareas_edit" class="label label-rouded label-danger pull-right"></span>
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>                     
                    
                            <div class="col-md-6">
                                <div class="form-group has-danger dropdown">
                                <label class="control-label">Subarea: <span id="maidsinarea" style="color:red;"></span></label>
                                    <div class="dropdown">
                                        <button class="btn btn-primary dropdown-toggle col-md-8" type="button" data-toggle="dropdown"  
                                        id="subdisable_edit" >
                                        <span id="subarealabel_edit">Select</span>

                                        <span class="caret"></span></button>
                                        <ul  id="subarea_edit" class="dropdown-menu">
                                            <li value="0">
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Select Date:</label>
                                    <input type="hidden" class="form-control" name="id_edit" id="id_edit" value=""/>
                                    <input type="text" class="form-control" name="datetimes_edit" id="datetimes_edit" autocomplete="off" style="height:34px;"/>
                                            <input type="hidden" class="form-control" name="schedule_start_edit" id="schedule_start_edit"/>
                                            <input type="hidden" class="form-control" name="schedule_end_edit" id="schedule_end_edit"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Time from:</label>
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input id="timefrom_edit" type="text" name="timefrom_edit" class="form-control input-small" autocomplete="off" 
                                        style="height: 34px;">
                         
                                    </div>
                                </div>
                            </div>

                             <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Time To:</label>
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input id="timeto_edit" type="text" name="timeto_edit" class="form-control input-small" autocomplete="off" 
                                        style="height: 34px;">
           
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Select Travel Interval</label>
                                    <select class="form-control custom-select" name="interval_edit" id="interval_edit">
                                        <option value="0">--Select--</option>
                                        <option value="10">10 mins</option>
                                        <option value="20">20 mins</option>
                                        <option value="30">30 mins</option>
                                        <option value="40">40 mins</option>
                                        <option value="50">50 mins</option>
                                        <option value="60">60 mins</option>
                                        <option value="70">1 hour and 10 mins</option>
                                        <option value="80">1 hour and 20 mins</option>
                                        <option value="90">1 hour and 30 mins</option>
                                        <option value="100">1 hour and 40 mins</option>
                                        <option value="110">1 hour and 50 mins</option>
                                        <option value="120">2 hours</option>
                                    </select>
                                </div>
                            </div>
            


                            <div class="col-md-4">
                                <div class="form-group has-danger">
                                <label class="control-label">Availability: </label>
                                    <div class="col-md-12">
                                        <span id="availmaids_edit" style="color:red;"></span>
                                   
                                    </div>
                                </div>
                            </div>


                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Select Maid</label>
                                    <select class="form-control custom-select" name="maids_id_edit" id="maids_id_edit" required="required">
                       <!--                  <option value="">--Select--</option> -->
                         
                                    </select>
                                </div>
                            </div>
             
                            <div class="col-md-3">
                                <div class="form-group has-danger">
                                    <label class="control-label">Schedule Color</label>
                                    <input id="schedule_color_edit" name="schedule_color_edit" type="color" class="form-control" style="height:34px;" autocomplete="off"/>
                                </div>
                            </div>
               
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Select Driver</label>
                                    <select class="form-control custom-select" name="driver_id_edit" id="driver_id_edit">
                                        <option value="">--Select--</option>
                                        @foreach ($drivers as $list)
                                          <option value="{{ $list->id }}">{{ $list->fullname }}</option>
                                        @endforeach 
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Select Customer</label>
                                    <select class="form-control custom-select" name="customer_id_edit" id="customer_id_edit" required="required">
                                        <option value="">--Select--</option>
                                        @foreach ($customers as $list)
                                        <option value="{{ $list->id }}">{{ $list->fullname }}</option>
                                        @endforeach 
                                    </select>

                                </div>
                            </div>
                        </div>

                        <!--/row-->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group checkbox">
                                    <div class="checkbox">
                                        <label style="font-size: 1em">
                                            <input type="checkbox" name="useaddress_edit" value="" id="useaddress_edit">
                                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                            Use customer's current address ?  
                                        </label>
                                    </div>
                                </div>
                            </div>
                  
                        </div>
            
            
                        <div class="row">

                            <div class="col-md-12 " id="customer_address_hide2">
                                <div class="form-group">
                                    <label>Enter new customer address</label>
                                    <input type="text" class="form-control" name="customer_address_edit" id="customer_address_edit" autocomplete="off">
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <label>Amount To Pay:  <span id="to_pay_view_edit" class="text-danger" style="font-weight: bolder;"></span></label>
                                <input type="hidden" name="to_pay_edit" id="to_pay_edit">
                                <input id="payment_edit" name="payment_edit" style="font-size:36px;height:60px"  type="text" class="form-control input-lg text-center autonumeric font-bold">
                            </div>
     

                     <!--        <div class="col-md-6 ">
                                <div class="form-group">
                                    <label>Cash Payment received</label>
                                    <input type="number" class="form-control" name="payment_edit" id="payment_edit" autocomplete="off">
                                </div>
                            </div> -->

                            <div class="col-md-3 ml-auto">
                                <div class="form-group">
                                    <label class="control-label">Service done ?</label>
                                    <select class="form-control custom-select" name="completed_edit" id="completed_edit" style="height:42px;">
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>

                                </div>
                            </div>
     
                        </div>

                    </div>
                    <!-- </form> -->
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" id="save_edit" class="btn btn-success" ><i class="fa fa-check"></i> Save</button>
                    <input type="hidden" class="form-control" name="use_edit" id="use_edit">
                    <button type="button" class="btn btn-inverse" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                </div>
                </form>
              </div>
            </div>
    </div>
    <!-- modal end -->

    <?php $routes = Route::current()->getName() ?>
    @if ( $routes== 'dashboard' || $routes== 'maids-schedule' )
        @include('layouts.dashboard.footer')
    @endif

</body>

@endsection


@section('content-scripts')



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/timepicker/migrate.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <!--Custom JavaScript -->

    <!-- fullcalendar -->    
    <script src="{{ asset('dashtemplate/fullcalendar/lib/moment.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/fullcalendar/lib/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/fullcalendar/scheduler.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/owl-carousel/owl.carousel-init.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/sweetalert/sweetalert.min.js') }}"></script> 
    <script src="{{ asset('dashtemplate/js/lib/sweetalert/sweetalert.init.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/toastr/toastr.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/colorpicker/js/bootstrap-colorpicker.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/jquery-format/src/jquery.format.js') }}"></script>

    <!-- scripit init-->
    <script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <script src="{{ asset('dashtemplate/timepicker/jquery.ui.timepicker.js') }}"></script>
    <script src="{{ asset('dashtemplate/timepicker/jquery.ui.core.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/timepicker/jquery.ui.position.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/timepicker/jquery.ui.widget.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/timepicker/jquery.ui.tabs.min.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js"></script>


<script>

  $(document).ready(function() {

    var d = new Date();
    var h = d.getHours();
    var m = d.getMinutes();


        // $('#schedule_color, #schedule_color_edit').colorpicker().on('changeColor', function(e) {
        //     $('#schedule_color, #schedule_color_edit').html("");
        //     $('#schedule_color, #schedule_color_edit')[0].style.backgroundColor = e.color.toString(
        //         'rgba');
        // });

    // $('#schedule_color, #schedule_color_edit').colorpicker({
    //     format: 'hex' 
    // });

    //current address
    $('#useaddress').prop('checked', true);
    $('#use, #use_edit').val('yes');
    $("#customer_address_hide, #customer_address_hide2").hide();


    //initialize area dropdown
    getAreaAndCount();

    if($('#dropdownarea').val()==''){
        // $("#subdisable").prop("disabled", true);
    }



    setTimeout(function() {
    $('#successMessage').fadeOut('fast');
    }, 10000);

    $("#maids_id, #timefrom, #timeto, #timeto_edit").prop("disabled", true);
    var curDate = '<?php echo $currentD;?>';
    var curTime = '<?php echo $currentT;?>';
    var custname = '';
    var myEvent;

    var area;
    var subarea;

    $('input[name="datetimes"]').daterangepicker(
        { 
        autoUpdateInput: false,
        timePicker: false,
        singleDatePicker: true,  
        minDate: new Date(),
        startDate: moment().startOf('hour'),
        endDate: moment().startOf('hour').add(36, 'hour'),
            locale: {

                format: 'YYYY-MM-DD'
            }
        },
        function(start, end, label) {
            $('#schedule_start').val(start.format('YYYY-MM-DD'));
            $('#schedule_end').val(end.format('YYYY-MM-DD'));
            $('#datetimes').val(start.format('YYYY-MM-DD'));

            get_available_maids($('#schedule_start').val(),$('#schedule_end').val(),$('#interval').val(),'add');
            $("#timefrom").prop('disabled', false);

            if($('#datetimes').val() > curDate){
                $('#timefrom').timepicker({
                    minTime: { hour: 08, minute: 00 },
                    maxTime: { hour: 20, minute: 00 },
                    showLeadingZero: true
                }); 
          
            }else{
                $('#timefrom').timepicker({
                    minTime: { hour: h, minute: m },
                    maxTime: { hour: 20, minute: 00 },
                    showLeadingZero: true
                }); 
            }



            //alert('A date range was chosen: ' + start.format('YYYY-MM-DD hh:mm:ss') + ' to ' + end.format('YYYY-MM-DD hh:mm:ss'));
        }
    );



    //timepicker
    $('#timeto').timepicker({
        maxTime: { hour: 20, minute: 00 },
        showLeadingZero: true
    });
 

    $('#timefrom').focus(function(){

        if($('#datetimes').val() > curDate){
            $('#timefrom').timepicker({
                minTime: { hour: 08, minute: 00 },
                maxTime: { hour: 20, minute: 00 },
                showLeadingZero: true
            }); 
      
        }else{
            $('#timefrom').timepicker({
                minTime: { hour: h, minute: m },
                maxTime: { hour: 20, minute: 00 },
                showLeadingZero: true
            }); 
        }

    });

    $('#timefrom').on('change',function(){

        $("#timeto").prop('disabled', false);

        var startDate = $('#datetimes').val();
        var startTime = $('#timefrom').val();

        var date = new Date(startDate + ' ' + startTime);

        var newtimeto = moment(date).add(1, 'hours').format('HH:mm');
        var newdate = moment(date).add(1, 'hours');

        $('#timeto').val(newtimeto);
        var hour = newdate.hour();
        var minutes = newdate.minutes();

        var datetimefrom = moment(date).format('YYYY-MM-DD HH:mm:ss');
        $('#schedule_start').val(datetimefrom);

        tpMinMaxSetMinTime(hour,minutes);

    });


    // $('#timeto').on('change',function(){  
    //     getAmount($('#timefrom').val(),$('#timeto').val());
    // });

    $('#timeto').on('change',function(){  
        var calendarDate   = $('#datetimes').val();
        var endDateandTime = new Date(calendarDate + ' ' + $('#timeto').val());
        var datetimeto = moment(endDateandTime).format('YYYY-MM-DD HH:mm:ss');
        $('#schedule_end').val(datetimeto);
        get_available_maids($('#schedule_start').val(),datetimeto,$('#interval').val(),'add');
        getAmount($('#timefrom').val(),$('#timeto').val());
    });

 
    function tpMinMaxSetMinTime( hours, minutes ) {
        $('#timeto').timepicker('option', { minTime: { hour: hours, minute: minutes} });
        var startDate = $('#datetimes').val();
        var date = new Date(startDate + ' ' +  $('#timeto').val());
        var datetimeto = moment(date).format('YYYY-MM-DD HH:mm:ss');
        $('#schedule_end').val(datetimeto);

        getAmount($('#timefrom').val(),$('#timeto').val());
        get_available_maids($('#schedule_start').val(),datetimeto,$('#interval').val(),'add');
    }

    //fullcalendar
    $('#calendar').fullCalendar({
        height: 'auto',
        schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
        now: curDate,
        minTime: '08:00:00', 
        maxTime: '21:00:00', 
        eventTextColor: '#000000',
            resourceAreaWidth: '20%',
            editable: false, 
            aspectRatio: 1.8,
            scrollTime: '00:00', 
                header: {
                    left: 'today prev,next',
                    center: 'title',
                    right: 'agendaWeek,month'
                },
        defaultView: 'agendaWeek',
        resourceColumns: [
            {
              labelText: 'Maid',
              field: 'maid'
            },
            {
              labelText: 'Driver',
              field: 'driver'
            }
        ],
        resources: {
            url: '{{route('getJsonRequestResource')}}',
            type: 'POST', 
            data: {
              _token : $('meta[name="csrf-token"]').attr('content')  
            },
            error: function() {
              $('#script-warning').show();
            }
        },


        events: { 
            url: '{{route('getJsonRequestEvent')}}',
            type: 'POST',
            data: function() {

                    if($('#dropdownarea').val()== 0 && $('#dropdownsub').val() == 0 ){
                        return {
                            _token : $('meta[name="csrf-token"]').attr('content'),
                        }; 
                    }else{
                        return {
                            _token : $('meta[name="csrf-token"]').attr('content'),
                            area:    $('#dropdownarea').val(),
                            subarea: $('#dropdownsub').val()
                        }; 
                    }
              
                },
            error: function() {
              $('#script-warning').show();
            }
        },

        eventMouseover: function (returnD, event, view) {

            tooltip = '<div class="tooltiptopicevent" style="width:auto;height:auto;background:#e7e7e7;position:absolute;z-index:10001;padding:10px 10px 10px 10px ;  line-height: 200%;">' + 
            // 'Service Number : '  + returnD.title + '</br>' + 
            'Service Start : ' + returnD.start.format('M/DD hh:mm A') + '</br>' + 
            'Service End : ' + returnD.end.format('M/DD hh:mm A') + '</br>' +
            'Customer : '  + returnD.customer + '</br>' +   
            'Customer Address: '  + returnD.customer_address + '</br>' +          
            '</div>';
         
         

            $("body").append(tooltip);
            $(this).mouseover(function (e) {
                $(this).css('z-index', 10000);
                $('.tooltiptopicevent').fadeIn('500');
                $('.tooltiptopicevent').fadeTo('10', 1.9);
            }).mousemove(function (e) {
                $('.tooltiptopicevent').css('top', e.pageY + 10);
                $('.tooltiptopicevent').css('left', e.pageX + 20);
            });


        },

        eventMouseout: function (data, event, view) {
            $(this).css('z-index', 8);
            $('.tooltiptopicevent').remove();

        },


        eventClick: function(data, jsEvent, view) { 
            $('#myModal2').modal('show');
  
            myEvent = data;
            $.ajax({
                url: '{{route('maids_itinerary_update')}}',
                type: 'POST',
                data: {
                    _token : $('meta[name="csrf-token"]').attr('content'),
                    action : 'view',
                    id: myEvent.id
                },
                success: function(data){  
                    $('#id_edit').val(myEvent.id);
                    // $("#maids_id_edit").prop('disabled', true);
                    // $("#driver_id_edit").prop('disabled', true);
                    // $("#customer_id_edit").prop('disabled', true);
                    // $("#area_edit").prop('disabled', true);
                    // $("#subarea_edit").prop('disabled', true);
                    // $("#useaddress_edit").prop('disabled', true);

                    var newd = JSON.parse(data);

                    $('#to_pay_view_edit').html(newd.payment_sub+' AED');
                    $('#to_pay_edit').val(newd.payment_sub);
                    $('#arealabel_edit').html(newd.area);
                    $('#subarealabel_edit').html(newd.sub_area);

                    var formatStartDate =    moment(newd.schedule_start);
                    var jsDateStart = formatStartDate.toDate()

                    var formatEndDate =    moment(newd.schedule_end);
                    var jsDateEnd = formatEndDate.toDate()

                    var formatStartDate = $.format.date(jsDateStart, 'yyyy-MM-dd');

                    //initialize time 
                    var starttime = $.format.date(jsDateStart, 'HH:mm:ss');
                    var endtime = $.format.date(jsDateEnd, 'HH:mm:ss');

                    $('#timefrom_edit').val(starttime);
                    $('#timeto_edit').val(endtime);

                    //initialize date for maid list
                    var datefrom = new Date(formatStartDate + ' ' +  starttime);
                    var dateto = new Date(formatStartDate + ' ' +  endtime);
                    var datetimefrom = moment(datefrom).format('YYYY-MM-DD HH:mm:ss');
                    var datetimeto = moment(dateto).format('YYYY-MM-DD HH:mm:ss');
                    get_available_maids(datetimefrom,datetimeto,$('#interval_edit').val(),'edit');


                    var edith = jsDateStart.getHours();
                    var editm = jsDateStart.getMinutes();

                    

                    if($('#datetimes_edit').val() > curDate){
                        $('#timefrom_edit').timepicker({
                            minTime: { hour: 08, minute: 00 },
                            maxTime: { hour: 20, minute: 00 },
                            showLeadingZero: true
                        }); 
                  
                    }else{
                        $('#timefrom_edit').timepicker({
                            minTime: { hour: edith, minute: editm },
                            maxTime: { hour: 20, minute: 00 },
                            showLeadingZero: true
                        }); 
                    }

                    //datepicker
                    $('input[name="datetimes_edit"]').daterangepicker(
                        { 
                        autoUpdateInput: false,
                        timePicker: false,
                        singleDatePicker: true,  
                        minDate: jsDateStart,
                        startDate: jsDateStart,
                        endDate: moment().startOf('hour').add(36, 'hour'),
                            locale: {

                                format: 'YYYY-MM-DD'
                            }
                        },
                        function(start, end, label) {
                            $('#schedule_start_edit').val(start.format('YYYY-MM-DD'));
                            $('#schedule_end_edit').val(end.format('YYYY-MM-DD'));
                            $('#datetimes_edit').val(start.format('YYYY-MM-DD'));

                        }
                    );


                    //timepicker
                    $('#timeto_edit').timepicker({
                        maxTime: { hour: 20, minute: 00 },
                        showLeadingZero: true
                    });
                 

                    $('#timefrom_edit').click(function(){

                        if($('#datetimes_edit').val() > curDate){
                            $('#timefrom_edit').timepicker({
                                minTime: { hour: 08, minute: 00 },
                                maxTime: { hour: 20, minute: 00 },
                                showLeadingZero: true
                            }); 
                      
                        }else{
                            $('#timefrom_edit').timepicker({
                                minTime: { hour: edith, minute: editm },
                                maxTime: { hour: 20, minute: 00 },
                                showLeadingZero: true
                            }); 
                        }

                    });

     

                    $('#timefrom_edit').on('change',function(){

                        $("#timeto_edit").prop('disabled', false);

                        var startDate = $('#datetimes_edit').val();
                        var startTime = $('#timefrom_edit').val();

                        var date = new Date(startDate + ' ' + startTime);

                        var newtimeto = moment(date).add(1, 'hours').format('HH:mm');
                        var newdate = moment(date).add(1, 'hours');

                        $('#timeto_edit').val(newtimeto);
                        var hour = newdate.hour();
                        var minutes = newdate.minutes();

                        var datetimefrom = moment(date).format('YYYY-MM-DD HH:mm:ss');
                        $('#schedule_start_edit').val(datetimefrom);

                        tpMinMaxSetMinTimeEdit(hour,minutes);

                    });

                 
                    function tpMinMaxSetMinTimeEdit( hours, minutes ) {
                        $('#timeto_edit').timepicker('option', { minTime: { hour: hours, minute: minutes} });
                        var startDate = $('#datetimes_edit').val();
                        var date = new Date(startDate + ' ' +  $('#timeto_edit').val());
                        var datetimeto = moment(date).format('YYYY-MM-DD HH:mm:ss');
                        $('#schedule_end_edit').val(datetimeto);

                        get_available_maids($('#schedule_start_edit').val(),datetimeto,$('#interval_edit').val(),'edit');
                   
                    }

                    $('#timeto_edit').on('change',function(){
                        var timeto = $(this).val();
                        var startDate = $('#datetimes_edit').val();
                        var date = new Date(startDate + ' ' +  timeto);
                        var datetimeto = moment(date).format('YYYY-MM-DD HH:mm:ss');
                        $('#schedule_end_edit').val(datetimeto);
                    });

                    getAreaSubArea(newd.area_id);
        

                    if(newd.customer_address_id==null){
                        $('#useaddress_edit').prop('checked', true);
                        $("#customer_address_edit").prop('disabled', true);
                        $('#customer_address_edit').val(newd.address);
                    }else{
                        $('#customer_address_edit').val(newd.customer_address);
                    }
                  
                        $('#servicenumber_edit').html(newd.schedule_code);
                        $('#datetimes_edit').val(formatStartDate);
                        $('#new_datetimes_edit').val(formatStartDate);
                        $('#schedule_start_edit').val(datetimefrom);
                        $('#schedule_end_edit').val(datetimeto);

                        // $('#maids_id_edit').val(newd.fullname);
                        // alert(newd.fullname);
                        $('#maids_id_edit').append('<option value="'+newd.maids_id+'">'+newd.fullname+'</option>');

                        $('#schedule_color_edit').val(newd.schedule_color);
                        $('#driver_id_edit').val(newd.driver_id);
                        $('#customer_id_edit').val(newd.customer_id);
                        $('#completed_edit').val(newd.completed);
                        $('#payment_edit').val(newd.payment);

                        $('#area_edit').val(newd.area_id);
                        $('#subarea_edit').val(newd.sub_area_id);
        



                        $('#useaddress_edit').click(function(){

                            if ($('input[name=useaddress_edit]').prop('checked')) {
                                $('#use_edit').val('yes');
                                $("#customer_address_edit").prop('disabled', true);
                                $("#customer_address_edit").val(newd.address);
                               $("#customer_address_hide2").hide();
                         
                            }else{
                                $('#use_edit').val('no');
                                $("#customer_address_edit").prop('disabled', false);
                                $("#customer_address_edit").val(newd.customer_address);
                                $("#customer_address_hide2").show();
                            }
                        });

                }

            });

        }


    });

    $('#datetimes').click(function(){
        $('#maids_id').html("");
        $("#maids_id").prop("disabled", true);
    });

    $('#datetimes_edit').click(function(){
        // $('#maids_id_edit').html("");
        // $("#maids_id_edit").prop("disabled", true);
    });


    //add task button
    $('#addmaidsched').on('click',function(){
 
        $('#areavalue').val($('#dropdownarea').val());
        $('#subareavalue').val($('#dropdownsub').val());

        $('#useaddress').on('change',function(){

            if ($('input[name=useaddress]').prop('checked')) {
                $('#use').val('yes');
                $("#customer_address_hide, #customer_address_hide2").hide();
                // $("#customer_address").prop('disabled', true);
    
         
            }else{
                $('#use').val('no');
                $("#customer_address_hide").show();
                // $("#customer_address").prop('disabled', false);
       
          
            }
        });


        $.ajax({
            url: '{{route('getCountPerAreaMaids')}}',
            type: 'POST',
            data: {

                _token  : $('meta[name="csrf-token"]').attr('content'),
                area_id : $('#area').val(),
                subarea_id : $('#subarea').val()

            },    
            success: function(data){
                $.each(JSON.parse(data),function(idx,val){
            
                    $('#areaval').html(val.area);
                    $('#subareaval').html(val.sub_area);

                });      
            }
        });


        if( $('#dropdownarea').val()==0 || $('#dropdownsub').val()==0){

            foralert('error','Please select Area & Sub Area first!')

          
        }else{
            $('#myModal').modal('show');
        }



    });

    //modal close
    $('#myModal, #myModal2').on('hidden.bs.modal', function () {
        $('#areaval').html("");
        $('#subareaval').html("");
        $('#calendar').fullCalendar('refetchEvents');
        location.reload();
    });

    //avail
    $('#interval').on('change', function() {
        get_available_maids($('#schedule_start').val(),$('#schedule_end').val(),$('#interval').val(),'add');

    });

     $('#interval_edit').on('change', function() {
        get_available_maids($('#schedule_start_edit').val(),$('#schedule_end_edit').val(),$('#interval_edit').val(),'edit');
    });
    
    
    //modal save edit
    $('#save_edit').click(function(){
        // alert($('#id_edit').val());
        if($('#to_pay_edit').val() != $('#payment_edit').val() ){
                foralert('error','Please enter exact amount!');

        }else{
            $.ajax({
                url: '{{route('maids_itinerary_update')}}',
                type: 'POST',
                data: 
                {
                    action: 'edit',
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    id: $('#id_edit').val(),
                    area_id: $('#area_edit').val(),
                    sub_area_id: $('#subarea_edit').val(),
                    schedule_start: $('#schedule_start_edit').val(),
                    schedule_end: $('#schedule_end_edit').val(),
                    customer_id: $('#customer_id_edit').val(),
                    customer_address: $("#customer_address_edit").val(),
                    maids_id: $('#maids_id_edit').val(),
                    driver_id: $('#driver_id_edit').val(),
                    schedule_color: $('#schedule_color_edit').val(),
                    completed: $('#completed_edit').val(),
                    payment: $('#payment_edit').val(),
                    use: $('#use_edit').val()
                },
                success: function(data){
              
                    // $('body').html(data);
                    $('#myModal2').modal('hide');

                  
                } 
     
            });
        }



    });


    $('#cancel').click(function() {
        location.reload();
    });


    //click option list add
    $('#area').on('click','li',function(){
        var area = $(this).val();
        $("#subdisable").prop("disabled", false);
        $('#dropdownarea').val(area);
        //process done
        // procDoneTask();
        getlabel(area,'forarea');
        //append select text
        $('#subarealabel').html("Select");
        //subarea dropdown list
        getAreaSubArea(area);
        $('#calendar').fullCalendar('refetchEvents');
    });

    //click option list edit
    $('#area_edit').on('click','li',function(){
        var area = $(this).val();
        $("#subdisable_edit").prop("disabled", false);
        // $('#dropdownarea_edit').val(area);
        $('#area_edit').val(area);
        // procDoneTask();
        getlabel(area,'forarea');
        //append select text
        $('#subarealabel_edit').html("Select");
        //subarea dropdown list
        getAreaSubArea(area);
        $('#calendar').fullCalendar('refetchEvents');
    });

    //subarea dropdown list
    $('#subarea').on('click','li',function(){
        var subarea = $(this).val();
        $('#dropdownsub').val(subarea);
        getlabel(subarea,'forsubarea');
        $('#subarealabel').html($(this).text());
        $('#calendar').fullCalendar('refetchEvents');  
    });

    //disable subarea if area has empty val
    $("#subdisable").click(function(){
        if($('#dropdownarea').val()==''){
            foralert('error','Please select Area first!');
            $("#subdisable").prop("disabled", true);
        }
     });

    //subarea dropdown list edit
    $('#subarea_edit').on('click','li',function(){
        var subarea = $(this).val();
        $('#subarea_edit').val(subarea);
        getlabel(subarea,'forsubarea');
        $('#subarealabel_edit').html($(this).text());

        // getCountPerSubMaids(subarea,'add');

        $('#calendar').fullCalendar('refetchEvents');  
    });



    function get_available_maids(schedule_start,schedule_end,interval,actionFor){

        if(actionFor =='add'){
            $.ajax({
                url: '{{route('get_available_maids')}}',
                type: 'POST',
                data: {
                    _token : $('meta[name="csrf-token"]').attr('content'),
                    schedule_start: schedule_start,
                    schedule_end: schedule_end,
                    interval: interval
                },
                success: function(returnD){
                    // $('body').html(returnD);
                    $("#maids_id").html("");
                    $("#maids_id").prop("disabled", false);
                    var ctr = 1;
                    $.each(JSON.parse(returnD),function(idx,val){
                        $('#maids_id').append('<option value="'+val.id+'">'+val.fullname+'</option>');
                        $('#availmaids').html(ctr+ " Maids are available");
                        ctr++;
                    });
                 
                }
            });
        }else{

            $.ajax({
                url: '{{route('get_available_maids')}}',
                type: 'POST',
                data: {
                    _token : $('meta[name="csrf-token"]').attr('content'),
                    schedule_start: schedule_start,
                    schedule_end: schedule_end,
                    interval: interval
                },
                success: function(returnD){
                    // $('body').html(returnD);
                    // $("#maids_id_edit").html("");
                    // $("#maids_id_edit").prop("disabled", false);
                    var ctr = 1;
                    $.each(JSON.parse(returnD),function(idx,val){
                       $('#maids_id_edit').append('<option value="'+val.id+'">'+val.fullname+'</option>');
                        $('#availmaids_edit').html(ctr+ " Maids are available");
                        ctr++;
                    });
                 
                }
            });
        }
    }

    //dropdown count and area list 
    function getAreaAndCount(){
        $.ajax({
            url: '{{route('getAreaAndCount')}}',
            type: 'POST',
            data: {

                _token  : $('meta[name="csrf-token"]').attr('content')
            },    
            success: function(data){  
                var sum=0;
                $.each(JSON.parse(data), function( idx, val ) {
                    sum+= Number(val.count);
                    $('#allareas').html(sum);
                    $('#allareas_edit').html(sum);
                    if(val.count>0){
                        $("#area").append('<li value="'+val.id+'"><a href="#">'
                        +val.area+'<span class="label label-rouded label-danger pull-right">'+val.count +'</span></a> </li>');
                        $("#area_edit").append('<li value="'+val.id+'"><a href="#">'
                        +val.area+'<span class="label label-rouded label-danger pull-right">'+val.count +'</span></a> </li>');
                    }else{
                        $("#area").append('<li value="'+val.id+'"><a href="#">'+val.area+'</a> </li>');
                        $("#area_edit").append('<li value="'+val.id+'"><a href="#">'+val.area+'</a> </li>');
                    }
       

                });

               
            }
        });
    }
    

    //dropdown count and subarea list 
    function getAreaSubArea(area_id){
        $('#subarea').html('');
        $('#subarea_edit').html('');
        $('#dropdownsub').val(0);
        $.ajax({
            url: '{{route('getAreaSubArea')}}',
            type: 'POST',
            data: {

                _token  : $('meta[name="csrf-token"]').attr('content'),
                area_id : area_id
            },    
            success: function(data){  
  
                $.each(JSON.parse(data), function( idx, val ) {
                    if(val.count>0){
                        $("#subarea").append('<li value="'+val.id+'"><a href="#">'
                        +val.sub_area+'<span id="spanlabel" class="label label-rouded label-danger pull-right">'
                        + val.count +'</span></a> </li>');
                        $("#subarea_edit").append('<li value="'+val.id+'"><a href="#">'
                        +val.sub_area+'<span id="spanlabel" class="label label-rouded label-danger pull-right">'
                        + val.count +'</span></a> </li>');
                    }else{
                        $("#subarea").append('<li value="'+val.id+'"><a href="#">'+val.sub_area+'</a> </li>');
                        $("#subarea_edit").append('<li value="'+val.id+'"><a href="#">'+val.sub_area+'</a> </li>');
                    }
                 
                });

               
            }
        });
    }

    //get label
    function getlabel(id,actionfor){

        if(id==0){
            $('#arealabel').html('All');
            $('#subarealabel').html('All');
        }else{
            $.ajax({
                url: '{{route('getlabel')}}',
                type: 'POST',
                data: {

                    _token  : $('meta[name="csrf-token"]').attr('content'),
                    action :actionfor,
                    id : id
                },    
                success: function(data){  
                    var text = JSON.parse(data)
                    $('#areaval').html(text.area);
                    $('#subareaval').html(text.sub_area);
                    $('#arealabel').html(text.area);
                    $('#subarealabel').html(text.sub_area);

                    $('#areaval_edit').html(text.area);
                    $('#subareaval_edit').html(text.sub_area);
                    $('#arealabel_edit').html(text.area);
                    $('#subarealabel_edit').html(text.sub_area);

                }
            }); 
        }


    }

    // function procDoneTask(){
    //     $.ajax({
    //         url: '{{route('procDoneTask')}}',
    //         type: 'POST',
    //         data: {
    //             _token  : $('meta[name="csrf-token"]').attr('content')
    //         }
    //     }); 
    // }

    function getAmount(from,to){
 
        $.ajax({
            url: '{{ route('getAmount') }}',
            type: 'POST',
            data: {
                _token  : $('meta[name="csrf-token"]').attr('content'),
                timefrom: from,
                timeto: to
            },
            success: function (data){


            var parsed = JSON.parse(data);

                $('#to_pay').val(parsed.amount);
                $('#to_pay_view').val(parsed.view+' '+'AED');
                
                // $('#to_pay').val(data);
                // $('#to_pay_view').val(data+' '+'AED');
            }
        }); 
    }

    function foralert(action,message){
        if(action=='success'){
            toastr.success(message,'Success',{
                "positionClass": "toast-top-right",
                timeOut: 5000,
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut",
                "tapToDismiss": false
            });
        }else{
            toastr.error(message,'Attention!',{
                "positionClass": "toast-top-right",
                timeOut: 5000,
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut",
                "tapToDismiss": false

            });
        }

    }  



  });

</script>
@endsection