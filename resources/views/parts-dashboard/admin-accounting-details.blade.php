

@extends('layouts.app')



@section('content-styles')

    <!-- Custom CSS -->



    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />

    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">

    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('dashtemplate/css/lib//daterangepicker/daterangepicker.css') }}" />

    <link rel="stylesheet" type="text/css" href="{{ asset('dashtemplate/button/buttonLoader.css') }}" />

    <style type="text/css">

    .input-xl {
        font-size: 1.5rem;
        height: 3.25rem;
        line-height: 3.25rem;
        max-width: 50%;
        border-radius: .25rem;
        background-color: #fff !important;
    }

    .btn:disabled {
        background-color: #6c757d !important;
        border: 1px solid #6c757d;
        color:  #fff !important;
        padding: 3px 10px;
        line-height: 13px;
        color: #ffffff;
        font-weight: 400;
        border-radius: 4px;
        font-size: 75%;
    }

    .btn-secondary {
        background-color: #6c757d !important;
        color:  #fff !important;
    }

    .btn-primary{
        padding: 3px 10px;
        line-height: 13px;
        color: #ffffff;
        font-weight: 400;
        border-radius: 4px;
        font-size: 75%;
        background-color: #6352ce;
        border: 1px solid #6352ce;
    }

    .preloader {
        background: transparent;
    }
    .circular {
        height: 100px;
        width: 100px;
    }
    </style>

@endsection



@section('content')

<body class="fix-header fix-sidebar">



@include('layouts.dashboard.header')

        <div class="container-fluid app" id="content">
            <form method="GET" enctype="multipart/form-data" action="{{ url('admin/accounting/').'/'.$maids_schedule['id'].'/transaction-details/pdf' }}">
                @csrf
                <div class="row" style="padding-top: 20px;">

                    <div class="col-sm-12 col-md-3 form-group">
                       <select class="form-control" name="export" id="export" style="height: 42px !important;border-radius: 3px;">
                            <option value="1">PDF</option>
                            <option value="2">Excel</option>
                        </select>
                    </div>

                    <div class="col-sm-12 col-md-6 form-group">

                        <button type="submit" class="btn btn-secondary" id="export_send" style="border-radius: 3px !important;">
                          Export
                        </button>
                        @if( $sales_details['payment_sum'] != '0.00' && $sales_details['payment_status'] == '3' ||  $sales_details['payment_status'] == '1')
                        <button type="button" class="btn btn-secondary" id="send_email" style="border-radius: 3px !important;">
                            Email Receipt
                        </button>
                        @endif
                    </div>

                </div>
            </form>

                <div class="row form-body card" style="margin-top: 0px !important;">

                    <div class="col-sm-12 col-md-12">

                        <div class="panel-body">

                            <div class="row">

                                <div class="col-md-12" style="margin-bottom: -10px;">

                                <div class="text-center"><img src="{{ asset('img/logoblue.png')}}" alt="homepage" class="dark-logo" width="220"></div>

                                    <div class="pull-left" style="padding-top: 15px;">

                                        <h4 style="margin-bottom:0px" class="text-upper font-bold">Customer: 

                                            <span class="text-primary text-uppercase">{{  $maids_schedule->getCustomer['fullname'] }}</span>

                                        </h4>

                                    </div>

                                    <div class="p-15 bg-light-gray clearfix" id="employee_detals" style="margin-bottom:20px">



                                            @if($sales_details['payment_sum'] == '0.00' && $sales_details['payment_status'] == '0')

                                                <span style="font-size:15px;padding-left:20px;padding-right:20px;"

                                                    class="pull-right label label-danger">Unpaid</span>

                                            @elseif( $sales_details['payment_sum'] != '0.00' && $sales_details['payment_status'] == '3' )

                                                <span style="font-size:15px;padding-left:20px;padding-right:20px;"

                                                    class="pull-right label label-warning">Partialy Paid</span>

                                            @elseif( $sales_details['payment_status'] == '1' )

                                                 <span style="font-size:15px;padding-left:20px;padding-right:20px;"

                                                    class="pull-right label label-success">Paid</span>

                                            @endif



                                 

                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-12 ">

                   

                                    @if( $sales_details['payment_status'] == '1' )

                                        <span class="pull-right">Full payment received on 

                                            {{ $sales_details['updated_at']->format('l j F Y') }}

                                        </span>

                                    @endif

                         

                                  

                                    <h3 class="font-bold no-margin">Service No. <span class="text-primary">{{ $maids_schedule['schedule_code'] }}</span></h3>

                                    <div class="table-responsive" >

                                        <table class="table table-bordered " id="accounting">

                                            <thead>

                                                <tr>

                                                    <th>Description</th>
                                                    <th>Status</th>
                                                    <th>Payment Status</th>
                                                    <th>Service Date</th>
                                                    <th>Service Time</th>
                                                    <th>Price</th>
                                                    <th>VAT</th>
                                                    <th>Action</th>

                                                </tr>

                                            </thead>

                                            <tbody>

                                                @foreach($maids_relation as $data_list)

                                                <tr>

                                                    <td>

                                                        Cleaning <br>

                                                        <small>by 

                                         

                                                        <span class="text-primary">{{ strtoupper($data_list->getMaid['fullname']) }}</span>

                                                    

                                                        </small>

                                                    </td>

                                                    @if($data_list->getSaleSummary['cancel'] == "")
                                                      <td> <span class="label label-success">Confirmed</span> </td>
                                                    @else
                                                        <td> <span class="label label-warning">Cancelled</span> </td>
                                                    @endif

                                                    @if($data_list->getSaleSummary['is_paid'] == '1')
                                                        <td> <span class="label label-success">Paid</span> </td>
                                                    @else
                                                        <td> <span class="label label-danger">Unpaid</span> </td>
                                                    @endif

                                                    <td>  {{ $data_list->schedule_start->format('l j F Y') }} </td>

                                                    <td>
                                                        {{ strtoupper($data_list->schedule_start->format('H:i:s')) }} 
                                                        - 
                                                        {{ strtoupper($data_list->schedule_end->format('H:i:s')) }} 
                                                    </td>

                                                    <td>{{ $data_list->getSaleSummary['price'] }}</td>
                                                    <td>{{ $data_list->getSaleSummary['payment_vat'] }}</td>
                                                    <td>
                                                        @if($data_list->getSaleSummary['is_paid'] != '1' && $data_list->getSaleSummary['cancel'] != '1')
                                                        <button data-id="{{ $data_list->getSaleSummary['id'] }}" type="button" 
                                                            class="btn btn-primary paynow">ADD</button>
                                                        @endif
                                                    </td>

                                                </tr>

                                                @endforeach

                                            </tbody>

                                        </table>

                                    </div>

                                </div>



                                <div class="col-md-6">

                                    <table class="table">

                                        <tbody>

                                            <tr>

                                                <td class="no-border">Subtotal</td>
                                                    <td class="text-right no-border">{{ $sales_details['payment_sub_sum'] }}</td>

                                            </tr>

                                            <tr>

                                                <td>Discount</td>
                                                    <td class="text-right">{{ $sales_details['payment_disc_sum'] }}</td>

                                            </tr>

                                            <tr>
                                                <td>VAT(5%)</td>
                                                    <td class="text-right">{{ $sales_details['payment_vat_sum'] }}</td>
                                            </tr>

                                            <tr>

                                                <th class="font-bold text-15">Total</th>
                                                    <th class="font-bold text-right text-15">
                                                        {{ ($sales_details['payment_total_sum'])  }}
                                                    </th>

                                            </tr>

                                            <tr>

                                                <th class="font-bold text-15">Balance</th>

                                                <th class="font-bold text-right text-15 text-danger">

                                                {{ number_format($sales_details['payment_total_sum'] - $sales_details['payment_sum'], 2, '.', ',')  }}

                                                </th>

                                            </tr>

                                        </tbody>

                                    </table>

                                </div>

                            </div>

                        </div>

                    </div>
            </div>



<input type="hidden" value="{{ $sales_details['service_number'] }}" name="schedulecode" id="schedulecode">
<input type="hidden" value="{{ $sales_details['id'] }}" name="sum_details_id" id="sum_details_id">
<input type="hidden" name="to_pay_sum_id"  id="to_pay_sum_id">
<input type="hidden" name="id_arr[]"  id="id_arr">
<input type="hidden" name="customer_name" id="customer_name" value="{{ $maids_schedule->getCustomer['fullname'] }}">
<input type="hidden" name="customer_email" id="customer_email" value="{{ $maids_schedule->getCustomer['email'] }}">
<input type="hidden" name="transaction_id" id="transaction_id" value="{{ $maids_schedule->id }}">

            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="text-center m-t-15">Payment Type:</label>
                                <select class="form-control" name="export" id="export" style="height: 42px !important; width: 100%;border-radius: 3px;">
                                    <option value="1">Cash</option>
                                    <option value="2">Credit Card</option>
                                </select>

                            </div> 
                        </div>
                    </div>

                    <div class="row ">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="text-center m-t-15">Payment Amount:</label>
                                    <div class="form-inline">
                                        <input name="payment" id="payment" value="0.00" 
                                                type="text" class="mr-1 form-control input-xl text-center autonumeric 
                                                font-bold text-danger" readonly>
                                        <button class="mr-1 btn btn-secondary" style="height: 3.25rem;" id="clear">Clear</button>
                                        @if(count($maids_relation) > 1 && $sales_details['payment_status'] != '1' )
                                        <button class="mr-1 btn btn-secondary" style="height: 3.25rem;" id="payall">Pay All</button>
                                        @endif
                                    </div>
                            </div>

                            <div class="form-group">
                                <div class="form-inline">
                                    @if( $sales_details['payment_status'] != '1' )
                                        <button class="mr-1 btn btn-secondary btn-lg" id="btnpaynow"style="height: 3.25rem;" >Pay Now</button>
                                    @endif
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>




    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Are you sure you want to save payment ?</h4>
                </div>
                <div class="modal-body">
                    <label class="control-label">Payment Note:</label>
                    <textarea class="form-control" rows="4" cols="50" name="payment_note" id="payment_note" 
                    style="border-radius: 2px;"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-sm btn-danger" style="color:white" data-dismiss="modal" style="color:black;">No</button>
                    <button type="button" id="save" class="btn-sm btn-success">Yes</button>
                </div>
            </div>
        </div>
    </div>

@include('layouts.dashboard.footer')



                                                                                                                                                                                                                                                                                                                                                                                                              

</body>



@endsection



@section('content-scripts')



<script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>

<!-- Bootstrap tether Core JavaScript -->

<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>

<!-- slimscrollbar scrollbar JavaScript -->

<script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>

<!--Menu sidebar -->

<script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>

<!--stickey kit -->

<script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>

<!--Custom JavaScript -->



<!-- scripit init-->

<script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>



<!-- datatables -->

<script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>



<!-- highcharts -->

<script src="https://code.highcharts.com/highcharts.js"></script>

<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script src="https://code.highcharts.com/modules/export-data.js"></script>



<!-- vue js -->

<script src="{{ asset('vue/dist/vue.js') }}"></script>

<script src="{{ asset('vue/dist/vue.min.js') }}"></script>



<script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>



<!-- moment -->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script src="{{ asset('dashtemplate/button/jquery.buttonLoader.js') }}"></script>
<script src="{{ asset('dashtemplate/button/jquery.buttonLoader.min.js') }}"></script>


<script type="text/javascript">

$(document).ready(function(){

    $('#accounting').DataTable();
    $("#payment").on("keypress keyup blur",function (event) {

            //this.value = this.value.replace(/[^0-9\.]/g,'');

    $(this).val($(this).val().replace(/[^0-9\.]/g,''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }

    });



    $('#btnpaynow').on('click', function(){

        if($('#payment').val() == '0.00'){
            foralert('warning','Please add item to pay.');
        }else{
            $('#myModal').modal('show'); 
        }
    });

    $('#save').on('click', function(){
        $('#myModal').modal('hide');
                   $('.preloader').show(); 
        $.ajax({

            url: '{{ route('checkBalance') }}',
            type: 'POST',
            data: 
            {
                _token: $('meta[name="csrf-token"]').attr('content'),
                sum_details_id: $('#sum_details_id').val()
            },

            success: function(data){

                var parsed = JSON.parse(data);

                if(parsed.partial == 'yes'){

                    var payment = parseFloat($('#payment').val());
                    var paid = parseFloat(parsed.total_payment);
                    var totalamount = (payment+paid);

                    if( parseFloat(totalamount) >  parseFloat(parsed.topay)){
                       foralert('warning','The payment is greater than the current balance');
                    }else{

                        processPayment();  
                    }

                }else{

                    if($('#payment').val() == '0.00' ){
                        foralert('error','Please add a item');
                    }else if( $('#payment').val() < parsed.topay && $('#payment').val() != '0.00' ){
                        foralert('error','Partial payment is not allowed on this transaction');
                    }else if( parsed.topay == parsed.total_payment ){
                        foralert('warning','No payment is needed');
                    }else{

                        var payment = parseFloat($('#payment').val());
                        var paid = parseFloat(parsed.total_payment);
                        var totalamount = (payment+paid);

                        if( parseFloat(totalamount) >  parseFloat(parsed.topay)){
                           foralert('warning','The payment is greater than the current balance');
                        }else{
                 
                            processPayment();  
                        }
                    }
                }
            } 
        });
    });



    function processPayment(){
        $('.preloader').show(); 
        $.ajax({
            url: '{{ route('savepayment') }}',
            type: 'POST',
            data: 
            {
                _token: $('meta[name="csrf-token"]').attr('content'),
                service_number: $('#schedulecode').val(),
                sum_details_id: $('#sum_details_id').val(),
                payment: $('#payment').val(),
                to_pay_summary: storedFunc.ShowArrID(),
                to_pay_notes: $('#payment_note').val(),
                transaction_id: $('#transaction_id').val()

            },
            success: function(data){
                $('.preloader').hide(); 
                 foralert('success','Payment accepted');
            } 
        });

    }





    function foralert(action,message){

        if(action=='success'){

            toastr.success(message,'Success!',{
                "positionClass": "toast-top-right",
                timeOut: 2500,
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut",
                "tapToDismiss": false,
                    onHidden: function () {
                       location.reload();
                    }
            });

        }else if(action=='warning'){

            toastr.warning(message,'Attention!',{
                "positionClass": "toast-top-right",
                timeOut: 3000,
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut",
                "tapToDismiss": false,
            })

        }else{

            toastr.error(message,'Attention!',{
                "positionClass": "toast-top-right",
                timeOut: 3000,
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut",
                "tapToDismiss": false
            });

        }
    } 

    $('#export_send').on('click', function(){
        if($('#export').val()=='1'){
            $('.preloader').show(); 
        }
    });

    var storedFunc = (function() {
        var totalAmount = 0;
        var newval = 0;
        var ArrID = [];
        return {
            ShowArrID: function(){
                return ArrID;
            },
            callFunc: function() {
                var newTotal = newval += totalAmount;
                $('#payment').val(parseFloat(newTotal).toFixed(2));
            },
            SumValues: function(newVar) {
                totalAmount = newVar;
            },

            StoreArrID: function(newVar){
                ArrID.push(newVar);
            }
        };
    })();


    $('#clear').on('click', function(){
        location.reload();
        // $('.paynow').removeClass( "btn-success" ).addClass( "btn-primary" );
        // storedFunc.SumValues(parseFloat(0.00));
        // $('#payment').val('0.00');
        // $('.paynow').prop('disabled', false);
    });

    $('#payall').on('click', function(){
        var totalInAmount = 0;
        var newTotalInAmount = 0;
        $.ajax({
            url: '{{ route('payAllSumId') }}',
            type: 'POST',
            data: 
            {
                _token: $('meta[name="csrf-token"]').attr('content'),
                service_number: $('#schedulecode').val()
            },
            success: function(data){
                var parsed = JSON.parse(data);              
                $.each(parsed, function(index,value){
                    // $('.paynow').prop('disabled', true);
                    // $('.paynow').removeClass( "btn-primary" ).addClass( "btn-success" );
                    totalInAmount += +value.payment_sub;
                    storedFunc.StoreArrID(value.id);
                });
                $('#payment').val(parseFloat(totalInAmount).toFixed(2));
            }

        });
    });

    $('#accounting tbody').on('click','.paynow', function(){

        $(this).prop('disabled', true);
        $(this).removeClass( "btn-primary" ).addClass( "btn-secondary" );
        $(this).html('ADDED');
        var summary_id = $(this).data('id');
        $.ajax({
            url: '{{ route('payPerSumId') }}',
            type: 'POST',
            data: 
            {
                _token: $('meta[name="csrf-token"]').attr('content'),
                sum_id: summary_id
            },
            success: function(data){
                var parsed = JSON.parse(data);
                $('#to_pay_sum_id').val(parsed.id);
                storedFunc.StoreArrID(parsed.id);
                storedFunc.SumValues(parseFloat(parsed.payment_sub));
                storedFunc.callFunc();
            }

        });

    });

    $('#send_email').on('click',function(){

      var btn = $(this);
      $(btn).buttonLoader('start');
      btn.html('Sending..');


        $.ajax({
            url: '{{ route('sendEmailReceipt') }}',
            type: 'POST',
            data: 
            {
                _token: $('meta[name="csrf-token"]').attr('content'),
                transaction_id: $('#transaction_id').val()
            },
            success: function(data){
    
                $(btn).buttonLoader('stop');
                btn.html('Email Sent');
                setTimeout(function () {
                     btn.html('Email Receipt');
                }, 5000);

            }

        });
    });




});



</script>

@endsection