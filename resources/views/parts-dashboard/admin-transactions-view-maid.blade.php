
@extends('layouts.app')

@section('content-styles')
    <!-- Custom CSS -->

    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('dashtemplate/css/lib//daterangepicker/daterangepicker.css') }}" />

@endsection

@section('content')
<body class="fix-header fix-sidebar">

@include('layouts.dashboard.header')
    
        <div class="container-fluid app">
                <div class="row form-body card">
            
                    <div class="col-sm-12 col-md-12">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12" style="margin-bottom: -10px;">
                                    <div class="p-15 bg-light-gray clearfix" id="employee_detals" style="margin-bottom:20px">
                                       
                                            @if($data->getSaleSummary['payment_total'] != 0.00)
                                                <span style="font-size:15px;padding-left:20px;padding-right:20px;"
                                                    class="pull-right label label-success">Paid</span>
                                            @else
                                                <span style="font-size:15px;padding-left:20px;padding-right:20px;"
                                                    class="pull-right label label-danger">Unpaid</span>
                                            @endif
                                
                                        <div class="clearfix">
                                            <div class="clientlogo" style="background-size: 80px 80px;!important; height: 80px; width: 80px;">
                                                @if($data->getMaid['photo'] != '')
                                                <img src= " {{ asset('storage/maids').'/'.$data->getMaid['photo'] }}" 
                                                style="background-size: 80px 80px;!important; height: 80px; width: 80px;">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="pull-left" style="padding-top: 15px;">
                                            <h4 style="margin-bottom:0px" class="text-upper font-bold">{{ $data->getMaid['fullname'] }} </h4>
                                            <p style="margin-bottom:0px"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 ">
                                    @if($data->getSaleSummary['payment_total'] != 0.00)
                                        <span class="pull-right">The customer payment was received on 
                                            {{ $data->getSaleSummary['payment_date']->format('l j F Y') }}
                                        </span>
                                    @endif
                                  
                                    <h1 class="font-bold no-margin">SERVICE No. <span class="text-primary">{{ $data->schedule_code }}</span></h1>
                                    <div class="table-responsive" >
                                        <table class="table table-bordered ">
                                            <thead>
                                                <tr>
                                                    <th>Description</th>
                                                    <th>Service Time</th>
                                                    <th>Price</th>
                                   
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        Cleaning 
                                                    </td>
                                                    <td>
                                                        {{ strtoupper($data->schedule_start->format('H:i:s')) }} 
                                                        - 
                                                        {{ strtoupper($data->schedule_end->format('H:i:s')) }} 
                                                    </td>
                                                    <td>{{ $data->getSaleSummary['price'] }}</td>
                                        
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                  <!--               <div class="col-md-6">
                                    <form action="http://192.168.0.198/salon_ocean/sales_controller/send_email" method="post" accept-charset="utf-8">
                                    <input type="hidden" name="csrf_test_name" value="7bc241f83eb08fcb76b0e15f31ce5c5e">
                                    <div class="form-group">
                                        <label>Send Invoice</label>
                                        <div class="input-group">
                                            <input type="hidden" name="invoice_id" value="821">
                                            <input required="" name="cus_email" value="" type="email" placeholder="Email Address" class="form-control">
                                            <span class="input-group-btn">
                                                <button type="submit" class="btn btn-primary">Send Email</button>
                                            </span>
                                        </div>
                                    </div>
                                    </form>
                                </div> -->
                                <div class="col-md-6">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td class="no-border">Subtotal</td>
                                                <td class="text-right no-border">{{ $data->getSaleSummary['payment_sub'] }}</td>
                                            </tr>
                                            <tr>
                                                <td>Discount</td>
                                                <td class="text-right">{{ $data->getSaleSummary['payment_disc'] }}</td>
                                            </tr>
                                            <tr>
                                                <td>VAT(5%)</td>
                                                <td class="text-right">{{ $data->getSaleSummary['payment_vat'] }}</td>
                                            </tr>
                                            <tr>
                                                <th class="font-bold text-15">Total</th>
                                                <th class="font-bold text-right text-15">{{ $data->getSaleSummary['payment_sub'] + $data->getSaleSummary['payment_disc']}}</th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>

     <!--                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" style="margin-bottom:0px">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default btn-download dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            More Actions <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="http://192.168.0.198/salon_ocean/invoices/821/download" class="font-bold">Download</a></li>
                                                <li><a href="javascript:;" onclick="document.toPrint.printThePage();" class="font-bold">Print</a></li>
                                                <li><a href="javascript:;" data-status="2" id="btnvoid" class="text-red font-bold">Void</a></li>
                                                <li><a href="javascript:;" data-status="3" id="btnrefund" class="text-red font-bold">Refund</a></li>
                                            </ul>
                                        </div>
                                        <a href="{{ url('admin/transactions') }}" class="btn btn-primary">Close</a>
                                    </div>
                                    <form action="http://192.168.0.198/salon_ocean/invoices/udate_status" id="frmchangestatus" method="post" accept-charset="utf-8">
                                    <input type="hidden" name="csrf_test_name" value="7bc241f83eb08fcb76b0e15f31ce5c5e">
                                    <input type="hidden" name="invoice_id" value="821">
                                    <input type="hidden" name="invoice_status">
                                    </form>                     <iframe style="visibility: hidden;height:0px;margin:0px;overflow:hidden" name="toPrint" src="http://192.168.0.198/salon_ocean/invoices/821/print"></iframe>
                                </div>
                            </div> -->

                        </div>
                    </div>
                          
            </div>

                        <!-- Start Page Content -->

                </div>

@include('layouts.dashboard.footer')

                                                                                                                                                                                                                                                                                                                                                                                                              
</body>

@endsection

@section('content-scripts')

<script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>
<!--Menu sidebar -->
<script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>
<!--stickey kit -->
<script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
<!--Custom JavaScript -->

<!-- scripit init-->
<script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>

<!-- datatables -->
<script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>

<!-- highcharts -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<!-- vue js -->
<script src="{{ asset('vue/dist/vue.js') }}"></script>
<script src="{{ asset('vue/dist/vue.min.js') }}"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<!-- moment -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    

<script type="text/javascript">
$(document).ready(function(){


});
</script>
@endsection