@extends('layouts.app')

@section('content-styles')
    <!-- Custom CSS -->
    <link href="{{ asset('dashtemplate/css/lib/chartist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/lib/calendar2/semantic.ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/lib/calendar2/pignose.calendar.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/lib/owl.carousel.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />

    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">

    <link href="{{asset('dashtemplate/fullcalendar/lib/fullcalendar.min.css') }}" rel='stylesheet'/>
    <link href="{{asset('dashtemplate/fullcalendar/lib/fullcalendar.print.min.css') }}" rel='stylesheet' media='print' />
    <link href="{{asset('dashtemplate/fullcalendar/scheduler.min.css') }}" rel='stylesheet' />

    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

@endsection



@section('content')
<body class="fix-header fix-sidebar mini-sidebar">

<?php $routes = Route::current()->getName(); ?>
@if ( $routes== 'dashboard' || $routes== 'drivers-schedule' )
    @include('layouts.dashboard.header')
@endif

    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->

        @if(session('status'))
        <div class="alert alert-{{session('status')[0]}}" style="background-color: #dff0d8 !important;color: #3c763d!important; border-color: #d6e9c6 !important;">
        {{session('status')[1]}}
        </div>
        @endif

        <div class="row">   
            <div id='calendar'></div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->



<?php $routes = Route::current()->getName() ?>
@if ( $routes== 'dashboard' )
    @include('layouts.dashboard.footer')

@endif
    
</body>
@endsection

@section('content-scripts')
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <!--Custom JavaScript -->

    <!-- fullcalendar -->    
    <script src="{{ asset('dashtemplate/fullcalendar/lib/moment.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/fullcalendar/lib/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/fullcalendar/scheduler.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/owl-carousel/owl.carousel-init.js') }}"></script>

    <!-- scripit init-->
    <script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script>

  $(document).ready(function() { // document ready
    procDoneTask();
    var curDate = '<?php echo $currentD;?>';
    var res = '<?php echo $resource; ?>';
    var ev = '<?php echo $events; ?>';
    var custname = '';
    var myEvent;
    $('#use').val('no');

    $('#calendar').fullCalendar({
      schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
      now: curDate,
    minTime: '06:00:00', 
    maxTime: '22:00:00', 
        resourceAreaWidth: '20%',
      editable: false, // enable draggable events
      aspectRatio: 1.8,
      scrollTime: '06:00', // undo default 6am scrollTime
      header: {
        left: 'today prev,next',
        center: 'title',
        right: 'timelineDay,agendaWeek,month'
      },
    defaultView: 'timelineDay',
    views: {
    timelineThreeDays: {
          type: 'timeline',
          duration: { days: 3 }
        }
      },
      // resourceLabelText: 'Maids',
        resourceAreaWidth: '28%',
        resourceColumns: [
        {
          labelText: 'Driver',
          field: 'driver'
        },
        {
          labelText: 'Maid',
          field: 'maid'
        }
      ],
  // [ { id: '0', resourceId: '0', start: '2018-05-22T02:00:00', end: '2018-05-24T07:00:00', title: 'event 1' }]
      resources: {!!$resource!!},
      events:  {!!$events!!},

        eventMouseover: function (returnD, event, view) {
  
            tooltip1 = '<div class="tooltiptopicevent" style="width:auto;height:auto;background:#e7e7e7;position:absolute;z-index:10001;padding:10px 10px 10px 10px ;  line-height: 200%;">' + 
            'Service Task : Drop to destination </br>' + 
            'Service Number : '  + returnD.title + '</br>' + 
            'Start : ' + returnD.start.format('M/DD hh:mm A') + '</br>' + 
            'End : ' + returnD.end.format('M/DD hh:mm A') + '</br>' +
            'Destination : '  + returnD.customer_address + '</br>' +          
            '</div>';
         
            tooltip2 = '<div class="tooltiptopicevent" style="width:auto;height:auto;background:#e7e7e7;position:absolute;z-index:10001;padding:10px 10px 10px 10px ;  line-height: 200%;">' + 
            'Service Task : Pickup to destination</br>' + 
            'Service Number : '  + returnD.title + '</br>' + 
            'Start : ' + returnD.start.format('M/DD hh:mm A') + '</br>' + 
            'End : ' + returnD.end.format('M/DD hh:mm A') + '</br>' +
            'Destination : '  + returnD.customer_address + '</br>' +          
            '</div>';   

            if(returnD.toolbar == '0') {
                $("body").append(tooltip2);
            }else{
                $("body").append(tooltip1);
            }
      
            $(this).mouseover(function (e) {
                $(this).css('z-index', 10000);
                $('.tooltiptopicevent').fadeIn('500');
                $('.tooltiptopicevent').fadeTo('10', 1.9);
            }).mousemove(function (e) {
                $('.tooltiptopicevent').css('top', e.pageY + 10);
                $('.tooltiptopicevent').css('left', e.pageX + 20);
            });

      
        },
        eventMouseout: function (data, event, view) {
            $(this).css('z-index', 8);
            $('.tooltiptopicevent').remove();

        },

 
        eventClick: function(data, jsEvent, view) { 
            procDoneTask();
          // alert();
            // myEvent = data;
            // $('#myModal').modal('show');
            // $.ajax({
            //     url: '{{route('maids_itinerary_update')}}',
            //     type: 'POST',
            //     data: {
            //         _token : $('meta[name="csrf-token"]').attr('content'),
            //         action : 'view',
            //         id: myEvent.id
            //     },
            //     success: function(data){  
            //         var newd = JSON.parse(data);

            //         if(newd.customer_address_id==null){
            //             $('#useaddress').prop('checked', true);
            //             $("#customer_address").prop('disabled', true);
            //             $('#customer_address').val(newd.address);
            //         }else{
            //             $('#customer_address').val(newd.customer_address);
            //         }

            //             $('#servicenumber').html(newd.schedule_code);
            //             $('#datetimes').val(newd.schedule_start + ' - ' + newd.schedule_end);
            //             $('#schedule_start').val(newd.schedule_start);
            //             $('#schedule_end').val(newd.schedule_end);

            //             $('#maids_id').val(newd.maids_id);
            //             $('#schedule_color').val(newd.schedule_color);
            //             $('#driver_id').val(newd.driver_id);
            //             $('#customer_id').val(newd.customer_id);
            //             $('#completed').val(newd.completed);
            //             $('#payment').val(newd.payment);

            //         $('#useaddress').click(function(){

            //             if ($('input[name=useaddress]').prop('checked')) {
            //                 $('#use').val('yes');
            //                 $("#customer_address").prop('disabled', true);
            //                 $("#customer_address").val(newd.address);
                     
            //             }else{
            //                 $('#use').val('no');
            //                 $("#customer_address").prop('disabled', false);
            //                 $("#customer_address").val(newd.customer_address);
                      
            //             }
            //         });

            //     }

            // });

        }


  });

    function procDoneTask(){
        $.ajax({
            url: '{{route('procDoneTask')}}',
            type: 'POST',
            data: {
                _token  : $('meta[name="csrf-token"]').attr('content')
            }
        }); 
    }

});


</script>


@endsection