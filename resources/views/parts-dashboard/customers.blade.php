

@extends('layouts.app')



@section('content-styles')

    <!-- Custom CSS -->



    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />

    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">

    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">





@endsection



@section('content')

<body class="fix-header fix-sidebar">



    <?php $routes = collect(request()->segments())->last(); ?>

    @if ( $routes== 'customers'  )

        @include('layouts.dashboard.header')

    @endif   



     <div class="container-fluid" id="forcustomer">



        @if(session('status'))

        <div id="successMessage" class="alert alert-{{session('status')[0]}}" style="background-color: #dff0d8 !important;color: #3c763d!important; border-color: #d6e9c6 !important;">

        {{session('status')[1]}}

        </div>

        @endif



        <div id="successMessage2" class="alert alert" style="background-color: #dff0d8 !important;color: #3c763d!important; 

        border-color: #d6e9c6 !important;">

            Customer is successfully deleted!

        </div>



  

        <div class="card">

            <div class="card-body">

                <a class="pull-right" href="{{ route('customers.create') }}"><button class="btn btn-success">Add Customers</button></a>

                <div class="table-responsive m-t-40">

                    <table id="myTableAcco" class="table table-bordered table-striped">

                        <thead>



                            <tr>

                                <th>Fullname</th>

                                <th>Gender</th>

                                <th>Mobile</th>

                                <th>Email</th>

                                <th>Birthdate</th>

                                <th style=" white-space: nowrap;">Action</th>

                           

                            </tr>

                        </thead>

                        <tbody>

               

                            @foreach($clist as $list)



                            <tr>

                                <td >{{ $list->fullname }}</td>

                                <td>{{ $list->gender == "1" ? "Female" : "Male" }}</td>

                                <td>{{ $list->mobile }}</td>

                                <td>{{ $list->email }}</td>

                                <td>{{ $list->birthdate->format('Y-m-d') }}</td>

                                <td>

                                    <a href="{{ route('customers.show', $list->id) }}"><button class="btn-sm btn-success">View</button></a> |

                                    <a href="{{ route('customers.edit', $list->id) }}"><button class="btn-sm btn-primary">Edit</button></a> |
                                    @if (\Auth::user()->access == 99)
                                    <button class="btn-sm btn-danger"  v-on:click="openmodal('{{$list->id}}','{{$list->fullname}}')" 

                                        data-toggle="modal" data-target="#mymodal">Delete</button>
                                    @endif
                                                                                    

                                </td>

                            </tr>

                       

                            @endforeach

     

                        </tbody>

                    </table>

                </div>

            </div>

        </div>





        <!-- modal delete -->

        <div class="modal" tabindex="-1" role="dialog" id="mymodal">

          <div class="modal-dialog modal-md" role="document">

            <div class="modal-content">

              <div class="modal-header">

              </div>

              <div class="modal-body">

                    <p>Are you sure you want to delete customer: @{{customer}} ? </p>

              </div>

              <div class="modal-footer">

                <button type="button" class="btn btn-danger"  data-dismiss="modal" v-on:click="deleApp()">Yes</button>

                <button type="button" class="btn btn-success" data-dismiss="modal">No</button>

              </div>

            </div>

          </div>

        </div>



   









    </div>

            

    <?php $routes = collect(request()->segments())->last(); ?>

    @if ( $routes== 'customers'  )

        @include('layouts.dashboard.footer')



    @endif                                                                                                                                                                                                                                                                                                                                                                                                                 

</body>



@endsection



@section('content-scripts')



    <script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap tether Core JavaScript -->

    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- slimscrollbar scrollbar JavaScript -->

    <script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>

    <!--Menu sidebar -->

    <script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>

    <!--stickey kit -->

    <script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>

    <!--Custom JavaScript -->



    <!-- fullcalendar -->    

    <script src="{{ asset('dashtemplate/fullcalendar/lib/moment.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/fullcalendar/lib/fullcalendar.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/fullcalendar/scheduler.min.js') }}"></script>



    <!-- scripit init-->

    <script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>



    <script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>





    <script src="{{ asset('vue/dist/vue.js') }}"></script>

    <script src="{{ asset('vue/dist/vue.min.js') }}"></script>



    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>





<script type="text/javascript">



$(document).ready(function(){



    $('#successMessage2').hide();

    setTimeout(function() {

        $('#successMessage').fadeOut('fast');

    }, 5000);



});





Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

new Vue({



    el: '#forcustomer',



    data: {



        deleId : '',

        customer: '',

    },





    methods : {



        openmodal: function(id,customer){

            this.deleId = id;

            this.customer = customer;

        },  



        deleApp: function(){

 

            this.$http.post('{{ route('delcustomer') }}',{'id':this.deleId}).then((response) => { 

                $('#successMessage2').show();

                setTimeout(function() {

                    window.location.href = "{{ url('admin/customers') }}"; 

                }, 1500);



            });



        }











      

    }













});



</script>

@endsection