

@extends('layouts.app')



@section('content-styles')

    <!-- Custom CSS -->



    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />

    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">

    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">





@endsection



@section('content')

<body class="fix-header fix-sidebar">



@include('layouts.dashboard.header')



                                                                                                                                                        

<div class="container-fluid" id="formaids">





<!-- Start Page Content -->

    <div class="row">

        <div class="col-md-12">

            <div class="card">

                <div class="card-body">

                    <div class="row">

                        <div class="col-lg-12">

                            <div id="invoice" class="effect2">

                                <div id="invoice-top">

                                                    

                                    <div class="clientlogo">

                                        @if($ddata->photo != '')

                                        <img src= " {{ asset('storage/drivers').'/'.$ddata->photo }}" >

                                        @endif

                              



                                    </div>

                                    <div class="invoice-info">

                                        <h3 class="text-primary">{{$ddata->fullname}}</h3>

                                        <hr>

                                        <p > 

                                            <i class="fa fa-birthday-cake" aria-hidden="true"></i>

                                                {{$ddata->birthdate->format('Y-m-d')}}

                                        </p>

                                        <p> 

                                            <i class="fa fa-venus-mars" aria-hidden="true"></i>

                                                {{ $ddata->gender == "1" ? "Female" : "Male" }}

                                       </p>

                                        <p> 

                                            <i class="fa fa-mobile" aria-hidden="true"></i>

                                                {{$ddata->mobile}}

                                       </p>

                                    </div>

                                    <!--End Info-->

                

                                    <div class="title col-md-4 text-center booking">

                                        <h4 class="text-danger">Booking Summary</h4>

                                            <hr>

                                            <div class="row">

                                                <div class="col-sm-6 "> 

                                                    <p>TOTAL BOOKINGS:</p>

                                                </div>

                                            

                                                <div class="col-sm-6 ">

                                                    <p class="text-primary">{{$hours['totalbooked']}}</p>

                                                </div>

                                            </div>

                                     

                                            <div class="row">

                                                 <div class="col-sm-6 ">

                                                    <p>WORKING HOURS:</p>

                                                </div>



                                                 <div class="col-sm-6 ">

                                                    <p class="text-primary">{{$ddata->shift_start}} - {{$ddata->shift_end}}</p>

                                                </div>

                                            </div>

                                        

                                            <div class="row">

                                                 <div class="col-sm-6 ">

                                                    <p>HOURS WORKED:</p>

                                                </div>

                                                 <div class="col-sm-6">

                                                    <p class="text-primary">{{$hours['totalhours']}}</p>

                                                </div>

                                            </div>



                                    </div>

                                  

                                </div>

                        

                                <div id="invoice-bot">

                                    <div class="table-responsive m-t-40">

                                      

                                        <div class="table-responsive m-t-40">

                                            <h4 class="text-primary">Bookings</h4>  

                                            <table id="myTablemaid" class="table table-bordered table-striped">

                                                <thead>

                                                    <tr>

                                                        <th>Service #</th>

                                                        <th>Booking Date</th>

                                                        <th>Time</th>

                                                        <th>Client</th>

                                                        <th>Status</th>

                                         

                                                    </tr>

                                                </thead>

                                                <tbody>

                                                    @foreach($dsched as $list)

                                                    <tr>

                                                        <td>{{$list['schedule_code']}}</td>

                                                        <td>{{$list['schedule_start']}}</td>

                                                        <td>{{$list['time']}}</td>

                                                            <td>

                                                                <a class="text-primary" href="{{ route('customers.show', $list['customer_id']) }}">{{$list['client']}}</a>

                                                            </td>

                                                        <td>

                                                            @if($list['status'] == '1' )

                                                                <span class="label label-success">Done</span>

                                                            @elseif($list['status'] == '3')

                                                                <span class="label label-warning">On queue</span>

                                                            @else

                                                                <span class="label label-danger">Canceled</span>

                                                            @endif

                                                        </td>

                                                       

                                                    </tr>

                                                    @endforeach

                                                </tbody>

                                            </table>

                                        </div>

                                    </div>       

                                </div>

                           

                            </div>

                            <a class="pull-right" href="{{ route('maids.index') }}"><button class="btn btn-success">Back</button></a>

                       

                        </div>



                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- End PAge Content -->



</div>



    @include('layouts.dashboard.footer')

                                                                                                                                                                                                                                                                                                                                                                                                         

</body>



@endsection



@section('content-scripts')

    <script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap tether Core JavaScript -->

    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- slimscrollbar scrollbar JavaScript -->

    <script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>

    <!--Menu sidebar -->

    <script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>

    <!--stickey kit -->

    <script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>

    <!--Custom JavaScript -->



    <!-- fullcalendar -->    

    <script src="{{ asset('dashtemplate/fullcalendar/lib/moment.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/fullcalendar/lib/fullcalendar.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/fullcalendar/scheduler.min.js') }}"></script>



    <!-- scripit init-->

    <script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>



    <script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>





    <script src="{{ asset('vue/dist/vue.js') }}"></script>

    <script src="{{ asset('vue/dist/vue.min.js') }}"></script>



    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>





@endsection