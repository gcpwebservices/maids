
@extends('layouts.app')

@section('content-styles')
    <!-- Custom CSS -->

    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('dashtemplate/css/lib//daterangepicker/daterangepicker.css') }}" />

    <link rel="stylesheet" href="{{asset('dashtemplate/timepicker/jquery.ui.timepicker.css') }}">
    <link rel="stylesheet" href="{{asset('dashtemplate/timepicker/jquery-ui-1.10.0.custom.min.css') }}">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('dashtemplate/js/lib/bootstrap-tagsinput-latest/src/bootstrap-tagsinput.css') }}">

    <style type="text/css">
        td,th{ 
            font-size: 12px; 

        }
        .dataTables_wrapper .dataTables_paginate{
            padding-bottom: 1.25em !important;
        }
        .dataTables_wrapper .dataTables_paginate {
            font-size: 0.75em !important;
        }

    </style>
@endsection

@section('content')
<body class="fix-header fix-sidebar">

@include('layouts.dashboard.header')
    
    <div class="container-fluid app">
        <div class="row form-body card">
            <div class="col-sm-12 col-md-4">
                <div class="input-group input-group-flat ">
                    <span class="input-group-btn" style="width: 30px; margin-top:9px;">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input type="text" name="dates" id="dates" class="form-control">  
                        <input type="hidden" name="from" id="from" class="form-control">  
                        <input type="hidden" name="to" id="to" class="form-control">  
         <!--            <span class="input-group-btn">
                        <button class="btn btn-primary btn-group-right" type="button" id="button" >
                            <i class="ti-search" ></i>
                        </button>
                    </span>  -->       
                </div>
            </div>
          
        </div>

        <div class="card"> 
            <div class="table-responsive">
                <table id="example23" class="display nowrap table table-hover table-striped table-bordered" 
                cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Service No.</th>
                            <th>Customer</th>
                            <th>Maid</th>
                            <th>Driver</th>
                            <th>Transaction Type</th>
                            <th>Service Status</th>
                            <th>Payment Status</th>
                            <th>Service Date</th>
                            <th>Service Time</th>
                   
                        </tr>
                    </thead>
     
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>


<input type="hidden" name="idval" id="idval">
<input type="hidden" name="appointment_id" id="appointment_id">
<input type="hidden" name="customer_id" id="customer_id">
<input type="hidden" name="done" id="done">
<input type="hidden" name="amount" id="amount">
<div class="modal" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="schedule_code" class="text-primary"></h3>
                <button type="button" id="edit" class="btn-sm btn-primary" style="color:white !important;">Edit</button>
            </div>
            <div class="modal-body">          
                <div class="container-fluid" id="forview">
                    <div class="panel-body">
                        <div class="col-sm-12 col-md-12">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="clearfix">
                                        <img src="" id="cus_img" class="img img-circle pull-left m-r-15" 
                                        style="width:100px;height:100px">
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-8 table-responsive">
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <th>Customer:</th>
                                                            <td class="p-l-15 text-primary font-weight-bold" id="cus_name"></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Mobile:</th>
                                                            <td class="p-l-15 text-primary font-weight-bold" id="cus_mobile"></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Property Address:</th>
                                                            <td class="p-l-15 text-primary font-weight-bold" id="cus_address"></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Area:</th>
                                                            <td class="p-l-15 text-primary font-weight-bold" id="cus_area"></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Subarea:</th>
                                                            <td class="p-l-15 text-primary font-weight-bold" id="cus_subarea"></td>
                                                        </tr>
                                                  
                                                    </tbody>
                                                </table>
                                            </div>  
                                            <div class="col-md-4 table-responsive">
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <th>Maid:</th>
                                                            <td class="p-l-15 text-primary font-weight-bold" id="maid_name"></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Driver:</th>
                                                            <td class="p-l-15 text-primary font-weight-bold" id="driver_name"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>      
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="padding-top:20px;">
                                <div class="col-sm-12 col-md-12 table-responsive">
                                    <table class="table table-hover ">
                                        <thead>
                                            <tr>
                                                <th>Service Date</th>
                                                <th>Servie Time</th>
                                                <th>Services</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="font-weight-bold" id="service_date"></td>
                                                <td class="font-weight-bold" id="service_time"></td>
                                                <td class="text-primary font-weight-bold" id="service"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row" style="padding-top:20px;">
                                <div class="col-sm-12 col-md-12 table-responsive">
                                    <table class="table table-hover ">
                                        <thead>
                                            <tr>
                                                <th>Transaction Type</th>
                                                <th>Service Status</th>
                                                <th>Payment Status</th>
                                                <th>VAT</th>
                                                <th>Service Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="text-primary font-weight-bold" id="transaction_type" 
                                                style="font-size: 100% !important"></td>
                                                <td class="font-weight-bold" id="service_status" 
                                                style="font-size: 100% !important"></td>
                                                <td class="font-weight-bold" id="payment_status" 
                                                style="font-size: 100% !important"></td>
                                                <td class="text-primary font-weight-bold" id="payment_vat" 
                                                style="font-size: 100% !important"></td>
                                                <td class="text-primary font-weight-bold" id="price" 
                                                style="font-size: 100% !important"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid" id="foredit">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Select Area</label>
                                    <select class="form-control" name="area_edit" id="area_edit" 
                                        style="width: 100%;">
                                        <option value="">Select Area</option>
                                        @foreach($area as $alist)
                                            <option value="{{ $alist->area_id }}">{{ $alist->area }} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Select Subarea</label>
                                    <select class="form-control" name="subarea_edit" id="subarea_edit" 
                                        style="width: 100%;">    
                                    </select>
                                    <input type="hidden" id="subarea_edit_val"name="subarea_edit_val">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="service">Services:</label>
                                    <input type="text" data-role="tagsinput" name="service_2" id="service_2">
                                </div> 
                            </div> 

                        </div>

                        <div class="row" id="address_hide">

                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Enter new property address</label>
                                    <input type="text" class="form-control" name="customer_address_edit" 
                                    id="customer_address_edit" autocomplete="off" style="border-radius: 3px; border-color: #aaa;">
                                </div>
                            </div>
                            
                        </div>

                        <div class="row">
                         
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Select Date:</label>
                                    <input type="text" class="form-control" name="datetimes_edit" id="datetimes_edit" autocomplete="off" style="border-radius: 3px; border-color: #aaa;"/>
                                            <input type="hidden" class="form-control" name="schedule_start_edit" 
                                            id="schedule_start_edit"/>
                                            <input type="hidden" class="form-control" name="schedule_end_edit" 
                                            id="schedule_end_edit"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                <label for="timefrom">Time:</label>
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input id="timefrom_edit" type="text" name="timefrom_edit" class="form-control input-small" 
                                        autocomplete="off" placeholder="Start" style="border-radius: 3px; border-color: #aaa;" required="required">
                                        &ensp;
                                        <input id="timeto_edit" type="text" name="timeto_edit" class="form-control input-small" 
                                        autocomplete="off" placeholder="End" style="border-radius: 3px; border-color: #aaa;">
                                    </div>
                                </div>
                            </div>

                        </div>
    
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Select Maid    <span id="availmaids_edit" style="color:red;"></span></label>
                                    <input type="hidden" name="maids_id" id="maids_id">
                                    <select class="form-control custom-select" name="maids_id_edit" id="maids_id_edit" 
                                    required="required" style="width: 100%;">

                                    </select>
                                </div>

                            </div>
             
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Select Driver</label>
                                    <select class="form-control custom-select" name="driver_id_edit" id="driver_id_edit"
                                    style="width: 100%;">
                                    <option value="">Select Driver</option>
                                    @foreach($dlist as $list)
                                    <option value="{{ $list->id }}">{{ $list->fullname }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Arrival Window</label>
                                    <select class="form-control custom-select" name="interval_edit" id="interval_edit"
                                    style="width: 100%;">
                                        <option value="">Select Interval</option>
                                        <option value="10">10 mins</option>
                                        <option value="20">20 mins</option>
                                        <option value="30">30 mins</option>
                                        <option value="40">40 mins</option>
                                        <option value="50">50 mins</option>
                                        <option value="60">60 mins</option>
                                        <option value="70">1 hour and 10 mins</option>
                                        <option value="80">1 hour and 20 mins</option>
                                        <option value="90">1 hour and 30 mins</option>
                                        <option value="100">1 hour and 40 mins</option>
                                        <option value="110">1 hour and 50 mins</option>
                                        <option value="120">2 hours</option>
                                    </select>
                                  
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-8 col-md-pull-4 col-sm-12">

                                <div class="form-group">
                                    <input type="hidden" name="to_pay" id="to_pay">
                                    <label>Amount to Pay:</label>
                                    <input id="to_pay_view" readonly style="font-size:36px; width:50%; height:60px" value="0.00" 
                                    type="text" class="form-control input-lg text-center autonumeric font-bold">
                                </div>
                    

                            </div>

                            <div class="col-md-4 -md-push-8 col-sm-12">
                                <div class="form-group">
                                    <label>Service Staus:</label>
                                    <input type="hidden" name="completed_edit" id="completed_edit">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" 
                                        id="myonoffswitch">
                                        <label class="onoffswitch-label" for="myonoffswitch">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>  

                        </div>
                      <small class="text-danger">Note: Changing the Date, Time and Arrival will update the Maids list.</small>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-sm btn-basic" data-dismiss="modal" style="color:black;">Close</button>
                <button type="button" id="savebutton" class="btn-sm btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>



@include('layouts.dashboard.footer')

                                                                                                                                                                                                                                                                                                                                                                                                   
</body>

@endsection

@section('content-scripts')

<script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>
<!--Menu sidebar -->
<script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>
<!--stickey kit -->
<script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
<!--Custom JavaScript -->

<!-- scripit init-->
<script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>

<!-- datatables -->
<script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>

<!-- highcharts -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<!-- vue js -->
<script src="{{ asset('vue/dist/vue.js') }}"></script>
<script src="{{ asset('vue/dist/vue.min.js') }}"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<!-- moment -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<!-- time picker -->
<script src="{{ asset('dashtemplate/timepicker/migrate.js') }}"></script>
<script src="{{ asset('dashtemplate/timepicker/jquery.ui.timepicker.js') }}"></script>
<script src="{{ asset('dashtemplate/timepicker/jquery.ui.core.min.js') }}"></script>
<script src="{{ asset('dashtemplate/timepicker/jquery.ui.position.min.js') }}"></script>
<script src="{{ asset('dashtemplate/timepicker/jquery.ui.widget.min.js') }}"></script>
<script src="{{ asset('dashtemplate/timepicker/jquery.ui.tabs.min.js') }}"></script>

<!-- others -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="{{ asset('dashtemplate/js/lib/bootstrap-tagsinput-latest/src/bootstrap-tagsinput.js') }}"></script>

<script type="text/javascript">
$(document).ready(function(){

$('#myModal').on('hidden.bs.modal', function () {
        toastr.clear();
});

$('#myonoffswitch').on('change',function(){
    if ($(this).is(":checked")){
        $('#completed_edit').val(1);
    }else{
        $('#completed_edit').val(3);
    }
});


$("#maids_id_edit").select2({
    placeholder: "Select Maid"
});

$("#driver_id_edit").select2({
    placeholder: "Select Driver"
});

$("#interval_edit").select2({
    placeholder: "Select Interval"
});


$('#savebutton').hide();
$('#foredit').hide();
$('#address_hide').hide();
$('#subarea_edit').prop('disabled', true);

$(document).on('hide.bs.modal','#myModal', function () {
    $('#savebutton').hide();
    $('#foredit').hide();
    $('#forview').show();
    $('#address_hide').hide();
    $('#subarea_edit').prop('disabled', true);
    // window.location.href = '{{ url("admin/maids-schedule-table") }}';
    salesdatatable.ajax.reload();

});


$('#edit').on('click', function () {
    if($('#done').val() == '0'){
        $('#savebutton').show();
        $('#foredit').show();
        $('#forview').hide();
    }else{
        foralert('warning','Service is done. No editing needed.');
    }

});

$('#paynow').on('click', function () {
    $('#myModal').modal('hide');
    $('#myModalNew').modal('show');
})


var from;
var to;
var dropdownlabel;

$('input[name="dates"]').daterangepicker({
    "showDropdowns": false,
    ranges: {
        // 'Today':        [moment(), moment()],
        // 'Yesterday':    [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days':  [moment().subtract(6, 'days'), moment()],
        'Last 15 Days': [moment().subtract(14, 'days'), moment()],
        'This Month':   [moment().startOf('month'), moment().endOf('month')],
        'Last Month':   [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },

        "startDate": moment().subtract(6, 'days'),
        "endDate":   moment(),

    }, function(start, end, label) {
     
        dropdownlabel = label;
        from = start.format('YYYY-MM-DD');
        to = end.format('YYYY-MM-DD');
        $('[name="from"]').val(from);
        $('[name="to"]').val(to);

        salesdatatable.ajax.reload();

});

    var salesdatatable =  $('#example23').DataTable({
        processing: true,
        serverSide: true,
        ordering: true,

        oLanguage: {
            sProcessing: "<span id='spinner' class='fa fa-spinner fa-2x fa-spin text-blue'></span>"
        },
        ajax: { 
            url: '{{ route('MaidsScheduleTable') }}',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: function ( d ) {
                d.datefrom = $('[name="from"]').val();
                d.dateto = $('[name="to"]').val();
               
            }
        },
        "createdRow": function( row, data, dataIndex ) {

            $('td',row).hover(function() {
                $(this).addClass('pointer');
            });
            
            $('td',row).on('click',function(){
                $('#myModal').modal('toggle');
                $('#idval').val(data.schedule_code);
       
                    $.ajax({
                        url: '{{ route('MaidsScheduleTableDetail') }}',
                        type: 'POST',
                        headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data:{ maids_schedule_id: data.id },
                            success: function(data){
                                
                                var parsed = JSON.parse(data);
                                var source = "{!! asset('storage/customers') !!}"; 

                                //modal view
                                if( parsed.maids_relation.completed == 1 ){
                                    $('#done').val(1);
                                }else{
                                    $('#done').val(0);
                                }
                                $('#appointment_id').val(parsed.maids_relation.id);
                                $('#schedule_code').html('Service No: '+parsed.maids_relation.schedule_code);
                                $('#cus_img').attr('src', source+'/'+parsed.maids_relation.get_customer.photo);
                                $('#cus_name').html(parsed.maids_relation.get_customer.fullname);
                                $('#cus_mobile').html(parsed.maids_relation.get_customer.mobile);
                                $('#cus_address').html(parsed.maids_relation.get_customer.address);
                                $('#cus_area').html(parsed.maids_relation.get_area.area);
                                $('#cus_subarea').html(parsed.maids_relation.get_subarea.sub_area);
                                $('#maid_name').html(parsed.maids_relation.get_maid.fullname);
                                var viewDriver = (parsed.maids_relation.driver_id != null) ? 
                                    parsed.maids_relation.get_driver.fullname : 'N/A';
                                $('#driver_name').html(viewDriver);
                                            
                    
                                $('#service_date').html(moment(parsed.maids_relation.schedule_start).format('ll'));
                                $('#service_time').html(
                                    moment(parsed.maids_relation.schedule_start).format('LT')
                                    + ' - ' +
                                    moment(parsed.maids_relation.schedule_end).format('LT')
                                );

                                if(parsed.maids_relation.completed == 1){
                                    $('#service_status').html('<span class="label label-success">Done</span>');
                                }else if(parsed.maids_relation.completed == 3){
                                    $('#service_status').html('<span class="label label-warning">On&nbspqueue</span>');
                                }else{
                                    $('#service_status').html('<span class="label label-danger">Cancelled</span>');
                                }
                                
                                $('#service').html(parsed.maids_relation.notes);
                            
                                if( parsed.payment_details.payment_sum == '0.00' 
                                    && parsed.payment_details.payment_status == '0' ){
                                    $('#payment_status').html('<span class="label label-danger">Unpaid</span>'); 
                                }else if( parsed.payment_details.payment_sum != '0.00' 
                                    && parsed.payment_details.payment_status == '3' ){
                                    $('#payment_status').html('<span class="label label-warning">Partially Paid</span>'); 
                                }else if( parsed.payment_details.payment_status == '1'){
                                    $('#payment_status').html('<span class="label label-success">Paid</span>'); 
                                }

                                if( parsed.payment_details.transaction_type == '0'){
                                    $('#transaction_type').html('One Time'); 
                                }else{
                                    $('#transaction_type').html('Reccurring'); 
                                }

                                $('#payment_vat').html(parsed.maids_relation.get_sale_summary.payment_vat);
                                $('#price').html(parsed.maids_relation.get_sale_summary.price);
                                //modal view end

                                //modal edit
                                $('#customer_id').val(parsed.maids_relation.customer_id);
                                $('#to_pay_view').val(parsed.maids_relation.get_sale_summary.payment_sub+' '+'AED');

                                if( parsed.maids_relation.completed == 1 ){
                                    $('#myonoffswitch').attr('checked', 'checked');
                                }else{
                                    $('#myonoffswitch').removeAttr('checked');
                                }

                                $("#area_edit").select2({
                                    placeholder: parsed.maids_relation.get_area.area
                                });
                                $("#area_edit").val(parsed.maids_relation.area_id);

                                $("#subarea_edit").select2({
                                    placeholder: parsed.maids_relation.get_subarea.sub_area
                                });
                                $("#subarea_edit_val").val(parsed.maids_relation.area_id);
                                
                                $('#customer_address_edit').val(parsed.maids_relation.get_customer.address);

                                $('#service_2').tagsinput('add', parsed.maids_relation.notes);

                                $('#datetimes_edit').val(moment(parsed.maids_relation.schedule_start).format('YYYY-MM-DD'));
                                var newtimefrom = moment(parsed.maids_relation.schedule_start).format('HH:mm');
                                var newtimeto = moment(parsed.maids_relation.schedule_end).format('HH:mm');
                                $('#schedule_start_edit').val(parsed.maids_relation.schedule_start);
                                $('#schedule_end_edit').val(parsed.maids_relation.schedule_end);
                                $('#timefrom_edit').val(newtimefrom);
                                $('#timeto_edit').val(newtimeto);


                                $("#maids_id_edit").select2({
                                    placeholder: parsed.maids_relation.get_maid.fullname
                                });
                                $('#maids_id').val(parsed.maids_relation.maids_id);

                                var editDriver = (parsed.maids_relation.driver_id != null) ? parsed.maids_relation.get_driver.fullname : 'Select Driver';
                                $("#driver_id_edit").select2({
                                    placeholder: editDriver
                                });
                                $('#driver_id_edit').val(parsed.maids_relation.driver_id);
                                $('#completed_edit').val(3);
                                $('#to_pay').val(parsed.maids_relation.get_sale_summary.payment_sub);
                                $('#amount').val(parsed.maids_relation.get_sale_summary.price);

                            }

                    });

            });
        },

        columns: [
            {data:'schedule_code',name:'schedule_code',class:'nowrap'},
            {data:'get_customer.fullname',name:'getCustomer.fullname'},
            {data:'get_maid.fullname',name:'getMaid.fullname'},
            {data:
                function(data) {
                    if(data.get_driver)
                    {
                        return data.get_driver.fullname;
                    }else{
                        return 'N/A';
                    }
                },name:'getDriver.fullname'
            },
            {data:'transaction',name:'transaction'},
            {data:'completed',name:'completed'},
            {data:'payment',name:'payment'},
            {data:'schedule_date',name:'schedule_date'},
            {data:'schedule_start',name:'schedule_start'}

        ]
    });


var details = $('#examp').DataTable({
        processing: true,
        serverSide: true,
        ordering: true,
        pageLength: 5,
            "searching": false,
            "lengthChange": false,
        oLanguage: {
            sProcessing: "<span id='spinner' class='fa fa-spinner fa-2x fa-spin text-blue'></span>"
        },

            ajax: { 
                url: '{{ route('MaidsScheduleTableDetail') }}',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: function ( d ) {
                    d.service_num = $('#idval').val();
                    d.postaction = 'table';        
                }
            },
  
        columns: [
            {data:'schedule_code',name:'schedule_code',class:'nowrap'},
            {data:'schedule_start',name:'schedule_start'},
            {data:'startendtime',name:'startendtime'},
            {data:'get_sale_summary.payment_sub',name:'getSaleSummary.payment_sub'}
        ]
    });



    //dropdown area and subarea
    $('#area_edit').on('change',function(){
        $('#subarea_edit').html('');
        subarea($(this).val());
        $('#address_hide').show();
        $('#subarea_edit').prop('disabled', false);
    });

    $('#subarea_edit').on('change',function(){
        $('#subarea_edit_val').val($(this).val());

    });

    function subarea(area_id){

        $.ajax({
            url: '{{ route('getareas') }}',
                type: 'POST',
                data: {
                _token  : $('meta[name="csrf-token"]').attr('content'),
                area : area_id,
                actionfor: 'change'
            },  
            success:function(data){
                $.each(JSON.parse(data), function( idx, val ) {     
                    $("#subarea_edit").append('<option value='+val.id+'>'+val.sub_area+' </option>');
                });
            }
        }); 
        

    }


    //time and date
    var curDate = '<?php echo $currentD;?>';
    var curTime = '<?php echo $currentT;?>';

    var d = new Date();
    var h = d.getHours();
    var m = d.getMinutes();


    $("#timefrom_edit").prop('disabled', true);
    $("#timeto_edit").prop('disabled', true);
    // $("#maids_id_edit").prop('disabled', true);
    // $("#driver_id_edit,#interval_edit").prop('disabled', true);


    $('#timeto_edit').timepicker({
        maxTime: { hour: 20, minute: 00 },
        showLeadingZero: true
    });

 

    $('#timefrom_edit').focus(function(){

        if($('#datetimes_edit').val() > curDate){
            $('#timefrom_edit').timepicker({
                minTime: { hour: 08, minute: 00 },
                maxTime: { hour: 20, minute: 00 },
                showLeadingZero: true
            }); 
      
        }else{
            $('#timefrom_edit').timepicker({
                minTime: { hour: h, minute: m },
                maxTime: { hour: 20, minute: 00 },
                showLeadingZero: true
            }); 
        }

    });


    $('#timefrom_edit').on('change',function(){

        $("#timeto_edit").prop('disabled', false);

        var startDate = $('#datetimes_edit').val();
        var startTime = $('#timefrom_edit').val();

        var date = new Date(startDate + ' ' + startTime);

        var newtimeto = moment(date).add(1, 'hours').format('HH:mm');
        var newdate = moment(date).add(1, 'hours');

        $('#timeto_edit').val(newtimeto);

        var hour = newdate.hour();
        var minutes = newdate.minutes();

        var datetimefrom = moment(date).format('YYYY-MM-DD HH:mm:ss');
        $('#schedule_start_edit').val(datetimefrom);

        tpMinMaxSetMinTime(hour,minutes);

        $("#maids_id_edit").prop("disabled", false);

    });




    $('#timeto_edit').on('change',function(){  
        var calendarDate   = $('#datetimes_edit').val();
        var endDateandTime = new Date(calendarDate + ' ' + $('#timeto_edit').val());
        var datetimeto = moment(endDateandTime).format('YYYY-MM-DD HH:mm:ss');
        $('#schedule_end_edit').val(datetimeto);
        get_available_maids($('#schedule_start_edit').val(),datetimeto,$('#interval_edit').val());
        getAmount($('#timefrom_edit').val(),$('#timeto_edit').val());
    });

    function tpMinMaxSetMinTime( hours, minutes ) {
        $('#timeto_edit').timepicker('option', { minTime: { hour: hours, minute: minutes} });
        var startDate = $('#datetimes_edit').val();
        var date = new Date(startDate + ' ' +  $('#timeto_edit').val());
        var datetimeto = moment(date).format('YYYY-MM-DD HH:mm:ss');
        $('#schedule_end_edit').val(datetimeto);
        get_available_maids($('#schedule_start_edit').val(),datetimeto,$('#interval').val());
        getAmount($('#timefrom_edit').val(),$('#timeto_edit').val());
    }


    $('input[name="datetimes_edit"]').daterangepicker(
        { 
        autoUpdateInput: false,
        timePicker: false,
        singleDatePicker: true, 
        minDate: new Date(),
        startDate: moment().startOf('hour'),
        endDate: moment().startOf('hour').add(36, 'hour'),
            locale: {

                format: 'YYYY-MM-DD'
            }
        },
        function(start, end, label) {
            $('#schedule_start_edit').val(start.format('YYYY-MM-DD'));
            $('#schedule_end_edit').val(end.format('YYYY-MM-DD'));
            $('#datetimes_edit').val(start.format('YYYY-MM-DD'));

            get_available_maids($('#schedule_start_edit').val(),$('#schedule_end_edit').val(),$('#interval_edit').val());

            $("#timefrom_edit").prop('disabled', false);
            $("#timeto_edit").prop('disabled', false);

            if($('#datetimes_edit').val() > curDate){
                $('#timefrom_edit').timepicker({
                    minTime: { hour: 08, minute: 00 },
                    maxTime: { hour: 20, minute: 00 },
                    showLeadingZero: true
                }); 
          
            }else{
                $('#timefrom_edit').timepicker({
                    minTime: { hour: h, minute: m },
                    maxTime: { hour: 20, minute: 00 },
                    showLeadingZero: true
                }); 
            }

        }
    );


    //maids on change

    $('#maids_id_edit').on('change', function(){
        $('#maids_id').val($(this).val());
    });


    $('#interval_edit').on('change', function(){
        get_available_maids($('#schedule_start_edit').val(),$('#schedule_end_edit').val(),$(this).val());
        //alert($(this).val());
    });

    //availability of maids
    function get_available_maids(schedule_start,schedule_end,arival){

        $('#maids_id_edit').empty();
        $.ajax({
            url: '{{route('get_available_maids')}}',
            type: 'POST',
            data: {
                _token : $('meta[name="csrf-token"]').attr('content'),
                schedule_start: schedule_start,
                schedule_end: schedule_end,
                interval: arival
            },
            success: function(data){          
                $("#driver_id_edit,#interval_edit").prop("disabled", false);
                $('#maids_id_edit').append('<option>Select Maid</option>');
                var ctr = 1;
                console.log(data);
                $.each(JSON.parse(data),function(idx,val){
             
                    $('#maids_id_edit').append('<option value="'+val.id+'">'+val.fullname+'</option>');
                    $('#availmaids_edit').html(ctr+ " available");

                    ctr++;
                });
             
            }
        });
    }

    function getAmount(from,to){
        $.ajax({
            url: '{{ route('getAmount') }}',
            type: 'POST',
            data: {
                _token  : $('meta[name="csrf-token"]').attr('content'),
                timefrom: from,
                timeto: to
            },
            success: function (data){

            var parsed = JSON.parse(data);
                $('#amount').val(parsed.amount);
                $('#to_pay').val(parsed.view);
                $('#to_pay_view').val(parsed.view+' '+'AED');

            }
        }); 
    }

        //save edit
    $('#savebutton').on('click',function(){

        var service_num     = $('#idval').val();
        var appointment_id  = $('#appointment_id').val();
        var customer_id     = $('#customer_id').val();
        var areaEdit        = $('#area_edit').val();
        var subareaEdit     = $('#subarea_edit_val').val();
        var cusaddEdit      = $('#customer_address_edit').val();
        var service_edit    = $('#service_2').val();
        var dateEdit        = $('#datetimes_edit').val();
        var startEdit       = $('#schedule_start_edit').val();
        var endEdit         = $('#schedule_end_edit').val();
        var maidEdit        = $('#maids_id').val();
        var driverEdit      = $('#driver_id_edit').val();
        var intervalEdit    = $('#interval_edit').val();
        var statusEdit      = $('#completed_edit').val();

        var topayEdit       = $('#to_pay').val();
        var amount          = $('#amount').val();

        // alert(amount+' '+service_num+' '+appointment_id+' '+customer_id+' '+areaEdit+' '+subareaEdit+' '+cusaddEdit+' '+service_edit+' '+dateEdit+' '+startEdit+' '+endEdit+' '+maidEdit+' '+driverEdit+' '+intervalEdit+' '+statusEdit+' '+topayEdit);


        $.ajax({
            url: '{{ route('editAppointment') }}',
            type: 'POST',
            data: {
                _token  : $('meta[name="csrf-token"]').attr('content'),

                schedule_code:  service_num,
                customer_id:    customer_id,
                area:           areaEdit,
                subarea:        subareaEdit,
                address:        cusaddEdit,
                appointment_id: appointment_id,
                service:        service_edit,
                schedule_start: startEdit,
                schedule_end:   endEdit,
                maids_id:       maidEdit,
                driver_id:      driverEdit,
                interval:       intervalEdit,
                completed:      statusEdit,
                topay:          topayEdit,
                amount:         amount


            },
            success: function (data){
                // $('body').html(data);
                foralert('success','Appointment  is successfully edited.');
            }
        }); 

    });


    //foralert
    function foralert(action,message){
        if(action=='success'){
            toastr.success(message,'Success!',{
                "positionClass": "toast-top-right",
                timeOut: 5000,
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut",
                "tapToDismiss": false,
                    onHidden: function () {
                        window.location.href = '{{ url("admin/maids-schedule-table") }}';
                    }
            });
        }else if(action=='warning'){
            toastr.warning(message,'Attention!',{
                "positionClass": "toast-top-right",
                timeOut: 5000,
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut",
                "tapToDismiss": false,


            })
        }else{
            toastr.error(message,'Attention!',{
                "positionClass": "toast-top-right",
                timeOut: 5000,
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut",
                "tapToDismiss": false

            });
        }

    } 





});


</script>
@endsection