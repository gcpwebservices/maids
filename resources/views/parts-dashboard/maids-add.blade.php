

@extends('layouts.app')



@section('content-styles')

    <!-- Custom CSS -->



    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />

    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">

    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">





@endsection



@section('content')

<body class="fix-header fix-sidebar">



    <?php $routes = $routes = Request::segment(3); ?>

    @if ( $routes== 'create'  )

        @include('layouts.dashboard.header')

    @endif                                                                                                                                                             



<div class="container-fluid" id="formaids" style="padding-top: 20px;">

    <div class="panel-body">

        <form method="POST" enctype="multipart/form-data" action="{{route('maids.store')}}" novalidate>

        @csrf

        <div class="row">

            <div class="col-md-12">

                <div class="row">

                    <div class="col-md-3">

           

                        <img style="cursor: pointer;width:100%"  onclick="$('#photo').trigger('click')"  id="cur_img" 

                        class="img img-thumbnail img-responsive" src="{{ asset('img/default_big.png') }}">

                        <p class="text-center">( Click to browse image )</p>

                        <input class="hide" type="file" name="photo" id="photo">



                    </div>

                    <div class="col-md-9">

                        <div class="row">

                            <div class="col-md-12">

                                <div class="hpanel">

                                    <div class="panel-heading" style="padding-bottom: 20px;">

                                        <span><i class="fa fa-user"></i> Personal Information</span></div>

                                    <div class="panel-body">

                                        <div class="row">

                                            <div class="col-md-8">

                                                <div class="form-group">

                                                    <label>First Name <span class="required">*</span></label>

                                                    <input required="" name="fullname" type="text" 

                                                    class="form-control" autocomplete="off">

                                                </div>

                                            </div>

                                            <div class="col-md-3">

                                                <div class="form-group">

                                                    <label class="control-label">Gender</label>

                                                    <select class="form-control custom-select" name="gender" required="required">

                                                        <option value="">--select--</option>

                                                        <option value="2">Male</option>

                                                        <option value="1">Female</option>

                                                    </select>            

                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-md-6">

                                                <div class="form-group">

                                                    <label class="control-label">Date of Birth</label>

                                                    <input type="date"  class="form-control" name="birthdate" 

                                                    id="birthdate" autocomplete="off" 

                                                    required="required">

                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-md-6">

                                                <div class="form-group">

                                                    <label>Mobile Number <span class="required">*</span></label>

                                                    <input required="" name="mobile" 

                                                    type="text" class="form-control" value="@{{mobile}}" required="required" 

                                                    placeholder="+971" maxlength="13" autocomplete="off">

                                                </div>

                                            </div>



                                        </div>

                                        <div class="row">



                                            <div class="col-md-3">

                                                <div class="form-group">

                                                    <label class="control-label">Shift Start</label>

                                                    <input type="time" class="form-control" name="shift_start" 

                                                    id="shift_start" autocomplete="off" 

                                                    required="required">

                                                </div>

                                            </div>



                                            <div class="col-md-3">

                                                <div class="form-group">

                                                    <label class="control-label">Shift End</label>

                                                    <input type="time"  class="form-control" name="shift_end" 

                                                    id="shift_end" autocomplete="off" 

                                                    required="required">

                                                </div>

                                            </div>



                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="form-group">

                    <button class="btn btn-primary" id="btnsubmit" type="submit"><i class="fa fa-save"></i> Save Maid</button>

                    <a href="{{ route('maids.index') }}" class="btn btn-default"><i class="fa fa-times"></i> Cancel</a>

                </div>

            </div>      

        </div>

        </form>            

    </div>



</div>



    <?php $routes = $routes = Request::segment(3); ?>

    @if ( $routes== 'create'  )

        @include('layouts.dashboard.footer')

    @endif                                                                                                                                                                                                                                                                                                                                                                                                            

</body>



@endsection



@section('content-scripts')

    <script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap tether Core JavaScript -->

    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- slimscrollbar scrollbar JavaScript -->

    <script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>

    <!--Menu sidebar -->

    <script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>

    <!--stickey kit -->

    <script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>

    <!--Custom JavaScript -->



    <!-- fullcalendar -->    

    <script src="{{ asset('dashtemplate/fullcalendar/lib/moment.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/fullcalendar/lib/fullcalendar.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/fullcalendar/scheduler.min.js') }}"></script>



    <!-- scripit init-->

    <script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>



    <script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>





    <script src="{{ asset('vue/dist/vue.js') }}"></script>

    <script src="{{ asset('vue/dist/vue.min.js') }}"></script>



    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <script type="text/javascript">



    $(document).ready(function(){



        $("#photo").change(function () {

            readURL(this);

        });



        function readURL(input) {

            if (input.files && input.files[0]) {

                var reader = new FileReader();



                reader.onload = function (e) {

                    // alert(e.target.result);

                    $('#cur_img').attr('src', e.target.result);

                }



                reader.readAsDataURL(input.files[0]);

            }

        }





    }); 





    Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

    new Vue({



        el: '#formaids',



        data: {



            mobile : '+971'





        },



        methods : {



        }







    });



    </script>



@endsection