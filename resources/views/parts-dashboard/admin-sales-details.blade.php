

@extends('layouts.app')



@section('content-styles')

    <!-- Custom CSS -->



    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />

    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">

    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('dashtemplate/css/lib//daterangepicker/daterangepicker.css') }}" />



@endsection



@section('content')

<body class="fix-header fix-sidebar">



@include('layouts.dashboard.header')

    

      <div class="container-fluid app">

                <div class="row form-body card">

             

                        <div class="row">



                            <div class="col-md-4">

                                <div class="input-group input-group-flat ">

                                    <span class="input-group-btn" style="width: 30px; margin-top:9px;">

                                        <i class="fa fa-calendar"></i>

                                    </span>

                                    <input type="text" name="dates" id="dates" class="form-control">  

                                    <span class="input-group-btn">

                                        <button class="btn btn-primary btn-group-right" type="button" v-on:click="dateRange()">

                                            <i class="ti-search" ></i>

                                        </button>

                                    </span>        

                                </div>

                            </div>



                            <div class="col-md-2 pull-right">

                                <div class="dropdown">

                                    <button class="btn btn-primary dropdown-toggle btn-block" type="button" data-toggle="dropdown" aria-expanded="false">

                                        Report Type

                                        <span class="caret"></span>

                                    </button>

                                    <ul class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -2px, 0px); top: 0px; left: 0px; will-change: transform;">

                                        <li v-on:click=""><a href="{{ url('/admin/sales/details') }}"> Table</a></li>

                                        <li v-on:click=""><a href="{{ url('/admin/sales') }}"> Graph</a></li>

          

                                    </ul>

                                </div>

                            </div>

                  

                        </div><!-- row -->  

                </div>



                <!-- Start Page Content -->

                <dir class="card"> 

                    <div class="row" style="padding-top:30px;">

                        <div class="col-md-12">

                            <h4 style="font-weight:normal!important;">Transaction Summary</h4>

                            <h4 class="text-primary" style="font-weight:normal!important;">@{{labels}}</h4>

                            <table class="table" >

                                <thead style="font-weight: bold">

                                    <tr>

                                        <td>TYPE</td>

                                        <td class="text-center">BOOKED </td>

                                        <td class="text-center">PAID </td>

                                        <td class="text-center">REFUND </td>

                                        <td class="text-right">TOTAL</td>

                                        <td class="text-right">DISCOUNT</td>

                                        <td class="text-right">VAT</td>

                                        <td class="text-right">GRAND TOTAL</td>

                                    </tr>

                                </thead>

                                <tbody>

                                    <tr>

                                        <td>Cleaning</td>

                                        <td class="text-center">@{{summaryjson.bookqty}}</td>

                                        <td class="text-center">@{{summaryjson.paidqty}}</td>

                                        <td class="text-center">@{{summaryjson.refundqty}}</td>

                                        <td class="text-right text-primary">@{{summaryjson.total}}</td>

                                        <td class="text-right">@{{summaryjson.disc}}</td>

                                        <td class="text-right">@{{summaryjson.vat}}</td>

                                        <td class="text-right text-primary">@{{summaryjson.grand}}</td>

                                    </tr>

                                </tbody>

                            </table>

                        </div>

       <!--                  <div class="col-md-5">

                            <h4 style="font-weight:normal!important">Cash Movement Summary</h4>

                            <table class="table">

                                <thead>

                                    <tr>

                                        <td>PAYMENT TYPE</td>

                                        <td class="text-right">PAYMENTS COLLECTED</td>

                                        <td class="text-right">REFUND PAID</td>

                                    </tr>

                                </thead>

                                <tbody>

                                    

                                    <tr>

                                        <td>Cash</td>

                                        <td class="text-right">0.00</td>

                                        <td class="text-right">-0.00</td>

                                    </tr>

                                    <tr>

                                        <th>Total Discount</th>

                                        <th class="text-right">-0.00</th>

                                        <th></th>

                                    </tr>

                                    <tr>

                                        <th>Total VAT</th>

                                        <th class="text-right">0.00</th>

                                        <th></th>

                                    </tr>

                                    <tr>

                                        <th>Payments Collected</th>

                                        <th class="text-right">0.00</th>

                                        <th class="text-right">-0.00</th>

                                    </tr>

                              

                                </tbody>

                            </table>

                        </div> -->

                    </div>

                </dir>



            </div>



@include('layouts.dashboard.footer')



                                                                                                                                                                                                                                                                                                                                                                                                              

</body>



@endsection



@section('content-scripts')



<script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>

<!-- Bootstrap tether Core JavaScript -->

<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>

<!-- slimscrollbar scrollbar JavaScript -->

<script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>

<!--Menu sidebar -->

<script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>

<!--stickey kit -->

<script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>

<!--Custom JavaScript -->



<!-- scripit init-->

<script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>



<!-- datatables -->

<script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>



<!-- highcharts -->

<script src="https://code.highcharts.com/highcharts.js"></script>

<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script src="https://code.highcharts.com/modules/export-data.js"></script>



<!-- vue js -->

<script src="{{ asset('vue/dist/vue.js') }}"></script>

<script src="{{ asset('vue/dist/vue.min.js') }}"></script>



<script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>



<!-- moment -->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    



<script type="text/javascript">

$(document).ready(function(){





    $('input[name="dates"]').daterangepicker({

            "showDropdowns": false,

            ranges: {

                // 'Today':        [moment(), moment()],

                // 'Yesterday':    [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

                'Last 7 Days':  [moment().subtract(6, 'days'), moment()],

                'Last 15 Days': [moment().subtract(14, 'days'), moment()],

                'This Month':   [moment().startOf('month'), moment().endOf('month')],

                'Last Month':   [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

            },



                "startDate": moment().subtract(6, 'days'),

                "endDate":   moment(),



            }, function(start, end, label) {



                dropdownlabel = label;

                datefrom = start.format('YYYY-MM-DD');

                dateto = end.format('YYYY-MM-DD');



     



    });









});



var datefrom;

var dateto;

var dropdownlabel;





var year = moment().year();

const monthNames = ["January", "February", "March", "April", "May", "June",

  "July", "August", "September", "October", "November", "December"

];



const d = new Date();





Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

new Vue({

    el: '.app',



    data () {

        return {

            labels:  null,

            paymentsum: null,

            series: null,

            branch: 'Select Branches',

            range: 'Report Range',



            summaryjson: [],

        }

    },



    mounted: function(){



        this.labels = 'Last 7 Days';



        this.$http.post('{{ route('TranSummary') }}').then((response) => {   

            let toparse = JSON.parse(response.data);

            this.summaryjson = toparse;

        });

    },



    methods: {



        fillData: function(){

           

        },



        dateRange: function(){

            



            if(dropdownlabel == 'Custom Range'){

                this.labels = 'From '+' '+moment(datefrom).format('ll') +' '+' To '+' '+moment(dateto).format('ll');

            }else{

                this.labels = dropdownlabel;  

            }

            this.$http.post('{{ route('TranSummaryFilter') }}',{'datefrom':datefrom, 'dateto':dateto}).then((response) => {

                let toparse = JSON.parse(response.data);

                this.summaryjson = toparse;

            });



      

        },





        showChart: function(){



        }



    }



});





</script>

@endsection