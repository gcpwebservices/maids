

@extends('layouts.app')



@section('content-styles')

    <!-- Custom CSS -->



    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />

    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">

    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('dashtemplate/css/lib//daterangepicker/daterangepicker.css') }}" />



@endsection



@section('content')

<body class="fix-header fix-sidebar">



@include('layouts.dashboard.header')

    

        <div class="container-fluid app">



            

                <div class="row form-body card">

            

                    <div class="col-sm-12 col-md-4">

                        <div class="input-group input-group-flat ">

                            <span class="input-group-btn" style="width: 30px; margin-top:9px;">

                                <i class="fa fa-calendar"></i>

                            </span>

                            <input type="text" name="dates" id="dates" class="form-control" >          

                        </div>

                    </div>

                  

                </div>



                <!-- Start Page Content -->

                <div class="row card">   

                    <div class="col-sm-12 col-md-12">

             

                            <div class="col-sm-12 col-md-12">

                                <div class="card-title">

                                    <h4>Sales Graph </h4>

                                </div>

                                <div class="chart-responsive" id="container">

                                    

                                </div>

                            </div>

              

                    </div>

                </div>  



                <div class="card"> 

                    <div class="row" style="padding-top:30px;">

                        <div class="col-sm-12 col-md-12">

                            <h4 style="font-weight:normal!important;">Sales Summary</h4>

                            <h4 class="text-primary" style="font-weight:normal!important;">@{{fortable}}</h4>

                            <div class="table-responsive">

                                <table class="table" >

                                    <thead style="font-weight: bold">

                                        <tr>

                                            <td>TYPE</td>

                                            <td class="text-center">BOOKED </td>

                                            <td class="text-center">PAID </td>

                                            <td class="text-center">CANCELLED </td>

                                     

                                            <td class="text-right">DISCOUNT</td>

                                            <td class="text-right">VAT</td>

                                            <td class="text-right">GRAND TOTAL</td>

                                        </tr>

                                    </thead>

                                    <tbody>

                                        <tr>

                                            <td>Cleaning</td>

                                            <td class="text-center">@{{summaryjson.bookqty}}</td>

                                            <td class="text-center">@{{summaryjson.paidqty}}</td>

                                            <td class="text-center">@{{summaryjson.refundqty}}</td>



                                            <td class="text-right">@{{summaryjson.disc}}</td>

                                            <td class="text-right">@{{summaryjson.vat}}</td>

                                            <td class="text-right text-primary">@{{summaryjson.grand}}</td>

                                        </tr>

                                    </tbody>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>



        </div>



@include('layouts.dashboard.footer')



                                                                                                                                                                                                                                                                                                                                                                                                              

</body>



@endsection



@section('content-scripts')



<script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>

<!-- Bootstrap tether Core JavaScript -->

<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>

<!-- slimscrollbar scrollbar JavaScript -->

<script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>

<!--Menu sidebar -->

<script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>

<!--stickey kit -->

<script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>

<!--Custom JavaScript -->



<!-- scripit init-->

<script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>



<!-- datatables -->

<script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>



<!-- highcharts -->

<script src="https://code.highcharts.com/highcharts.js"></script>

<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script src="https://code.highcharts.com/modules/export-data.js"></script>



<!-- vue js -->

<script src="{{ asset('vue/dist/vue.js') }}"></script>

<script src="{{ asset('vue/dist/vue.min.js') }}"></script>



<script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>



<!-- moment -->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    



<script type="text/javascript">

$(document).ready(function(){





    $('input[name="dates"]').daterangepicker({

            "showDropdowns": false,

            ranges: {

                // 'Today':        [moment(), moment()],

                // 'Yesterday':    [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

                'Last 7 Days':  [moment().subtract(6, 'days'), moment()],

                'Last 15 Days': [moment().subtract(14, 'days'), moment()],

                'This Month':   [moment().startOf('month'), moment().endOf('month')],

                'Last Month':   [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

            },



                "startDate": moment().subtract(6, 'days'),

                "endDate":   moment(),



            }, function(start, end, label) {

         

                dropdownlabel = label;

                datefrom = start.format('YYYY-MM-DD');

                dateto = end.format('YYYY-MM-DD');



                vuejs.dateRange();



    });





});



var datefrom;

var dateto;

var dropdownlabel;



var year = moment().year();

const monthNames = ["January", "February", "March", "April", "May", "June",

  "July", "August", "September", "October", "November", "December"

];



const d = new Date();





Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

var vuejs = new Vue({

    el: '.app',



    data () {

        return {

            labels:  null,

            paymentsum: null,

            series: null,

            branch: 'Select Branches',

            range: 'Report Range',



            summaryjson: [],

            fortable: null



        }

    },



    mounted: function(){



        datefrom = moment().subtract(7, 'days');

        dateto = moment();

        dropdownlabel = 'Last 7 Days';

        this.dateRange();

        this.$http.post('{{ route('TranSummaryFilter') }}').then((response) => {   

         

            if(typeof response.data === 'string') {

               let toparse = JSON.parse(response.data);

               this.summaryjson = toparse;

            }

      

            if(typeof response.data === 'object') {

                let toparse = response.data;

               this.summaryjson = toparse;

            }



        });

    },







    methods: {







        dateRange: function(){





            if(dropdownlabel == 'Custom Range'){

                this.fortable = 'From '+' '+moment(datefrom).format('ll') +' '+' To '+' '+moment(dateto).format('ll');

                this.series   = 'From '+' '+moment(datefrom).format('ll') +' '+' To '+' '+moment(dateto).format('ll');

            }else{

                this.fortable = dropdownlabel; 

                this.series   = dropdownlabel;

            }

      

            this.$http.post('{{ route('daterange') }}',{'datefrom':datefrom, 'dateto':dateto}).then((response) => {



                if(typeof response.data === 'object') {



                    let toparse = response.data;

                    let stringify = response.data;

                    let dates = [];

                    let total = [];



                    let i = 0;

                    let g = 0;



                    $.map( toparse, function( val ) {

                        total[i++] = Math.floor(val.payment);

                    });



                    $.map( toparse, function( val ) {

                        dates[g++] = moment(val.payment_date).format('ll');

                    });





                    this.paymentsum = total;

                    this.labels = dates;



                }



                if(typeof response.data === 'string') {



                    let toparse = JSON.parse(response.data);

                    let stringify = JSON.stringify(response.data);

                    let dates = [];

                    let total = [];



                    let i = 0;

                    let g = 0;



                    $.map( toparse, function( val ) {

                        total[i++] = Math.floor(val.payment);

                    });



                    $.map( toparse, function( val ) {

                        dates[g++] = moment(val.payment_date).format('ll');

                    });





                    this.paymentsum = total;

                    this.labels = dates;

                

                }

 

    

                this.showChart();

            

            });



            this.$http.post('{{ route('TranSummaryFilter') }}',{'datefrom':datefrom, 'dateto':dateto}).then((response) => {



                if(typeof response.data === 'string') {

                   let toparse = JSON.parse(response.data);

                   this.summaryjson = toparse;

                }

          

                if(typeof response.data === 'object') {

                    let toparse = response.data;

                   this.summaryjson = toparse;

                }





            });

      

        },





        showChart: function(){



            chart = Highcharts.chart('container', {

                loading: {

                    hideDuration: 1000,

                    showDuration: 5000

                },

                chart: {

                    type: 'column'

                },

                title: {

                    text: ''

                },

                // legend: {

                //     layout: 'vertical',

                //     align: 'left',

                //     verticalAlign: 'top',

                //     x: 150,

                //     y: 100,

                //     floating: true,

                //     borderWidth: 1,

                //     backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'

                // },

                // exporting: {

                //     buttons: {

                //         contextButton: {

                //             text: 'Export',

                //             verticalAlign: 'bottom',

                //             y: -5

                //         }

                //     }

                // },

                xAxis: {

                    categories: this.labels,

        

                },

                yAxis: {

                    title: {

                        text: ''

                    }

                },

                tooltip: {

                    shared: true,

                    valueSuffix: ' ',

                    pointFormat: "{point.y:.2f} AED"

                },

                credits: {

                    enabled: false

                },

                plotOptions: {

                    area: {

                        pointStart: 1940,

                        marker: {

                            enabled: false,

                            symbol: 'circle',

                            radius: 2,

                            states: {

                                hover: {

                                    enabled: true

                                }

                            }

                        }

                    }

                },

                // plotOptions: {

                //     areaspline: {

                //         fillOpacity: 0.5

                //     }

                // },

                legend: {

                    enabled: true

                },

                series: [{

                    name: this.series,

                    data: this.paymentsum

                }]

            });

        }



    }



});





</script>

@endsection