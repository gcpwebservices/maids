

@extends('layouts.app')



@section('content-styles')

    <!-- Custom CSS -->



    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />

    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">



    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('dashtemplate/css/lib//daterangepicker/daterangepicker.css') }}" />

    <style type="text/css">

        .dataTables_length {

            display: none;

        }

        .dataTable > thead > tr > th[class*="sort"]::after{display: none}



        .dataTables_wrapper .dataTables_paginate{

            padding-bottom: 1.25em !important;

        }

        .dataTables_wrapper .dataTables_paginate {

            font-size: 0.75em !important;

        }

    </style>

@endsection



@section('content')

<body class="fix-header fix-sidebar">



@include('layouts.dashboard.header')

    

<div class="container-fluid app">



    <div class="card">

        <div class="row">

            <div class="col-md-8">

                <div class="col-md-12">

                    <div class="card-title text-center">

                        <?php  $month = date("F", strtotime("-1 months")); $year = date("Y");?>

                        <h4 class="text-primary">{{$month}} {{$year}} Top 10 Maids </h4>



                    </div>

                    <div class="card-body">

                        <div class="table-responsive">

                            <table id="topmaids" class="table table-bordered">

                                <thead>

                                    <tr>

                                        <th>#</th>

                                        <th>Fullname</th>

                                        <th>Booking</th>

                                    </tr>

                                </thead>

                                <tbody>

                                    <?php $count=1;?>
                                    @if($data != '')
                                    @foreach($data as $list)

                                    <tr>

                    

                                        <td>{{$count}}</td>

                                        <td><a href="{{ route('maids.show', $list['id']) }}">{{ $list['fullname'] }}</a></td>

                                        <td class="text-primary">{{ $list['booking'] }}</td>

                         

                                    </tr>

                                    <?php $count++;?>

                                    @endforeach
                                    @endif
                                </tbody>

                            </table>

                        </div>

                    </div>

                </div>



                <div class="col-md-12" id="container" style="height: 400px;"></div>    

              

                     

            </div>



            <div class="col-md-4">

    

                <div class="card-title">

                    <h4 class="text-primary">Maids Profile </h4>

                </div>

                

                <div class="recent-comment">
                @if($data != '')
                    @foreach($data as $list)

                    <div class="media">

                        <div class="media-left">

                            <a href="{{ route('maids.show', $list['id']) }}">

                                <img alt="" src=" {{ asset('storage/maids').'/'.$list['photo'] }}" class="media-object">

                            </a>

                        </div>

                        <div class="media-body">

                            <h4 class="media-heading">{{$list['fullname']}}</h4>

                            <p>Gender: 

                                @if($list['gender'] == 1)

                                    Female

                                @else

                                    Male

                                @endif

                            </p>

                            <p>Birthdate: {{$list['dob'] }}</p>

                        

                        </div>

                    </div>

                    @endforeach
                @endif
                </div>

            </div>

        </div>

    </div>

</div>









@include('layouts.dashboard.footer')



                                                                                                                                                                                                                                                                                                                                                                                                   

</body>



@endsection



@section('content-scripts')



<script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>

<!-- Bootstrap tether Core JavaScript -->

<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>

<!-- slimscrollbar scrollbar JavaScript -->

<script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>

<!--Menu sidebar -->

<script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>

<!--stickey kit -->

<script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>

<!--Custom JavaScript -->



<!-- scripit init-->

<script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>



<!-- datatables -->

<script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>



<!-- highcharts -->

<script src="https://code.highcharts.com/highcharts.js"></script>

<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script src="https://code.highcharts.com/modules/export-data.js"></script>



<!-- vue js -->

<script src="{{ asset('vue/dist/vue.js') }}"></script>

<script src="{{ asset('vue/dist/vue.min.js') }}"></script>



<script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>



<!-- moment -->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    



<script type="text/javascript">

$(document).ready(function(){



    var table = $('#topmaids').DataTable({

        "pageLength": 10,

        "paging":   true,

        "ordering": true,

        "info":     true,

        "autoWidth": false,

        "searching": false,

        "columns": [

            { "width": "5%" },

            { "width": "20%" },

            { "width": "15%" },

        ]

    });



// Create the chart

Highcharts.chart('container', {

    chart: {

        type: 'column'

    },

    title: {

        style: {

            color: '#007bff',

        },

        text: '<h4 class="text-primary">Maids Graph</h4>'

    },

    subtitle: {

        text: ''

    },

    xAxis: {

        type: 'category'

    },

    yAxis: {

        title: {

            text: ''

        }



    },

    legend: {

        enabled: false

    },

    credits: {

        enabled: false

    },

    exporting: { 

        enabled: false 

    },

    plotOptions: {

        series: {

            borderWidth: 0,

            dataLabels: {

                enabled: true,

                format: '{point.y}'

            }

        }

    },



    tooltip: {

        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',

        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> bookings<br/>'

    },



    "series": [

        {

            "name": "Maids",

            "colorByPoint": true,

            "data": [
            <?php if(!empty($data)) {?>
                <?php foreach($data as $list){ ?>

                {   

                    <?php echo "name:".'"'.$list['fullname'].'",'; ?>

                    <?php echo "y:".$list['booking'].',' ?>

                },

                <?php }?>
            <?php }?>
            ]

        }

    ],

});





});





</script>

@endsection