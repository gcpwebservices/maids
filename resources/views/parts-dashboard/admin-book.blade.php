
@extends('layouts.app')

@section('content-styles')
    <!-- Custom CSS -->

    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('dashtemplate/css/lib//daterangepicker/daterangepicker.css') }}" />

    <link rel="stylesheet" href="{{asset('dashtemplate/timepicker/jquery.ui.timepicker.css') }}">
    <link rel="stylesheet" href="{{asset('dashtemplate/timepicker/jquery-ui-1.10.0.custom.min.css') }}">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

@endsection

@section('content')
<body class="fix-header fix-sidebar">

@include('layouts.dashboard.header')
    
<div class="container-fluid">

    <div class="row card">
        <div class="col-md-12">
            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <div class="input-group input-group-flat ">
                        <span class="input-group-btn" style="width: 30px; margin-top:9px;">
                            <i class="fa fa-user"></i>
                        </span>
                        <div class="row-fluid">
                            <select class="form-control" name="customer" id="customer" style="width: 250px;">
                                <option value="">Select Customer</option>

                                @foreach($clist as $customer)
                                    <option value="{{ $customer->id }}">{{ $customer->fullname }}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>       
                </div>
            </div>
        </div>
    </div>


    <div class="row card" id="customer_info">
        <div class="col-sm-12 col-md-12">
                
            <div class="row">
                <div class="col-md-4">
                    <div class="clearfix">
                        <img src="http://192.168.0.198/salon_ocean/public/images/default.png" class="img img-circle pull-left m-r-15" style="width:100px;height:100px">
                        <div class="pull-left">
                            <h3 class="text-upper font-bold">AA Risha</h3>
                            <p></p>
                            <div class="form-group">
                                <a href="http://192.168.0.198/salon_ocean/customer/1/edit" class="btn btn-primary btn-xs">
                                    <i class="fa fa-pencil"></i>
                                    Change Propery address
                                </a>
                        
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table>
                                    <tbody>
                                
                                        <tr>
                                            <th>Mobile No:</th>
                                            <td class="p-l-15">0507561021</td>
                                        </tr>
                                        <tr>
                                            <th>Email Address:</th>
                                            <td class="p-l-15"></td>
                                        </tr>
                                        <tr>
                                            <th>Property Address:</th>
                                            <td class="p-l-15">Abubaker 11 Road. Almakawi</td>
                                        </tr>
                                  
                                    </tbody>
                                </table>
                            </div>
            
                        </div>
                    </div>
                </div>
                <div class="col-md-2" >
                    <button type="button" class="close" aria-label="Close" id="remove"  data-placement="left"
                    data-toggle="tooltip" title="Remove customer">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>
            </div>

        </div>
    </div>
   

    <div class="row card">
        <div class="col-md-12">
            <div class="hpanel">
                <div class="panel-body">
                    
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-12">
                                 
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li class="nav-item"> 
                                                <a class="nav-link active" data-toggle="tab" role="tab" aria-selected="false">
                                                    <span class="hidden-sm-up">
                                                        <i class="ti-home"></i>
                                                    </span> 
                                                    <span class="hidden-xs-down" style="font-weight: bold;">
                                                        One-Off Job
                                                    </span>
                                                </a> 
                                            </li>
                        
                         
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content tabcontent-border">

                                            <div class="tab-pane p-20 active show" id="home" role="tabpanel">

                                                <div class="row p-20">
                                                    <div class="col-md-6">
                                                        <select class="form-control" name="area" id="area" style="width: 250px;">
                                                            <option value="">Select Area</option>
                                                            @foreach($area as $list)
                                                                <option value="{{ $list->id }}">{{ $list->area }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="col-md-6">
                                                        
                                                        <div class="col-md-6">
                                                            <select class="form-control" name="subarea" id="subarea" style="width: 250px;">
                                                                <option value="">Select Subarea</option>
                                                                @foreach($sub as $list)
                                                                    <option value="{{ $list->area_id }}">{{ $list->sub_area }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="row p-20">
                                                    <div class="col-md-5">
                                                      <label class="control-label"> Date:</label>
                                                      <input type="text" class="form-control" name="datetimes" id="datetimes" autocomplete="off" style="height:42px;"/>
                                                    </div>
                                                    <div class="col-md-6" >
                                                       <label class="control-label"> Time:</label>
                                                        <div class="input-group bootstrap-timepicker timepicker">
                                                            <input id="timefrom" type="text" name="timefrom" class="form-control input-small" 
                                                            autocomplete="off" placeholder="Start">

                                                            <input id="timeto" type="text" name="timeto" class="form-control input-small" 
                                                            autocomplete="off" placeholder="End">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="text-center m-t-15">Pay</label>
                                        <input id="input_pay" readonly="" style="font-size:36px;height:60px" value="0" type="text" class="form-control input-lg text-center autonumeric font-bold" data-parsley-id="6720"><ul class="parsley-errors-list" id="parsley-id-6720"></ul>
                                    </div>
                                    <div class="form-group">
                                        <label>Payment Note:</label>
                                        <textarea name="payment_note" class="form-control input-lg" rows="4" data-parsley-id="5700"></textarea><ul class="parsley-errors-list" id="parsley-id-5700"></ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group clearfix">
                                        <label>Payment Method:</label>
                                        <!-- <div class="pull-right"><span><input data-parsley-requred="false" type="radio" name="payment_method" value="Cash" checked class="icheck"> Cash</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><input data-parsley-requred="false" type="radio" name="payment_method" value="Card" class="icheck"> Card</span></div>
                                         -->
                                        <select name="payment_method" class="form-control" data-parsley-id="7836">
                                            <option value="Cash">Cash</option>
                                            <option value="Card">Card</option>
                                        </select><ul class="parsley-errors-list" id="parsley-id-7836"></ul>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group"><button type="submit" id="btn-checkout" class="btn btn-primary font-bold" style="width:100%">Checkout</button></div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group"><button type="submit" id="btn-save-unpaid" class="btn font-bold" style="width:100%">Save Unpaid</button></div>
                                </div>
                                <input type="hidden" id="input-checkout" name="checkout">
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>       
    </div>

</div>

@include('layouts.dashboard.footer')

                                                                                                                                                                                                                                                                                                                                                                                                              
</body>

@endsection

@section('content-scripts')

<script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('dashtemplate/timepicker/migrate.js') }}"></script>

<!-- Bootstrap tether Core JavaScript -->
<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>
<!--Menu sidebar -->
<script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>
<!--stickey kit -->
<script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
<!--Custom JavaScript -->

<!-- scripit init-->
<script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>

<!-- datatables -->
<script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>

<!-- highcharts -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<!-- vue js -->
<script src="{{ asset('vue/dist/vue.js') }}"></script>
<script src="{{ asset('vue/dist/vue.min.js') }}"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<!-- moment -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    

<script src="{{ asset('dashtemplate/timepicker/jquery.ui.timepicker.js') }}"></script>
<script src="{{ asset('dashtemplate/timepicker/jquery.ui.core.min.js') }}"></script>
<script src="{{ asset('dashtemplate/timepicker/jquery.ui.position.min.js') }}"></script>
<script src="{{ asset('dashtemplate/timepicker/jquery.ui.widget.min.js') }}"></script>
<script src="{{ asset('dashtemplate/timepicker/jquery.ui.tabs.min.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>



<script type="text/javascript">
$(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip(); 

    $('#remove').on('click',function(){
        $('#customer_info').hide();
    });
   


    $('#customer_info').hide();

    $('#customer').select2();
    $('#subarea').select2();
    $('#area').select2();

    var curDate = '<?php echo $currentD;?>';
    var curTime = '<?php echo $currentT;?>';



    var d = new Date();
    var h = d.getHours();
    var m = d.getMinutes();

    $("#timefrom").prop('disabled', true);
    $("#timeto").prop('disabled', true);

    $('#timeto').timepicker({
        maxTime: { hour: 20, minute: 00 },
        showLeadingZero: true
    });
 

    $('#timefrom').focus(function(){

        if($('#datetimes').val() > curDate){
            $('#timefrom').timepicker({
                minTime: { hour: 08, minute: 00 },
                maxTime: { hour: 20, minute: 00 },
                showLeadingZero: true
            }); 
      
        }else{
            $('#timefrom').timepicker({
                minTime: { hour: h, minute: m },
                maxTime: { hour: 20, minute: 00 },
                showLeadingZero: true
            }); 
        }

    });

    $('#timefrom').on('change',function(){

        $("#timeto").prop('disabled', false);

        var startDate = $('#datetimes').val();
        var startTime = $('#timefrom').val();

        var date = new Date(startDate + ' ' + startTime);

        var newtimeto = moment(date).add(1, 'hours').format('HH:mm');
        var newdate = moment(date).add(1, 'hours');

        $('#timeto').val(newtimeto);
        var hour = newdate.hour();
        var minutes = newdate.minutes();

        var datetimefrom = moment(date).format('YYYY-MM-DD HH:mm:ss');
        $('#schedule_start').val(datetimefrom);

        tpMinMaxSetMinTime(hour,minutes);

    });

    function tpMinMaxSetMinTime( hours, minutes ) {
        $('#timeto').timepicker('option', { minTime: { hour: hours, minute: minutes} });
        var startDate = $('#datetimes').val();
        var date = new Date(startDate + ' ' +  $('#timeto').val());
        var datetimeto = moment(date).format('YYYY-MM-DD HH:mm:ss');
        $('#schedule_end').val(datetimeto);
    }

    $('input[name="datetimes"]').daterangepicker(
        { 
        autoUpdateInput: false,
        timePicker: false,
        singleDatePicker: true,  
        minDate: new Date(),
        startDate: moment().startOf('hour'),
        endDate: moment().startOf('hour').add(36, 'hour'),
            locale: {

                format: 'YYYY-MM-DD'
            }
        },
        function(start, end, label) {
            $('#schedule_start').val(start.format('YYYY-MM-DD'));
            $('#schedule_end').val(end.format('YYYY-MM-DD'));
            $('#datetimes').val(start.format('YYYY-MM-DD'));

            // get_available_maids($('#schedule_start').val(),$('#schedule_end').val(),$('#interval').val(),'add');
            $("#timefrom").prop('disabled', false);
            $("#timeto").prop('disabled', false);

            if($('#datetimes').val() > curDate){
                $('#timefrom').timepicker({
                    minTime: { hour: 08, minute: 00 },
                    maxTime: { hour: 20, minute: 00 },
                    showLeadingZero: true
                }); 
          
            }else{
                $('#timefrom').timepicker({
                    minTime: { hour: h, minute: m },
                    maxTime: { hour: 20, minute: 00 },
                    showLeadingZero: true
                }); 
            }

            //alert('A date range was chosen: ' + start.format('YYYY-MM-DD hh:mm:ss') + ' to ' + end.format('YYYY-MM-DD hh:mm:ss'));
        }
    );




 $('#customer').on('change', function() {
    $('#customer_info').show();
      var data = $(this).val();
        alert(data);
    })

});
</script>
@endsection