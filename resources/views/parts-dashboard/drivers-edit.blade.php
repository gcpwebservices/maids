

@extends('layouts.app')



@section('content-styles')

    <!-- Custom CSS -->



    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />

    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">

    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">



@endsection



@section('content')

<body class="fix-header fix-sidebar">



    <?php $routes = $routes = Request::segment(4); ?>

    @if ( $routes== 'edit'  )

        @include('layouts.dashboard.header') 

    @endif    



<div class="container-fluid" id="formaids" style="padding-top: 20px;">



    @if(session('status'))

    <div id="successMessage" class="alert alert-{{session('status')[0]}}" style="background-color: #dff0d8 !important;color: #3c763d!important; border-color: #d6e9c6 !important;">

    {{session('status')[1]}}

    </div>

    @endif



    <div class="panel-body">

        <form method="POST" enctype="multipart/form-data" action="{{ route('drivers.update', $dedit->id) }}" novalidate>

        @csrf

        @method('PUT')

        <div class="row">

            <div class="col-md-12">

                <div class="row">

                    <div class="col-md-3">

           

                        @if($dedit->photo != '')

                           <img style="cursor: pointer;width:100%" onclick="$('#photo').trigger('click')" id="cur_img" 

                           class="img img-thumbnail img-responsive" src="{{ asset('storage/drivers').'/'.$dedit->photo }}">

                            <p class="text-center">( Click to browse image )</p>

                        @else

                          <img style="cursor: pointer;width:100%"  onclick="$('#photo').trigger('click')"  id="cur_img" 

                          class="img img-thumbnail img-responsive" src="{{ asset('img/default_big.png') }}">

                            <p class="text-center">( Click to browse image )</p>

                        @endif

                        <input class="hide" type="file" name="photo" id="photo">



                    </div>

                    <div class="col-md-9">

                        <div class="row">

                            <div class="col-md-12">

                                <div class="hpanel">

                                    <div class="panel-heading" style="padding-bottom: 20px;">

                                        <span><i class="fa fa-user"></i> Personal Information</span></div>

                                    <div class="panel-body">

                                        <div class="row">

                                            <div class="col-md-8">

                                                <div class="form-group">

                                                    <label class="control-label">Fullname</label>

                                                    <input type="text" id="fullname" name="fullname" value="{{$dedit->fullname}}" 

                                                    class="form-control" autocomplete="off" required="Required">                                                    

                                                </div>

                                            </div>

                                            <div class="col-md-3">

                                                <div class="form-group">

                                                    <label class="control-label">Gender</label>

                                                    <select class="form-control custom-select"  name="gender" required="required">            

                                                        <option value="1" @if(old('gender',$dedit->gender) == '1') selected @endif >

                                                            Female

                                                        </option>

                                                        <option value="2" @if(old('gender',$dedit->gender) == '2') selected @endif >

                                                            Male

                                                        </option>

                                                    </select>            

                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-md-6">

                                               <div class="form-group">

                                                    <label class="control-label">Date of Birth</label>

                                                    <input type="date"  class="form-control" name="birthdate" 

                                                    id="birthdate" autocomplete="off" value="{{ date('Y-m-d',strtotime($dedit->birthdate)) }}"

                                                    required="required">

                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">



                                            <div class="col-md-6">

                                                <div class="form-group">

                                                    <label class="control-label">Mobile</label>

                                                    <input type="text" class="form-control" name="mobile" 

                                                    id="mobile" autocomplete="off" value="{{$dedit->mobile}}" 

                                                    required="required" placeholder="+971" maxlength="13">

                                                </div>

                                            </div>



                                        </div>

                                        <div class="row">

                                            

                                            <div class="col-md-3">

                                                <div class="form-group">

                                                    <label class="control-label">Shift Start</label>

                                                    <input type="time" class="form-control" name="shift_start" 

                                                    id="shift_start" autocomplete="off" value="{{$dedit->shift_start}}"

                                                    required="required">

                                                </div>

                                            </div>



                                            <div class="col-md-3">

                                                <div class="form-group">

                                                    <label class="control-label">Shift End</label>

                                                    <input type="time"  class="form-control" name="shift_end" 

                                                    id="shift_end" autocomplete="off" value="{{$dedit->shift_end}}"

                                                    required="required">

                                                </div>

                                            </div>



                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="form-group">

                    <button class="btn btn-primary" id="btnsubmit" type="submit"><i class="fa fa-save"></i> Save</button>

                    <a href="{{ route('drivers.index') }}" class="btn btn-default"><i class="fa fa-times"></i> Cancel</a>

                </div>

            </div>      

        </div>

        </form>            

    </div>



</div>



    <?php $routes = $routes = Request::segment(4); ?>

    @if ( $routes== 'edit'  )

        @include('layouts.dashboard.footer') 

    @endif    

                                                                                                                                                                                                                                                                                                                                                                                                          

</body>



@endsection



@section('content-scripts')

    <script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap tether Core JavaScript -->

    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- slimscrollbar scrollbar JavaScript -->

    <script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>

    <!--Menu sidebar -->

    <script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>

    <!--stickey kit -->

    <script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>

    <!--Custom JavaScript -->



    <!-- fullcalendar -->    

    <script src="{{ asset('dashtemplate/fullcalendar/lib/moment.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/fullcalendar/lib/fullcalendar.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/fullcalendar/scheduler.min.js') }}"></script>



    <!-- scripit init-->

    <script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>



    <script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>





    <script src="{{ asset('vue/dist/vue.js') }}"></script>

    <script src="{{ asset('vue/dist/vue.min.js') }}"></script>



    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <script type="text/javascript">



    $(document).ready(function(){

        setTimeout(function() {

            $('#successMessage').fadeOut('fast');

        }, 5000);





        //photo change onclick

        $("#photo").change(function () {

            readURL(this);

        });



        function readURL(input) {

            if (input.files && input.files[0]) {

                var reader = new FileReader();



                reader.onload = function (e) {

                    // alert(e.target.result);

                    $('#cur_img').attr('src', e.target.result);

                }



                reader.readAsDataURL(input.files[0]);

            }

        }





    });





    Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

    new Vue({



        el: '#formaids',



        data: {



            mobile : '+971'





        },



        methods : {



        }







    });



    </script>



@endsection