
@extends('layouts.app')

@section('content-styles')
    <!-- Custom CSS -->

    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">


@endsection

@section('content')
<body class="fix-header fix-sidebar">

    <?php $routes = Route::current()->getName(); ?>
    @if ( $routes== 'add-drivers')
        @include('layouts.dashboard.header')
    @endif

     <div class="container-fluid" id="fordrivers">

        @if(session('status'))
        <div id="successMessage" class="alert alert-{{session('status')[0]}}" style="background-color: #dff0d8 !important;color: #3c763d!important; border-color: #d6e9c6 !important;">
        {{session('status')[1]}}
        </div>
        @endif

        <div class="card">
            <div class="card-body">
                 <button class="btn btn-success pull-right" data-toggle="modal" data-target="#create-drivers">Add Driver</button>
                <div class="table-responsive m-t-40">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Fullname</th>
                                <th>Gender</th>
                                <th>Birthdate</th>
                                <th>Action</th>
                           
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($dlist as $list)
                            <tr>
                                <td >{{ $list->fullname }}</td>
                                <td>{{ $list->gender == "1" ? "Female" : "Male" }}</td>
                                <td>{{ $list->birthdate->format('Y-m-d') }}</td>
                                <td>
                                    <button class="btn-sm btn-success"  v-on:click="getid('{{$list->id}}')">Edit</button> |
                                    <button class="btn-sm btn-danger"  v-on:click="openmodal('{{$list->id}}')">Delete</button>
                                </td>
                            </tr>
                      
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>




            <div class="modal fade" id="create-drivers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card card-outline-primary">
                                        <div class="card-body">
                                            <form method="POST" enctype="multipart/form-data" v-on:submit.prevent="createDrivers">
                                            @csrf
                                            <input type="hidden" value="adddrivers" name="action" v-model="driverItem.action">
                                                <div class="form-body">
                                                    <h3 class="card-title m-t-15">Person Info</h3>
                                                    <hr>
                                                    <div class="row p-t-20">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Fullname</label>
                                                                <input type="text" id="fullname" name="fullname" class="form-control" autocomplete="off"
                                                                v-model="driverItem.fullname" required="Required">                                                    
                                                            </div>
                                                        </div>

                                                         <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Gender</label>
                                                                <select class="form-control custom-select" v-model="driverItem.gender" name="gender" required="required">
                                                                    <option value="">--select--</option>
                                                                    <option value="2">Male</option>
                                                                    <option value="1">Female</option>
                                                                </select>
                                                                                  
                                                            </div>
                                                        </div>
                                               
                                                 
                                                    </div>
                                          
                                                    <div class="row">
                                              
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Date of Birth</label>
                                                                <input type="date" v-model="driverItem.birthdate" class="form-control" name="birthdate" 
                                                                id="birthdate" autocomplete="off" 
                                                                required="required">
                                                              
                                                            </div>
                                                        </div>
                                                  
                                                    </div>
                                           

                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                                    <button type="button"  class="closemodal btn btn-inverse pull-right">Cancel</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- edit user -->
            <div class="modal fade" id="edit-drivers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card card-outline-primary">
                                        <div class="card-body">
                                        <form method="POST" enctype="multipart/form-data" v-on:submit.prevent="updateDrivers">
                                            @csrf
        
                                                <div class="form-body">
                                                    <h3 class="card-title m-t-15">Person Info</h3>
                                                    <hr>
                                                    <div class="row p-t-20">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Fullname</label>
                                                                <input type="text" id="fullname" name="fullname" class="form-control" autocomplete="off"
                                                                v-model="returned.fullname" required="Required">                                                    
                                                            </div>
                                                        </div>

                                                         <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Gender</label>
                                                                <select class="form-control custom-select" v-model="returned.gender" name="gender" required="required">
                                                                    <option value="">--select--</option>
                                                                    <option value="2">Male</option>
                                                                    <option value="1">Female</option>
                                                                </select>
                                                                                  
                                                            </div>
                                                        </div>
                                               
                                                 
                                                    </div>
                                          
                                                    <div class="row">
                                              
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Date of Birth</label>
                                                                <input type="date" v-model="returned.birthdate" class="form-control" name="birthdate" 
                                                                id="birthdate" autocomplete="off" 
                                                                required="required">
                                                              
                                                            </div>
                                                        </div>
                                                  
                                                    </div>
                                           

                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                                    <button type="button"  class="closemodal btn btn-inverse pull-right">Cancel</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- delete -->

            <div class="modal fade" id="del-drivers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-md">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                  </div>
                  <div class="modal-body">
                    <p>Are you sure you want to delete the driver?</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button"  class=" btn btn-danger pull-left" v-on:click="deleteDrivers()">Yes</button>
                    <button type="button" class="btn btn-success pull-right closemodal" data-dismiss="modal">No</button>
                  </div>
                </div>

              </div>
            </div>



    </div>

    <?php $routes = Route::current()->getName() ?>
    @if ( $routes== 'add-drivers'  )
        @include('layouts.dashboard.footer')

    @endif                                                                                                                                                                                                                                                                                                                                                                                                                 
</body>

@endsection

@section('content-scripts')

    <script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <!--Custom JavaScript -->

    <!-- fullcalendar -->    
    <script src="{{ asset('dashtemplate/fullcalendar/lib/moment.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/fullcalendar/lib/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/fullcalendar/scheduler.min.js') }}"></script>

    <!-- scripit init-->
    <script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>


    <script src="{{ asset('vue/dist/vue.js') }}"></script>
    <script src="{{ asset('vue/dist/vue.min.js') }}"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>


<script type="text/javascript">

    $(document).ready(function(){
        

        $('.closemodal').click(function(){
            // $("#edit-user").modal('hide');
            window.location.href = "{{url('admin/add-drivers')}}"; 
        });

      
    });


    Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

    new Vue({

        el: '#fordrivers',

        data: {
            formErrors : {},
            
            driverItem : {
                'fullname':'',
                'gender':'',
                'birthdate':'',
                'action': ''
            },
            returned : {

                'id':'',
                'fullname':'',
                'gender':'',
                'birthdate': '',
                'action': 'editdrivers'
            },

            deleId : {
                'id': ''
            }

   
        },


        methods : {
            createDrivers: function(){
                var input = this.driverItem;
                this.$http.post('{{ route('drivers') }}',input).then((response) => {
                    window.location.href = "{{url('admin/add-drivers')}}"; 
                }, (response) => {
                    this.formErrors = response.data;
                });
            },

            getid: function(id){
                this.$http.post('{{ route('drivers') }}',{'id':id, 'action':'getid'}).then((response) => {
                    var toparse = JSON.parse(response.data);
                        this.returned.id = toparse.id;
                        this.returned.fullname = toparse.fullname;
                        this.returned.gender = toparse.gender;
                        this.returned.birthdate = moment(toparse.birthdate).format('YYYY-MM-DD');
                    $("#edit-drivers").modal('show');
                });
            },

            updateDrivers: function(){
                var input = this.returned;
                this.$http.post('{{ route('drivers') }}',input).then((response) => {
                    window.location.href = "{{url('admin/add-drivers')}}";
                }, (response) => {
                    this.formErrors = response.data;
                });
            },
            openmodal: function(id){
                $("#del-drivers").modal('show');
                this.deleId.id = id;
            },  

            deleteDrivers: function(){
                this.$http.post('{{ route('drivers') }}',{'id':this.deleId, 'action':'deldriver'}).then((response) => { 
                    window.location.href = "{{ url('admin/add-drivers') }}"; 
                });

            }

 



          
        }






    });

</script>
@endsection