
@extends('layouts.app')

@section('content-styles')
    <!-- Custom CSS -->

    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('dashtemplate/css/lib//daterangepicker/daterangepicker.css') }}" />
    <style type="text/css">
        td,th{ 
            font-size: 12px; 
         
        }
        .dataTables_wrapper .dataTables_paginate{
            padding-bottom: 1.25em !important;
        }
        .dataTables_wrapper .dataTables_paginate {
            font-size: 0.75em !important;
        }
    </style>
@endsection

@section('content')
<body class="fix-header fix-sidebar">

@include('layouts.dashboard.header')
    
    <div class="container-fluid app">
        <div class="row form-body card">
            <div class="col-sm-12 col-md-4">
                <div class="input-group input-group-flat ">
                    <span class="input-group-btn" style="width: 30px; margin-top:9px;">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input type="text" name="dates" id="dates" class="form-control">  
                        <input type="hidden" name="from" id="from" class="form-control">  
                        <input type="hidden" name="to" id="to" class="form-control">  
         <!--            <span class="input-group-btn">
                        <button class="btn btn-primary btn-group-right" type="button" id="button" >
                            <i class="ti-search" ></i>
                        </button>
                    </span>  -->       
                </div>
            </div>
          
        </div>

        <div class="card"> 
            <div class="table-responsive">
                <table id="example23" class="display nowrap table table-hover table-striped table-bordered" 
                cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Service No.</th>
                            <th>Customer</th>
                            <th>Maid</th>
                            <th>Driver</th>
                            <th>Transaction Type</th>
                            <th>Service Status</th>
                            <th>Payment Status</th>
                            <th>Service Date</th>
                            <th>Service Time</th>
                            <th>Price</th>
                   
                        </tr>
                    </thead>
     
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>


<input type="hidden" name="idval" id="idval">




@include('layouts.dashboard.footer')

                                                                                                                                                                                                                                                                                                                                                                                                   
</body>

@endsection

@section('content-scripts')

<script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>
<!--Menu sidebar -->
<script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>
<!--stickey kit -->
<script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
<!--Custom JavaScript -->

<!-- scripit init-->
<script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>

<!-- datatables -->
<script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>

<!-- highcharts -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<!-- vue js -->
<script src="{{ asset('vue/dist/vue.js') }}"></script>
<script src="{{ asset('vue/dist/vue.min.js') }}"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<!-- moment -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    

<script type="text/javascript">
$(document).ready(function(){


$('#paynow').on('click', function () {
    $('#myModal').modal('hide');
    $('#myModalNew').modal('show');
})


var from;
var to;
var dropdownlabel;

$('input[name="dates"]').daterangepicker({
    "showDropdowns": false,
    ranges: {
        // 'Today':        [moment(), moment()],
        // 'Yesterday':    [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days':  [moment().subtract(6, 'days'), moment()],
        'Last 15 Days': [moment().subtract(14, 'days'), moment()],
        'This Month':   [moment().startOf('month'), moment().endOf('month')],
        'Last Month':   [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },

        "startDate": moment().subtract(6, 'days'),
        "endDate":   moment(),

    }, function(start, end, label) {
     
        dropdownlabel = label;
        from = start.format('YYYY-MM-DD');
        to = end.format('YYYY-MM-DD');
        $('[name="from"]').val(from);
        $('[name="to"]').val(to);

        salesdatatable.ajax.reload();

});

    var salesdatatable =  $('#example23').DataTable({
        processing: true,
        serverSide: true,
        ordering: true,
        // dom: 'Bfrtip',

        // buttons: [
        //     'copy', 'csv', 'excel', 'pdf', 'print'
        // ],
        oLanguage: {
            sProcessing: "<span id='spinner' class='fa fa-spinner fa-2x fa-spin text-blue'></span>"
        },
        ajax: { 
            url: '{{ route('AccountingTable') }}',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: function ( d ) {
                d.datefrom = $('[name="from"]').val();
                d.dateto = $('[name="to"]').val();
               
            }
        },
        "createdRow": function( row, data, dataIndex ) {

            $('td',row).hover(function() {
                $(this).addClass('pointer');
            });
            
            $('td',row).on('click',function(){
                $('#myModal').modal('toggle');
                // $('#idval').val(data.id);
                window.location.href = '{{ url("admin/accounting") }}'+'/'+data.id+'/transaction-details';
            });
        },

        columns: [
            {data:'schedule_code',name:'schedule_code',class:'nowrap'},
            {data:'get_customer.fullname',name:'getCustomer.fullname'},
            {data:'get_maid.fullname',name:'getMaid.fullname'},
            {data:
                function(data) {
                    if(data.get_driver)
                    {
                        return data.get_driver.fullname;

                    }else{
                        return 'N/A';
                    }
                },name:'getDriver.fullname'
            },
            {data:'get_sales_details.transaction_type',name:'getSalesDetails.transaction_type'},
            {data:'completed',name:'completed'},
            {data:'get_sales_details.payment_status',name:'getSalesDetails.payment_status'},
            {data:'schedule_date',name:'schedule_date'},
            {data:'schedule_start',name:'schedule_start'},
            {data:'get_sale_summary.price',name:'getSaleSummary.price'}


        ]
    });



});


</script>
@endsection