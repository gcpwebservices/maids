
@extends('layouts.app')

@section('content-styles')
    <!-- Custom CSS -->

    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">


@endsection

@section('content')
<body class="fix-header fix-sidebar">

    <?php $routes = Route::current()->getName(); ?>
    @if ( $routes== 'add-maids' )
        @include('layouts.dashboard.header')
    @endif

<div class="container-fluid" id="formaids">
    <div class="row">
        <div class="col-md-12">
            <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                    
                    @{{dropdownmaids.fullname}}
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    @foreach ($mlist as $list)
                        <li class="dntclick" v-on:click="dropdownMaids('{{$list->id}}'),
                            dropdownBooking('{{$list->id}}'),dropdownTable('{{$list->id}}')">
                            <a href="#">{{$list->fullname}}</a>
                        </li>
                    @endforeach
                </ul>
        
            </div>
        </div>
    </div>

<!-- Start Page Content -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @if(session('status'))
                    <div id="successMessage" class="alert alert-{{session('status')[0]}}" style="background-color: #dff0d8 !important;color: #3c763d!important; border-color: #d6e9c6 !important;">
                    {{session('status')[1]}}
                    </div>
                    @endif
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item"> 
                            <a class="nav-link active" data-toggle="tab" href="#overview" role="tab">
                                <span class="hidden-sm-up"><i class="ti-home"></i></span> 
                                <span class="hidden-xs-down">Overview</span>
                            </a> 
                        </li>
                        <li class="nav-item"> 
                            <a class="nav-link" data-toggle="tab" href="#account" role="tab">
                                <span class="hidden-sm-up"><i class="ti-user"></i></span> 
                                <span class="hidden-xs-down">Account</span>
                            </a> 
                        </li>
                <!--         <li class="nav-item"> 
                            <a class="nav-link" data-toggle="tab" href="#schedule" role="tab">
                                <span class="hidden-sm-up"><i class="ti-email"></i></span> 
                                <span class="hidden-xs-down">Schedule</span>
                            </a> 
                        </li> -->
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content tabcontent-border">

                        <div class="tab-pane active" id="overview" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="invoice" class="effect2">
                                        <div id="invoice-top">
                                            <div class="clientlogo"></div>
                                            <div class="invoice-info">
                                                <h3 class="text-primary">@{{dropdownmaids.fullname}}</h3>
                                                <hr>
                                                <p > 
                                                    <i class="fa fa-birthday-cake" aria-hidden="true"></i>
                                                        @{{dropdownmaids.birthdate}}
                                                </p>
                                                <p> 
                                                    <i class="fa fa-venus-mars" aria-hidden="true"></i>
                                                        @{{dropdownmaids.gender}}
                                               </p>
                                                <p> 
                                                    <i class="fa fa-mobile" aria-hidden="true"></i>
                                                        @{{dropdownmaids.mobile}}
                                               </p>
                                            </div>
                                            <!--End Info-->
                                       
                                            <div class="title col-md-4 text-center booking">
                                                <h4 class="text-danger">Booking Summary</h4>
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-sm-6 "> 
                                                            <p>TOTAL BOOKINGS:</p>
                                                        </div>
                                                    
                                                        <div class="col-sm-6 ">
                                                            <p class="text-primary">@{{dropdownbooking.totalbooked}}</p>
                                                        </div>
                                                    </div>
                                             
                                                    <div class="row">
                                                         <div class="col-sm-6 ">
                                                            <p>WORKING HOURS:</p>
                                                        </div>

                                                         <div class="col-sm-6 ">
                                                            <p class="text-primary">@{{dropdownmaids.shift_start}} - @{{dropdownmaids.shift_end}}</p>
                                                        </div>
                                                    </div>
                                                
                                                    <div class="row">
                                                         <div class="col-sm-6 ">
                                                            <p>HOURSE WORKED:</p>
                                                        </div>
                                                         <div class="col-sm-6">
                                                            <p class="text-primary">@{{dropdownbooking.totalhours}}</p>
                                                        </div>
                                                    </div>

                                            </div>
                                          
                                        </div>
                                
                                        <div id="invoice-bot">
                                            <div class="table-responsive m-t-40">
                                                <h4 class="text-primary">Bookings</h4>  
    
                                                <table  id="" class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Service #</th>
                                                            <th>Booking Date</th>
                                                            <th>Time</th>
                                                            <th>Client</th>
                                                            <th>Status</th>
                                             
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <tr v-for="item in dropdowntable">
                                                            <td >@{{ item.schedule_code }}</td>
                                                            <td >@{{ item.schedule_start }}</td>
                                                            <td >@{{ item.time }}</td>
                                                            <td >@{{ item.client }}</td>
                                                            <td >@{{ item.status }}</td>
                                                        </tr>
                                                  
                                                    </tbody>
                                                </table>


                                                <div class="dropdown" style="padding-bottom: 1em;">
                                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                                                        Limit Records:
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                      
                                                        <li class="dntclick" v-on:click="dropdownLimit('5')">
                                                            <a href="#">5</a>
                                                        </li>
                                                        <li class="dntclick" v-on:click="dropdownLimit('10')">
                                                            <a href="#">10</a>
                                                        </li>
                                                        <li class="dntclick" v-on:click="dropdownLimit('15')">
                                                            <a href="#">15</a>
                                                        </li>
                                                        <li class="dntclick" v-on:click="dropdownLimit('20')">
                                                            <a href="#">25</a>
                                                        </li>
                                                    
                                                    </ul>

                                                </div>
                                                

                                            </div>       
                                        </div>
                                        <!--End InvoiceBot-->
                                    </div>

                                    <!--End Invoice-->
                                </div>


                            </div>

                        </div>

                        <div class="tab-pane  p-20" id="account" role="tabpanel">
                            <button class="btn btn-success pull-right" data-toggle="modal" data-target="#create-maids">Add Maid</button>
                            <div class="table-responsive m-t-40">
       
                            </div>
                        </div>
<!-- 
                        <div class="tab-pane p-20" id="schedule" role="tabpanel">
                       
                        </div> -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End PAge Content -->


        <!-- create -->
        <div class="modal fade" id="create-maids" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card card-outline-primary">
                                    <div class="card-body">
                                        <form method="POST" enctype="multipart/form-data" v-on:submit.prevent="createMaids">
                                        @csrf
                                        <input type="hidden" value="addmaids" name="action" v-model="maidItem.action">
                                            <div class="form-body">
                                                <h3 class="card-title m-t-15">Person Info</h3>
                                                <hr>
                                                <div class="row p-t-20">
                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <label class="control-label">Fullname</label>
                                                            <input type="text" id="fullname" name="fullname" class="form-control" autocomplete="off"
                                                            v-model="maidItem.fullname" required="Required">                                                    
                                                        </div>
                                                    </div>

                                                     <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label">Gender</label>
                                                            <select class="form-control custom-select" v-model="maidItem.gender" name="gender" required="required">
                                                                <option value="">--select--</option>
                                                                <option value="2">Male</option>
                                                                <option value="1">Female</option>
                                                            </select>            
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Date of Birth</label>
                                                            <input type="date" v-model="maidItem.birthdate" class="form-control" name="birthdate" 
                                                            id="birthdate" autocomplete="off" 
                                                            required="required">
                                                        </div>
                                                    </div>
                                           
                                             
                                                </div>
                                      
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label">Mobile</label>
                                                            <input type="text" v-model="maidItem.mobile" class="form-control" name="mobile" 
                                                            id="mobile" autocomplete="off" 
                                                            required="required" placeholder="+971">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label">Shift Start</label>
                                                            <input type="time" v-model="maidItem.shift_start" class="form-control" name="shift_start" 
                                                            id="shift_start" autocomplete="off" 
                                                            required="required">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label">Shift End</label>
                                                            <input type="time" v-model="maidItem.shift_end" class="form-control" name="shift_end" 
                                                            id="shift_end" autocomplete="off" 
                                                            required="required">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                                <button type="button"  class="closemodal btn btn-inverse pull-right">Cancel</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- edit user -->
        <div class="modal fade" id="edit-maids" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card card-outline-primary">
                                    <div class="card-body">
                                    <form method="POST" enctype="multipart/form-data" v-on:submit.prevent="updateMaids">
                                        @csrf

                                            <div class="form-body">
                                                <h3 class="card-title m-t-15">Person Info</h3>
                                                <hr>
                                                <div class="row p-t-20">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Fullname</label>
                                                            <input type="text" id="fullname" name="fullname" class="form-control" autocomplete="off"
                                                            v-model="returned.fullname" required="Required">                                                    
                                                        </div>
                                                    </div>

                                                     <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Gender</label>
                                                            <select class="form-control custom-select" v-model="returned.gender" name="gender" required="required">
                                                                <option value="">--select--</option>
                                                                <option value="2">Male</option>
                                                                <option value="1">Female</option>
                                                            </select>
                                                                              
                                                        </div>
                                                    </div>
                                           
                                             
                                                </div>
                                      
                                                <div class="row">
                                          
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Date of Birth</label>
                                                            <input type="date" v-model="returned.birthdate" class="form-control" name="birthdate" 
                                                            id="birthdate" autocomplete="off" 
                                                            required="required">
                                                          
                                                        </div>
                                                    </div>
                                              
                                                </div>
                                       

                                            </div>
                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                                <button type="button"  class="closemodal btn btn-inverse pull-right">Cancel</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- delete -->
        <div class="modal fade" id="del-maids" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
              </div>
              <div class="modal-body">
                <p>Are you sure you want to delete the maid?</p>
              </div>
              <div class="modal-footer">
                <button type="button"  class=" btn btn-danger pull-left" v-on:click="deleteMaids()">Yes</button>
                <button type="button" class="btn btn-success pull-right closemodal" data-dismiss="modal">No</button>
              </div>
            </div>

          </div>
        </div>


</div>

    <?php $routes = Route::current()->getName() ?>
    @if ( $routes== 'add-maids'  )
        @include('layouts.dashboard.footer')

    @endif                                                                                                                                                                                                                                                                                                                                                                                                                 
</body>

@endsection

@section('content-scripts')

    <script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <!--Custom JavaScript -->

    <!-- fullcalendar -->    
    <script src="{{ asset('dashtemplate/fullcalendar/lib/moment.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/fullcalendar/lib/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/fullcalendar/scheduler.min.js') }}"></script>

    <!-- scripit init-->
    <script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>


    <script src="{{ asset('vue/dist/vue.js') }}"></script>
    <script src="{{ asset('vue/dist/vue.min.js') }}"></script>
    <script src="{{ asset('vue/dist/vuejs-datatable.js') }}"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    

<script type="text/javascript">
    $(document).ready(function(){
        $('.dntclick').click( function(e) {
             e.preventDefault();

        });

        // $('#myTableOver').DataTable();
        $('.closemodal').click(function(){
            // $("#edit-user").modal('hide');
            window.location.href = "{{url('admin/add-maids')}}"; 
        });

    
    });

    Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
    new Vue({   

        el: '#formaids',

        data: {

            dropdownmaids : {
                'fullname':'Select a Maid',
                'gender':'',
                'birthdate':'',
                'mobile':'',
                'shift_start': '',
                'shift_end': ''
            },
    
            dropdownbooking : {
                'totalbooked': '',
                'totalhours': '',
            },
            maidsid: '',
            limit: '',
            search: '',

            dropdowntable : [{}],

            formErrors : {},
            
            maidItem : {
                'fullname':'',
                'gender':'',
                'birthdate':'',
                'mobile':'',
                'shift_start':'',
                'shift_end':'',
                'action': ''
            },
            returned : {

                'id':'',
                'fullname':'',
                'gender':'',
                'birthdate': '',
                'action': 'editmaids'
            },

            deleId : {
                'id': ''
            }

   
        },


        methods : {

            dropdownMaids: function(id){
                this.$http.post('{{ route('getMaidDetails') }}',{'id':id, 'action':'top'}).then((response) => {
                    var toparse = JSON.parse(response.data);
                    var newgender;
                    if( toparse.gender == '2'){
                        var newgender = 'Male';
                    }else{
                        var newgender = 'Female';
                    }
     
                    this.dropdownmaids.fullname = toparse.fullname;
                    this.dropdownmaids.gender = newgender;
                    this.dropdownmaids.mobile = toparse.mobile;
                    this.dropdownmaids.photo = toparse.photo;
                    this.dropdownmaids.shift_start = toparse.shift_start;
                    this.dropdownmaids.shift_end = toparse.shift_end;
                    this.dropdownmaids.birthdate = moment(toparse.birthdate).format('YYYY-MM-DD');
                    
        
                });
            },

            dropdownBooking: function(id){
                this.$http.post('{{ route('getMaidDetails') }}',{'id':id, 'action':'bottom'}).then((response) => {
                    var toparse = JSON.parse(response.data);

                    this.dropdownbooking.totalbooked = toparse.totalbooked;    
                    this.dropdownbooking.totalhours = toparse.totalhours;    
                    
        
                });
            },

            dropdownLimit: function(limit){
                var id = this.maidsid;
                 this.$http.post('{{ route('getMaidDetails') }}',{'id':id, 'limit':limit,'action':'table'}).then((res) => {
                    var toparse = JSON.parse(res.data);
                    this.dropdowntable = toparse;
   
                });
            },


            dropdownTable: function(id){
                var limit = this.limit;
                this.maidsid = id;
                this.$http.post('{{ route('getMaidDetails') }}',{'id':id, 'limit':limit,'action':'table'}).then((res) => {
                    var toparse = JSON.parse(res.data);
                    this.dropdowntable = toparse;
   
                });
            },

            createMaids: function(){
                var input = this.maidItem;
                this.$http.post('{{ route('maids') }}',input).then((response) => {
                    window.location.href = "{{url('admin/add-maids')}}"; 
                }, (response) => {
                    this.formErrors = response.data;
                });
            },

            getid: function(id){
                this.$http.post('{{ route('maids') }}',{'id':id, 'action':'getid'}).then((response) => {
                    var toparse = JSON.parse(response.data);
                        this.returned.id = toparse.id;
                        this.returned.fullname = toparse.fullname;
                        this.returned.gender = toparse.gender;
                        this.returned.birthdate = moment(toparse.birthdate).format('YYYY-MM-DD');
                    $("#edit-maids").modal('show');
                });
            },

            updateMaids: function(){
                var input = this.returned;
                this.$http.post('{{ route('maids') }}',input).then((response) => {
                    window.location.href = "{{url('admin/add-maids')}}";
                }, (response) => {
                    this.formErrors = response.data;
                });
            },
            openmodal: function(id){
                $("#del-maids").modal('show');
                this.deleId.id = id;
            },  

            deleteMaids: function(){
            
                this.$http.post('{{ route('maids') }}',{'id':this.deleId, 'action':'delmaids'}).then((response) => { 
                    window.location.href = "{{ url('admin/add-maids') }}"; 
                });

            }

 



          
        }






    });




</script>
@endsection