

@extends('layouts.app')



@section('content-styles')

    <!-- Custom CSS -->



    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />

    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">

    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">



    <link rel="stylesheet" type="text/css" href="{{ asset('dashtemplate/css/lib//daterangepicker/daterangepicker.css') }}" />



    <link rel="stylesheet" href="{{asset('dashtemplate/timepicker/jquery.ui.timepicker.css') }}">

    <link rel="stylesheet" href="{{asset('dashtemplate/timepicker/jquery-ui-1.10.0.custom.min.css') }}">



    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('dashtemplate/js/lib/bootstrap-tagsinput-latest/src/bootstrap-tagsinput.css') }}">



    <style type="text/css">

        td,th{ 

            font-size: 12px; 



        }

        .dataTables_wrapper .dataTables_paginate{

            padding-bottom: 1.25em !important;

        }

        .dataTables_wrapper .dataTables_paginate {

            font-size: 0.75em !important;

        }



        .form-control {

            height: auto !important;

        }



    </style>

@endsection



@section('content')

<body class="fix-header fix-sidebar">



@include('layouts.dashboard.header')

    

    <div class="container-fluid app">

        <div class="row form-body card">

            <div class="col-sm-12 col-md-4">

                <div class="input-group input-group-flat ">

                    <span class="input-group-btn" style="width: 30px; margin-top:9px;">

                        <i class="fa fa-calendar"></i>

                    </span>

                    <input type="text" name="dates" id="dates" class="form-control">  

                        <input type="hidden" name="from" id="from" class="form-control">  

                        <input type="hidden" name="to" id="to" class="form-control">  

   

                </div>

            </div>

          

        </div>



        <div class="card"> 

            <div class="table-responsive">

                <table id="example23" class="display nowrap table table-hover table-striped table-bordered" 

                cellspacing="0" width="100%">

                    <thead>

                        <tr>

                            <th>Service No.</th>

                            <th>Customer</th>

                            <th>Maid</th>

                            <th>Driver</th>

                            <th>Transaction Type</th>

                            <th>Service Status</th>



                            <th>Service Date</th>

                            <th>Service Time</th>

                   

                        </tr>

                    </thead>

     

                    <tbody>



                    </tbody>

                </table>

            </div>

        </div>

    </div>



<input type="hidden" name="idval" id="idval">

<input type="hidden" name="appointment_id" id="appointment_id">

<input type="hidden" name="customer_id" id="customer_id">

<input type="hidden" name="done" id="done">

<input type="hidden" name="amount" id="amount">

<div class="modal" id="myModal">

    <div class="modal-dialog modal-lg">

        <div class="modal-content">

            <div class="modal-header">

                <h3 id="schedule_code" class="text-primary"></h3>

           

            </div>

            <div class="modal-body">          

                              <div class="container-fluid" id="forview">

                    <div class="panel-body">

                        <div class="col-sm-12 col-md-12">

                            <div class="row">

                                <div class="col-md-2">

                                    <div class="clearfix">

                                        <img src="" id="cus_img" class="img img-circle pull-left m-r-15" 

                                        style="width:100px;height:100px">

                                    </div>

                                </div>

                                <div class="col-md-10">

                                    <div class="panel-body">

                                        <div class="row">

                                            <div class="col-md-8 table-responsive">

                                                <table>

                                                    <tbody>

                                                        <tr>

                                                            <th>Customer:</th>

                                                            <td class="p-l-15 text-primary font-weight-bold" id="cus_name"></td>

                                                        </tr>

                                                        <tr>

                                                            <th>Mobile:</th>

                                                            <td class="p-l-15 text-primary font-weight-bold" id="cus_mobile"></td>

                                                        </tr>

                                                        <tr>

                                                            <th>Property Address:</th>

                                                            <td class="p-l-15 text-primary font-weight-bold" id="cus_address"></td>

                                                        </tr>

                                                        <tr>

                                                            <th>Area:</th>

                                                            <td class="p-l-15 text-primary font-weight-bold" id="cus_area"></td>

                                                        </tr>

                                                        <tr>

                                                            <th>Subarea:</th>

                                                            <td class="p-l-15 text-primary font-weight-bold" id="cus_subarea"></td>

                                                        </tr>

                                                  

                                                    </tbody>

                                                </table>

                                            </div>  

                                            <div class="col-md-4 table-responsive">

                                                <table>

                                                    <tbody>

                                                        <tr>

                                                            <th>Maid:</th>

                                                            <td class="p-l-15 text-primary font-weight-bold" id="maid_name"></td>

                                                        </tr>

                                                        <tr>

                                                            <th>Driver:</th>

                                                            <td class="p-l-15 text-primary font-weight-bold" id="driver_name"></td>

                                                        </tr>

                                                    </tbody>

                                                </table>

                                            </div>      

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <div class="row" style="padding-top:20px;">

                                <div class="col-sm-12 col-md-12 table-responsive">

                                    <table class="table table-hover ">

                                        <thead>

                                            <tr>

                                                <th>Service Date</th>

                                                <th>Servie Time</th>

                                                <th>Services</th>

                                            </tr>

                                        </thead>

                                        <tbody>

                                            <tr>

                                                <td class="font-weight-bold" id="service_date"></td>

                                                <td class="font-weight-bold" id="service_time"></td>

                                                <td class="text-primary font-weight-bold" id="service"></td>

                                            </tr>

                                        </tbody>

                                    </table>

                                </div>

                            </div>

                            <div class="row" style="padding-top:20px;">

                                <div class="col-sm-12 col-md-12 table-responsive">

                                    <table class="table table-hover ">

                                        <thead>

                                            <tr>

                                                <th>Transaction Type</th>

                                                <th>Service Status</th>

         

                                                <th>VAT</th>

                                                <th>Service Price</th>

                                            </tr>

                                        </thead>

                                        <tbody>

                                            <tr>

                                                <td class="text-primary font-weight-bold" id="transaction_type" 

                                                style="font-size: 100% !important"></td>

                                                <td class="font-weight-bold" id="service_status" 

                                                style="font-size: 100% !important"></td>

                                               

                                                <td class="text-primary font-weight-bold" id="payment_vat" 

                                                style="font-size: 100% !important"></td>

                                                <td class="text-primary font-weight-bold" id="price" 

                                                style="font-size: 100% !important"></td>

                                            </tr>

                                        </tbody>

                                    </table>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>







            </div>

            <div class="modal-footer">

                <button type="button" class="btn-sm btn-basic" data-dismiss="modal" style="color:black;">Close</button>

                <button type="button" id="OpenReason" class="btn-sm btn-primary">Cancel Appointment</button>

            </div>

        </div>

    </div>

</div>





<div class="modal" id="myModal2">

    <div class="modal-dialog modal-md">

        <div class="modal-content">

            <div class="modal-header">

            </div>

            <div class="modal-body">  



                <div class="container">

                    <div class="form-group">

                        <label for="comment">Reason for cancelling:</label>

                        <textarea class="form-control" rows="5" name="comment" id="comment" autocomplete="off"></textarea>

                    </div>

                </div>    



            </div>

            <div class="modal-footer">

                <button type="button" class="btn-sm btn-basic" data-dismiss="modal" style="color:black;">Close</button>

                <button type="button" id="cancel" class="btn-sm btn-primary">Save</button>

            </div>

        </div>

    </div>

</div>





@include('layouts.dashboard.footer')



                                                                                                                                                                                                                                                                                                                                                                                                   

</body>



@endsection



@section('content-scripts')



<script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>

<!-- Bootstrap tether Core JavaScript -->

<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>

<!-- slimscrollbar scrollbar JavaScript -->

<script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>

<!--Menu sidebar -->

<script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>

<!--stickey kit -->

<script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>

<!--Custom JavaScript -->



<!-- scripit init-->

<script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>



<!-- datatables -->

<script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>



<!-- highcharts -->

<script src="https://code.highcharts.com/highcharts.js"></script>

<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script src="https://code.highcharts.com/modules/export-data.js"></script>



<!-- vue js -->

<script src="{{ asset('vue/dist/vue.js') }}"></script>

<script src="{{ asset('vue/dist/vue.min.js') }}"></script>



<script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>



<!-- moment -->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>



<!-- time picker -->

<script src="{{ asset('dashtemplate/timepicker/migrate.js') }}"></script>

<script src="{{ asset('dashtemplate/timepicker/jquery.ui.timepicker.js') }}"></script>

<script src="{{ asset('dashtemplate/timepicker/jquery.ui.core.min.js') }}"></script>

<script src="{{ asset('dashtemplate/timepicker/jquery.ui.position.min.js') }}"></script>

<script src="{{ asset('dashtemplate/timepicker/jquery.ui.widget.min.js') }}"></script>

<script src="{{ asset('dashtemplate/timepicker/jquery.ui.tabs.min.js') }}"></script>



<!-- others -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script src="{{ asset('dashtemplate/js/lib/bootstrap-tagsinput-latest/src/bootstrap-tagsinput.js') }}"></script>



<script type="text/javascript">

$(document).ready(function(){



    $('#OpenReason').click(function (){



                $.ajax({



                    url: '{{ route('cancelAppointment') }}',

                    type: 'POST',

                    headers: {

                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },

                    data:{ 

                        action: 'check',

                        id: $('#appointment_id').val()

                    },

                    success: function(data){



                        if(data == 'true'){



                            getDetails($('#appointment_id').val());

                            $('#myModal').modal('hide');

                            $('#myModal2').modal('toggle');



                        }else if(data == 'cancelled'){

                            foralert('warning', 'Appointment is already cancelled.');

                        }else{

                            foralert('warning', 'Only unpaid & on queue appointments are eligable for cancellation.');

                        }

                    }

                });





    });



    $('#myModal').on('hidden.bs.modal', function () {

            toastr.clear();

    });



    $('#myModal2').on('hidden.bs.modal', function () {

        salesdatatable.ajax.reload();

        toastr.clear();

    });



    $('input[name="dates"]').daterangepicker({

        "showDropdowns": false,

        ranges: {

            // 'Today':        [moment(), moment()],

            // 'Yesterday':    [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

            'Last 7 Days':  [moment().subtract(6, 'days'), moment()],

            'Last 15 Days': [moment().subtract(14, 'days'), moment()],

            'This Month':   [moment().startOf('month'), moment().endOf('month')],

            'Last Month':   [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

        },



            "startDate": moment().subtract(6, 'days'),

            "endDate":   moment(),



        }, function(start, end, label) {

         

            dropdownlabel = label;

            from = start.format('YYYY-MM-DD');

            to = end.format('YYYY-MM-DD');

            $('[name="from"]').val(from);

            $('[name="to"]').val(to);



            salesdatatable.ajax.reload();



    });





    var salesdatatable =  $('#example23').DataTable({

        processing: true,

        serverSide: true,

        ordering: true,



        oLanguage: {

            sProcessing: "<span id='spinner' class='fa fa-spinner fa-2x fa-spin text-blue'></span>"

        },

        ajax: { 

            url: '{{ route('MaidsScheduleTableCancel') }}',

            type: 'POST',

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            },

            data: function ( d ) {

                d.datefrom = $('[name="from"]').val();

                d.dateto = $('[name="to"]').val();

               

            }

        },

        "createdRow": function( row, data, dataIndex ) {



            $('td',row).hover(function() {

                $(this).addClass('pointer');

            });

            

            $('td',row).on('click',function(){

                $('#myModal').modal('toggle');

                $('#idval').val(data.schedule_code);

      

                getDetails(data.id);

            });

        },



        columns: [

            {data:'schedule_code',name:'schedule_code',class:'nowrap'},

            {data:'get_customer.fullname',name:'getCustomer.fullname'},

            {data:'get_maid.fullname',name:'getMaid.fullname'},

            {data:

                function(data) {

                    if(data.get_driver)

                    {

                        return data.get_driver.fullname;

                    }else{

                        return 'N/A';

                    }

                },name:'getDriver.fullname'

            },

            {data:'transaction',name:'transaction'},

            {data:'completed',name:'completed'},



            {data:'schedule_date',name:'schedule_date'},

            {data:'schedule_start',name:'schedule_start'}



        ]

    });



    

    //save cancel

    $('#cancel').on('click', function(){

        if($('#comment').val() != ''){

         $.ajax({



                url: '{{ route('cancelAppointment') }}',

                type: 'POST',

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },

                data:{ 

                    action: 'cancel',

                    id: $('#appointment_id').val(),

                    comment: $('#comment').val()

                },

                success: function(data){

                    foralert('success', 'appointment is successfully cancelled.')

                }

            });



        }else{

            foralert('error', 'Please enter the reason of cancellation.')

        }



    });





    function getDetails(id){

        $.ajax({

            url: '{{ route('MaidsScheduleTableDetail') }}',

            type: 'POST',

            headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            },

            data:{ maids_schedule_id: id },

                success: function(data){

                    

                    var parsed = JSON.parse(data);

                    var source = "{!! asset('storage/customers') !!}"; 



                    //modal view

                    $('#appointment_id').val(parsed.maids_relation.id);

                    $('#schedule_code').html('Service No: '+parsed.maids_relation.schedule_code);

                    $('#cus_img').attr('src', source+'/'+parsed.maids_relation.get_customer.photo);

                    $('#cus_name').html(parsed.maids_relation.get_customer.fullname);

                    $('#cus_mobile').html(parsed.maids_relation.get_customer.mobile);

                    $('#cus_address').html(parsed.maids_relation.get_customer.address);

                    $('#cus_area').html(parsed.maids_relation.get_area.area);

                    $('#cus_subarea').html(parsed.maids_relation.get_subarea.sub_area);

                    $('#maid_name').html(parsed.maids_relation.get_maid.fullname);

                    var viewDriver = (parsed.maids_relation.driver_id != null) ? 

                        parsed.maids_relation.get_driver.fullname : 'N/A';

                    $('#driver_name').html(viewDriver);

                                

        

                    $('#service_date').html(moment(parsed.maids_relation.schedule_start).format('ll'));

                    $('#service_time').html(

                        moment(parsed.maids_relation.schedule_start).format('LT')

                        + ' - ' +

                        moment(parsed.maids_relation.schedule_end).format('LT')

                    );



                    if(parsed.maids_relation.completed == 1){

                        $('#service_status').html('<span class="label label-success">Done</span>');

                    }else if(parsed.maids_relation.completed == 3){

                        $('#service_status').html('<span class="label label-warning">On&nbspqueue</span>');

                    }else{

                        $('#service_status').html('<span class="label label-danger">Cancelled</span>');

                    }

                    

                    $('#service').html(parsed.maids_relation.notes);

                

                    if( parsed.payment_details.payment_sum == '0.00' 

                        && parsed.payment_details.payment_status == '0' ){

                        $('#payment_status').html('<span class="label label-danger">Unpaid</span>'); 

                    }else if( parsed.payment_details.payment_sum != '0.00' 

                        && parsed.payment_details.payment_status == '3' ){

                        $('#payment_status').html('<span class="label label-warning">Partially Paid</span>'); 

                    }else if( parsed.payment_details.payment_status == '1'){

                        $('#payment_status').html('<span class="label label-success">Paid</span>'); 

                    }



                    if( parsed.payment_details.transaction_type == '0'){

                        $('#transaction_type').html('One Time'); 

                    }else{

                        $('#transaction_type').html('Reccurring'); 

                    }



                    $('#payment_vat').html(parsed.maids_relation.get_sale_summary.payment_vat);

                    $('#price').html(parsed.maids_relation.get_sale_summary.price);

                    //modal view end



                }



        });

    }





    //foralert

    function foralert(action,message){

        if(action=='success'){

            toastr.success(message,'Success!',{

                "positionClass": "toast-top-right",

                timeOut: 5000,

                "closeButton": true,

                "debug": false,

                "newestOnTop": true,

                "progressBar": true,

                "preventDuplicates": true,

                "onclick": null,

                "showDuration": "300",

                "hideDuration": "1000",

                "extendedTimeOut": "1000",

                "showEasing": "swing",

                "hideEasing": "linear",

                "showMethod": "fadeIn",

                "hideMethod": "fadeOut",

                "tapToDismiss": false,

                    onHidden: function () {

                       $('#myModal2').modal('hide');

                    }

            });

        }else if(action=='warning'){

            toastr.warning(message,'Attention!',{

                "positionClass": "toast-top-right",

                timeOut: 5000,

                "closeButton": true,

                "debug": false,

                "newestOnTop": true,

                "progressBar": true,

                "preventDuplicates": true,

                "onclick": null,

                "showDuration": "300",

                "hideDuration": "1000",

                "extendedTimeOut": "1000",

                "showEasing": "swing",

                "hideEasing": "linear",

                "showMethod": "fadeIn",

                "hideMethod": "fadeOut",

                "tapToDismiss": false,





            })

        }else{

            toastr.error(message,'Attention!',{

                "positionClass": "toast-top-right",

                timeOut: 5000,

                "closeButton": true,

                "debug": false,

                "newestOnTop": true,

                "progressBar": true,

                "preventDuplicates": true,

                "onclick": null,

                "showDuration": "300",

                "hideDuration": "1000",

                "extendedTimeOut": "1000",

                "showEasing": "swing",

                "hideEasing": "linear",

                "showMethod": "fadeIn",

                "hideMethod": "fadeOut",

                "tapToDismiss": false



            });

        }



    } 











});





</script>

@endsection