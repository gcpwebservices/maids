
@extends('layouts.app')

@section('content-styles')
    <!-- Custom CSS -->

    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('dashtemplate/css/lib//daterangepicker/daterangepicker.css') }}" />

@endsection

@section('content')
<body class="fix-header fix-sidebar">

@include('layouts.dashboard.header')
    
      <div class="container-fluid app">
                <div class="row form-body card">
                    <div class="col-sm-12 col-md-8">
                        <div class="row">

                            <div class="col-sm-12 col-md-6">
                                <div class="input-group input-group-flat ">
                                    <span class="input-group-btn" style="width: 30px; margin-top:9px;">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" name="dates" id="dates" class="form-control">  
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary btn-group-right" type="button" v-on:click="dateRange()">
                                            <i class="ti-search" ></i>
                                        </button>
                                    </span>        
                                </div>
                            </div>

            <!--                 <div class="col-sm-12 col-md-5">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle btn-block" type="button" data-toggle="dropdown" aria-expanded="false">
                                    Select Branch
                                    <span class="caret"></span></button>
                                    <ul class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -2px, 0px); top: 0px; left: 0px; will-change: transform;">
                                        <li v-on:click.prevent="dateRange()"><a href="">Test</a></li>
                                        <li v-on:click.prevent="fillData('range','week')"><a href="">Week</a></li>
                                        <li v-on:click.prevent="fillData('range','month')"><a href="">Month</a></li>
                                        <li v-on:click.prevent="fillData('range','year')"><a href="">Year</a></li>
                                    </ul>
                                </div>
                            </div> -->
                <!--             <div class="col-sm-12 col-md-3">

                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle btn-block" type="button" data-toggle="dropdown" aria-expanded="false">
                                        @{{ branch }}
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -2px, 0px); top: 0px; left: 0px; will-change: transform;">
                                        <li v-on:click.prevent=""><a href="">Branch 1</a></li>
                                        <li v-on:click.prevent=""><a href="">Branch 2</a></li>
                                        <li v-on:click.prevent=""><a href="">Branch 3</a></li>
                                        <li v-on:click.prevent=""><a href="">Branch 4</a></li>
                                        <li v-on:click.prevent=""><a href="">All</a></li>
                                    </ul>
                                </div>
                            </div> -->
                       
                 

          <!--                   <div class="col-sm-12 col-md-3">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle btn-block" type="button" data-toggle="dropdown" aria-expanded="false">
                                        Customer
                                            <span class="caret"></span></button>
                                    <ul class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -2px, 0px); top: 0px; left: 0px; will-change: transform;">
                                        <li><a href="#">Customer 1</a></li>
                                        <li><a href="#">Customer 2</a></li>
                                        <li><a href="#">Customer 3</a></li>
                                        <li><a href="#">Customer 4</a></li>
                           
                                    </ul>
                                </div>
                            </div> -->

                        </div><!-- row -->                 
                    </div>
                </div>

                <!-- Start Page Content -->
                <div class="row card">   
                    <div class="col-sm-12 col-md-12">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div class="card-title">
                                    <h4>Summary Graph </h4>
                                </div>
                                <div class="chart-responsive" id="container">
                                    
                                </div>
                            </div>
                        </div>
                    </div>   
                </div>  <!-- row card-->

<!--                 <div class="row card">
                    <dir class="col-sm-12 col-md-12">
                        
                            <div class="row">

                                <div class="col-sm-12 col-md-4">
                                    <div class="card-title">
                                        <h4>Summary Table </h4>
                                    </div>
                                      <ul class="list-group mb-3">
                                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                                          <div>
                                            <h6 class="my-0">Total 1</h6>
                                            <small class="text-muted">Brief description</small>
                                          </div>
                                          <span class="text-muted">$12</span>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                                          <div>
                                            <h6 class="my-0">Total 2</h6>
                                            <small class="text-muted">Brief description</small>
                                          </div>
                                          <span class="text-muted">$8</span>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                                          <div>
                                            <h6 class="my-0">Total 3</h6>
                                            <small class="text-muted">Brief description</small>
                                          </div>
                                          <span class="text-muted">$5</span>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between bg-light">
                                          <div class="text-success">
                                            <h6 class="my-0">Total 4</h6>
                                            <small>EXAMPLECODE</small>
                                          </div>
                                          <span class="text-success">-$5</span>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between">
                                          <span>Total (AED)</span>
                                          <strong>$20</strong>
                                        </li>
                                  </ul>
                                </div>

                            </div>
                    </dir>

                </div> -->
                <!-- End PAge Content -->
            </div>

@include('layouts.dashboard.footer')

                                                                                                                                                                                                                                                                                                                                                                                                              
</body>

@endsection

@section('content-scripts')

<script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>
<!--Menu sidebar -->
<script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>
<!--stickey kit -->
<script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
<!--Custom JavaScript -->

<!-- scripit init-->
<script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>

<!-- datatables -->
<script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>

<!-- highcharts -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<!-- vue js -->
<script src="{{ asset('vue/dist/vue.js') }}"></script>
<script src="{{ asset('vue/dist/vue.min.js') }}"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<!-- moment -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>


<script type="text/javascript">

$(document).ready(function(){


    $('input[name="dates"]').daterangepicker({
            "showDropdowns": false,
            ranges: {
                // 'Today':        [moment(), moment()],
                // 'Yesterday':    [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days':  [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month':   [moment().startOf('month'), moment().endOf('month')],
                'Last Month':   [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },

            }, function(start, end, label) {

                dropdownlabel = label;
                datefrom = start.format('YYYY-MM-DD');
                dateto = end.format('YYYY-MM-DD');

     

    });




});


var year = moment().year();
const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

const d = new Date();

var datefrom;
var dateto;
var dropdownlabel;

Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
new Vue({
    el: '.app',

    data () {
        return {
            labels:  null,
            paymentsum: null,
            series: null,
            branch: 'Select Branches',
            range: 'Report Range',

     
        }
    },

    mounted: function(){



    },

    methods: {

        fillData: function(report,type){
        
            if(report == 'range' && type == 'year'){
                this.series = year;
                this.range = 'Year';
                this.$http.post('{{ route('year') }}').then((response) => {
                    let toparse = JSON.parse(response.data);
                    this.paymentsum = toparse;
                    this.labels = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'];
                    this.showChart();
                    
            
                });
            }
            if(report == 'range' && type == 'week'){

                this.range = 'Week';
                this.$http.post('{{ route('week') }}').then((response) => {
                    let toparse = JSON.parse(response.data);

                    this.paymentsum = toparse.values;
                    var pfrom = toparse.dates[0];
                    var pto   = toparse.dates[1];
                    var from  = pfrom['date'];
                    var to    = pto['date'];
                    
                    this.series = moment(from).format('ll')+' - '+moment(to).format('ll');
                    this.labels = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
                    this.showChart();
            
                });
            }
            if(report == 'range' && type == 'month'){
                this.series = monthNames[d.getMonth()]+' '+year;
                this.range = 'Month';
                this.$http.post('{{ route('month') }}').then((response) => {
                    let toparse = JSON.parse(response.data);

                    this.paymentsum = toparse;
                    this.labels = ['week 1','week 2','week 3','week 4','week 5'];
                    this.showChart();
           
                });
            } 
             
        },  

        dateRange: function(){
      
            this.$http.post('{{ route('daterange') }}',{'datefrom':datefrom, 'dateto':dateto}).then((response) => {

                let toparse = JSON.parse(response.data);
                let stringify = JSON.stringify(response.data);
                var dates = [];
                var total = [];

                var i = 0;
                var g = 0;

                $.map( toparse, function( val ) {
                    total[i++] = Math.floor(val.payment);
                });

                $.map( toparse, function( val ) {
                    dates[g++] = moment(val.created_at).format('ll');
                });

                this.paymentsum = total;
                this.labels = dates;
                this.series = dropdownlabel;
                this.showChart();
            
            });

      
        },

        showChart: function(){

        chart = Highcharts.chart('container', {
                loading: {
                    hideDuration: 1000,
                    showDuration: 5000
                },
                chart: {
                    type: 'areaspline'
                },
                title: {
                    text: ''
                },
                // legend: {
                //     layout: 'vertical',
                //     align: 'left',
                //     verticalAlign: 'top',
                //     x: 150,
                //     y: 100,
                //     floating: true,
                //     borderWidth: 1,
                //     backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
                // },
                exporting: {
                    buttons: {
                        contextButton: {
                            text: 'Export',
                            verticalAlign: 'bottom',
                            y: -5
                        }
                    }
                },
                xAxis: {
                    categories: this.labels,
        
                },
                yAxis: {
                    title: {
                        text: ''
                    }
                },
                tooltip: {
                    shared: true,
                    valueSuffix: ' ',
                    pointFormat: "{point.y:.2f} AED"
                },
                credits: {
                    enabled: false
                },
                plotOptions: {
                    areaspline: {
                        fillOpacity: 0.5
                    }
                },
                legend: {
                    enabled: true
                },
                series: [{
                    name: this.series,
                    data: this.paymentsum
                }]
            });
        }

    }

});






</script>
@endsection