

@extends('layouts.app')



@section('content-styles')

    <!-- Custom CSS -->



    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />

    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">

    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('dashtemplate/css/lib//daterangepicker/daterangepicker.css') }}" />



    <link rel="stylesheet" href="{{asset('dashtemplate/timepicker/jquery.ui.timepicker.css') }}">

    <link rel="stylesheet" href="{{asset('dashtemplate/timepicker/jquery-ui-1.10.0.custom.min.css') }}">



    <link rel="stylesheet" href="{{asset('dashtemplate/js/lib/bootstrap-tagsinput-latest/src/bootstrap-tagsinput.css') }}">

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <style type="text/css">
        .preloader {
            background: transparent;
        }
        .circular {
            height: 100px;
            width: 100px;
        }
    </style>

@endsection



@section('content')

<body class="fix-header fix-sidebar">



@include('layouts.dashboard.header')

    

<div class="container-fluid">



        @if(session('status'))

        <div id="successMessage" class="alert alert-{{session('status')[0]}}" 

        style="background-color: #dff0d8 !important;color: #3c763d!important; border-color: #d6e9c6 !important; 

        margin-top: 19px !important ">

        {{session('status')[1]}}

        </div>

        @endif



        <div class="row" style="padding-top: 20px;">



            <div class="col-sm-12 col-md-4 form-group">

               <select class="form-control" name="customer" id="customer" required="required">

                    <option value="">Select Customer</option>

                    @foreach($clist as $customer)

                    <option value="{{ $customer->id }}">{{ $customer->fullname }}</option>

                    @endforeach

                </select>

            </div>

     

      

            <div class="col-sm-12 col-md-4 form-group">

                <select class="form-control" name="type" id="type"  required="required">

                    <option value="1">One Time</option>

                    <option value="2">Recurrence</option>

                </select>

            </div>



             <div class="col-sm-12 col-md-2 form-group">

                <button  class="btn btn-primary" id="addCust" style="border-radius: 3px !important;">

                    <i class="fa fa-user-plus"></i>

                </button>

            </div>



        </div>





    <div class="row card" id="customer_info">

        <input type="hidden" name="customer_id" id="customer_id">

        <div class="col-sm-12 col-md-12">

            <div class="row">

                <div class="col-md-4">

                    <div class="clearfix">

                        <img src="" id="cus_img" class="img img-circle pull-left m-r-15" 

                        style="width:100px;height:100px">

                        <div class="pull-left">

                            <h3 class="text-upper font-bold" id="cus_name"></h3>

                            <p></p>

                            <div class="form-group">

                                <input type="hidden" name="dropdown_cus" id="dropdown_cus">

                                <button type="button" id="change_add" class="btn btn-primary btn-xs">

                                    <i class="fa fa-pencil"></i>

                                    Change Propery address

                                </button>

                        

                            </div>

                        </div>

                    </div>

                </div>

                <div class="col-md-4 ">

                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-12">

                                <table>

                                    <tbody>

                                        <tr>

                                            <th>Mobile No:</th>

                                            <td class="p-l-15" id="cus_mobile"></td>

                                        </tr>

                                        <tr>

                                            <th>Email Address:</th>

                                            <td class="p-l-15" id="cus_email"></td>

                                        </tr>

                                        <tr>

                                            <th>Property Address:</th>

                                            <td class="p-l-15" id="cus_address"></td>

                                        </tr>

                                        <tr>

                                            <th>Area:</th>

                                            <td class="p-l-15" id="cus_area"></td>

                                        </tr>

                                        <tr>

                                            <th>Subarea:</th>

                                            <td class="p-l-15" id="cus_subarea"></td>

                                        </tr>

                                  

                                    </tbody>

                                </table>

                            </div>

            

                        </div>

                    </div>

                </div>



                <div class="col-md-3">

                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-12">

                                <table>

                                    <tbody>

                                        <tr>

                                            <th>Property Type:</th>

                                            <td class="p-l-15" id="cus_p_type"></td>

                                        </tr>

                                        <tr>

                                            <th>Room:</th>

                                            <td class="p-l-15" id="cus_room"></td>

                                        </tr>

                                        <tr>

                                            <th>Toilet:</th>

                                            <td class="p-l-15" id="cus_toilet"></td>

                                        </tr>

                                        <tr>

                                            <th>Garden:</th>

                                            <td class="p-l-15" id="cus_garden"></td>

                                        </tr>

                                        <tr>

                                            <th>Parking:</th>

                                            <td class="p-l-15" id="cus_parking"></td>

                                        </tr>

                                  

                                    </tbody>

                                </table>

                            </div>

            

                        </div>

                    </div>

                </div>

                <div class="col-md-1" >

                    <button type="button" class="close" aria-label="Close" id="remove"  data-placement="left"

                    data-toggle="tooltip" title="Remove customer">

                        <span aria-hidden="true">&times;</span>

                    </button>



                </div>

            </div>



        </div>

    </div>

   



    <div class="row card" id="appoint">

        <div class="col-md-12">

            <div class="hpanel">

                <div class="panel-body">

                    

                    <div class="row ">

                        <div class="col-md-8">

                            <label class="text-center m-t-15">Appointment Information:</label>

                            <div class="row">

                                <div class="col-md-12" id="schedtimedate">

                                    <input type="hidden" name="area_id" id="area_id">

                                    <input type="hidden" name="subarea_id" id="subarea_id">

                                    <input type="hidden" name="save_type" id="save_type">



                  

                                        <div class="col-md-12">



                                            <div class="row">

                                                <div class="col-md-4">

                                                    <div class="form-group">

                                                        <label for="datetimes">Date:</label>

                                                        <input type="text" class="form-control" name="datetimes" size="16" id="datetimes" 

                                                        autocomplete="off" style="height:42px; border-radius: 3px; border-color: #aaa;" 

                                                        required="required"/>

                                                        <input type="hidden" class="form-control" name="schedule_start" id="schedule_start"/>

                                                        <input type="hidden" class="form-control" name="schedule_end" id="schedule_end"/>

                                                    </div>



                                                    <div class="form-group">

                                                        <label for="timefrom">Time:</label>

                                                        <div class="input-group bootstrap-timepicker timepicker">

                                                            <input id="timefrom" type="text" name="timefrom" class="form-control input-small" 

                                                            autocomplete="off" placeholder="Start" style="border-radius: 3px; border-color: #aaa;" required="required">

                                                            &ensp;

                                                            <input id="timeto" type="text" name="timeto" class="form-control input-small" 

                                                            autocomplete="off" placeholder="End" style="border-radius: 3px; border-color: #aaa;">

                                                        </div>

                                                    </div>



                                                  

                                                </div>



                                                <div class="col-md-8">

                                                    <input type="hidden" name="maids_id" id="maids_id">

                                                    <div class="form-group">
                                                        <label for="num_employee">Number of Maid: <span  class="text-primary" id=""></span></label>
                                                        <select class="form-control" name="num_employee" id="num_employee" style="width: 100%;" 
                                                        required="required">
                                                            <option value="select">Select</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                        </select>

                                                    </div>


                                                    <div class="form-group">

                                                        <label for="employee">Maid: <span  class="text-primary" id="availmaids"></span></label>

                                                        <select class="form-control" name="employee[]" id="employee" style="width: 100%;" 

                                                        required="required" multiple="multiple">

                                                            <option value="select">Select Maid</option>

                                                        </select>

                                                    </div>



                                                    <div class="form-group">

                                                        <label for="service">Services:</label>

                                                        <input type="text" data-role="tagsinput" name="service" id="service">

                                                    </div> 

                                                

                                                </div>                             

                                            </div>

                                        </div>

                                    

                                </div>





                    

                                <div class="col-md-12" id="scheddrivers">

                                    <div class="row">

                                        <div class="col-md-6">



                                            <div class="form-group">

                                                <label for="employee">Driver: </label>

                                                <select class="form-control" name="driver_id" id="driver_id" style="width: 100%;" 

                                                required="required">

                                                    <option value="">Select Driver</option>

                                                    @foreach($dlist as $list)

                                                        <option value="{{ $list->id }}">{{ $list->fullname }}</option>

                                                    @endforeach

                                                </select>

                                            </div>

                   

                                        </div>



                                        <div class="col-md-6">

                                             <div class="form-group">

                                                <label for="employee">Arival Window: </label>

                                                <select class="form-control" name="interval" id="interval" style="width: 100%;" 

                                                required="required">

                                                    <option value="0">Select Interval</option>

                                                    <option value="10">10 mins</option>

                                                    <option value="20">20 mins</option>

                                                    <option value="30">30 mins</option>

                                                    <option value="40">40 mins</option>

                                                    <option value="50">50 mins</option>

                                                    <option value="60">60 mins</option>

                                                    <option value="70">1 hour and 10 mins</option>

                                                    <option value="80">1 hour and 20 mins</option>

                                                    <option value="90">1 hour and 30 mins</option>

                                                    <option value="100">1 hour and 40 mins</option>

                                                    <option value="110">1 hour and 50 mins</option>

                                                    <option value="120">2 hours</option>

                                        

                                                </select>

                                            </div>

                                           

                                        </div>





                                    </div>

                                 

                                </div>

                  





                            </div>





                        </div>

                        <div class="col-md-4">

                            <div class="row ">

                                <div class="col-md-12">

                     

                                    <div class="form-group">

                                        <input type="hidden" name="to_pay" id="to_pay">

                                        <label class="text-center m-t-15">Payment Information</label>

                                        <input id="to_pay_view" readonly="" style="font-size:36px;height:60px" value="0.00" type="text" class="form-control input-lg text-center autonumeric font-bold">

                                    </div>

                       

                                </div>

                            </div>

                            <div class="row">

                   

                                <div class="col-md-12">

                                    <div class="form-group">

                                        <button type="button" id="" class="save-unpaid btn btn-primary font-bold" style="width:100%">Save Unpaid</button>

                                    </div>

                                </div>



                            </div>

                        </div>

                        

                    </div>

                    

                </div>

            </div>

            <small class="text-danger">Note: Changing the Customer, Date, Time and Arrival will update the Maids list.</small>

        </div>       

    </div>



     <div class="row card" id="appoint2">

        <div class="col-md-12">

            <div class="hpanel">

                <div class="panel-body">

                    

                    <div class="row ">

                        <div class="col-md-8">

                            <label class="text-center m-t-15">Appointment Information: </label>

                            <div class="row">

                                <div class="col-md-12" id="schedtimedate">



             

                                        <div class="col-md-12">



                                            <div class="row">

                                                <div class="col-md-4">





                                                    <div class="form-group">

                                                        <label for="datetimes">Date Range:</label>

                                                            <input type="text" name="recur_date" id="recur_date" class="form-control" 

                                                            style="border-radius: 3px; border-color: #aaa;">  

                                                            <input type="hidden" name="recur_date_from" id="recur_date_from" class="form-control">  

                                                            <input type="hidden" name="recur_date_to" id="recur_date_to" class="form-control">  

                                                    </div>



                                                    <div class="form-group">

                                                        <label for="timefrom">Time:</label>

                                                        <div class="input-group bootstrap-timepicker timepicker">

                                                            <input type="text" name="timefrom_2" id="timefrom_2" class="form-control input-small" 

                                                            autocomplete="off" placeholder="Start" style="border-radius: 3px; border-color: #aaa;" required="required">

                                                            &ensp;

                                                            <input type="text" name="timeto_2" id="timeto_2" class="form-control input-small" 

                                                            autocomplete="off" placeholder="End" style="border-radius: 3px; border-color: #aaa;">

                                                        </div>

                                                    </div>



                                                    <input type="hidden" name="weekdays" id="weekdays">

                                                    <div class="form-group" id="recur_days_hide">

                                                        <label for="service">Day:</label>

                                                        <select class="form-control" name="recur_days" id="recur_days" style="width: 100%;" multiple>

                                                                <option value="7">Sunday</option>

                                                                <option value="1">Monday</option>

                                                                <option value="2">Tuesday</option>

                                                                <option value="3">Wednesday</option>

                                                                <option value="4">Thursday</option>

                                                                <option value="5">Friday</option>

                                                                <option value="6">Saturday</option>                                

                                                        </select>

                                                    </div>



                                                

                                                </div>



                                                <div class="col-md-8">

                                                    <input type="hidden" name="maids_id_2" id="maids_id_2">

                                                    <div class="form-group">

                                                        <label for="num_employee_2">Number of Maid: <span  class="text-primary" id=""></span></label>

                                                        <select class="form-control" name="num_employee_2" id="num_employee_2" style="width: 100%;" 

                                                        required="required">

                                                            <option value="select">Select</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                        </select>

                                                    </div>


                                                    <div class="form-group">

                                                        <label for="employee">Maid: <span  class="text-primary" id="availmaids_2"></span></label>

                                                        <select class="form-control" name="employee_2[]" id="employee_2" 

                                                        style="width: 100%;" required="required" multiple="multiple">

                                                        </select>

                                                    </div>


                                  


                                                    <div class="form-group">

                                                        <label for="service">Services:</label>

                                                        <input type="text" data-role="tagsinput" name="service_2" id="service_2">

                                                    </div> 



                                                </div>    

                                            </div>



                                        </div>



                                </div>



                                <div class="col-md-12" id="scheddrivers">

                                    <div class="row">



                                        <div class="col-md-6">



                                            <div class="form-group">

                                                <label for="employee">Driver: </label>

                                                <select class="form-control" name="driver_id_2" id="driver_id_2" style="width: 100%;" 

                                                required="required">

                                                    <option value="">Select Driver</option>

                                                    @foreach($dlist as $list)

                                                        <option value="{{ $list->id }}">{{ $list->fullname }}</option>

                                                    @endforeach

                                                </select>

                                            </div>

                                        </div>

                                        <div class="col-md-6">

                                            <div class="form-group">

                                                <label for="employee">Arival Window: </label>

                                                <select class="form-control" name="interval_2" id="interval_2" style="width: 100%;" 

                                                required="required">

                                                    <option value="0">Select Interval</option>

                                                    <option value="10">10 mins</option>

                                                    <option value="20">20 mins</option>

                                                    <option value="30">30 mins</option>

                                                    <option value="40">40 mins</option>

                                                    <option value="50">50 mins</option>

                                                    <option value="60">60 mins</option>

                                                    <option value="70">1 hour and 10 mins</option>

                                                    <option value="80">1 hour and 20 mins</option>

                                                    <option value="90">1 hour and 30 mins</option>

                                                    <option value="100">1 hour and 40 mins</option>

                                                    <option value="110">1 hour and 50 mins</option>

                                                    <option value="120">2 hours</option>

                                        

                                                </select>

                                               

                                            </div>

                                        </div>

                                    </div>

                                </div>



                            </div>





                        </div>



                        <div class="col-md-4">

                            <div class="row ">

                                <div class="col-md-12">

                     

                                    <div class="form-group">

                                        <input type="hidden" name="to_pay_2" id="to_pay_2">

                                        <label class="text-center m-t-15">Payment Information</label>

                                        <input id="to_pay_view_2" readonly="" style="font-size:36px;height:60px" value="0.00" type="text" class="form-control input-lg text-center autonumeric font-bold">

                                    </div>

                                </div>

                            </div>

                            <div class="row">



                                <div class="col-md-12">

                                    <div class="form-group">

                                        <button type="button" class="save-unpaid btn btn-primary font-bold" style="width:100%">Save Unpaid</button>

                                    </div>

                                </div>

              

                            </div>

                        </div>

                        

                    </div>

                    

                </div>

            </div>

              <small class="text-danger">Note: Changing the Customer, Date, Time and Arrival will update the Maids list.</small>

        </div>       

    </div>



</div>







<div class="modal" id="myModal">

    <div class="modal-dialog modal-lg">

        <div class="modal-content">

            <div class="modal-body">          

                <div class="container-fluid" style="padding-top:30px;">

                    <div class="panel-body">

                        <div class="basic-form">

                            <form method="POST" enctype="multipart/form-data" action="{{route('quickAdd')}}">

                            @csrf

                            <div class="row">

                                <div class="col-md-12">

                                    <div class="row">

                                        <div class="col-md-3">

                               

                                            <img style="cursor: pointer;width:100%"  onclick="$('#photo').trigger('click')"  id="cur_img" 

                                            class="img img-thumbnail img-responsive" src="{{ asset('img/default_big.png') }}">

                                            <p class="text-center">( Click to browse image )</p>

                                            <input class="hide" type="file" name="photo" id="photo">



                                        </div>

                                        <div class="col-md-9">

                                            <div class="row">

                                                <div class="col-md-12">

                                                    <div class="hpanel">

                                                        <div class="panel-heading" style="padding-bottom: 20px;">

                                                            <span><i class="fa fa-user"></i> Personal Information</span></div>

                                                        <div class="panel-body">

                                                            <div class="row">

                                                                <div class="col-md-12">

                                                                    <div class="form-group">

                                                                        <label>Full Name <span class="required">*</span></label>

                                                                        <input required="" name="fullname" type="text" 

                                                                        class="form-control input-default" autocomplete="off" >

                                                                    </div>

                                                                </div>

                                              

                                                            </div>

                                    

                                                            <div class="row">

                                                                <div class="col-md-6">

                                                                    <div class="form-group">

                                                                        <label>Email Address</label>

                                                                        <input name="email" type="email" id="email" autocomplete="off" 

                                                                        class="form-control input-default">

                                                                    </div>

                                                                </div>

                                                                <div class="col-md-6">

                                                                    <div class="form-group"  id="forcustomers">

                                                                        <label>Mobile Number <span class="required">*</span></label>

                                                                        <input required="" name="mobile" 

                                                                        type="text" class="form-control input-default" 

                                                                        value="@{{mobile}}" required="required" 

                                                                        placeholder="+971" maxlength="13" autocomplete="off">

                                                                    </div>

                                                                </div>

                                                            </div>

                                                            <div class="row">

                                                                <div class="col-md-12">

                                                                    <div class="form-group">

                                                                        <label>Address</label>

                                                                        <textarea name="address" class="form-control input-default" ></textarea>

                                                                    </div>

                                                                </div>

                                                            </div>

                                                            <div class="row">

                                                                <div class="col-md-6">

                                                                    <div class="form-group">

                                                                        <label>Area</label>

                                                                        <select name="addArea" id="addArea" class="form-control input-default">

                                                                        <option value="">-Select-</option>

                                                                            @foreach ($area as $alist)

                                                                            <option value="{{ $alist->id }}">{{ $alist->area }}</option>

                                                                            @endforeach

                                                                        </select>

                                                                    </div>

                                                                </div>

                                                                <div class="col-md-6">

                                                                    <div class="form-group">

                                                                        <label>Subarea</label>

                                                                        <select name="addSubarea" id="addSubarea" class="form-control input-default">

                                                                            

                                                                        </select>

                                                                        <input type="hidden" id="add_subarea_val" 

                                                                        name="add_subarea_val">

                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                        

                                </div>      

                            </div>

              

                        </div>

                    </div>

                </div>

            </div>

            <div class="modal-footer">

                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Save</button>

                </form>  

                <button type="button" class="btn btn-inverse" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>

            </div>

        </div>

    </div>

</div>





<div class="modal" id="myModalEdit">

    <div class="modal-dialog modal-lg">

        <div class="modal-content">

            <div class="modal-body">          

                <div class="container-fluid" style="padding-top:30px;">

                    <div class="panel-body">

                        <div class="basic-form">



                            <div class="col-md-12">

                                <div class="row">

                                    <div class="col-md-12">

                                        <div class="hpanel">

                                            <div class="panel-heading" style="padding-bottom: 20px;">

                                                <span><i class="fa fa-home"></i> Propery Address</span></div>

                                            <div class="panel-body">

                                                <div class="row">

                                                    <div class="col-md-6">

                                                        <div class="form-group">

                                                            <label>Area</label>

                                                            <select name="area_edit" id="area_edit" class="form-control input-default">

                                                            <option value="">-Select-</option>

                                                                @foreach ($area as $alist)

                                                                <option value="{{ $alist->id }}">{{ $alist->area }}</option>

                                                                @endforeach

                                                            </select>

                                                            <input type="hidden" id="area_edit_val" name="area_edit_val">

                                                        </div>

                                                    </div>

                                                    <div class="col-md-6">

                                                        <div class="form-group">

                                                            <label>Subarea</label>

                                                            <select name="subarea_edit" id="subarea_edit" class="form-control input-default">

                                                                

                                                            </select>

                                                            <input type="hidden" id="subarea_edit_val" name="subarea_edit_val">

                                                        </div>

                                                    </div>

                                  

                                                </div>

                        

                            

                                                <div class="row">

                                                    <div class="col-md-12">

                                                        <div class="form-group">

                                                            <label>Address</label>

                                                            <textarea name="address" id="address" class="form-control input-default" ></textarea>

                                                        </div>

                                                    </div>

                                                </div>

                                    

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                  

              

                        </div>

                    </div>

                </div>

            </div>



            <div class="modal-footer">

                <button type="button" id="save_edit" class="btn btn-success"><i class="fa fa-check"></i> Save</button>

                <button type="button" class="btn btn-inverse" id="save_edit_close" data-dismiss="modal">

                    <i class="fa fa-close"></i> Close</button>

            </div>

        </div>

    </div>

</div>





    



@include('layouts.dashboard.footer')



                                                                                                                                                                                                                                                                                                                                                                                                              

</body>



@endsection



@section('content-scripts')



<script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>

<script src="{{ asset('dashtemplate/timepicker/migrate.js') }}"></script>



<!-- Bootstrap tether Core JavaScript -->

<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>

<!-- slimscrollbar scrollbar JavaScript -->

<script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>

<!--Menu sidebar -->

<script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>

<!--stickey kit -->

<script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>

<!--Custom JavaScript -->



<!-- scripit init-->

<script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>



<!-- datatables -->

<script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>



<!-- highcharts -->

<script src="https://code.highcharts.com/highcharts.js"></script>

<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script src="https://code.highcharts.com/modules/export-data.js"></script>



<!-- vue js -->

<script src="{{ asset('vue/dist/vue.js') }}"></script>

<script src="{{ asset('vue/dist/vue.min.js') }}"></script>



<script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>



<!-- moment -->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    



<script src="{{ asset('dashtemplate/timepicker/jquery.ui.timepicker.js') }}"></script>

<script src="{{ asset('dashtemplate/timepicker/jquery.ui.core.min.js') }}"></script>

<script src="{{ asset('dashtemplate/timepicker/jquery.ui.position.min.js') }}"></script>

<script src="{{ asset('dashtemplate/timepicker/jquery.ui.widget.min.js') }}"></script>

<script src="{{ asset('dashtemplate/timepicker/jquery.ui.tabs.min.js') }}"></script>



<script src="{{ asset('dashtemplate/js/lib/bootstrap-tagsinput-latest/src/bootstrap-tagsinput.js') }}"></script>





<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>



<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>





<script type="text/javascript">



Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

new Vue({



el: '#forcustomers',



data: {



    mobile : '+971'





},



methods : {



}







});



$(document).ready(function(){



    $('#addCust').click(function(){

        $('#myModal').modal('toggle');

    });



    $('#appoint').show();

    $('#appoint2').hide();



    $('#type').on('change',function(){

        if($(this).val()=='2'){

            $("#employee_2").empty().trigger('change');

            $('#appoint2').show();

            $('#appoint').hide();

            $('#save_type').val(2);



            $('#timefrom_2').timepicker({

                minTime: { hour: 08, minute: 00 },

                maxTime: { hour: 20, minute: 00 },

                showLeadingZero: true

            }); 



            $('#recur_days').val('');

            $('#recur_date').prop('disabled', false);

    

        }else{

            $('#save_type').val(1);

            $('#appoint').show();

            $('#appoint2').hide();



        }

    });







    setTimeout(function() {

        $('#successMessage').fadeOut('fast');

    }, 10000);



    $('[data-toggle="tooltip"]').tooltip();

    //initialize dropdown area 

    area();



    $('#remove').on('click',function(){  



        var modal = $('#myModal');

        modal.load();

        $("#customer").val('');

        $("#customer").select2({

            placeholder: "Select Customer"

        });

        $('#customer_info').hide();

      



    });





    $("#employee,#employee_2").select2({

        placeholder: "Select Maid"

    });


    $("#num_employee,#num_employee_2").select2({

        placeholder: "Select"

    });


    $("#driver_id,#driver_id_2").select2({

        placeholder: "Select Driver"

    });



    $("#interval,#interval_2").select2({

        placeholder: "Select Interval"

    });



    $('#customer_info').hide();



    $('#customer,#area,#subarea,#type,#recur').select2();



    // $('input[name="datetimes_2"]').prop('disabled', true);



 

    $("#recur_date").prop('disabled', true);



    $("#timefrom_2").prop('disabled', true);

    $("#timeto_2").prop('disabled', true);

    $("#employee_2").prop('disabled', true);
    $("#num_employee_2").prop('disabled', true);

    $('#recur_days').prop('disabled', true);

    $("#driver_id_2,#interval_2").prop('disabled', true);



    // $('#recur').on('change',function(){

    



    // });



    var curDate = '<?php echo $currentD;?>';

    var curTime = '<?php echo $currentT;?>';







    var d = new Date();

    var h = d.getHours();

    var m = d.getMinutes();





//one time



    $("#timefrom").prop('disabled', true);

    $("#timeto").prop('disabled', true);

    $("#employee").prop('disabled', true);

    $("#num_employee").prop('disabled', true);

    $("#driver_id,#interval").prop('disabled', true);





    $('#timeto').timepicker({

        maxTime: { hour: 20, minute: 00 },

        showLeadingZero: true

    });



 



    $('#timefrom').focus(function(){



        if($('#datetimes').val() > curDate){

            $('#timefrom').timepicker({

                minTime: { hour: 08, minute: 00 },

                maxTime: { hour: 20, minute: 00 },

                showLeadingZero: true

            }); 

      

        }else{

            $('#timefrom').timepicker({

                minTime: { hour: h, minute: m },

                maxTime: { hour: 20, minute: 00 },

                showLeadingZero: true

            }); 

        }



    });





    $('#timefrom').on('change',function(){



        $("#timeto").prop('disabled', false);



        var startDate = $('#datetimes').val();

        var startTime = $('#timefrom').val();



        var date = new Date(startDate + ' ' + startTime);



        var newtimeto = moment(date).add(1, 'hours').format('HH:mm');

        var newdate = moment(date).add(1, 'hours');



        $('#timeto').val(newtimeto);

        var hour = newdate.hour();

        var minutes = newdate.minutes();



        var datetimefrom = moment(date).format('YYYY-MM-DD HH:mm:ss');

        $('#schedule_start').val(datetimefrom);



        tpMinMaxSetMinTime(hour,minutes);



        $("#employee").prop("disabled", false);
        $("#num_employee").prop("disabled", false);






    });









    $('#timeto').on('change',function(){  

 
        var calendarDate   = $('#datetimes').val();

        var endDateandTime = new Date(calendarDate + ' ' + $('#timeto').val());

        var datetimeto = moment(endDateandTime).format('YYYY-MM-DD HH:mm:ss');

        $('#schedule_end').val(datetimeto);

        get_available_maids($('#schedule_start').val(),datetimeto,$('#interval').val());


        getAmount($('#timefrom').val(),$('#timeto').val(),$('#num_employee').val());

    });



    function tpMinMaxSetMinTime( hours, minutes ) {

        $('#timeto').timepicker('option', { minTime: { hour: hours, minute: minutes} });

        var startDate = $('#datetimes').val();

        var date = new Date(startDate + ' ' +  $('#timeto').val());

        var datetimeto = moment(date).format('YYYY-MM-DD HH:mm:ss');

        $('#schedule_end').val(datetimeto);

        get_available_maids($('#schedule_start').val(),datetimeto,$('#interval').val());

        getAmount($('#timefrom').val(),$('#timeto').val(),$('#num_employee').val());


    }





    $('input[name="datetimes"]').daterangepicker(

        { 

        autoUpdateInput: false,

        timePicker: false,

        singleDatePicker: true, 

        minDate: new Date(),

        startDate: moment().startOf('hour'),

        endDate: moment().startOf('hour').add(36, 'hour'),

            locale: {



                format: 'YYYY-MM-DD'

            }

        },

        function(start, end, label) {

            $('#schedule_start').val(start.format('YYYY-MM-DD'));

            $('#schedule_end').val(end.format('YYYY-MM-DD'));

            $('#datetimes').val(start.format('YYYY-MM-DD'));



            get_available_maids($('#schedule_start').val(),$('#schedule_end').val(),$('#interval').val());



            $("#timefrom").prop('disabled', false);

            $("#timeto").prop('disabled', false);



            if($('#datetimes').val() > curDate){

                $('#timefrom').timepicker({

                    minTime: { hour: 08, minute: 00 },

                    maxTime: { hour: 20, minute: 00 },

                    showLeadingZero: true

                }); 

          

            }else{

                $('#timefrom').timepicker({

                    minTime: { hour: h, minute: m },

                    maxTime: { hour: 20, minute: 00 },

                    showLeadingZero: true

                }); 

            }



        }

    );





    $('#customer').on('change', function() {



        $("#type").prop('disabled', false);

        $('#customer_info').show();

        var cus_id = $(this).val();

        $('#dropdown_cus').val(cus_id);



        $('#area,#subarea').html('');

        $('#to_pay_view').val('0.00');

        //$('#datetimes').val('');

        $('#timefrom').val('');

        $('#timeto').val('');

        $('#comment').html('');



        $("#employee").empty().trigger('change');

        $('#driver_id').val('').change();

        $('#availmaids').html('');

        $('comment').html('');





        $("#area,#subarea,#employee,#num_employee,#driver_id").prop('disabled', true);



        viewCustomer(cus_id);



    

    });



    function viewCustomer(cus_id) {



        $.ajax({

            url: '{{ route('getCUstomerDetails') }}',

                type: 'POST',

                data: {

                _token  : $('meta[name="csrf-token"]').attr('content'),

                customer : cus_id,

            },  

            success:function(data){

                var parsed = JSON.parse(data);

                var source = "{!! asset('storage/customers') !!}"; 



                $('#cus_img').attr('src', source+'/'+parsed.photo);

                $('#cus_name').html(parsed.fullname);

                $('#cus_mobile').html(parsed.mobile);

                $('#cus_email').html(parsed.email);

                $('#cus_address').html(parsed.address);

                $('#cus_area').html(parsed.get_cus_area.area);

                $('#cus_subarea').html(parsed.get_cus_sub_area.sub_area);

                var property = parsed.property;

                var propType;

                if( property == '1'){

                    propType = 'Flat';

                }else if(property == '2'){

                    propType = 'Villa';

                }else if(property == '3'){

                    propType = 'Office';

                }else{

                    propType = 'Industrial';

                }

                $('#cus_p_type').html(propType);

                $('#cus_room').html(parsed.room);

                $('#cus_toilet').html(parsed.toilet);

                $('#cus_garden').html(parsed.garden);

                $('#cus_parking').html(parsed.parking);



                $('#area').append('<option>'+parsed.get_cus_area.area+'</option>');

                $('#subarea').append('<option>'+parsed.get_cus_sub_area.sub_area+'</option>');





                $('#customer_id').val(parsed.id);

                $('#area_id').val(parsed.get_cus_area.area_id);

                $('#subarea_id').val(parsed.get_cus_sub_area.id);



 

            }

        });

    }

    //num
    $('#num_employee').on('change',function(){
        if($(this).val() == 1){
            $("#employee").select2({
                maximumSelectionLength: 1
              });

            getAmount($('#timefrom').val(),$('#timeto').val(),1);

        }
        if($(this).val() == 2){
            $("#employee").select2({
                maximumSelectionLength: 2
            });
            getAmount($('#timefrom').val(),$('#timeto').val(),2);
        }

        if($(this).val() == 3){
            $("#employee").select2({
                maximumSelectionLength: 3
            });
            getAmount($('#timefrom').val(),$('#timeto').val(),3);
        }
        if($(this).val() == 4){
            $("#employee").select2({
                maximumSelectionLength: 4
            });
            getAmount($('#timefrom').val(),$('#timeto').val(),4);
        }
   
    });


    $('#num_employee_2').on('change',function(){
        if($(this).val() == 1){
            $("#employee_2").select2({
                maximumSelectionLength: 1
              });

            getAmountRecur($('#timefrom_2').val(),$('#timeto_2').val(),1);

        }
        if($(this).val() == 2){
            $("#employee_2").select2({
                maximumSelectionLength: 2
            });
            getAmountRecur($('#timefrom_2').val(),$('#timeto_2').val(),2);
        }

        if($(this).val() == 3){
            $("#employee_2").select2({
                maximumSelectionLength: 3
            });
            getAmountRecur($('#timefrom_2').val(),$('#timeto_2').val(),3);
        }
        if($(this).val() == 4){
            $("#employee_2").select2({
                maximumSelectionLength: 4
            });
            getAmountRecur($('#timefrom_2').val(),$('#timeto_2').val(),4);
        }
   
    });



    $('#employee').on('change', function(){
        $('#maids_id').val($(this).val());
    });



    $('#change_add').on('click',function(){

        $('#myModalEdit').modal('toggle');

        var id = $('#dropdown_cus').val();



        $.ajax({

                url: '{{ route('getCUstomerDetails') }}',

                    type: 'POST',

                    data: {

                    _token  : $('meta[name="csrf-token"]').attr('content'),

                    customer : id,

                },  

                success:function(data){

                    var parsed = JSON.parse(data);

                    $("#area_edit").select2({

                        placeholder: parsed.get_cus_area.area

                    });

                    $("#area_edit").val(parsed.get_cus_area.area_id);



                    $("#subarea_edit").select2({

                        placeholder: parsed.get_cus_sub_area.sub_area

                    });

                    $("#subarea_edit_val").val(parsed.get_cus_sub_area.id);



                    $('#address').val(parsed.address);



                }

            });





    });





    $('#save_edit').on('click', function(){



        $('#myModalEdit').modal('hide');

        var customer_id = $('#dropdown_cus').val();



        $.ajax({

            url: '{{ route('quickEdit') }}',

                type: 'POST',

                data: {

                _token  : $('meta[name="csrf-token"]').attr('content'),

                customer : customer_id,

                area: $('#area_edit').val(),

                subarea: $('#subarea_edit_val').val(),

                address: $('#address').val()

            },  

            success:function(data){

                    viewCustomer($('#dropdown_cus').val());



 

            }

        });

    });





    $(document).on('hide.bs.modal','#myModalEdit', function () {

          viewCustomer($('#dropdown_cus').val());

    });







    $('#area_edit').on('change',function(){

        $('#subarea_edit').html('');

        subarea($(this).val(),'change');



        $('#address_hide').show();

        $('#subarea_edit').prop('disabled', false);

    });



    $('#subarea_edit').on('change',function(){

        $('#subarea_edit_val').val($(this).val());



    });







    //dropdown area

    function area(){

        $.ajax({

            url: '{{ route('getAreaOnly') }}',

                type: 'POST',

                data: {

                _token  : $('meta[name="csrf-token"]').attr('content')



            },  

            success:function(data){

                // $.each(JSON.parse(data), function( idx, val ) {     

                //     $("#area").append('<option value='+val.id+'>'+val.area+' </option>');

                // });

            }

        });  

    }

    //dropdown subarea

    function subarea(area_id,action){

        if(action == 'change'){
            $.ajax({
                url: '{{ route('getareas') }}',
                    type: 'POST',
                    data: {
                    _token  : $('meta[name="csrf-token"]').attr('content'),
                    area : area_id,
                    actionfor: action
                },  

                success:function(data){
                    // $.each(JSON.parse(data), function( idx, val ) {     
                    //     $("#subarea").append('<option value='+val.id+'>'+val.sub_area+' </option>');
                    //     $("#subarea_edit").append('<option value='+val.id+'>'+val.sub_area+' </option>');
                    // });
                }
            }); 
        }

    }


    $('#interval').on('change', function() {
        get_available_maids($('#schedule_start').val(),$('#schedule_end').val(),$(this).val());
    });


    //save
    $('.save-unpaid').on('click', function (){

        $('.preloader').show(); 
        
            var area     = $('#area_id').val();
            var subarea  = $('#subarea_id').val();
            var customer = $('#customer_id').val();



        if($('#save_type').val()=='2'){


            var num_employee_2 = $('#num_employee_2').val();

            var recur_f  = $('#recur_date_from').val();
            var recur_t  = $('#recur_date_to').val();
            var timeF    = $('#timefrom_2').val();
            var timeT    = $('#timeto_2').val();            

            var employee_2 = $('#maids_id_2').val();

            var employee_arr_2;
            if(! $.isArray([employee_2])){
          
                employee_arr_2 = [employee_2];
            }else{

                employee_arr_2 = employee_2;
            }
            var topay    = $('#to_pay_2').val();
            var comment  = $('#service_2').val();
            var arival   = $('#interval_2').val();
            var driver   = $('#driver_id_2').val();

            if(customer == '' || area == '' || subarea == '' || employee_2 == ''|| topay == ''){
                foralert('error','Incomplete appointment details');
            }else{

                $.ajax({
                    url: '{{route('MaidsScheduleRecur')}}',
                    type: 'POST',
                    data: {
                        _token : $('meta[name="csrf-token"]').attr('content'),
                        days: $('#weekdays').val().split(','),
                        area_id: area,
                        subarea_id: subarea,
                        maids_id_2: employee_arr_2,
                        driver_id: driver,
                        customer_id: customer,
                        schedule_timeF: timeF,
                        schedule_timeT: timeT,
                        schedule_dateF: recur_f,
                        schedule_dateT: recur_t,
                        to_pay: topay,
                        notes: comment,
                        interval: arival,
                        action: 'save',
                        num_employee_2: num_employee_2
                    },

                    success: function(returnD){
                        $('.preloader').hide(); 
                        foralert('success','Recurring schedules are successfully added');

                    }

                });



            }



        }else{
            var num_employee = $('#num_employee').val();
            var employee     = $('#maids_id').val();
            var employee_arr;
            if($('#maids_id').val().length > 1){
                employee_arr = [employee];
            }else{
                employee_arr = employee;
            }

    
            var driver       = $('#driver_id').val();
            var appointStart = $('#schedule_start').val();
            var appointEnd   = $('#schedule_end').val();
            var topay        = $('#to_pay').val();
            var comment      = $('#service').val();
            var arival       = $('#interval').val();

            if(customer == '' || area == '' || subarea == '' || employee == ''|| topay == ''){
                foralert('error','Incomplete appointment details');
            }else{

                $.ajax({
                    url: '{{route('newAppointment')}}',
                    type: 'POST',
                    data: {
                        _token : $('meta[name="csrf-token"]').attr('content'),
                        area_id: area,
                        subarea_id: subarea,
                        maids_id: employee_arr,
                        driver_id: driver,
                        customer_id: customer,
                        schedule_start: appointStart,
                        schedule_end: appointEnd,
                        to_pay: topay,
                        notes: comment,
                        interval: arival,
                        num_employee: num_employee
                    },

                    success: function(returnD){

                       $('.preloader').hide(); 
                       foralert('success','Maid schedule is successfully added with service #: S#'+returnD);

                    }

                });


            }


        }



    }); 





    function foralert(action,message){

        if(action=='success'){

            toastr.success(message,'Attention',{

                "positionClass": "toast-top-right",

                timeOut: 3000,

                "closeButton": true,

                "debug": false,

                "newestOnTop": true,

                "progressBar": true,

                "preventDuplicates": true,

                "onclick": null,

                "showDuration": "300",

                "hideDuration": "1000",

                "extendedTimeOut": "1000",

                "showEasing": "swing",

                "hideEasing": "linear",

                "showMethod": "fadeIn",

                "hideMethod": "fadeOut",

                "tapToDismiss": false,

                    onHidden: function () {

                        window.location.href = '{{ url("admin/maids-schedule") }}';

                    }

            });

        }else{

            toastr.error(message,'Attention!',{

                "positionClass": "toast-top-right",

                timeOut: 5000,

                "closeButton": true,

                "debug": false,

                "newestOnTop": true,

                "progressBar": true,

                "preventDuplicates": true,

                "onclick": null,

                "showDuration": "300",

                "hideDuration": "1000",

                "extendedTimeOut": "1000",

                "showEasing": "swing",

                "hideEasing": "linear",

                "showMethod": "fadeIn",

                "hideMethod": "fadeOut",

                "tapToDismiss": false



            });

        }



    } 





});





//recurring


    $('input[name="recur_date"]').daterangepicker({

        "autoUpdateInput": false,

        "showDropdowns": false,

            "startDate": moment(),

            "endDate":   moment().add(1, 'M'),



        }, function(start, end, label) {

            $('input[name="recur_date"]').val(start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));

            from = start.format('YYYY-MM-DD');

            to = end.format('YYYY-MM-DD');



            $('#recur_date_from').val(from);

            $('#recur_date_to').val(to);



            $("#timefrom_2").prop('disabled', false);



            get_available_maids($('#recur_date_from').val(),$('#recur_date_to').val(),$('#interval_2').val());



    });











    $("#recur_days").select2({

        maximumSelectionLength: 7,

        placeholder: "Select a day of the week"

    });



    //reccurring weekly



    $("#recur_days").on("select2:select select2:unselect", function (e) {



        $('#weekdays').val($(this).val());

        if($(this).val()==''){

            $('#to_pay_2').val('');

            $('#to_pay_view_2').val('0.00'+' '+'AED');

        }else{

            getAmountRecur($('#timefrom_2').val(),$('#timeto_2').val(),0);

        }

       





    });



    $('#timefrom_2').focus(function(){



        $('#timefrom_2').timepicker({

            minTime: { hour: 08, minute: 00 },

            maxTime: { hour: 20, minute: 00 },

            showLeadingZero: true

        }); 



    });



    $('#timefrom_2').on('change',function(){



        $("#timeto_2").prop('disabled', false);



        var startDate = moment().format('YYYY-MM-DD');

        var startTime = $('#timefrom_2').val();



        var date = new Date(startDate + ' ' + startTime);



        var newtimeto = moment(date).add(1, 'hours').format('HH:mm');

        var newdate = moment(date).add(1, 'hours');



        $('#timeto_2').val(newtimeto);

        var hour = newdate.hour();

        var minutes = newdate.minutes();



        var datetimefrom = moment(date).format('YYYY-MM-DD HH:mm:ss');

        $('#schedule_start_2').val(datetimefrom);



        tpMinMaxSetMinTime_2(hour,minutes);



        $("#employee_2").prop("disabled", false);
        $("#num_employee_2").prop("disabled", false);

        $("#driver_id_2").prop('disabled', false);

        $('#recur_days').prop('disabled', false);

        $('#interval_2').prop('disabled',false);


    });



    $('#timeto_2').timepicker({

        maxTime: { hour: 20, minute: 00 },

        showLeadingZero: true

    });



    $('#timeto_2').on('change',function(){  

        var calendarDate   = $('#recur_date_to').val();

        var endDateandTime = new Date(calendarDate + ' ' + $('#timeto_2').val());

        var datetimeto = moment(endDateandTime).format('YYYY-MM-DD HH:mm:ss');

        $('#schedule_end_2').val(datetimeto);

        get_available_maids($('#recur_date_from').val(),datetimeto,$('#interval_2').val());

        getAmount($('#timefrom_2').val(),$('#timeto_2').val(),0);

    });





    function tpMinMaxSetMinTime_2( hours, minutes ) {

        $('#timeto_2').timepicker('option', { minTime: { hour: hours, minute: minutes} });

        var startDate = $('#datetimes_2').val();

        var date = new Date(startDate + ' ' +  $('#timeto_2').val());

        var datetimeto = moment(date).format('YYYY-MM-DD HH:mm:ss');

        $('#schedule_end_2').val(datetimeto);

        // get_available_maids($('#schedule_start_2').val(),datetimeto,$('#interval_2').val());

        // getAmountRecur($('#timefrom_2').val(),$('#timeto_2').val());

    }





    $('input[name="datetimes_2"]').daterangepicker(

        { 

        autoUpdateInput: false,

        timePicker: false,

        singleDatePicker: true,  

        minDate: new Date(),

        startDate: moment().startOf('hour'),

        endDate: moment().startOf('hour').add(36, 'hour'),

            locale: {



                format: 'YYYY-MM-DD'

            }

        },

        function(start, end, label) {

            $('#schedule_start_2').val(start.format('YYYY-MM-DD'));

            $('#schedule_end_2').val(end.format('YYYY-MM-DD'));

            $('#datetimes_2').val(start.format('YYYY-MM-DD'));



            get_available_maids($('#schedule_start_2').val(),$('#schedule_end_2').val(),$('#interval_2').val());



            // $("#timefrom_2").prop('disabled', false);

            // $("#timeto_2").prop('disabled', false);



            // if($('#datetimes_2').val() > curDate){

            //     $('#timefrom_2').timepicker({

            //         minTime: { hour: 08, minute: 00 },

            //         maxTime: { hour: 20, minute: 00 },

            //         showLeadingZero: true

            //     }); 

          

            // }else{

            //     $('#timefrom_2').timepicker({

            //         minTime: { hour: h, minute: m },

            //         maxTime: { hour: 20, minute: 00 },

            //         showLeadingZero: true

            //     }); 

            // }



        }

    );



    $('#employee_2').on('change', function(){

        $('#maids_id_2').val($(this).val());

    });





    //available maids

    function get_available_maids(schedule_start,schedule_end,arival){



        $('#employee').empty();
        $('#employee_2').empty();

        $.ajax({

            url: '{{route('get_available_maids')}}',

            type: 'POST',

            data: {

                _token : $('meta[name="csrf-token"]').attr('content'),

                schedule_start: schedule_start,

                schedule_end: schedule_end,

                interval: arival

            },

            success: function(data){          

                $("#driver_id,#interval").prop("disabled", false);

                $('#employee,#employee_2').append('<option>Select Maid</option>');

                var ctr = 1;

    
                if(JSON.parse(data) != ''){
                    $.each(JSON.parse(data),function(idx,val){

                        $('#employee').append('<option value="'+val.id+'">'+val.fullname+'</option>');

                        $('#availmaids').html(ctr+ " available");

                        $('#employee_2').append('<option value="'+val.id+'">'+val.fullname+'</option>');

                        $('#availmaids_2').html(ctr+ " available");

                        ctr++;

                    });
                }else{

                    $('#availmaids, #availmaids_2').html("no available maids");
                }

  

             

            }

        });

    }



    //amount to pay

    function getAmount(from,to,number){



        $.ajax({

            url: '{{ route('getAmount') }}',

            type: 'POST',

            data: {

                _token  : $('meta[name="csrf-token"]').attr('content'),

                timefrom: from,
                timeto: to,
                employee_number: number

            },

            success: function (data){



            var parsed = JSON.parse(data);



                $('#to_pay').val(parsed.amount);

                $('#to_pay_view').val(parsed.view+' '+'AED');

     

                // $('#to_pay_2').val(data);

                // $('#to_pay_view_2').val(data+' '+'AED');



            }

        }); 

    }

 



    function getAmountRecur(from,to,employee){



        var recur_f  = $('#recur_date_from').val();

        var recur_t  = $('#recur_date_to').val();

    

        $.ajax({

            url: '{{ route('MaidsScheduleRecur') }}',

            type: 'POST',

            data: {

                _token  : $('meta[name="csrf-token"]').attr('content'),

                days: $('#weekdays').val().split(','),

                timefrom: from,
                timeto: to,
                schedule_dateF: recur_f,
                schedule_dateT: recur_t,
                num_employee_2: employee,
                action: 'amount'

        },

        success: function (data){



            var parsed = JSON.parse(data);



            $('#to_pay_2').val(parsed.amount);

            $('#to_pay_view_2').val(parsed.view+' '+'AED');



        }

    }); 



    }



</script>





<script type="text/javascript">

$(document).ready(function(){






    //photo change onclick

    $("#photo").change(function () {

        readURL(this);

    });



    function readURL(input) {

        if (input.files && input.files[0]) {

            var reader = new FileReader();



            reader.onload = function (e) {

                // alert(e.target.result);

                $('#cur_img').attr('src', e.target.result);

            }



            reader.readAsDataURL(input.files[0]);

        }

    }





    $('#addArea').on('change',function(){

        $('#addSubarea').html('');

        subarea($(this).val());

    });



    $('#addSubarea').on('change',function(){

        $('#add_subarea_val').val($(this).val());

    });



    function subarea(area_id){



        $.ajax({

            url: '{{ route('getareas') }}',

                type: 'POST',

                data: {

                _token  : $('meta[name="csrf-token"]').attr('content'),

                area : area_id,

                actionfor: 'change'

            },  

            success:function(data){

                $.each(JSON.parse(data), function( idx, val ) {     

                    $("#addSubarea").append('<option value='+val.id+'>'+val.sub_area+' </option>');

                });

            }

        }); 

        



    }

}); 



</script>



@endsection