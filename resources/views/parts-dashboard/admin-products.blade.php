@extends('layouts.app')

    <!-- Vendor styles -->


@section('content-styles')

    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />

    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">

    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('dashtemplate/css/lib//daterangepicker/daterangepicker.css') }}" />

    <style type="text/css">
        
    .stronger {
        font-size: 35px;
    }

    </style>
@endsection

@section('content')

<body class="fix-header fix-sidebar">



@include('layouts.dashboard.header')

    
<div class="container">
    <div class="card">
        <div class="row">
            
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
            
                <!-- PRICE ITEM -->
                <div class="panel price panel-red">
                    <div class="panel-heading  text-center">
                    <h3>PRO PLAN</h3>
                    </div>
                    <div class="panel-body text-center">
                        <p class="lead" style="font-size:40px"><strong class="stronger">35 / Hour</strong></p>
                    </div>
                    <ul class="list-group list-group-flush text-center">
                        <li class="list-group-item"><i class="icon-ok text-danger"></i> Personal use</li>
                        <li class="list-group-item"><i class="icon-ok text-danger"></i> Unlimited projects</li>
                        <li class="list-group-item"><i class="icon-ok text-danger"></i> 27/7 support</li>
                    </ul>
                    <div class="panel-footer">
                        <a class="btn btn-lg btn-block btn-danger" href="#">Activate</a>
                    </div>
                </div>
                <!-- /PRICE ITEM -->
                
                
            </div>
                    
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
            
                <!-- PRICE ITEM -->
                <div class="panel price panel-blue">
                    <div class="panel-heading arrow_box text-center">
                    <h3>DEV PLAN</h3>
                    </div>
                    <div class="panel-body text-center">
                        <p class="lead" style="font-size:40px"><strong class="stronger">35 / Hour</strong></p>
                    </div>
                    <ul class="list-group list-group-flush text-center">
                        <li class="list-group-item"><i class="icon-ok text-info"></i> Personal use</li>
                        <li class="list-group-item"><i class="icon-ok text-info"></i> Unlimited projects</li>
                        <li class="list-group-item"><i class="icon-ok text-info"></i> 27/7 support</li>
                    </ul>
                    <div class="panel-footer">
                        <a class="btn btn-lg btn-block btn-info" href="#">Activate</a>
                    </div>
                </div>
                <!-- /PRICE ITEM -->
                
                
            </div>
                    
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
            
                <!-- PRICE ITEM -->
                <div class="panel price panel-green">
                    <div class="panel-heading arrow_box text-center">
                    <h3>FREE PLAN</h3>
                    </div>
                    <div class="panel-body text-center">
                        <p class="lead" style="font-size:40px"><strong class="stronger">35 / Hour</strong></p>
                    </div>
                    <ul class="list-group list-group-flush text-center">
                        <li class="list-group-item"><i class="icon-ok text-success"></i> Personal use</li>
                        <li class="list-group-item"><i class="icon-ok text-success"></i> Unlimited projects</li>
                        <li class="list-group-item"><i class="icon-ok text-success"></i> 27/7 support</li>
                    </ul>
                    <div class="panel-footer">
                        <a class="btn btn-lg btn-block btn-success" href="#">Activate</a>
                    </div>
                </div>
                <!-- /PRICE ITEM -->
                
                
            </div>
                    
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
            
                <!-- PRICE ITEM -->
                <div class="panel price panel-grey">
                    <div class="panel-heading arrow_box text-center">
                    <h3>FREE PLAN</h3>
                    </div>
                    <div class="panel-body text-center">
                        <p class="lead" style="font-size:40px"><strong class="stronger">35 / Hour</strong></p>
                    </div>
                    <ul class="list-group list-group-flush text-center">
                        <li class="list-group-item"><i class="icon-ok text-success"></i> Personal use</li>
                        <li class="list-group-item"><i class="icon-ok text-success"></i> Unlimited projects</li>
                        <li class="list-group-item"><i class="icon-ok text-success"></i> 27/7 support</li>
                    </ul>
                    <div class="panel-footer">
                        <a class="btn btn-lg btn-block btn-primary" href="#">Activate</a>
                    </div>
                </div>
                <!-- /PRICE ITEM -->
                
                
            </div>           
                 
        </div>
    </div>   
</div>


@include('layouts.dashboard.footer')



                                                                                                                                                                                                                                                                                                                                                                                                   

</body>

@endsection

@section('content-scripts')


<!-- scripit init-->
<script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>

<!-- Bootstrap tether Core JavaScript -->

<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>

<!-- slimscrollbar scrollbar JavaScript -->

<script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>

<!--Menu sidebar -->

<script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>

<!--stickey kit -->

<script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>

<!--Custom JavaScript -->



<!-- scripit init-->

<script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>



<!-- App scripts -->



@endsection