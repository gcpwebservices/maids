
@extends('layouts.app')

@section('content-styles')
    <!-- Custom CSS -->

    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">


@endsection

@section('content')
<body class="fix-header fix-sidebar">

    <?php $routes = $routes = Request::segment(4); ?>
    @if ( $routes== 'edit'  )
        @include('layouts.dashboard.header')
    @endif                                                                                                                                                             

<div class="container-fluid" id="formaids" style="padding-top: 20px;">

    @if(session('status'))
    <div id="successMessage" class="alert alert-{{session('status')[0]}}" style="background-color: #dff0d8 !important;color: #3c763d!important; border-color: #d6e9c6 !important;">
    {{session('status')[1]}}
    </div>
    @endif


    <div class="row">
        <div class="col-lg-12">
            <div class="card card-outline-primary">
                <div class="card-body">
                    <a class="pull-right" href="{{ route('users.index') }}"><button class="btn btn-inverse">Cancel</button></a>
                    <form method="POST" enctype="multipart/form-data" action="{{route('users.update', $uedit->id)}}">
                        @csrf
                        @method('PUT')
                        <div class="form-body">
                            <h3 class="card-title m-t-15">Personal Info</h3>
                            <hr>
                            <div class="row p-t-20">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="control-label">Fullname</label>
                                        <input type="text" id="fullname" name="fullname" class="form-control" autocomplete="off" 
                                        value="{{$uedit->name}}" required="Required">                                                    
                                    </div>
                                </div>

                               <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="control-label">Username</label>
                                        <input type="text" id="username" name="username" class="form-control" autocomplete="off" 
                                        value="{{$uedit->username}}" required="Required">                                                    
                                    </div>
                                </div>
                         
                            </div>
                  
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="control-label">Email</label>
                                        <input type="email" id="email" name="email" class="form-control" autocomplete="off" 
                                         value="{{$uedit->email}}" required="Required">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Access</label>
                                         <select class="form-control custom-select"  name="access" required="required">            
                                            <option value="99" @if(old('access',$uedit->access) == '99') selected @endif >
                                                Admin
                                            </option>
                                            <option value="90" @if(old('access',$uedit->access) == '90') selected @endif >
                                                Staff
                                            </option>
                                        </select>          
                                    </div>
                                </div>

                            </div>

                         

                        </div>
                        <button type="submit" class="btn btn-success pull-right"> <i class="fa fa-check"></i> Save</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

    <?php $routes = $routes = Request::segment(4); ?>
    @if ( $routes== 'edit'  )
        @include('layouts.dashboard.footer')
    @endif                                                                                                                                                                                                                                                                                                                                                                                                            
</body>

@endsection

@section('content-scripts')
    <script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <!--Custom JavaScript -->

    <!-- fullcalendar -->    
    <script src="{{ asset('dashtemplate/fullcalendar/lib/moment.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/fullcalendar/lib/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/fullcalendar/scheduler.min.js') }}"></script>

    <!-- scripit init-->
    <script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>


    <script src="{{ asset('vue/dist/vue.js') }}"></script>
    <script src="{{ asset('vue/dist/vue.min.js') }}"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script type="text/javascript">
    </script>

@endsection