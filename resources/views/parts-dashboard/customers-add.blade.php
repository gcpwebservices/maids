

@extends('layouts.app')



@section('content-styles')

    <!-- Custom CSS -->



    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />

    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">

    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('dashtemplate/js/lib/bootstrap-tagsinput-latest/src/bootstrap-tagsinput.css') }}">

    <style type="text/css">

        

        .bootstrap-tagsinput input{

            height: 29px !important;

        }



    </style>

@endsection



@section('content')

<body class="fix-header fix-sidebar">



    <?php $routes = $routes = Request::segment(3); ?>

    @if ( $routes== 'create'  )

        @include('layouts.dashboard.header')

    @endif                                                                                                                                                             



<div class="container-fluid" id="forcustomers" style="padding-top: 20px;">



    <div class="panel-body">

            <form method="POST" enctype="multipart/form-data" action="{{route('customers.store')}}" novalidate>

            @csrf

            <div class="row">

                <div class="col-md-12">

                    <div class="row">

                        <div class="col-md-3">

               

                            <img style="cursor: pointer;width:100%"  onclick="$('#photo').trigger('click')"  id="cur_img" 

                            class="img img-thumbnail img-responsive" src="{{ asset('img/default_big.png') }}">

                            <p class="text-center">( Click to browse image )</p>

                            <input class="hide" type="file" name="photo" id="photo">



                        </div>

                        <div class="col-md-9">

                            <div class="row">

                                <div class="col-md-12">

                                    <div class="hpanel">

                                        <div class="panel-heading" style="padding-bottom: 20px;">

                                            <span><i class="fa fa-user"></i> Personal Information</span></div>

                                        <div class="panel-body">

                                            <div class="row">

                                                <div class="col-md-8">

                                                    <div class="form-group">

                                                        <label>First Name <span class="required">*</span></label>

                                                        <input required="" name="fullname" type="text" 

                                                        class="form-control">

                                                    </div>

                                                </div>

                              

                                            </div>



                                            <div class="row">

                                                <div class="col-md-6">

                                                    <div class="form-group">

                                                        <label>Email Address</label>

                                                        <input name="email" type="email" id="email" autocomplete="off" class="form-control">

                                                    </div>

                                                </div>

                                                <div class="col-md-6">

                                                    <div class="form-group">

                                                        <label>Mobile Number <span class="required">*</span></label>

                                                        <input required="" name="mobile" 

                                                        type="text" class="form-control" value="@{{mobile}}" required="required" 

                                                        placeholder="+971" maxlength="13" autocomplete="off">

                                                    </div>

                                                </div>

                                            </div>



                                            <div class="row">

                                                <div class="col-md-12">

                                                    <div class="form-group">

                                                        <label>Address</label>

                                                        <textarea name="address" class="form-control" ></textarea>

                                                    </div>

                                                </div>

                                            </div>



                                            <div class="row">

                                                <div class="col-md-6">

                                                    <div class="form-group">

                                                        <label>Area</label>

                                                        <select name="area" id="area" class="form-control" >

                                                        <option value="">-Select-</option>

                                                            @foreach ($area as $alist)

                                                            <option value="{{ $alist->id }}">{{ $alist->area }}</option>

                                                            @endforeach

                                                        </select>

                                                    </div>

                                                </div>

                                                <div class="col-md-6">

                                                    <div class="form-group">

                                                        <label>Subarea</label>

                                                        <select name="subarea" id="subarea" class="form-control"></select>

                                                        <input type="hidden" id="subarea_val"name="subarea_val">

                                                    </div>

                                                </div>

                                            </div>



                                            <div class="row">

                                                <div class="col-md-4">

                                                    <div class="form-group">

                                                        <label class="control-label">Date of Birth</label>

                                                        <input type="date"  class="form-control" name="birthdate" 

                                                        id="birthdate" autocomplete="off" 

                                                        required="required">

                                                    </div>

                                                </div>



                                                <div class="col-md-4">

                                                    <div class="form-group">

                                                        <label class="control-label">Gender</label>

                                                        <select class="form-control custom-select" name="gender" required="required">

                                                            <option value="">--select--</option>

                                                            <option value="2">Male</option>

                                                            <option value="1">Female</option>

                                                        </select>            

                                                    </div>

                                                </div>

                                            </div>



                                            <div class="row" style="margin-top: 20px;">

                                                <div class="col-md-12">

                                                    <a href="#addinfo" class="" data-toggle="collapse" id="addinfoid">

                                                         Add More Information

                                                         <i class="fa fa-chevron-down" id="down"></i>

                                             

                                                    </a>

                                                </div>

                                            </div>



                                            <div id="addinfo" class="collapse">

                                  

                                                <div class="row">

                                                    <div class="col-md-5">

                                                        <div class="form-group">

                                                            <label class="control-label">Property Type</label>

                                                            <select class="form-control custom-select" name="property" id="property"

                                                            required="required">

                                                                <option value="">--select--</option>

                                                                <option value="1">Flat</option>

                                                                <option value="2">Villa</option>

                                                                <option value="3">Office</option>

                                                                <option value="4">Industrial</option>

                                                            </select>            

                                                        </div>

                                                    </div>



                                                </div>



                                                <div class="row">



                                                    <div class="col-md-3" id="forroom">

                                                        <div class="form-group">

                                                            <label class="control-label">No. of Rooms</label>

                                                            <select class="form-control custom-select" name="room" required="required">

                                                                <option value="">--select--</option>

                                                                <option value="1">1</option>

                                                                <option value="2">2</option>

                                                                <option value="3">3</option>

                                                                <option value="4">4</option>

                                                                <option value="5">5</option>

                                                                <option value="6+">6+</option>

                                                            </select>            

                                                        </div>

                                                    </div>



                                                    <div class="col-md-3" id="fortoilet">

                                                        <div class="form-group">

                                                            <label class="control-label">No. of Bathrooms</label>

                                                            <select class="form-control custom-select" name="toilet" required="required">

                                                                <option value="">--select--</option>

                                                                <option value="1">1</option>

                                                                <option value="2">2</option>

                                                                <option value="3">3</option>

                                                                <option value="4">4</option>

                                                                <option value="5">5</option>

                                                                <option value="6+">6+</option>

                                                            </select>            

                                                        </div>

                                                    </div>



                                                    <div class="col-md-3" id="forgarden">

                                                        <div class="form-group">

                                                            <label class="control-label">Garden</label>

                                                            <select class="form-control custom-select" name="garden" required="required">

                                                                <option value="">--select--</option>

                                                                <option value="1">Front</option>

                                                                <option value="2">Back</option>

                                                                <option value="3">Both</option>

                                                            </select>            

                                                        </div>

                                                    </div>



                                                    <div class="col-md-3" id="forparking">

                                                        <div class="form-group">

                                                            <label class="control-label">Parking</label>

                                                            <select class="form-control custom-select" name="parking" required="required">

                                                                <option value="">--select--</option>

                                                                <option value="1">Yes</option>

                                                                <option value="2">No</option>

                                        

                                                            </select>            

                                                        </div>

                                                    </div>



                                                </div>



                                            </div>



                                        </div>

                                    </div>

                                </div>

                            </div>



                        </div>



                    </div>

                    <div class="form-group pull-right" style="padding-top: 30px; ">

                        <button class="btn btn-primary" id="btnsubmit" type="submit"><i class="fa fa-save"></i> Save</button>

                        <a href="{{ route('customers.index') }}" class="btn btn-default"><i class="fa fa-times"></i> Cancel</a>

                    </div>

                </div>      

            </div>

            </form>            

    </div>

</div>



    <?php $routes = $routes = Request::segment(3); ?>

    @if ( $routes== 'create'  )

        @include('layouts.dashboard.footer')

    @endif                                                                                                                                                                                                                                                                                                                                                                                                            

</body>



@endsection



@section('content-scripts')

    <script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap tether Core JavaScript -->

    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- slimscrollbar scrollbar JavaScript -->

    <script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>

    <!--Menu sidebar -->

    <script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>

    <!--stickey kit -->

    <script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>

    <!--Custom JavaScript -->



    <!-- fullcalendar -->    

    <script src="{{ asset('dashtemplate/fullcalendar/lib/moment.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/fullcalendar/lib/fullcalendar.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/fullcalendar/scheduler.min.js') }}"></script>



    <!-- scripit init-->

    <script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>



    <script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>





    <script src="{{ asset('vue/dist/vue.js') }}"></script>

    <script src="{{ asset('vue/dist/vue.min.js') }}"></script>



    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>



    <script src="{{ asset('dashtemplate/js/lib/bootstrap-tagsinput-latest/src/bootstrap-tagsinput.js') }}"></script>



    <script type="text/javascript">

    $(document).ready(function(){



        $(window).keydown(function(event){

            if(event.keyCode == 13) {

              event.preventDefault();

              return false;

            }

        });



        

        $('#forroom,#fortoilet,#forgarden,#forparking').hide();





        $('#property').on('change',function(){

            var value = $(this).val();



            if(value == '1'){

                $('#forroom,#fortoilet').show();

                $('#forgarden,#forparking').hide();

            }



            if(value == '2'){

                $('#forroom,#fortoilet,#forgarden,#forparking').show();

            }



            if(value == '3'){

                $('#forroom,#fortoilet,#forparking').show();

                $('#forgarden').hide();

            }



            if(value == '4'){

                $('#forroom,#fortoilet,#forparking').show();

                $('#forgarden').hide();

            }



       

        });



        $('#addinfoid').on('click',function(){

            $(this).children('.fa').toggleClass('fa-chevron-up fa-chevron-down');

        });



        setTimeout(function() {

            $('#successMessage').fadeOut('fast');

        }, 5000);



        //photo change onclick

        $("#photo").change(function () {

            readURL(this);

        });



        function readURL(input) {

            if (input.files && input.files[0]) {

                var reader = new FileReader();



                reader.onload = function (e) {

                    // alert(e.target.result);

                    $('#cur_img').attr('src', e.target.result);

                }



                reader.readAsDataURL(input.files[0]);

            }

        }





        $('#area').on('change',function(){

            $('#subarea').html('');

            subarea($(this).val());

        });



        $('#subarea').on('change',function(){

            $('#subarea_val').val($(this).val());

        });



        function subarea(area_id){

    

            $.ajax({

                url: '{{ route('getareas') }}',

                    type: 'POST',

                    data: {

                    _token  : $('meta[name="csrf-token"]').attr('content'),

                    area : area_id,

                    actionfor: 'change'

                },  

                success:function(data){

                    $.each(JSON.parse(data), function( idx, val ) {     

                        $("#subarea").append('<option value='+val.id+'>'+val.sub_area+' </option>');

                    });

                }

            }); 

            



        }

    }); 





    Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

    new Vue({



        el: '#forcustomers',



        data: {



            mobile : '+971'





        },



        methods : {



        }







    });



    </script>



@endsection