

@extends('layouts.app')



@section('content-styles')

    <!-- Custom CSS -->



    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />

    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">

    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">



@endsection



@section('content')

<body class="fix-header fix-sidebar">





    @include('layouts.dashboard.header') 

 



<div class="container-fluid" id="forcustomers" style="padding-top: 20px;">



    @if(session('status'))

    <div id="successMessage" class="alert alert-{{session('status')[0]}}" style="background-color: #dff0d8 !important;color: #3c763d!important; border-color: #d6e9c6 !important;">

    {{session('status')[1]}}

    </div>

    @endif



        <div class="panel-body">

            <form method="POST" enctype="multipart/form-data" action="{{ route('customers.update', $cedit->id) }}" novalidate>

            @csrf

            @method('PUT')

            <div class="row">

                <div class="col-md-12">

                    <div class="row">

                        <div class="col-md-3">

                            @if($cedit->photo != '')

                               <img style="cursor: pointer;width:100%" onclick="$('#photo').trigger('click')" id="cur_img" 

                               class="img img-thumbnail img-responsive" src="{{ asset('storage/customers').'/'.$cedit->photo }}">

                                <p class="text-center">( Click to browse image )</p>

                            @else

                              <img style="cursor: pointer;width:100%"  onclick="$('#photo').trigger('click')"  id="cur_img" 

                              class="img img-thumbnail img-responsive" src="{{ asset('img/default_big.png') }}">

                                <p class="text-center">( Click to browse image )</p>

                            @endif

                            <input class="hide" type="file" name="photo" id="photo">

                        </div>

                        <div class="col-md-9">

                            <div class="row">

                                <div class="col-md-12">

                                    <div class="hpanel">

                                        <div class="panel-heading" style="padding-bottom: 20px;">

                                            <span><i class="fa fa-user"></i> Personal Information</span></div>

                                        <div class="panel-body">

                                            <div class="row">

                                                <div class="col-md-8">

                                                    <div class="form-group">

                                                        <label>First Name <span class="required">*</span></label>

                                                        <input required="" name="fullname" value="{{ $cedit->fullname }}" type="text" 

                                                        class="form-control">

                                                    </div>

                                                </div>

                                 

                                            </div>

                 

                                            <div class="row">

                                                <div class="col-md-6">

                                                    <div class="form-group">

                                                        <label>Email Address</label>

                                                        <input name="email" type="email" id="email" value="{{$cedit->email}}"  

                                                        autocomplete="off" class="form-control">

                                                    </div>

                                                </div>

                                                <div class="col-md-6">

                                                    <div class="form-group">

                                                        <label>Mobile Number <span class="required">*</span></label>

                                                        <input required="" name="mobile" value="{{$cedit->mobile}}" 

                                                        type="text" class="form-control" autocomplete="off"  

                                                        required="required" placeholder="+971" maxlength="13" value="@{{mobile}}">

                                                    </div>

                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="col-md-12">

                                                    <div class="form-group">

                                                        <label>Address</label>

                                                        <textarea name="address" class="form-control" >{{ $cedit->address }}</textarea>

                                                    </div>

                                                </div>

                                            </div>



                                            <div class="row">

                                                <div class="col-md-6">

                                                    <div class="form-group">

                                                        <label>Area</label>

                                                        <select name="area" id="area" class="form-control" >

                                                        <option value="">-Select-</option>

                                                            @foreach ($area as $alist)

                                                            <option value="{{ $alist->id }}"{{ ( $cedit->area == $alist->id ) ? ' selected' : '' }}>{{ $alist->area }}</option>

                                                            @endforeach

                                                        </select>

                                                    </div>

                                                </div>

                                                <div class="col-md-6">

                                                    <div class="form-group">

                                                        <label>Subarea</label>

                                                        <select name="subarea" id="subarea" class="form-control"></select>

                                                        <input type="hidden" id="subarea_val"name="subarea_val">

                                                    </div>

                                                </div>

                                            </div>



                                            <div class="row">

                                                <div class="col-md-6">

                                                    <div class="form-group">

                                                        <label>Birthday</label>

                                                        <input name="birthdate" type="date" class="form-control"  autocomplete="off"

                                                        value="{{ date('Y-m-d',strtotime($cedit->birthdate)) }}" id="birthdate">

                                                    </div>

                                                </div>



                                                <div class="col-md-6">

                                                    <div class="form-group">

                                                        <label class="control-label">Gender</label>

                                                        <select class="form-control custom-select"  name="gender" required="required">           

                                                            <option value="1" @if(old('gender',$cedit->gender) == '1') selected @endif >

                                                                Female

                                                            </option>

                                                            <option value="2" @if(old('gender',$cedit->gender) == '2') selected @endif >

                                                                Male

                                                            </option>

                                                        </select>           

                                                    </div>

                                                </div>



                                            </div>





                                            <div class="row" style="margin-top: 20px;">

                                                <div class="col-md-12">

                                                    <a href="#addinfo" class="" data-toggle="collapse" id="addinfoid">

                                                        More Information

                                                         <i class="fa fa-chevron-down" id="down"></i>

                                             

                                                    </a>

                                                </div>

                                            </div>



                                            <div id="addinfo" class="collapse">

                                                

                                                <div class="row">

                                                    <div class="col-md-5">

                                                        <div class="form-group">

                                                            <label class="control-label">Property Type</label>

                                                            <select class="form-control custom-select" name="property" id="property">

                                                                <option value="">--select--</option>

                                            

                                                                <option value="1" @if(old('property',$cedit->property) == '1') selected @endif >

                                                                    Flat

                                                                </option>

                                                                <option value="2" @if(old('property',$cedit->property) == '2') selected @endif >

                                                                    Villa

                                                                </option>

                                                                <option value="3" @if(old('property',$cedit->property) == '3') selected @endif >

                                                                    Office

                                                                </option>

                                                                <option value="4" @if(old('property',$cedit->property) == '4') selected @endif >

                                                                    Industrial

                                                                </option>

                                                            </select>            

                                                        </div>

                                                    </div>



                                                </div>



                                                <div class="row">



                                                    <div class="col-md-3" id="forroom">

                                                        <div class="form-group">

                                                            <label class="control-label">No. of Rooms</label>

                                                            <select class="form-control custom-select" name="room">

                                                                <option value="">--select--</option>                       



                                                                <option value="1" @if(old('room',$cedit->room) == '1') selected @endif >

                                                                    1

                                                                </option>

                                                                <option value="2" @if(old('room',$cedit->room) == '2') selected @endif >

                                                                    2

                                                                </option>

                                                                <option value="3" @if(old('room',$cedit->room) == '3') selected @endif >

                                                                    3

                                                                </option>

                                                                <option value="4" @if(old('room',$cedit->room) == '4') selected @endif >

                                                                    4

                                                                </option>

                                                                <option value="5" @if(old('room',$cedit->room) == '5') selected @endif >

                                                                    5

                                                                </option>

                                                                <option value="6+" @if(old('room',$cedit->room) == '6+') selected @endif >

                                                                    6+

                                                                </option>



                                                            </select>            

                                                        </div>

                                                    </div>



                                                    <div class="col-md-3" id="fortoilet">

                                                        <div class="form-group">

                                                            <label class="control-label">No. of Bathrooms</label>

                                                            <select class="form-control custom-select" name="toilet">

                                                                <option value="">--select--</option>

                                                                <option value="1" @if(old('toilet',$cedit->toilet) == '1') selected @endif >

                                                                    1

                                                                </option>

                                                                <option value="2" @if(old('toilet',$cedit->toilet) == '2') selected @endif >

                                                                    2

                                                                </option>

                                                                <option value="3" @if(old('toilet',$cedit->toilet) == '3') selected @endif >

                                                                    3

                                                                </option>

                                                                <option value="4" @if(old('toilet',$cedit->toilet) == '4') selected @endif >

                                                                    4

                                                                </option>

                                                                <option value="5" @if(old('toilet',$cedit->toilet) == '5') selected @endif >

                                                                    5

                                                                </option>

                                                                <option value="6+" @if(old('toilet',$cedit->toilet) == '6+') selected @endif >

                                                                    6+

                                                                </option>

                                                            </select>            

                                                        </div>

                                                    </div>



                                                    <div class="col-md-3" id="forgarden">

                                                        <div class="form-group">

                                                            <label class="control-label">Garden</label>

                                                            <select class="form-control custom-select" name="garden">

                                                            <option value="">--select--</option>

                                             

                                                            <option value="1" @if(old('garden',$cedit->garden) == '1') selected @endif >

                                                                Front

                                                            </option>

                                                            <option value="2" @if(old('garden',$cedit->garden) == '2') selected @endif >

                                                                Back

                                                            </option>

                                                            <option value="3" @if(old('garden',$cedit->garden) == '3') selected @endif >

                                                                Both

                                                            </option>



                                                            </select>            

                                                        </div>

                                                    </div>



                                                    <div class="col-md-3" id="forparking">

                                                        <div class="form-group">

                                                            <label class="control-label">Parking</label>

                                                            <select class="form-control custom-select" name="parking">

                                                                <option value="">--select--</option>

                                                     

                                                                <option value="1" @if(old('parking',$cedit->parking) == '1') selected @endif >

                                                                    Yes

                                                                </option>

                                                                <option value="2" @if(old('parking',$cedit->parking) == '2') selected @endif >

                                                                    No

                                                                </option>



                                                            </select>            

                                                        </div>

                                                    </div>



                                                </div>



                                            </div>



                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="form-group pull-right" style="  padding-top: 30px; ">

                        <button class="btn btn-primary" id="btnsubmit" type="submit"><i class="fa fa-save"></i> Save</button>

                        <a href="{{ route('customers.index') }}" class="btn btn-success"> Back</a>

                    </div>

                </div>      

            </div>

            </form>            

        </div>



</div>







    @include('layouts.dashboard.footer') 

 

                                                                                                                                                                                                                                                                                                                                                                                                          

</body>



@endsection



@section('content-scripts')

    <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>

    <script type="text/javascript"  src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap tether Core JavaScript -->

    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- slimscrollbar scrollbar JavaScript -->

    <script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>

    <!--Menu sidebar -->

    <script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>

    <!--stickey kit -->

    <script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>

    <!--Custom JavaScript -->



    <!-- fullcalendar -->    

    <script src="{{ asset('dashtemplate/fullcalendar/lib/moment.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/fullcalendar/lib/fullcalendar.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/fullcalendar/scheduler.min.js') }}"></script>



    <!-- scripit init-->

    <script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>



    <script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>





    <script src="{{ asset('vue/dist/vue.js') }}"></script>

    <script src="{{ asset('vue/dist/vue.min.js') }}"></script>



    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>



<script type="text/javascript">



$(document).ready(function(){

    $('#forroom,#fortoilet,#forgarden,#forparking').hide();

    $('#addinfoid').on('click',function(){

        $(this).children('.fa').toggleClass('fa-chevron-up fa-chevron-down');

    });



    var value = $('#property').val();

    if(value == '1'){

        $('#forroom,#fortoilet').show();

        $('#forgarden,#forparking').hide();

    }



    if(value == '2'){

        $('#forroom,#fortoilet,#forgarden,#forparking').show();

    }



    if(value == '3'){

        $('#forroom,#fortoilet,#forparking').show();

        $('#forgarden').hide();

    }



    if(value == '4'){

        $('#forroom,#fortoilet,#forparking').show();

        $('#forgarden').hide();

    }





    $('#property').on('change',function(){

        var value = $(this).val();



        if(value == '1'){

            $('#forroom,#fortoilet').show();

            $('#forgarden,#forparking').hide();

        }



        if(value == '2'){

            $('#forroom,#fortoilet,#forgarden,#forparking').show();

        }



        if(value == '3'){

            $('#forroom,#fortoilet,#forparking').show();

            $('#forgarden').hide();

        }



        if(value == '4'){

            $('#forroom,#fortoilet,#forparking').show();

            $('#forgarden').hide();

        }



   

});

        



    setTimeout(function() {

        $('#successMessage').fadeOut('fast');

    }, 5000);



    //photo change onclick

    $("#photo").change(function () {

        readURL(this);

    });



    function readURL(input) {

        if (input.files && input.files[0]) {

            var reader = new FileReader();



            reader.onload = function (e) {

                // alert(e.target.result);

                $('#cur_img').attr('src', e.target.result);

            }



            reader.readAsDataURL(input.files[0]);

        }

    }





    subarea($('#area').val(),'ini');

    $('#area').on('change',function(){

        $('#subarea').html('');

        subarea($(this).val(),'change');

    });



    $('#subarea').on('change',function(){

        $('#subarea_val').val($(this).val());

    });



    function subarea(area_id,action){

        if(action == 'change'){

            $.ajax({

                url: '{{ route('getareas') }}',

                    type: 'POST',

                    data: {

                    _token  : $('meta[name="csrf-token"]').attr('content'),

                    area : area_id,

                    actionfor: action

                },  

                success:function(data){

                    $.each(JSON.parse(data), function( idx, val ) {     

                        $("#subarea").append('<option value='+val.id+'>'+val.sub_area+' </option>');

                    });

                }

            }); 

        }else{

            $.ajax({

                url: '{{ route('getareas') }}',

                    type: 'POST',

                    data: {

                    _token  : $('meta[name="csrf-token"]').attr('content'),

                    area : area_id,

                    actionfor: action

                },  

                success:function(data){

                    var append = JSON.parse(data)

                    $("#subarea").append('<option value='+append.id+'>'+append.sub_area+' </option>');

                    $('#subarea_val').val(append.id);

                }

            }); 

        }



    }







});







Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

new Vue({



    el: '#forcustomers',



    data: {



        mobile : '+971'





    },



    methods : {



    }







});



</script>



@endsection