

@extends('layouts.app')



@section('content-styles')

    <!-- Custom CSS -->



    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />

    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">

    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('dashtemplate/css/lib//daterangepicker/daterangepicker.css') }}" />

    <style type="text/css">

        thead { display: table-header-group !important; }

        tfoot { display: table-row-group !important;}

        tr { page-break-inside: avoid !important;}

        p { color: #455a64; }

        .mytable{

             border-bottom: 1px solid #ddd;

        }

    </style>

@endsection



@section('content')



    

<div class="container-fluid app" id="content">

    <div class="col-sm-12 col-md-12">

        

            <div class="row">

                <div class="col-md-12" style="margin-bottom: -10px;">

                    <div class="text-center"><img src="{{ asset('img/logoblue.png')}}" alt="homepage" class="dark-logo" width="220"></div>

                    <div class="pull-left" style="padding-top: 15px;">

                        <h4 style="margin-bottom:0px" class="text-upper font-bold">Customer: 

                            <span class="text-primary text-uppercase">{{  $maids_schedule->getCustomer['fullname'] }}</span>

                        </h4>

                    </div>

                    <div class="p-15 bg-light-gray clearfix" id="employee_detals" style="margin-bottom:20px">



                            @if($sales_details['payment_sum'] == '0.00' && $sales_details['payment_status'] == '0')

                                <span style="font-size:15px;padding-left:20px;padding-right:20px;"

                                    class="pull-right label label-danger">Unpaid</span>

                            @elseif( $sales_details['payment_sum'] != '0.00' && $sales_details['payment_status'] == '0' )

                                <span style="font-size:15px;padding-left:20px;padding-right:20px;"

                                    class="pull-right label label-warning">Partialy Paid</span>

                            @elseif( $sales_details['payment_status'] == '1' )

                                 <span style="font-size:15px;padding-left:20px;padding-right:20px;"

                                    class="pull-right label label-success">Paid</span>

                            @endif



                 

                    </div>

                </div>

            </div>



            <div class="row">

                <div class="col-md-12 ">

   

                    @if( $sales_details['payment_status'] == '1' )

                        <span class="pull-right">Full payment received on 

                            {{ $sales_details['updated_at']->format('l j F Y') }}

                        </span>

                    @endif

         

                  

                    <h3 class="font-bold no-margin">Service No. <span class="text-primary">{{ $maids_schedule['schedule_code'] }}</span></h3>

                    <div class="table-responsive" >

                        <table class="table table-bordered " id="accounting">

                            <thead>

                                <tr>



                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Payment Status</th>
                                    <th>Service Date</th>
                                    <th>Service Time</th>
                                    <th>Price</th>
                                    <th>VAT</th>

                           

                                </tr>

                            </thead>

                            <tbody>

                                @foreach($maids_relation as $data_list)

                                <tr>

                                    <td>

                                        Cleaning <br>

                                        <small>by 

                         

                                        <span class="text-primary">{{ strtoupper($data_list->getMaid['fullname']) }}</span>

                                    

                                        </small>

                                    </td>

                                    @if($data_list->getSaleSummary['cancel'] == "")

                                        <td class="text-primary"> Confirmed </td>

                                    @else

                                        <td class="text-danger"> Cancelled </td>

                                    @endif

                                    @if($data_list->getSaleSummary['is_paid'] == '1')
                                        <td class="text-primary"> Paid </td>
                                    @else
                                        <td class="text-danger"> Unpaid </td>
                                    @endif


                                    <td>  {{ $data_list->schedule_start->format('l j F Y') }} </td>

                                    <td>

                                        {{ strtoupper($data_list->schedule_start->format('H:i:s')) }} 

                                        - 

                                        {{ strtoupper($data_list->schedule_end->format('H:i:s')) }} 

                                    </td>

                                    <td>{{ $data_list->getSaleSummary['price'] }}</td>
                                    <td>{{ $data_list->getSaleSummary['payment_vat'] }}</td>
              

                               

                                </tr>

                                @endforeach

                            </tbody>

                        </table>

                    </div>

                </div>

            </div>   



            <div class="row">

                <div class="col-md-3">

                    <table class="table" style="width:300px !important">

                        <tbody>

                            <tr>

                                <td class="no-border">Subtotal</td>

                                    <td class="text-right no-border">{{ $sales_details['payment_sub_sum'] }}</td>

                            </tr>

                            <tr>

                                <td>Discount</td>

                                    <td class="text-right">{{ $sales_details['payment_disc_sum'] }}</td>

                            </tr>

                            <tr>

                                <td>VAT(5%)</td>

                                    <td class="text-right">{{ $sales_details['payment_vat_sum'] }}</td>

                            </tr>

                            <tr>

                                <th class="font-bold text-15">Total</th>

                                <th class="font-bold text-right">

                                    {{ ($sales_details['payment_total_sum'])  }}

                                </th>

                            </tr>



                            <tr>

                                <th class="font-bold text-15">Balance</th>

                                <th class="font-bold text-right text-danger">

                                {{ number_format($sales_details['payment_total_sum'] - $sales_details['payment_sum'], 2, '.', ',')  }}

                                </th>

                            </tr>



                        </tbody>

                    </table>

                </div>

            </div>

         

    </div>





   



</div>

                          

    

     









                                                                                                                                                                                                                                                                                                                                                                                                              





@endsection



