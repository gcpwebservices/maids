    

@extends('layouts.app')



@section('content-styles')

    <!-- Custom CSS -->



    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />

    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">

    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('dashtemplate/css/lib//daterangepicker/daterangepicker.css') }}" />

    <link rel="stylesheet" type="text/css" href="{{ asset('dashtemplate/css/buttons.css') }}" />
    
    <style type="text/css">



        body {

            font-size: 14px;

        }



        td,th{ 

            font-size: 14px; 

         

        }

        .dataTables_wrapper .dataTables_paginate{

            padding-bottom: 1.25em !important;

        }

        .dataTables_wrapper .dataTables_paginate {

            font-size: 0.75em !important;

        }



        .hpanel {

            background-color: none;

            border: none;

            box-shadow: none;

            margin-bottom: 25px;

        }



        .hpanel .panel-body {

            background: #fff;

            border: 1px solid #e4e5e7;

            border-radius: 2px;

            padding: 20px;

            position: relative;

        }



        .img-small {

            width: 62px;

            height: 62px;

        }



        .img-circle {

            border-radius: 50%;

        }

        .m-t-sm {

            margin-top: 10px;

        }

        .small {

            font-size: 85%;

        }



        .dataTables_length {

            display: none;

        }

        .dataTable > thead > tr > th[class*="sort"]::after{display: none}


        .btn-secondary {
            background-color: #6c757d !important;
            color:  #fff !important;
        }


    </style>

@endsection



@section('content')

<body class="fix-header fix-sidebar">



@include('layouts.dashboard.header')

    

    <div class="container-fluid app">

        <div class="row p-20">

            <div class="col-md-12">

        

                <div class="hpanel">



                    <div class="panel-body">

                        <?php  $from = date("F", strtotime("-3 months")); $to = date('F'); $year = date("Y");?>

                        <p>

                            <strong class="text-primary">Active Customers</strong> 

                            <br>

                            <small>From {{$from}} To {{$to}}, {{$year}} </small>

                        </p>



                        <div class="table-responsive">

                        <table id="activecus" class="table table-hover table-bordered table-striped">

                            <thead>

                                <tr>

                                    <th>Image</th>

                                    <th>Fullname</th>

                                    <th>Registration Date</th>

                                    <th>Services</th>

                                    <th>Profit</th>

                                    <th>Bookings</th>

                                    <th>Email Type</th>
                                    <th>Last Email</th>

                                    <th>Action</th>
                     

                                </tr>

                            </thead>



                            <tbody>

                                <?php $count=0; ?>

                                @foreach($data as $key => $list)

                                <tr>

                              

                                    <td>

                                        <img alt="logo" class="img-circle img-small" 

                                        src="{{ asset('storage/customers').'/'.$list['photo'] }}">

                                    </td>

                                    <td>

                               

                                        </br>

                                        <small class="small">{{$list['fullname'] }}</small>

                                 

                                    </td>

                                    <td>

                               

                                        </br>

                                        <small class="small">

                                            {{ \Carbon\Carbon::parse($list['registration'])->format('Y-m-d')}}

                                        </small>

                                    </td>

                                    <td class="issue-info">

                               

                                        <br/>

                                        <small class="small">

                                          

                                           {{ $list['services'] }}

             

                                        </small>

                                    </td>

                            

                                    <td>

                                     

                                      <br/>

                                         <small class="small">

                                              {{$list['profit']}}

                                        </small>

                                    

                                    </td>

                                    <td>

    

                                      <br/>

                                        <small class="small">{{$list['booking']}} </small>

                    

                                    </td>

                                   <td>
                                        </br>
                                        @if($list['last_email_type'] != '')
                                            @if($list['last_email_type'] == '1')
                                    
                                                <small class="small ">Miss You </small>
                                            @elseif($list->last_email_type == '2')
                              
                                                <small class="small ">Account </small>
                                            @elseif($list->last_email_type == '3')
                               
                                                <small class="small ">Promotion </small>
                                            @endif

                                        @endif
                                    </td>

                                    <td>
                                        @if($list['last_email'] != '')
                                            <small class="small">
                                                {{  Carbon\Carbon::parse($list['last_email'])->format('l j F Y')  }}
                                            </small>
                                        @endif
                                    </td>


                                    <td>
                                        <br>
                                        <button class="label label-info btn-primary send_mail"  data-toggle="modal" 
                                        data-target="#myModal"  data-id="{{ $list['id'] }}" >Send Email</button>
                                    </td>

                          

                                </tr>

                                <?php $count++; ?>

                                @endforeach

                            </tbody>

                        </table>

                        </div>

                    </div>



                </div>



          

            </div>

        </div>

    </div>



<input type="hidden" name="checkbox_val" id="checkbox_val">
<input type="hidden" name="customer_id" id="customer_id">


<div class="modal" id="myModal" data-backdrop="static" data-keyboard="false">

    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Select Email Template</h4>
                    <button type="button" class="btn btn-secondary" id="close" data-dismiss="modal">
                        <i class="fa fa-close"></i>
                    </button>
            </div>

            <div class="modal-body">    
                <div class="container">
                    <div class="">
       <!--              <div class="checkbox checkbox-primary checkbox-inline">
                        <input type="checkbox" class="styled checkbox-md check_class" id="Checkbox1" value="1">
                        <label for="Checkbox1"> Miss You </label>
                    </div> -->
                    <div class="checkbox checkbox-primary checkbox-inline">
                        <input type="checkbox" class="styled checkbox-md check_class" id="Checkbox2" value="2" >
                        <label for="Checkbox2"> Account </label>
                    </div>
                    <div class="checkbox checkbox-primary checkbox-inline">
                        <input type="checkbox" class="styled checkbox-md check_class" id="Checkbox3" value="3">
                        <label for="Checkbox3"> Promotion</label>
                    </div>
                    </div>
                </div> 
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="send" style="border-radius: 3px !important;">
                    Send
                </button>
             
            </div>

        </div>

    </div>

</div>


@include('layouts.dashboard.footer')



                                                                                                                                                                                                                                                                                                                                                                                                   

</body>



@endsection



@section('content-scripts')



<script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>

<!-- Bootstrap tether Core JavaScript -->

<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>

<!-- slimscrollbar scrollbar JavaScript -->

<script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>

<!--Menu sidebar -->

<script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>

<!--stickey kit -->

<script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>

<!--Custom JavaScript -->



<!-- scripit init-->

<script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>



<!-- datatables -->

<script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>

<script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>



<!-- highcharts -->

<script src="https://code.highcharts.com/highcharts.js"></script>

<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script src="https://code.highcharts.com/modules/export-data.js"></script>



<!-- vue js -->

<script src="{{ asset('vue/dist/vue.js') }}"></script>

<script src="{{ asset('vue/dist/vue.min.js') }}"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>


<!-- moment -->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script type="text/javascript">

$(document).ready(function(){

    $('#myModal').on('hidden.bs.modal', function () {

            location.reload();

    });

    var table = $('#activecus').DataTable({

        "pageLength": 10,

        "paging":   true,

        "ordering": true,

        "info":     true,

        "autoWidth": false,

        "searching": false,

        "columns": [

            { "width": "5%" },
            { "width": "15%" },
            { "width": "15%" },
            { "width": "20%" },
            { "width": "5%" },
            { "width": "5%" },
            { "width": "10%" },
            { "width": "10%" },
            { "width": "5%" },

        ],

        "aaSorting": [[ 5, "desc" ]]

    });




    $('#close').click(function  (){
        $('#myModal').modal('hide');
    });


    $('.send_mail').on('click', function(){
       $('#customer_id').val($(this).attr("data-id"));
    });


    $('input[type="checkbox"]').bind('click',function() {
        $('input[type="checkbox"]').not(this).prop("checked", false);
            var checked = $(this).val();
            $('#checkbox_val').val(checked);
    });


    $('#send').click(function(){
        if($('#checkbox_val').val() == ''){
            toastr.error('Please select a template! ','Attention!',{
                "positionClass": "toast-top-right",
                timeOut: 3000,
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut",
                "tapToDismiss": false
            });

        }else{

            var btn = $(this);
            btn.html('Sending..');
            $( "#close" ).prop( "disabled", true );
            var checkbox_val = $('#checkbox_val').val();

            // alert(checkbox_val);
            // alert($('#customer_id').val());

            $.ajax({
                url: '{{ route('emailCustomers') }}',
                type: 'POST',
                data: 
                {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    customer_id: $('#customer_id').val(),
                    checkbox_val: $('#checkbox_val').val()
                },
                success: function(data){

                    btn.html('Email Sent');
                    setTimeout(function () {
                        location.reload();

                         // btn.html('Send');
                         // $( "#close" ).prop( "disabled", false );
                    }, 3000);

                }

            });
         
        }


    });


});







</script>

@endsection