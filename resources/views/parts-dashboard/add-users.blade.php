
@extends('layouts.app')

@section('content-styles')
    <!-- Custom CSS -->

    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">


@endsection

@section('content')
<body class="fix-header fix-sidebar">

    <?php $routes = Route::current()->getName(); ?>
    @if ( $routes== 'add-users')
        @include('layouts.dashboard.header')
    @endif

     <div class="container-fluid" id="vueitems">

        @if(session('status'))
        <div id="successMessage" class="alert alert-{{session('status')[0]}}" style="background-color: #dff0d8 !important;color: #3c763d!important; border-color: #d6e9c6 !important;">
        {{session('status')[1]}}
        </div>
        @endif

        <div class="card">
            <div class="card-body">
                 <button class="btn btn-success pull-right" data-toggle="modal" data-target="#create-user">Add User</button>
                <div class="table-responsive m-t-40">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Fullname</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Action</th>
                           
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ulist as $list)
                            <tr>
                                <td >{{$list->name}}</td>
                                <td>{{$list->username}}</td>
                                <td>{{$list->email}}</td>
                                <td>
                                    <button class="btn-sm btn-success"  v-on:click="getid('{{$list->id}}')">Edit</button> |
                                    <button class="btn-sm btn-danger"  v-on:click="openmodal('{{$list->id}}')">Delete</button>
                                </td>
                            </tr>
                      
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>




            <div class="modal fade" id="create-user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card card-outline-primary">
                                        <div class="card-body">
                                            <form method="POST" enctype="multipart/form-data" v-on:submit.prevent="createUser">
                                            @csrf
                                            <input type="hidden" value="users" name="action" v-model="userItem.action">
                                                <div class="form-body">
                                                    <h3 class="card-title m-t-15">Person Info</h3>
                                                    <hr>
                                                    <div class="row p-t-20">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Fullname</label>
                                                                <input type="text" id="name" name="name" class="form-control" autocomplete="off" 
                                                                v-model="userItem.name" required="Required">
                                                           
                                                            </div>
                                                        </div>

                                                         <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Username</label>
                                                                <input type="text" id="username" name="username" class="form-control" autocomplete="off" 
                                                                v-model="userItem.username" required="Required">
                                                              
                                                            </div>
                                                        </div>
                                               
                                                 
                                                    </div>
                                          
                                                    <div class="row">
                                              
                                                        <div class="col-md-8">
                                                            <div class="form-group">
                                                                <label class="control-label">Email</label>
                                                                <input type="text" id="email" name="email" class="form-control" autocomplete="off" 
                                                                v-model="userItem.email" required="Required">
                                                       
                                                            </div>
                                                        </div>
                                                  
                                                    </div>
                                           

                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                                    <button type="button"  class="closemodal btn btn-inverse pull-right">Cancel</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- edit user -->
            <div class="modal fade" id="edit-user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card card-outline-primary">
                                        <div class="card-body">
                                            <form method="POST" enctype="multipart/form-data" v-on:submit.prevent="updateUser">
                                            @csrf
                                                <input type="hidden" name="id" v-model="returned.id">
                                                <div class="form-body">
                                                    <h3 class="card-title m-t-15">Person Info</h3>
                                                    <hr>
                                                    <div class="row p-t-20">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Full Name</label>
                                                                <input type="text" name="firstname" class="form-control" autocomplete="off" 
                                                                v-model="returned.name"  required="Required">
                                                           
                                                            </div>
                                                        </div>
                                                
                                                         <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Username</label>
                                                                <input type="text" name="username" class="form-control" autocomplete="off" 
                                                                v-model="returned.username" required="Required">
                                                              
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <!--/row-->
                                                    <div class="row">
                                               
                                                        <!--/span-->
                                                        <div class="col-md-8">
                                                            <div class="form-group">
                                                                <label class="control-label">Email</label>
                                                                <input type="text" name="email" class="form-control" autocomplete="off" 
                                                                v-model="returned.email" required="Required">
                                                       
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->     

                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                                    <button type="button" class="btn btn-inverse pull-right closemodal">Cancel</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- delete -->

            <div class="modal fade" id="del-user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-md">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                  </div>
                  <div class="modal-body">
                    <p>Are you sure you want to delete the user?</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button"  class=" btn btn-danger pull-left" v-on:click="deleteUser()">Yes</button>
                    <button type="button" class="btn btn-success pull-right closemodal" data-dismiss="modal">No</button>
                  </div>
                </div>

              </div>
            </div>



    </div>

    <?php $routes = Route::current()->getName() ?>
    @if ( $routes== 'add-users'  )
        @include('layouts.dashboard.footer')

    @endif                                                                                                                                                                                                                                                                                                                                                                                                                 
</body>

@endsection

@section('content-scripts')

    <script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <!--Custom JavaScript -->

    <!-- fullcalendar -->    
    <script src="{{ asset('dashtemplate/fullcalendar/lib/moment.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/fullcalendar/lib/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/fullcalendar/scheduler.min.js') }}"></script>

    <!-- scripit init-->
    <script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('dashtemplate/js/lib/datatables/datatables-init.js') }}"></script>


    <script src="{{ asset('vue/dist/vue.js') }}"></script>
    <script src="{{ asset('vue/dist/vue.min.js') }}"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>


<script type="text/javascript">

    $(document).ready(function(){
        

        $('.closemodal').click(function(){
            // $("#edit-user").modal('hide');
            window.location.href = "{{url('admin/add-users')}}"; 
        });

      
    });




    Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

    new Vue({

        el: '#vueitems',

        data: {
            formErrors : {},
            
            userItem : {
                'name':'',
                'username':'',
                'email':'',
                'action': ''
            },
            returned : {

                'id':'',
                'name':'',
                'username':'',
                'email':'',
                'action': 'edituser'
            },

            deleId : {
                'id': ''
            }

   
        },


        methods : {
            createUser: function(){
                var input = this.userItem;
                this.$http.post('{{ route('users') }}',input).then((response) => {
                    window.location.href = "{{url('admin/add-users')}}"; 
                }, (response) => {
                    this.formErrors = response.data;
                });
            },

            getid: function(id){
                this.$http.post('{{ route('users') }}',{'id':id, 'action':'getid'}).then((response) => {
                    var toparse = JSON.parse(response.data);
                        this.returned.id = toparse.id;
                        this.returned.name = toparse.name;
                        this.returned.email = toparse.email;
                        this.returned.username = toparse.username;
                    $("#edit-user").modal('show');
                });
            },

            updateUser: function(){
                var input = this.returned;
                this.$http.post('{{ route('users') }}',input).then((response) => {
                    window.location.href = "{{url('admin/add-users')}}";
                }, (response) => {
                    this.formErrors = response.data;
                });
            },
            openmodal: function(id){
                $("#del-user").modal('show');
                this.deleId.id = id;
            },  

            deleteUser: function(){
            
                this.$http.post('{{ route('users') }}',{'id':this.deleId, 'action':'deluser'}).then((response) => { 
                    window.location.href = "{{ url('admin/add-users') }}"; 
                });

            }

 



          
        }






    });

</script>
@endsection