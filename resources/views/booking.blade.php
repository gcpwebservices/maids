
@extends('layouts.app')


@section('content-styles')


    <link href="{{asset('css/styles.css')}}" rel="stylesheet" />
    <!-- CSS Files -->
    <link href="{{asset('booktemplate/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('booktemplate/css/paper-bootstrap-wizard.css')}}" rel="stylesheet" />

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{asset('booktemplate/css/demo.css')}}" rel="stylesheet" />

    <!-- Fonts and Icons -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>

    <link href="{{asset('booktemplate/css/themify-icons.css')}}" rel="stylesheet">

    <link href="{{asset('calendar/fullcalendar.css')}}" rel="stylesheet">
    <link href="{{asset('calendar/fullcalendar.print.css')}}" rel="stylesheet">

    <link href="{{asset('datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

@endsection

@section('content')
<body>  



<div class="container">
<div class="row">
    <div class="col-sm-9 ">

        <!--      Wizard container        -->
        <div class="wizard-container">
            <div class="card wizard-card" data-color="blue" id="wizard">
            <form action="" method="">
            <!--        You can switch " data-color="green" "  with one of the next bright colors: "blue", "azure", "orange", "red"       -->
            	<div class="wizard-header">
          <!--       	<h3 class="wizard-title">List your place</h3>
                	<p class="category">Tell Us About Your Place.</p> -->
            	</div>
				<div class="wizard-navigation">
					<div class="progress-with-circle">
					    <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4" style="width: 15%;"></div>
					</div>
					<ul>
                        <li id="locations">
							<a href="#location" data-toggle="tab">
								<div class="icon-circle">
									<i class="ti-map"></i>
								</div>
								Your Place
							</a>
						</li>
                        <li id="date">
							<a href="#type" data-toggle="tab">
								<div class="icon-circle">
									<i class="ti-direction-alt"></i>
								</div>
								Date & Time
							</a>
						</li>
                        <li id="review">
							<a href="#facilities" data-toggle="tab">
								<div class="icon-circle">
									<i class="ti-panel"></i>
								</div>
								Review
							</a>
						</li>
                        <li id="confirm">
							<a href="#description" data-toggle="tab">
								<div class="icon-circle">
									<i class="ti-comments"></i>
								</div>
								Confirm
							</a>
						</li>
                    </ul>
				</div>
                <div class="tab-content">
                    <div class="tab-pane" id="location">
                        	<div class="row">
                            	<div class="col-sm-12">
                                	<h2 class="info-text"> Tell Us About Your Place</h3>
                        		</div>
                        		<hr>
                            	<div class="col-sm-5 col-sm-offset-1">
                                	<div class="form-group">
                                    	<label>Street Address</label>
                                    	<input type="text" class="form-control" name="street" id="street" placeholder="Enter a location" >
                                	</div>
                            	</div>
                            	<div class="col-sm-5">
                    		      	<div class="form-group">
                                    	<label>Unit & Apt. Name</label>
                                    	<input type="text" class="form-control" name="unitname" id="unitname" placeholder="" >
                                	</div>

                            	</div>
                    
                        	</div>
   							<hr>
        	             	<div class="row">
 		                 		<div class="col-sm-12">
                                	<h3 class="info-text" id="forh3">Area to be cleaned</h3>
                        		</div>
            	        		<div class="col-sm-5 col-sm-offset-1">

                                	<div class="form-group">
                                    	<label>Bedrooms</label>
                                    	<select class="form-control" name="bedrooms" id="bedrooms">                          
                                         	<option value="0" selected="selected">0 </option>
                                            <option value="0">1 </option>
                                            <option value="1">2 </option>
                                            <option value="2">3 </option>
                                            <option value="3">4 </option>
                                            <option value="4">5+ </option>
                                    	</select>
                                	</div>
                            	</div>
                	     		<div class=" col-sm-5 ">
                                		<div class="form-group">
                                        	<label>Bathrooms</label>
                                        	<select class="form-control" name="bathrooms" id="bathrooms">                                     
	                                            <option value="0" selected="selected">1 </option>
	                                            <option value="1">2 </option>
	                                            <option value="2">3 </option>
	                                            <option value="3">4 </option>
	                                            <option value="4">5+ </option>
	                            	
                                    	</select>
                                	</div>
                            	</div>
                            	<div class="col-sm-4 col-sm-offset-1">
                    				<p>
                    					<a data-target="#cleaningProductsModal" data-toggle="modal" href="#">Included in every cleaning 
                						<i class="fa fa-question-circle"></i></a>
                    				</p>
                            	</div>
                            
							</div>
							<hr>
							<div class="row">
 		                 		<div class="col-sm-12">
                                	<h3 class="info-text" id="forh3">What other tasks should we do?</h3>
                        		</div>
             					<div class="col-sm-12">
                                    <div class="col-sm-2">
										<div class="choice" id="task" data-toggle="wizard-checkbox" >
                                            <input type="checkbox" name="task" id="task1" data-id="1.5" value="Laundry + Ironing" >
                                            <div class="card card-checkboxes card-hover-effect" style="height: 130px">
                                        	<label id="forlabel">Laundry + Ironing</label>
	                                			<div id="fori">
                                					<i class="ti-package"></i>
	                                			</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
										<div class="choice" id="task" data-toggle="wizard-checkbox">
                                            <input type="checkbox" name="task" id="task" data-id="0.5" value="Inside Fridge">
                                            <div class="card card-checkboxes card-hover-effect" style="height: 130px">
                                        	<label id="forlabel">Inside Fridge</label>                                
	                       		   	           	<div id="fori">
	                            					<i class="ti-package"></i>
	                                			</div>
                                            </div>
                                        </div>
                                    </div>
                                          <div class="col-sm-2">
										<div class="choice" id="task" data-toggle="wizard-checkbox">
                                            <input type="checkbox" name="task" id="task" data-id="0.5" value="Inside Oven">
                                            <div class="card card-checkboxes card-hover-effect" style="height: 130px">
                                    	  	<label id="forlabel">Inside Oven</label>
                                	  		 	<div id="fori">
	                            					<i class="ti-package"></i>
	                                			</div>
                                            </div>
                                        </div>
                                    </div>
                                          <div class="col-sm-2">
										<div class="choice" id="task" data-toggle="wizard-checkbox">
                                            <input type="checkbox" name="task" id="task" data-id="1" value="Inside Cabinets">
                                            <div class="card card-checkboxes card-hover-effect" style="height: 130px">
                               				<label id="forlabel">Inside Cabinets</label>
		 										<div id="fori">
	                            					<i class="ti-package"></i>
	                                			</div>
                                            </div>
                                        </div>
                                    </div>
                                          <div class="col-sm-2">
										<div class="choice" id="task" data-toggle="wizard-checkbox">
                                            <input type="checkbox" name="task" id="task" data-id="1" value="Interior Windows">
                                            <div class="card card-checkboxes card-hover-effect" style="height: 130px">
                                    	  	<label id="forlabel">Interior Windows</label>                    
                                            	<div id="fori">
	                            					<i class="ti-package"></i>
	                                			</div>
                                            </div>
                                        </div>
                                    </div>
                                  	<div class="col-sm-2">
										<div class="choice" id="task" data-toggle="wizard-checkbox">
                                            <input type="checkbox" name="task" id="walls" data-id="1" value="Interior Walls">
                                            <div class="card card-checkboxes card-hover-effect" style="height: 130px">
                                    	  	<label id="forlabel">Interior Walls</label>
                               					<div id="fori">
	                            					<i class="ti-package"></i>
	                                			</div>
                                            </div>
                                        </div>
                                    </div>
                         
                                </div>                        
							</div>
							<hr>
							<div class="row">
								<div class="col-sm-12">
                                	<h3 class="info-text" id="forh3">Hours to book cleaning</h3>
                        		</div>
            					<div class="col-sm-11 col-sm-offset-1">
                                	<div id="likep">We estimate <span id="pnewhours">4.0</span> hours of cleaning time. You can adjust the hours below.</div>  
                                </div>
								<div class="col-sm-5 col-sm-offset-1">
                                	<div class="form-group">
        							
                    					<select class="form-control" id="hours" >
                                    		<option value="3.0">3.0</option>
                                    		<option value="3.5">3.5</option>
                                    		<option id="newhours" selected="selected" value="4.0">4.0</option>
                                    		<option value="4.5">4.5</option>
                                    		<option value="5.0">5.0</option>
                                    		<option value="5.5">5.5</option>
                                    		<option value="6.0">6.0</option>
                                    		<option value="6.5">6.5</option>
                                    		<option value="7.0">7.0</option>
                                    		<option value="7.5">7.5</option>
                                    		<option value="8.0">8.0</option>
                                    		<option value="8.5">8.5</option>
                                    		<option value="9.0">9.0</option>
                                    		<option value="9.5">9.5</option>
                                    		<option value="10.0">10.0</option>
                                    		<option value="10.5">10.5</option>
                                    		<option value="11.0">11.0</option>
                                    		<option value="11.5">11.5</option>
                                    		<option value="12.0">12.0</option>
                                    		<option value="12.5">12.5</option>
                                    		<option value="13.0">13.0</option>
                                    		<option value="13.5">13.5</option>
                                    		<option value="14.0">14.0</option>
                                    		<option value="14.5">14.5</option>
                                    		<option value="15.0">15.0</option>
                                    		<option value="15.5">15.5</option>
                                    		<option value="16.0">16.0</option>
                                    		<option value="16.5">16.5</option>
                                    	</select>
                                	</div>
                                	<p>3.0 hours minimum</p>
                            	</div>
                            	<div class="alert alert-warning warning-message col-sm-10 col-sm-offset-1" id="hours-warning-message">
                            		<span class="alert-link">Please note that you've selected less hours than we estimate it will take to clean your place. We recommend prioritising areas to be cleaned during this time.</span>
                            	</div>
                        	</div>
                        	<hr>
                    		<div class="row">
                    			<div class="col-sm-12">
                                	<h3 class="info-text" id="forh3">Cleaning Products	
                                		<a data-target="#cleaningProductsModal1" data-toggle="modal" href="#">
                							<i class="fa fa-question-circle"></i>
                						</a>
                					</h3>
                        		</div>

                    		  	<div class="col-sm-5 col-sm-offset-1">
        					      	<div class="form-group">	
			            				<div class="checkbox checkbox-primary">
					                        <input id="checkbox1" type="checkbox" >
					                        <label for="checkbox1">Provide cleaning products</label>
					                    </div>
	                            	</div>
                            	</div>

                        	  	<div class="col-sm-5 ">
        					      	<div class="form-group">	
		            					<div class="checkbox checkbox-primary">
					                        <input id="checkbox2" type="checkbox" >
					                        <label for="checkbox2">I'll use my own cleaning products</label>
					                    </div>
	                            	</div>
                            	</div>
        					</div>

		                    <hr>
                    		<div class="row">
                    			<div class="col-sm-12">
                                	<h3 class="info-text" id="forh3">Email Address</h3>
                        		</div>
   								<div class="col-sm-8 col-sm-offset-2">
                                	<div class="form-group">
                             
                                    	<input type="text" class="form-control" name="emailaddress" id="emailaddress" placeholder="Enter Email Address" >
                                	</div>
                            	</div>
        					</div>
        					<div id="footerbuttons">
	        					<div class="pull-right">
		                            <input type='button' id="nextlocation" name='nextlocation' class='btn btn-next btn-fill btn-success btn-wd' value="Next"/>
		                            <input type='button' id="finishlocation" name='finishlocation' class='btn btn-finish btn-fill btn-success btn-wd' value="Finish" /> 
								</div>
								<div class="pull-left">
								    <input type='button' id="previouslocation" name='previouslocation' class='btn btn-previous btn-default btn-wd' value="Previous"/>
							    </div>
							</div>
                    </div><!--first-tab-->
                    <div class="tab-pane" id="type"><!--second-tab-->
        	             	<div class="row">
    	             			<div class="col-sm-12">
                                	<h2 class="info-text" >Choose a Date & Time</h3>
                        		</div>
								<hr>
             					<div class="col-sm-12 col-sm-offset-1">
                                    <div class="col-sm-2">
										<div class="choice" data-toggle="wizard-checkbox">
                                            <input type="checkbox" id="onetime" name="frequency" value="One Time" placeholder="Click to select a day">
                                            <div class="card card-checkboxes card-hover-effect" style="height: 120px">
                                        	<label id="forlabel">One Time</label>
	                                			<div id="fori">
                           							<i id="addonetime" class="ti-check"></i>
	                                			</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
										<div class="choice" data-toggle="wizard-checkbox">
                                            <input type="checkbox" id="week1" name="frequency" value="Weekly">
                                           	<div class="card card-checkboxes card-hover-effect" style="height: 120px">
                                        	<label id="forlabel">Weekly</label>                                
	                       		   	           	<div id="fori">
	                            					<i id="addweek1" class="ti-check"></i>
	                                			</div>
                                            </div>
                                        </div>
                                    </div>
                                          <div class="col-sm-2">
										<div class="choice" data-toggle="wizard-checkbox">
                                            <input type="checkbox" id="week2" name="frequency" value="Every 2nd Week">
                                            <div class="card card-checkboxes card-hover-effect" style="height: 120px">
                                    	  	<label id="forlabel">Every 2nd Week</label>
                                	  		 	<div id="fori">
                            						<i id="addweek2" class="ti-check"></i>
	                                			</div>
                                            </div>
                                        </div>
                                    </div>
                                          <div class="col-sm-2">
										<div class="choice" data-toggle="wizard-checkbox">
                                            <input type="checkbox" id="week3" name="frequency" value="Every 3rd Week">
                                            <div class="card card-checkboxes card-hover-effect" style="height: 120px">
                               				<label id="forlabel">Every 3rd Week</label>
		 										<div id="fori">
	                            					<i id="addweek3" class="ti-check"></i>
	                                			</div>
                                            </div>
                                        </div>
                                    </div>
                                          <div class="col-sm-2">
										<div class="choice" data-toggle="wizard-checkbox">
                                            <input type="checkbox" id="week4" name="frequency" value="Every 4th Week">
                                            <div class="card card-checkboxes card-hover-effect" style="height: 120px">
                                    	  	<label id="forlabel">Every 4th Week</label>  

                                            	<div id="fori">
	                            					<i id="addweek4" class="ti-check"></i>
	                                			</div>
                                            </div>
                                        </div>
                                    </div>
                      
                 
                                </div>                        
							</div>
							<hr>
							<div class="row">
                            <h5 class="info-text" id="forh3">What other tasks should we do? </h5>
                            <div class="row">
                                <div class="col-sm-12 col-sm-offset-1">
                            	   	<div class="col-sm-5">
							      		<div class="form-group">
						      				<label id="forlabel">Select Day</label>
							                <div class='input-group date'>
							                	
					                		    <input type='text' name="calendardate" class="form-control" id="calendardate" value=""/>
						                    	<span class="input-group-addon">
							                        <span class="glyphicon glyphicon-calendar"> 
							                        	<i class="fa fa-calendar"></i>
							                        </span>
					                    		</span>
						
							                </div>
							            </div>
					             	</div>
				             	   	<div class="col-sm-5">
							      		<div class="form-group">
						      				<label id="forlabel">Select Time</label>
							                <div class='input-group date' id='timeclick' style="z-index: 1000;">
							                    <input type='text' name="timepicker" class="form-control time ui-timepicker-input" id="timepicker" />
							                    <span class="input-group-addon">
							                        <span class="glyphicon glyphicon-calendar"> <i class="fa fa-clock-o"></i></span>
							                    </span>
							                </div>
                        
							            </div>
					             	</div>
                                </div>
                            </div>
                            </div>
                        	<div id="footerbuttons">
	                    		<div class="pull-right">
		                            <input type='button' id="nextdate" name='nextdate' class='btn btn-next btn-fill btn-success btn-wd' value="Next"/>
		                            <input type='button' id="finishdate" name='finishdate' class='btn btn-finish btn-fill btn-success btn-wd' value="Finish"/> 
								</div>
								<div class="pull-left">
								    <input type='button' id="previousdate" name='previousdate' class='btn btn-previous btn-default btn-wd' value="Previous"/>
							    </div>
						    </div>
                    </div><!-- second tab -->
                    <div class="tab-pane" id="facilities"><!-- third tab -->
                    		<div class="col-sm-12">
                            	<h2 class="info-text" >Review Your Booking</h3>
                    		</div>
                    		<hr>
                            <div class="row">
         						<div class="col-sm-12">
                            	   	<div class="col-sm-12">
            	   		               	<h5 class="info-text">
            	   		               		<p>Your appointment will be scheduled for 
                                            <span id="showdate"></span> 
                                            at <span id="showtime"></span>. 
                                    Review the details below before proceeding to booking confirmation</p>
            	   		               	</h5>
					             	</div>
	             					<hr>
             					  	<h5 class="info-text" id="forh3">Cleaning Address </h5>
				             	   	<div class="col-sm-6 col-sm-offset-1">
											<p>Rooms:</p>
											<p>Address:</p>
								<!-- 			<p>City:</p> -->
											<p>Frequency:</p>
					             	</div>
			             		   	<div class="col-sm-5">
											<p><span id="showbed"></span> Bedroom, <span id="showbat"></span> Bathroom</p>
											<p><span id="showaddress"></span></p>
						<!-- 					<p><span id="showcity"></span></p> -->
											<p><span id="showfreq"></span></p>
					             	</div>
					             	<hr>

					             	<h5 class="info-text" id="forh3">Extra Tasks </h5>
			             	   	   	<div class="col-sm-6 col-sm-offset-1">
											<p>Task:</p>		
					             	</div>
			             		   	<div id="tasktoclean" class="col-sm-5">
															
					             	</div>
					             	<hr>

				             	 	<h5 class="info-text" id="forh3">Contact Details </h5>
			             	   	   	<div class="col-sm-6 col-sm-offset-1">
											<p>Email Address:</p>		
					             	</div>
			             		   	<div class="col-sm-5">
											<p id="showemail"></p>							
					             	</div>
    	                    	</div>
                             
                            </div>
                        	<div id="footerbuttons">
	                    		<div class="pull-right">
		                            <input type='button' id="nextreview" name='nextreview' class='btn btn-next btn-fill btn-success btn-wd' value="Next"/>
		                            <input type='button' id="finishreview" name='finishreview' class='btn btn-finish btn-fill btn-success btn-wd' value="Finish"/> 
								</div>
								<div class="pull-left">
								    <input type='button' id="previousreview" name='previousreview' class='btn btn-previous btn-default btn-wd' value="Previous"/>
							    </div>
						    </div>
                        </div><!-- third tab -->
                        <div class="tab-pane" id="description"><!-- fourth tab -->
                        	<div class="col-sm-12">
                            	<h2 class="info-text" >Confirm Your Booking</h3>
                    		</div>
                    		<hr> 
                            <div class="row">
                                <div class="col-sm-5 col-sm-offset-1">
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input type="text" class="form-control" name="fname" id="fname"  >
                                        </div>
                                </div>
                                <div class="col-sm-5">
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input type="text" class="form-control" name="lname" id="lname" >
                                        </div>

                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-4 col-sm-offset-1">
                                    <div class="form-group">
                                        <label>Mobile Number</label>
                                        <input type="text" class="form-control" name="mobilenumber" id="mobilenumber"  >
                                        <p class="help-block" id="hint_id_contact">Numbers only, e.g. 27821237777 or 0821237777</p>
                                    </div>
                                </div>
                            </div>
                
                      
                            <hr>

                       <div class="row">    <!-- CREDIT CARD FORM STARTS HERE -->
                        <!-- You can make it whatever width you want. I'm making it full width
                             on <= small devices and 4/12 page width on >= medium devices -->
                            <div class="col-sm-10 col-sm-offset-1">        
                            
                            
                                <div class="panel panel-default credit-card-box">
                                    <div class="panel-heading display-table" >
                                        <div class="row display-tr" >
                                                <div class="col-sm-12">
                                                    <h3 class="panel-title display-td" >Payment Details</h3>
                                                </div>
                                            <div class="display-td" >                            
                                                <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                                            </div>
                                        </div>                    
                                    </div>
                                    <div class="panel-body">
                                        <form role="form" id="payment-form" method="POST" action="javascript:void(0);">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label for="cardNumber">CARD NUMBER</label>
                                                        <div class="input-group">
                                                            <input 
                                                                type="tel"
                                                                class="form-control"
                                                                name="cardNumber"
                                                                placeholder="Valid Card Number"
                                                                autocomplete="cc-number"
                                                                required autofocus 
                                                            />
                                                            <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                                        </div>
                                                    </div>                            
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-7 col-md-7">
                                                    <div class="form-group">
                                                        <label for="cardExpiry"><span class="hidden-xs">EXPIRATION</span><span class="visible-xs-inline">EXP</span> DATE</label>
                                                        <input 
                                                            type="tel" 
                                                            class="form-control" 
                                                            name="cardExpiry"
                                                            placeholder="MM / YY"
                                                            autocomplete="cc-exp"
                                                            required 
                                                        />
                                                    </div>
                                                </div>
                                                <div class="col-xs-5 col-md-5 pull-right">
                                                    <div class="form-group">
                                                        <label for="cardCVC">CV CODE</label>
                                                        <input 
                                                            type="tel" 
                                                            class="form-control"
                                                            name="cardCVC"
                                                            placeholder="CVC"
                                                            autocomplete="cc-csc"
                                                            required
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label for="couponCode">COUPON CODE</label>
                                                        <input type="text" class="form-control" name="couponCode" />
                                                    </div>
                                                </div>                        
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <button class="subscribe btn btn-success btn-lg btn-block" type="button">Start Subscription</button>
                                                </div>
                                            </div>
                                            <div class="row" style="display:none;">
                                                <div class="col-xs-12">
                                                    <p class="payment-errors"></p>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>            
                      
                                
                                
                            </div>            
                        
                        </div>   <!-- CREDIT CARD FORM ENDS HERE -->


            	<div id="footerbuttons">
            		<div class="pull-right">
                        <input type='button' id="nextconfirm" name='nextconfirm' class='btn btn-next btn-fill btn-success btn-wd' value="Next"/>
                        <input type='button' id="finishconfirm" name='finishconfirm' class='btn btn-finish btn-fill btn-success btn-wd' value="Finish"/> 
					</div>
					<div class="pull-left">
					    <input type='button' id="previousconfirm" name='previousconfirm' class='btn btn-previous btn-default btn-wd' value="Previous"/>
				    </div>
			    </div>

                    	</div><!-- fourth tab -->
                </div>
                    <div class="wizard-footer">
                    	<div class="pull-right">
                           <!--  <input type='button' id="next" class='btn btn-next btn-fill btn-success btn-wd' name='next' value='Next' />
                            <input type='button' class='btn btn-finish btn-fill btn-success btn-wd' name='finish' value='Finish' /> -->
						</div>

                        <div class="pull-left">
                        <!--     <input type='button' class='btn btn-previous btn-default btn-wd' name='previous' value='Previous' /> -->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div> <!-- wizard container -->
    </div>
   	<div class="col-sm-3">
	 	<div class="wizard-container">
	        <div class="card wizard-card" data-color="blue" id="wizard">
	        	<div class="row">
                    <div class="col-sm-5 col-sm-offset-1">
                	  	<h3 class="info-text">Summary</h3>
    	   			</div>
    	   			<div class="col-sm-10 col-sm-offset-1">
					
    	   				<div>

    	   					<span id="value" data-id="">4</span> Hour Cleaning 	
    	   					<a data-target="#hourCleaningModalLabel" data-toggle="modal" href="#">
    							<i class="fa fa-question-circle"></i>
    						</a>
    					</div>
    	   			</div>
	   			</div>	
                <hr>
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        All our Cleaners are experienced, insured, have been fully vetted and are rated by clients.
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        If you are not 100% happy with your clean, we will send out someone else at no additional cost.
                    </div>
                </div>


	 	     



			</div>
		</div>
   </div>


</div> <!-- row -->
</div>

<input type='hidden' id="time" data-id=""/>
<input type='hidden' id="date" data-id=""/>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Check Availability</h4>
      </div>
      <div class="modal-body">
     	<div id="calendar"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="modalclose" data-dismiss="modal">Close</button>
  
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="hourCleaningModalLabel" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Hourly rate</h4>
        </div>
        <div class="modal-body">
          <p>Your hourly rate is calculated based on your location and day of the week.</p>
        </div>
   
      </div>
      
    </div>
  </div>


  <div class="modal fade" id="cleaningProductsModal" tabindex="-1" role="dialog" aria-labelledby="cleaningProductsModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Included in every cleaning</h4>
      </div>
      <div class="modal-body">
      		<h3 class="zero-margin-top">Living room</h3><p class="lead">General clean of living room and other living areas includes:</p><ul class="list-wide"><li><i class="fa fa-check"></i> Dusting of furniture and surfaces</li><li><i class="fa fa-check"></i> Mopping and vacuuming of floors</li><li><i class="fa fa-check"></i> Dusting and wiping of skirtings</li><li><i class="fa fa-check"></i> Dusting and wiping of electronics, pictures frames etc.</li><li><i class="fa fa-check"></i> Dusting and wiping of light switches and other fixtures</li></ul><h3>Kitchen</h3><p class="lead">General clean of kitchen includes:</p><ul class="list-wide"><li><i class="fa fa-check"></i> Wiping of surfaces, sinks and appliances</li><li><i class="fa fa-check"></i> Washing of dishes</li><li><i class="fa fa-check"></i> Wiping outside of cupboards and fridge</li><li><i class="fa fa-check"></i> Wiping of walls</li><li><i class="fa fa-check"></i> Emptying bins and cleaning bin area</li><li><i class="fa fa-check"></i> Mopping floors</li></ul><h3>Bedrooms</h3><p class="lead">General clean of bedrooms includes:</p><ul class="list-wide"><li><i class="fa fa-check"></i> Dusting of furniture and surfaces</li><li><i class="fa fa-check"></i> Making bed</li><li><i class="fa fa-check"></i> Vacuuming and/or mopping floors</li><li><i class="fa fa-check"></i> Dusting and wiping of skirtings</li><li><i class="fa fa-check"></i> Folding or hanging of clothes in bedroom</li></ul><h3>Bathrooms</h3><p class="lead">General clean of bathroom includes:</p><ul class="list-wide"><li><i class="fa fa-check"></i> Cleaning of shower, bath and sinks</li><li><i class="fa fa-check"></i> Toilet clean</li><li><i class="fa fa-check"></i> Wiping of counters and taps</li><li><i class="fa fa-check"></i> Wiping of walls and mirrors</li><li><i class="fa fa-check"></i> Wiping outside of cupboards and cabinets</li><li><i class="fa fa-check"></i> Folding or hanging of clean towels</li><li><i class="fa fa-check"></i> Emptying bins and cleaning bin area</li><li><i class="fa fa-check"></i> Mopping floors</li></ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="modalclose" data-dismiss="modal">OK</button>
     
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="cleaningProductsModal1" tabindex="-1" role="dialog" aria-labelledby="cleaningProductsModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Included in every cleaning</h4>
      </div>
      <div class="modal-body">
      		<p>If you opt for using our cleaning products, your cleaning representative will bring a set of general cleaning products:</p><ul class="list-wide"><li><i class="fa fa-check"></i> General all-purpose cleaner</li><li><i class="fa fa-check"></i> A window cleaner (for windows and mirrors)</li><li><i class="fa fa-check"></i> A bathroom cleaner</li><li><i class="fa fa-check"></i> Dishwashing liquid</li></ul><p>We kindly request that you provide any products which may be specific to your home.</p><p><i class="fa fa-arrow-right"></i> Please note our cleaning representative do not bring equipment (e.g. mops, brooms, vacuum cleaners).</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="modalclose" data-dismiss="modal">OK</button>
     
      </div>
    </div>
  </div>
</div>

</body>
@endsection

@section('content-scripts')

    <!--   Core JS Files   -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js" type="text/javascript"></script>
    <script src="{{asset('booktemplate/js/jquery-2.2.4.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('booktemplate/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('booktemplate/js/jquery.bootstrap.wizard.js')}}" type="text/javascript"></script>

    <!--  Plugin for the Wizard -->
    <script src="{{asset('booktemplate/js/paper-bootstrap-wizard.js')}}" type="text/javascript"></script>

    <!--  More information about jquery.validate here: http://jqueryvalidation.org/  -->
    <script src="{{asset('booktemplate/js/jquery.validate.min.js')}}" type="text/javascript"></script>

    <!--   calendar   -->
    <script src="{{asset('calendar/fullcalendar.js')}}"></script>

    <!--   datetimepicker   -->
    <script src="{{asset('datetimepicker/js/bootstrap-material-datetimepicker.js')}}" type="text/javascript"></script>
    <!--   timepicker   -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

    <!--   creditcard   -->
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/3.0.0/jquery.payment.js" type="text/javascript"></script>
    <script src="{{asset('js/creditcard.js')}}" type="text/javascript"></script>

    <script type="text/javascript">

$(document).ready(function(){


    var storedFunc = (function() {

        var task = 0;
        var bnt = 0;
        var add = 3;
        var newtotal = 0;

        var time = '';
        var date = '';

        var bed = 0;
        var bat = 0;
        var address = '';
        var city = '';
        var freq = '';

        return {

            callFunc: function() {

                var wototal = bnt+task;
                var total = bnt+task+add;
                newtotal = parseFloat(total, 10);
                $('#value').html(newtotal);
                $('#value').data('id', newtotal);
                $('#hours').append($("<option selected='selected' ></option>").attr("value",newtotal).text(newtotal));
                $('#pnewhours').html(newtotal);
                $('#time').data('id',time);
                $('#date').data('id',date);

                $('#showtime').html(time);
                $('#showdate').html(date);

                $('#showbed').html(bed);
                $('#showbat').html(bat);
                $('#showcity').html(time);
            },

            valueTask: function(newVar) {
                task = newVar;
            },

            valueBnt: function(newVar) {
                bnt = newVar;
            },

             valueTime: function(newVar) {
                time = newVar;
            },

            valueDate: function(newVar) {
                date = newVar;
            },

            valueBed: function(newVar) {
                bed = newVar;
            },

            valueBat: function(newVar) {
                bat = newVar;
            },

        };
    })();



    //for stepper
    // $('#locations, #date, #review, #confirm').click(function(){return false;});
    // $( "#nextdate, #nextlocation" ).prop( "disabled", true );

    // $('input[name=emailaddress]').change(function() {
    //  var email = $('input[name=emailaddress]').val();
    //  if(email !=''){
    //      $( "#nextlocation" ).prop( "disabled", false );
    //      $('#locations').click(function(){return true;});
    //  }
    // });

    $('input[name=timepicker]').change(function() {
        var time = $('input[name=timepicker]').val();
            if(time !=''){
            $( "#nextdate" ).prop( "disabled", false );
            $('#date').click(function(){return true;});
        }
    });

    //for working hours 
    $('#hours-warning-message').hide();
        $("#hours").change(function(){
            var hours = this.value;
        if(hours=="3.0" || hours=='3.5'){
            $("#hours-warning-message").show();
        }else{
            $("#hours-warning-message").hide()
        }
    });     

    //for bedrooms and bathrooms
    $("#bedrooms,#bathrooms ").on('change',function() {
        var taskandmanual  = $('#value').data("id");
        var totalbed = 0;
        var totaltoilet = 0;

      $("#bedrooms").each(function() {
          totalbed += parseFloat((this.value), 10)||0;
          storedFunc.valueBed(totalbed);
      });
        $("#bathrooms").each(function() {
          totaltoilet += parseFloat((this.value), 10)||0;
          storedFunc.valueBat(totaltoilet);
      });

        var totalbnt = totalbed+totaltoilet;
        storedFunc.valueBnt(totalbnt);
        storedFunc.callFunc();

    });

    //for task
    $('.choice').on('click',function(){
        var sum = 0;
            $('input[name=task]:checked').each(function(e){
                // alert($(this).data('id'));
                sum += parseFloat($(this).data('id'), 10);
                $('#tasktoclean').append("<p>"+ $(this).val() +"</p>");
            });

        //for task show
        var seen = {};
        $('#tasktoclean > p').each(function() {
            var txt = $(this).text();
            if (seen[txt])
                $(this).remove();
            else
                seen[txt] = true;
        });


        var forfreq = 0;
        $('input[name=frequency]:checked').each(function(){
            forfreq = $(this).val();
            $('#showfreq').html(forfreq);
        });

        storedFunc.valueTask(sum);
        storedFunc.callFunc();

    });


    // for manual hours
    $("#hours ").change(function() {
        var totalmanual = 0;
            totalmanual += parseFloat($(this).val(), 10)||0;
            $("#value").html(totalmanual);
            $('#pnewhours').html(totalmanual);
    });

    //for calendar
    $('#calendardate').click(function(){
        $('#myModal').modal('show'); 
    });

    $('#calendar').fullCalendar({
       dayClick: function (date, allDay, jsEvent, view) {
        $(".fc-state-highlight").removeClass("fc-state-highlight");
        $(jsEvent.target).addClass("fc-state-highlight");
            var dateObject = (date.getDate()+"-"+date.getMonth()+"-"+date.getFullYear());
             // $(this).css('background-color', 'red');
            storedFunc.valueDate(dateObject);
            storedFunc.callFunc();

       }
    });

    //fortimepicker
    $('#timepicker').timepicker({
        change: function(time) {
            var getValue = $(this).val();
            storedFunc.valueTime(getValue);
            storedFunc.callFunc();
        },

        timeFormat: 'h:mm p',
        interval: 60,
        minTime: '24',
        maxTime: '7:00pm',
        defaultTime: '07',
        startTime: new Date(),
        dynamic: true,
        dropdown: true,
        scrollbar: false
    });

    //for mobile number input
    $('input[name="mobilenumber"]').on("keypress keyup blur",function (event) {    
        $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
    });


    //for showdata
    $('#nextlocation, #previousdate').click(function(){
        var showaddress = $('#street').val();
            $('#showaddress').html(showaddress);
        var email = $('#emailaddress').val();
            $('#showemail').html(email);
    });
    $('#modalclose').click(function(){
        var showdate = $('#date').data('id');
            document.getElementById('calendardate').value = showdate;
    });

    //for weeks
    // $('#nextlocation').click(function(){
       //  if($('#type').hasClass('active')){

          //   var count = 0;
          //   $('.choice').click(function(){
                // $(this).each(function() {
                //  if($(this).hasClass('active')){
                //      if(count > 0){
                //          $('.choice').removeClass('active');
                //          count = 0;
                //      }else{
                //          count++;
                //      }
                //  }
                // });
          //   });
       //  }

    // });

 //    $('#previousdate').click(function(){
    //  $('#type').removeClass('active');
    // });
    
    // $('.choice').click(function(){
    //  alert($('#task1').val());
    // });


});





</script>


@endsection