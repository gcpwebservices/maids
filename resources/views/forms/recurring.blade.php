     <div class="row card" id="appoint2">
        <div class="col-md-12">
            <div class="hpanel">
                <div class="panel-body">
                    
                    <div class="row ">
                        <div class="col-md-8">
                            <label class="text-center m-t-15">Appointment Information</label>
                            <div class="row">
                                <div class="col-md-12" id="schedtimedate">

                                    <div class="row p-20">
                                        <div class="col-md-12">

                                            <div class="row">
                                                <div class="col-md-4">


                                                    <div class="form-group">
                                                        <label for="datetimes">Reccurance:</label>
                                                        <select class="form-control" name="recur" id="recur" style="width: 220px;" required="required">
                                                            <option value="">Select</option>
                                                            <option value="day">Daily</option>
                                                            <option value="week">Weekly</option>
                                                            <option value="month">Montly</option>
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="datetimes">Date:</label>
                                                        <input type="text" class="form-control" name="datetimes_2" size="16" id="datetimes_2" 
                                                        autocomplete="off" style="height:42px; border-radius: 3px; border-color: #aaa;" 
                                                        required="required"/>
                                                        <input type="hidden" class="form-control" name="schedule_start_2" id="schedule_start_2"/>
                                                        <input type="hidden" class="form-control" name="schedule_end_2" id="schedule_end_2"/>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="timefrom">Time:</label>
                                                        <div class="input-group bootstrap-timepicker timepicker">
                                                            <input type="text" name="timefrom_2" id="timefrom_2" class="form-control input-small" 
                                                            autocomplete="off" placeholder="Start" style="border-radius: 3px; border-color: #aaa;" required="required">
                                                            &ensp;
                                                            <input type="text" name="timeto_2" id="timeto_2" class="form-control input-small" 
                                                            autocomplete="off" placeholder="End" style="border-radius: 3px; border-color: #aaa;">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="service">Service:</label>
                                                        <select class="form-control" name="service_2" id="service_2" style="width: 100%;">
                                                                <option value="">Select Service</option>
                                                                <option value="1">Cleaning</option>
                                                        </select>
                                                    </div> 
                                                </div>

                                                <div class="col-md-8">
                                                    <input type="hidden" name="maids_id_2" id="maids_id_2">
                                                    <div class="form-group">
                                                        <label for="employee">Maid: <span  class="text-primary" id="availmaids_2"></span></label>
                                                        <select class="form-control" name="employee_2" id="employee_2" style="width: 100%;" 
                                                        required="required">
                                                            <option value="select">Select Maid</option>
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="employee">Driver: </label>
                                                        <select class="form-control" name="driver_id_2" id="driver_id_2" style="width: 100%;" 
                                                        required="required">
                                                            <option value="">Select Driver</option>
                                                            @foreach($dlist as $list)
                                                                <option value="{{ $list->id }}">{{ $list->fullname }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="employee">Arival Window: </label>
                                                        <select class="form-control" name="interval_2" id="interval_2" style="width: 100%;" 
                                                        required="required">
                                                            <option value="0">Select Interval</option>
                                                            <option value="10">10 mins</option>
                                                            <option value="20">20 mins</option>
                                                            <option value="30">30 mins</option>
                                                            <option value="40">40 mins</option>
                                                            <option value="50">50 mins</option>
                                                            <option value="60">60 mins</option>
                                                            <option value="70">1 hour and 10 mins</option>
                                                            <option value="80">1 hour and 20 mins</option>
                                                            <option value="90">1 hour and 30 mins</option>
                                                            <option value="100">1 hour and 40 mins</option>
                                                            <option value="110">1 hour and 50 mins</option>
                                                            <option value="120">2 hours</option>
                                                
                                                        </select>
                                                    </div>

                                                </div>    
                                    

                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="comment">Appointment Note:</label>
                                                        <textarea class="form-control" rows="5" id="comment_2" 
                                                      style="border-radius: 3px; border-color: #aaa;"></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                       
                                    </div>
                                     


                                </div>


                            </div>


                        </div>
                        <div class="col-md-4">
                            <div class="row ">
                                <div class="col-md-12">
                     
                                    <div class="form-group">
                                        <input type="hidden" name="to_pay_2" id="to_pay_2">
                                        <label class="text-center m-t-15">Payment Information</label>
                                        <input id="to_pay_view_2" readonly="" style="font-size:36px;height:60px" value="0.00" type="text" class="form-control input-lg text-center autonumeric font-bold">
                                    </div>
                                    <div class="form-group">
                                        <label>Payment Note:</label>
                                        <textarea name="payment_note_2" class="form-control input-lg" rows="4" data-parsley-id="5700"></textarea><ul class="parsley-errors-list" id="parsley-id-5700"></ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="button" class="save-unpaid btn btn-primary font-bold" style="width:100%">Save Unpaid</button>
                                    </div>
                                </div>
              
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>       
    </div>
    