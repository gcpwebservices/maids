@extends('layouts.app')
@section('content-styles')

    <!-- Custom CSS -->

    <link href="{{ asset('dashtemplate/css/lib/chartist/chartist.min.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/lib/calendar2/semantic.ui.min.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/lib/calendar2/pignose.calendar.min.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/lib/owl.carousel.min.css') }}" rel="stylesheet" />

    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />

    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">

@endsection
@section('content')
<div id="appOne"></div>
<div id="dashboard">
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" style="margin: 65px 0;">
               <div class="text-center" style="margin-bottom:30px;"><img src="{{ asset('img/logoblue.png')}}" alt="" class="dark-logo" 
                width="300"></div>
                <div class="card-header">{{ __('Change Password') }}</div>

                <div style="margin-top: 15px;">
                @if(session('success'))

                <div id="successMessage" class="alert alert-{{session('success')}}" style="background-color: #dff0d8 !important;color: #3c763d!important; border-color: #d6e9c6 !important;  ">

                {{session('success')}}

                </div>

                @endif


                @if(session('error'))

                <div id="successMessage" class="alert alert-{{session('error')}}" style="background-color: #f44336 !important;color: white!important; border-color: #f44336 !important;">

                {{session('error')}}

                </div>

                @endif
                </div>

                <div class="card-body" style="margin-top: 15px;">
                    <form method="POST" action="{{ route('changePassword') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="passwordold" class="col-md-4 col-form-label text-md-right">{{ __('Old Password') }}</label>

                            <div class="col-md-6">
                                <input id="passwordold" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="passwordold" required>

                                @if ($errors->has('passwordold'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('passwordold') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('New Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                               <a href="{{  url('/') }}" class="btn btn-primary"> Back</a>
                            </div>
                        </div>


                    </form>


                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('content-scripts')

    <script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>

    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>



    <!-- Bootstrap tether Core JavaScript -->

    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- slimscrollbar scrollbar JavaScript -->

    <script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>

    <!--Menu sidebar -->

    <script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>

    <!--stickey kit -->

    <script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>

    <!--Custom JavaScript -->

    <script src="{{ asset('dashtemplate/js/lib/morris-chart/morris.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/morris-chart/raphael-min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/calendar-2/moment.latest.min.js') }}"></script>

    <!-- scripit init-->

    <script src="{{ asset('dashtemplate/js/lib/calendar-2/semantic.ui.min.js') }}"></script>

    <!-- scripit init-->

    <script src="{{ asset('dashtemplate/js/lib/calendar-2/prism.min.js') }}"></script>

    <!-- scripit init-->

    <script src="{{ asset('dashtemplate/js/lib/calendar-2/pignose.calendar.min.js') }}"></script>

    <!-- scripit init-->

    <script src="{{ asset('dashtemplate/js/lib/calendar-2/pignose.init.js') }}"></script>



    <script src="{{ asset('dashtemplate/js/lib/owl-carousel/owl.carousel.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/owl-carousel/owl.carousel-init.js') }}"></script>



    <!-- scripit init-->



@endsection

