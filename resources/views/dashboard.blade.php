

@extends('layouts.app')



@section('content-styles')

    <!-- Custom CSS -->

    <link href="{{ asset('dashtemplate/css/lib/chartist/chartist.min.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/lib/calendar2/semantic.ui.min.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/lib/calendar2/pignose.calendar.min.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/lib/owl.carousel.min.css') }}" rel="stylesheet" />

    <link href="{{ asset('dashtemplate/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />



    <link href="{{asset('dashtemplate/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/helper.css') }}" rel="stylesheet">

    <link href="{{ asset('dashtemplate/css/style.css') }}" rel="stylesheet">

    

@endsection







@section('content')

<body class="fix-header fix-sidebar">



    <?php $routes = Route::current()->getName() ?>

    @if ( $routes== 'dashboard' || $routes== 'maids-schedule' )

        @include('layouts.dashboard.header')

    @endif





    <div class="container-fluid">



        <div class="row">

            <div class="col-md-4">

                <div class="card p-30">

                    <div class="media">

                        <div class="media-left meida media-middle">

                            <span><i class="fa fa-money f-s-40 color-primary"></i></span>

                        </div>

                        <div class="media-body media-text-right">

                            <h2>{{$income}}</h2>

                            <p class="m-b-0" style="font-size:18px;">Total Income</p>

                            <span style="color:#99abb4;font-size: 13px;">for the last 7 days</span>

                        </div>

                    </div>

                </div>

            </div>

            <div class="col-md-4">

                <div class="card p-30">

                    <div class="media">

                        <div class="media-left meida media-middle">

                            <span><i class="fa fa-archive f-s-40 color-success"></i></span>

                        </div>

                        <div class="media-body media-text-right">

                            <h2>{{$bmaids}}</h2>

                            <p class="m-b-0" style="font-size:18px;">Booked Maids</p>

                            <span style="color:#99abb4;font-size: 13px;">for the last 7 days</span>

                        </div>

                    </div>

                </div>

            </div>



            <div class="col-md-4">

                <div class="card p-30">

                    <div class="media">

                        <div class="media-left meida media-middle">

                            <span><i class="fa fa-archive f-s-40 color-danger"></i></span>

                        </div>

                        <div class="media-body media-text-right">

                            <h2>{{$bcustomers}}</h2>

                            <p class="m-b-0" style="font-size:18px;">Booked Customers</p>

                            <span style="color:#99abb4;font-size: 13px;">for the last 7 days</span>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>



    <div id="appOne"> <dashpaginate></dashpaginate></div>



       

    </div>



    <?php $routes = Route::current()->getName() ?>

    @if ( $routes== 'dashboard' || $routes== 'maids-schedule' )

        @include('layouts.dashboard.footer')

    @endif

    

</body>

@endsection



@section('content-scripts')

    <script src="{{ asset('dashtemplate/js/lib/jquery/jquery.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/custom.min.js') }}"></script>

    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>



    <!-- Bootstrap tether Core JavaScript -->

    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/popper.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- slimscrollbar scrollbar JavaScript -->

    <script src="{{ asset('dashtemplate/js/jquery.slimscroll.js') }}"></script>

    <!--Menu sidebar -->

    <script src="{{ asset('dashtemplate/js/sidebarmenu.js') }}"></script>

    <!--stickey kit -->

    <script src="{{ asset('dashtemplate/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>

    <!--Custom JavaScript -->

    <script src="{{ asset('dashtemplate/js/lib/morris-chart/morris.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/morris-chart/raphael-min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/calendar-2/moment.latest.min.js') }}"></script>

    <!-- scripit init-->

    <script src="{{ asset('dashtemplate/js/lib/calendar-2/semantic.ui.min.js') }}"></script>

    <!-- scripit init-->

    <script src="{{ asset('dashtemplate/js/lib/calendar-2/prism.min.js') }}"></script>

    <!-- scripit init-->

    <script src="{{ asset('dashtemplate/js/lib/calendar-2/pignose.calendar.min.js') }}"></script>

    <!-- scripit init-->

    <script src="{{ asset('dashtemplate/js/lib/calendar-2/pignose.init.js') }}"></script>



    <script src="{{ asset('dashtemplate/js/lib/owl-carousel/owl.carousel.min.js') }}"></script>

    <script src="{{ asset('dashtemplate/js/lib/owl-carousel/owl.carousel-init.js') }}"></script>



    <!-- scripit init-->





@endsection



