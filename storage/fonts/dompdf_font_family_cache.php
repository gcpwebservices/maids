<?php return array (
  'sans-serif' => array(
    'normal' => $rootDir . '\lib\fonts\Helvetica',
    'bold' => $rootDir . '\lib\fonts\Helvetica-Bold',
    'italic' => $rootDir . '\lib\fonts\Helvetica-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $rootDir . '\lib\fonts\Times-Roman',
    'bold' => $rootDir . '\lib\fonts\Times-Bold',
    'italic' => $rootDir . '\lib\fonts\Times-Italic',
    'bold_italic' => $rootDir . '\lib\fonts\Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $rootDir . '\lib\fonts\Times-Roman',
    'bold' => $rootDir . '\lib\fonts\Times-Bold',
    'italic' => $rootDir . '\lib\fonts\Times-Italic',
    'bold_italic' => $rootDir . '\lib\fonts\Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $rootDir . '\lib\fonts\Courier',
    'bold' => $rootDir . '\lib\fonts\Courier-Bold',
    'italic' => $rootDir . '\lib\fonts\Courier-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $rootDir . '\lib\fonts\Helvetica',
    'bold' => $rootDir . '\lib\fonts\Helvetica-Bold',
    'italic' => $rootDir . '\lib\fonts\Helvetica-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $rootDir . '\lib\fonts\ZapfDingbats',
    'bold' => $rootDir . '\lib\fonts\ZapfDingbats',
    'italic' => $rootDir . '\lib\fonts\ZapfDingbats',
    'bold_italic' => $rootDir . '\lib\fonts\ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $rootDir . '\lib\fonts\Symbol',
    'bold' => $rootDir . '\lib\fonts\Symbol',
    'italic' => $rootDir . '\lib\fonts\Symbol',
    'bold_italic' => $rootDir . '\lib\fonts\Symbol',
  ),
  'serif' => array(
    'normal' => $rootDir . '\lib\fonts\Times-Roman',
    'bold' => $rootDir . '\lib\fonts\Times-Bold',
    'italic' => $rootDir . '\lib\fonts\Times-Italic',
    'bold_italic' => $rootDir . '\lib\fonts\Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $rootDir . '\lib\fonts\Courier',
    'bold' => $rootDir . '\lib\fonts\Courier-Bold',
    'italic' => $rootDir . '\lib\fonts\Courier-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $rootDir . '\lib\fonts\Courier',
    'bold' => $rootDir . '\lib\fonts\Courier-Bold',
    'italic' => $rootDir . '\lib\fonts\Courier-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $rootDir . '\lib\fonts\DejaVuSans-Bold',
    'bold_italic' => $rootDir . '\lib\fonts\DejaVuSans-BoldOblique',
    'italic' => $rootDir . '\lib\fonts\DejaVuSans-Oblique',
    'normal' => $rootDir . '\lib\fonts\DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $rootDir . '\lib\fonts\DejaVuSansMono-Bold',
    'bold_italic' => $rootDir . '\lib\fonts\DejaVuSansMono-BoldOblique',
    'italic' => $rootDir . '\lib\fonts\DejaVuSansMono-Oblique',
    'normal' => $rootDir . '\lib\fonts\DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $rootDir . '\lib\fonts\DejaVuSerif-Bold',
    'bold_italic' => $rootDir . '\lib\fonts\DejaVuSerif-BoldItalic',
    'italic' => $rootDir . '\lib\fonts\DejaVuSerif-Italic',
    'normal' => $rootDir . '\lib\fonts\DejaVuSerif',
  ),
  'fontawesome' => array(
    'normal' => $fontDir . '\a17da9591906f5ab35cc2d625c862f80',
  ),
  'simple-line-icons' => array(
    'normal' => $fontDir . '\3d8bcbf0af8fc504893a91bf887a31a5',
  ),
  'weathericons' => array(
    'normal' => $fontDir . '\39772bf3cf426c40d4a99fca67584310',
  ),
  'linea-arrows-10' => array(
    'normal' => $fontDir . '\aad6be2177413d4718a2f8d93a9b3c72',
  ),
  'linea-basic-10' => array(
    'normal' => $fontDir . '\a1d1a3fdfdbce1c6fb70c10a59c2f4bb',
  ),
  'linea-basic-elaboration-10' => array(
    'normal' => $fontDir . '\60ae9629ff4b0d7ed6b9642c6e260f6b',
  ),
  'linea-ecommerce-10' => array(
    'normal' => $fontDir . '\ee7a888f290cf67c78ed33d2a75ed616',
  ),
  'linea-music-10' => array(
    'normal' => $fontDir . '\8c4c92f4f20c489e68a6cb1c17043800',
  ),
  'linea-software-10' => array(
    'normal' => $fontDir . '\39f881b8a3a18a4c3a08ab5349ad4e0c',
  ),
  'linea-weather-10' => array(
    'normal' => $fontDir . '\7545c70e16554105698ad01d52900907',
  ),
  'themify' => array(
    'normal' => $fontDir . '\555aeb09927f2380be0c0fcc5f3f6d5a',
  ),
  'material design icons' => array(
    'normal' => $fontDir . '\a37d0e30d89ece83b3dc10859af37ba3',
  ),
  'source sans pro' => array(
    'normal' => $fontDir . '\208a0901ddc70424a014b9128361971b',
  ),
  'glyphicons halflings' => array(
    'normal' => $fontDir . '\d76812250a14612ce241ed5555f4242c',
  ),
) ?>