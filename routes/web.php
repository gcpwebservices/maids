<?php
use App\Customer;
use App\User;
use Illuminate\Mail\Markdown;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
route::get('/clear/config', function(){
		Artisan::call('config:clear');
	});

route::get('/clear/cache', function(){
	Artisan::call('cache:clear');
});

route::get('/storage/storage', function(){
	Artisan::call('storage:link');
});

Route::get('/', 'AdminController@index')->name('dashboard');
// Route::get('/', 'Dashbaord@index')->name('landing');
Route::get('/reg', function(){
	return view('auth.register');
});
Route::middleware('auth')->prefix('admin')->group(function () {


route::get('/changePassword', function(){
    $user = User::find(auth::user()->id);
	return view('auth.passwords.reset',$user);
});

Route::post('changePassword','AdminController@showChangePasswordForm')->name('changePassword');

Route::get('/mail', function(){
	return view('mail.test');
});


Route::get('/book', function(){
	return view('booking');
});


// Route::get('/mail', function () {
// 	$data['subject'] = 'test';
// 	$data['name'] = 'pwet';

// 	return view('mail.receipt', $data);
    // $markdown = new Markdown(view(), config('mail.markdown'));
    // return $markdown->render('mail.name');
// });

	Route::get('/dashapi', 'AdminController@dashBoardPaginate');

	Route::get('forbidden', function () {
		return view('layouts.dashboard.forbidden');
	});

	Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


 	Route::get('/maids-schedule', 'AdminController@maids_schedule')->name('maids-schedule');
 	Route::post('/maids-itinerary', 'AdminController@maids_itinerary')->name('maids-itinerary');

	Route::post('/addMaidsItinerary', 'AdminController@addMaidsItinerary')->name('addMaidsItinerary');
	Route::post('/maids_itinerary_update', 'AdminController@maids_itinerary_update')->name('maids_itinerary_update');
	Route::post('/get_available_maids', 'AdminController@get_available_maids')->name('get_available_maids');
	
	Route::get('/drivers-schedule', 'AdminController@drivers_schedule')->name('drivers-schedule');

	Route::post('/getCountPerAreaMaids', 'AdminController@getCountPerAreaMaids')->name('getCountPerAreaMaids');
	
	Route::any('/getAreaSubArea', 'AdminController@getAreaSubArea')->name('getAreaSubArea');
	
	Route::post('getAreaOnly', 'AdminController@getAreaOnly')->name('getAreaOnly');
	
	Route::post('/getJsonRequestResource', 'AdminController@getJsonRequestResource')->name('getJsonRequestResource');
	Route::post('/getJsonRequestEvent', 'AdminController@getJsonRequestEvent')->name('getJsonRequestEvent');

	Route::post('/getAreaAndCount', 'AdminController@getAreaAndCount')->name('getAreaAndCount');
	Route::post('/getlabel', 'AdminController@getlabel')->name('getlabel');

	Route::post('/procDoneTask', 'AdminController@procDoneTask')->name('procDoneTask');

	Route::post('getamount', 'AdminController@getAmount')->name('getAmount');

	Route::post('getamountwithtax', 'AdminController@getAmountWithTax')->name('getAmountWithTax');

	//maintenance	


	Route::resource('maids', 'AdminMaids');
	Route::resource('drivers', 'AdminDrivers');
	Route::resource('customers', 'AdminCustomers');

	Route::middleware('super')->group(function () {
		//admin maids
		Route::post('/delmaids','AdminMaids@delmaids')->name('delmaids');
		//admin drivers
		Route::post('/deldriver','AdminMaids@deldriver')->name('deldriver');
		//admin users
		Route::resource('users', 'AdminUsers');
		Route::post('/delusers','AdminUsers@delusers')->name('delusers');
		//admin customers
		Route::post('/delcustomer','AdminCustomers@delcustomer')->name('delcustomer');
		Route::post('getareas','AdminCustomers@getareas')->name('getareas');
	});

	//reports
	//sales
	Route::get('/reports/sales', 'AdminSales@index');
	Route::post('year', 'AdminSales@year')->name('year');
	Route::post('week', 'AdminSales@week')->name('week');
	Route::post('month', 'AdminSales@month')->name('month');
	Route::get('/reports/now', 'AdminSales@week');

	Route::get('/sales-test', 'AdminSales@index2');
	Route::post('/daterange', 'AdminSales@dateRange')->name('daterange');
	Route::post('TranSummaryFilter', 'AdminSales@TranSummaryFilter')->name('TranSummaryFilter');
	Route::get('/reports/details', 'AdminSalesDetails@index');

	Route::get('/reports/top-customers','AdminSales@TopCustomers' );
	Route::get('/reports/top-maids','AdminSales@TopMaids' );

	Route::get('/reports/active-customers','AdminSales@ActiveCustomers' );
	Route::get('/reports/inactive-customers','AdminSales@InactiveCustomers' );

	Route::get('/reports/cancelled-appointments', function(){
        return view('parts-dashboard.admin-cancelled-appointments');
	});
	Route::post('CancelledAppoint', 'AdminSales@CancelledAppoint')->name('CancelledAppoint');

	Route::post('ActiveAjax', 'AdminSales@ActiveAjax');
	Route::get('/transactions-customers', 'AdminTransactions@CustomersView');
	Route::post('TransactionTableCustomers', 'AdminTransactions@TransactionTableCustomers')->name('TransactionTableCustomers');
	Route::get('transactions/{id}/customer-transaction','AdminTransactions@TransactionCustomerView');

	Route::get('/transactions-maids', 'AdminTransactions@MaidsView');
	Route::get('transactions/{id}/maid-transaction','AdminTransactions@TransactionMaidView');
	Route::post('TransactionTableMaids', 'AdminTransactions@TransactionTableMaids')->name('TransactionTableMaids');

	Route::get('/appointments', 'AdminBook@index')->name('appointments');
	Route::post('getCUstomerDetails', 'AdminBook@getCUstomerDetails')->name('getCUstomerDetails');
	Route::post('newAppointment', 'AdminBook@newAppointment')->name('newAppointment');
	Route::get('/maids-schedule-table', 'AdminBook@MaidsScheduleTableView');
	Route::post('MaidsScheduleTableDetail', 'AdminBook@MaidsScheduleTableDetail')->name('MaidsScheduleTableDetail');
	Route::post('MaidsScheduleTable', 'AdminBook@MaidsScheduleTable')->name('MaidsScheduleTable');
	Route::post('MaidsScheduleTableCancel', 'AdminBook@MaidsScheduleTableCancel')->name('MaidsScheduleTableCancel');
	Route::any('/MaidsScheduleRecur', 'AdminBook@MaidsScheduleRecur')->name('MaidsScheduleRecur');
	Route::post('editAppointment', 'AdminBook@editAppointment')->name('editAppointment');
	Route::post('getamountRecur', 'AdminController@getamountRecur')->name('getamountRecur');
	Route::post('/appointments', 'AdminBook@quickAdd')->name('quickAdd');
	Route::post('quickEdit', 'AdminBook@quickEdit')->name('quickEdit');

	Route::get('/cancel-appointment', function(){
		return view('parts-dashboard.admin-cancel-appointment');
	});
	
	Route::post('cancelAppointment', 'AdminBook@cancelAppointment')->name('cancelAppointment');
	Route::get('/paginate', 'AdminPaginate@index');
	Route::get('/paginate/dropdown/{id}/fullname/{name}/api', 'AdminPaginate@returnApi');
	Route::get('/paginate/getDropdown','AdminPaginate@getDropdown');
	Route::get('/paginate/maids/api','AdminPaginate@returnApiMaids');

	Route::get('/accounting', 'AdminAccounting@index');
	Route::get('/accounting-paid', 'AdminAccounting@paidView');
	Route::get('/accounting-unpaid', 'AdminAccounting@unpaidView');
	Route::post('AccountingType', 'AdminAccounting@AccountingType')->name('AccountingType');
	Route::post('AccountingTable', 'AdminAccounting@AccountingTable')->name('AccountingTable');
	Route::get('/accounting/{id}/transaction-details','AdminAccounting@TransactionDetails');
	Route::post('sendEmailAccount', 'AdminAccounting@sendEmailAccount')->name('sendEmailAccount');

	Route::post('checkBalance', 'AdminAccounting@checkBalance')->name('checkBalance');
	Route::post('savepayment', 'AdminAccounting@savepayment')->name('savepayment');
	Route::post('payAllSumId', 'AdminAccounting@payAllSumId')->name('payAllSumId');
	Route::post('payPerSumId', 'AdminAccounting@payPerSumId')->name('payPerSumId');
	Route::get('/accounting/{id}/transaction-details/pdf','AdminAccounting@exporting');

	Route::post('sendEmail', 'MailController@sendEmail')->name('sendEmail');

	Route::get('/invoice', 'AdminAccounting@invoice');
	// Route::get('/mail', 'AdminAccounting@mail');

	Route::post('sendEmailReceipt', 'AdminAccounting@sendEmailReceipt')->name('sendEmailReceipt');
	Route::post('emailCustomers', 'AdminSales@emailCustomers')->name('emailCustomers');

	Route::get('/products', function(){
		return view('parts-dashboard.admin-products');
	});



});


